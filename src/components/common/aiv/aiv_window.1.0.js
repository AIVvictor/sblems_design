/**
 * 팝업 윈도우
 */
(function ($, W) {

    var me = W.AIV_WINDOW = {};

    // default option values
    me.options = {
        title: "세방 물류에너지 관리시스템",
        modal: true,
        resizable: false,
        width: 300,
        height: 500,
        templateType: 0,
        close: function (e) {
            this.destroy(); // 종료시 해당 팝업 DOM 제거
        }
    }
    var _templateId = 'kwindow-template';
    var _windowDivId = 'windowTemplateDiv';
    var _windowContentId = 'kwindow-details';


    /**
     *  window 객체 초기화
     */
    me.init = function (userOptions) {
        userOptions = userOptions || {};
        // 템플릿 초기화
        if (AIV_UTIL.isEmpty(userOptions.content)) {
            me.templateHtml = ''
        } else {
            me.templateHtml = userOptions.content;
            userOptions.templateType = 'custom';
        }

        // 옵션 merge
        me.options = $.extend(true, me.options, userOptions);

        // 템플릿 적용
        me.setTemplate(me.options.templateType);
        
        // 템플릿 html 을 삽입
        $('#' + _windowDivId).html(me.templateHtml);
        
        // kendo 윈도우 객체 생성
        delete  me.options.content; // 존재시 window open 후 해당 content text를 GET URL 로 통신하는 현상 존재
        me.self = $('#' + _windowContentId).kendoWindow(me.options).data("kendoWindow");
        return me;
    };


    /**
     *  초기화 화 된 window 객체를 popup
     */
    me.open = function (items) {
        items = items || {};

        // template이 없다면 기본 설정으로 적용
        if (AIV_UTIL.isEmpty(me.templateHtml)) {
            me.init();
        }

        // kendo 템플릿 내 값 세팅
        // if ( AIV_UTIL.isNotNull(items)) {
        // kendo 템플릿 객체 생성
        // }
        var kTemplate = kendo.template($("#" + _templateId).html());
        var content = kTemplate(items)
        me.self.content(content);

        // window 중앙정렬 및 popup
        me.self.center().open();
        $('.k-window.k-widget').removeClass('k-widget');    // css 정리를 위해 widget 클래스 제거

        // overlay 영역 클릭 이벤트시 window close
        $('.k-overlay').on('click', function () {
            me.self.close();
        });

        return me;
    }


    // 템플릿 세팅
    me.setTemplate = function (templateType) {
        // x-kendo-template 이 들어갈 공간은 하나만 생성하여 재사용
        if ($('#' + _windowDivId).length == 0) {
            var windowTemplateDiv = '<div id="' + _windowDivId + '" style="display:none;"></div>';
            $('body').append(windowTemplateDiv);
        }



        if (templateType == 0) {
            // 기본
            me.templateHtml += '<div id="' + _windowContentId + '"></div>';
            me.templateHtml += '<script type="text/x-kendo-template" id="' + _templateId + '">';
            me.templateHtml += '    <div id="' + _windowContentId + '-container">';
            me.templateHtml += '        #= content #';
            me.templateHtml += '    </div>';
            me.templateHtml += '</script>';
            me.templateHtml += '<style type="text/css">';
            me.templateHtml += '    #' + _windowContentId + '-container {';
            me.templateHtml += '        padding: 8px;';
            me.templateHtml += '        height: 95%;';
            me.templateHtml += '    }';
            me.templateHtml += '</style>';

        } else if (templateType == 1) {
            // 템플릿#1
            me.templateHtml += '<div id="' + _windowContentId + '"></div>';
            me.templateHtml += '<script type="text/x-kendo-template" id="' + _templateId + '">';
            me.templateHtml += '    <div id="' + _windowContentId + '-container">';
            me.templateHtml += '        <h2>#= dtFromDate # #= dtToDate #</h2>';
            me.templateHtml += '        <em>#= dtFromDate #</em>';
            me.templateHtml += '        <dl>';
            me.templateHtml += '            <dt>City: #= dtFromDate #</dt>';
            me.templateHtml += '            <dt>Birth Date: #= kendo.toString(dtFromDate, "yyyy/MM/dd") #</dt>';
            me.templateHtml += '        </dl>';
            me.templateHtml += '    </div>';
            me.templateHtml += '</script>';
            me.templateHtml += '<style type="text/css">';
            me.templateHtml += '    #' + _windowContentId + '-container {';
            me.templateHtml += '        padding: 10px;';
            me.templateHtml += '        height: 95%;';
            me.templateHtml += '    }';
            me.templateHtml += '    #' + _windowContentId + '-container h2 {';
            me.templateHtml += '        margin: 0;';
            me.templateHtml += '    }';
            me.templateHtml += '    #' + _windowContentId + '-container em {';
            me.templateHtml += '        color: #8c8c8c;';
            me.templateHtml += '    }';
            me.templateHtml += '    #' + _windowContentId + '-container dt {';
            me.templateHtml += '        margin:0;';
            me.templateHtml += '        display: inline;';
            me.templateHtml += '    }';
            me.templateHtml += '</style>';

        } else if (templateType == 2) {
            // 템플릿#2
            me.templateHtml += '<div id="' + _windowContentId + '"></div>';
            me.templateHtml += '<script type="text/x-kendo-template" id="' + _templateId + '">';
            me.templateHtml += '    <div id="' + _windowContentId + '-container">';
            me.templateHtml += '        <dl>';
            me.templateHtml += '        </dl>';
            me.templateHtml += '    </div>';
            me.templateHtml += '</script>';
            me.templateHtml += '<style type="text/css">';
            me.templateHtml += '    #' + _windowContentId + '-container {';
            me.templateHtml += '        padding: 10px;';
            me.templateHtml += '        height: 95%;';
            me.templateHtml += '    }';
            me.templateHtml += '    #' + _windowContentId + '-container h2 {';
            me.templateHtml += '        margin: 0;';
            me.templateHtml += '    }';
            me.templateHtml += '    #' + _windowContentId + '-container em {';
            me.templateHtml += '        color: #8c8c8c;';
            me.templateHtml += '    }';
            me.templateHtml += '    #' + _windowContentId + '-container dt {';
            me.templateHtml += '        margin:0;';
            me.templateHtml += '        display: inline;';
            me.templateHtml += '    }';
            me.templateHtml += '</style>';

        } else if (templateType == 'ctrlDtls') {
            // 차량관제 상세 정보
            me.templateHtml += '<div id="' + _windowContentId + '"></div>';
            me.templateHtml += '<script type="text/x-kendo-template" id="' + _templateId + '">';
            me.templateHtml += '<div class="row" style="min-height: 42px;">';
            me.templateHtml += '<div class="col-lg-6 pt-1 mb-3" id="driveDtlsSmry" >';
            me.templateHtml += '</div>';
            me.templateHtml += '<div class="col-lg-6 mb-3 text-right" id="driveDtlsSearch" >';
            me.templateHtml += '<input id="dlvyDe">';
            // me.templateHtml +=             '<button id="btnCoursTrace">';
            me.templateHtml += '<button id="btnExcel">';
            me.templateHtml += '</div>';
            me.templateHtml += '</div>';
            me.templateHtml += '<div class="row" style="height: 600px;padding: 0 15px;">';
            me.templateHtml += '<div class="col-lg-8" id="driveDtlsMap">';
            me.templateHtml += '</div>';
            me.templateHtml += '<div class="col-lg-4">';
            me.templateHtml += '<div class="row">';
            me.templateHtml += '<div class="col-lg-12" style="height: 300px; padding: 0 0px 15px 10px" >';
            me.templateHtml += '<div id="driveDtlsEvent"></div>';
            me.templateHtml += '</div>';
            me.templateHtml += '<div class="col-lg-12" style="height: 300px; padding: 0 0px 0px 10px;" >';
            me.templateHtml += '<div id="driveDtlsAdres"></div>';
            me.templateHtml += '</div>';
            me.templateHtml += '</div>';
            me.templateHtml += '</div>';
            me.templateHtml += '</div>';
            me.templateHtml += '</script>';
            me.templateHtml += '<style type="text/css">';
            me.templateHtml += '    .k-window-content.k-content {';
            me.templateHtml += '        overflow-x:hidden;';
            me.templateHtml += '    }';
            me.templateHtml += '    .k-window-content.k-content .k-button.k-primary {';
            me.templateHtml += '        padding: 2px 20px;';
            me.templateHtml += '        margin-left: 10px;';
            me.templateHtml += '    }';
            me.templateHtml += '</style>';

        } else if (templateType == 'noArvl') {
            // 미도착 배송지
            me.templateHtml += '<div id="' + _windowContentId + '"></div>';
            me.templateHtml += '<script type="text/x-kendo-template" id="' + _templateId + '">';
            me.templateHtml += '<div id="noArvlSmry" class="mt-1 mb-3"></div>'
            me.templateHtml += '<div id="noArvlSearch" class="row aiv_search_area" style="margin-left: 0px;margin-right: 0px;">'
            me.templateHtml += '<div class="col-lg-9 col-md-9 col-sm-9">'
            me.templateHtml += '<input id="lng" disabled />'
            me.templateHtml += '<input id="lat" disabled />'
            me.templateHtml += '<input id="adres" disabled />'
            me.templateHtml += '</div>'
            me.templateHtml += '<div class="col-lg-3 col-md-3 col-sm-3 text-right font-weight-bold">'
            me.templateHtml += '<button id="btnApplc" class="aiv_btn_1x"></button>'
            me.templateHtml += '</div>'
            me.templateHtml += '</div>'
            me.templateHtml += '<div style="height:520px; background-color: lightgray;">';
            me.templateHtml += '<div id="noArvlMap" >';
            me.templateHtml += '</div>';
            me.templateHtml += '</div>';
            me.templateHtml += '</script>'

        } else if (templateType == 'custom') {
            // do nothing
            me.templateHtml =
                '<div id="' + _windowContentId + '"></div>'
                + '<script type="text/x-kendo-template" id="' + _templateId + '">'
                + me.templateHtml
                + '</script>';
        }

        // $(templateHtml).appendTo("body");
        // $('body').append(templateHtml);
        $('#' + _windowDivId).html(me.templateHtml);

        // 템플릿 저장
        // me.detailsTemplate = kendo.template($('#' + _templateId).html());
    };


    /**
     * 템플릿#1, 윈도우 팝업
     * @param {타이틀} title 
     * @param {넓이} width 
     * @param {높이} height 
     * @param {데이타} items 
     * @param {옵션} options 
     */
    me.openTemplate = function (title, width, height, items, options) {

        options = $.extend(true, options, {
            templateType: options.templateType || 1,
            title: (typeof (title) == "undefined") ? me.title : title,
            modal: (typeof (options.modal) == "undefined") ? me.modal : options.modal,
            resizable: (typeof (options.resizable) == "undefined") ? me.resizable : options.resizable,
            width: (typeof (width) == "undefined") ? me.width : width,
            height: (typeof (height) == "undefined") ? me.height : height,
        });

        // if (templateHtml == '') 
        me.init(options);

        // me.self.content(me.detailsTemplate(items));
        me.self.content(me.detailsTemplate(items));
        me.self.center().open();
    };








    /**
     * 운행이력통계 - 상세 운행일지 조회
     * @param {*} dlvyDe 
     * @param {*} vrn 
     */
    me.driveDtls = function (dlvyDe, vrn) {
        var params = {
            dlvyDe: dlvyDe,
            vrn: vrn
        }

        // 일별 요약 객체 기본
        var sumry = {
            id: '-', aclGroupId: '-', beFinDt: '-', dlvyDe: '-'
            , driverCd: '-', driverNm: '-', drvDistance: '-', drvTime: '-'
            , maxDrvTime: '-', restTime: '-', vhcleTy: '-', vrn: '-'
            , fctryCd: '-', fctryNm: '-', mobileNo: '-'
        };

        // 일별 요약 데이터 가져와 기본 객체에 삽입
        AIV_COMM.ajax({
            method: "POST",
            async: false,
            url: AIV_COMM.HOST_URL + '/adm/stats/vhcle/drive/diary/smry',
            // data: params,
            data: {
                dlvyDe: dlvyDe,
                vrn: vrn
            },
            success: function(responseData) {
                sumry = responseData;

                var restTimeMin = (responseData.restTimeSec / 60).toFixed();   // 휴게시간 sec->min
                var restTime = AIV_UTIL.date.miToHhmi(restTimeMin, '시간 ', '분');

                var drvTimeMin = (responseData.drvTimeSec / 60).toFixed();   // 누적 운행시간 sec->min
                var drvTime = AIV_UTIL.date.miToHhmi(drvTimeMin, '시간 ', '분');
                
                var maxDrvTimeMin = (responseData.maxDrvTimeSec / 60).toFixed();   // 최대 연속 운행시간 sec->min
                var maxDrvTime = AIV_UTIL.date.miToHhmi(maxDrvTimeMin, '시간 ', '분');
                
                sumry.restTime = restTime;
                sumry.drvTime = drvTime;
                // sumry.drvDistance = responseData.drvDistance;
                sumry.maxDrvTime = maxDrvTime;

                sumry.beFinDt = AIV_UTIL.isEmpty( responseData.beFinDt ) ? '-' : responseData.beFinDt;
            }
        });

        
        var datas = [];
        AIV_COMM.ajax({
            method: "POST",
            async: false,
            url: AIV_COMM.HOST_URL + '/adm/stats/vhcle/drive/diary/list',
            data: params,
            success: function(responseData) {
                datas = responseData;

                $(datas).each(function(i, data){
                    if ( datas[i].eventDt != null ) {
                        datas[i].eventDt = new Date(datas[i].eventDt).format('yyyy-mm-dd hh:mi:ss');
                    } else {
                        datas[i].eventDt = '-';
                    }

                    if ( datas[i].statusCd == 'DELIVERY' || datas[i].statusCd == 'EGO' ) {
                        datas[i].statusNm = datas[i].statusNm + '<br>(' + datas[i].routeNo + ')';
                    }

                    if ( datas[i].remark == null ) {
                        datas[i].remark = '-';
                    }
                    if ( datas[i].eventTy == null ) {
                        datas[i].eventTy = '-';
                    }
                })
            }
        });


        var wOptions = {    // kendo window options
            title: '상세 이력정보 조회',
            width: 1200,
            height: 700
        };

        var _id = "driveDtls_" + kendo.guid();
        var _content = '';
        _content += '<table id="' + _id + '">';
        _content +=     '<colgroup>';
        _content +=         '<col style="width: 150px">';
        _content +=         '<col style="width: 170px">';
        _content +=         '<col style="width: 170px">';
        _content +=         '<col style="width: 170px">';
        _content +=         '<col style="width: 170px">';
        _content +=         '<col style="width: 170px">';
        _content +=     '</colgroup>';
        _content +=     '<thead>';
        _content +=         '<tr>';
        _content +=             '<th data-field="statusNm">진행상태</th>';
        _content +=             '<th data-field="dlvyLcNm">장소</th>';
        _content +=             '<th data-field="eventNm">구분</th>';
        _content +=             '<th data-field="eventDt">시간</th>';
        _content +=             '<th data-field="remark">비고</th>';
        _content +=             '<th data-field="eventTy">데이터타입</th>';
        _content +=         '</tr>';
        _content +=     '</thead>';
        _content +=     '<tbody>';
        _content +=         '<tr>';
        _content +=             '<td>이전일 업무종료시각: '+ sumry.beFinDt + ', 휴게시간: '+ sumry.restTime + '</td>';
        _content +=             '<td></td>';
        _content +=             '<td></td>';
        _content +=             '<td></td>';
        _content +=             '<td></td>';
        _content +=             '<td></td>';
        _content +=         '</tr>';
        $(datas).each(function (i, row) {
            _content +=     '<tr>';
            _content +=         '<td>' + row.statusNm + '</td>';
            _content +=         '<td>' + row.dlvyLcNm + '</td>';
            _content +=         '<td>' + row.eventNm + '</td>';
            _content +=         '<td>' + row.eventDt + '</td>';
            _content +=         '<td>' + row.remark + '</td>';
            _content +=         '<td>' + row.eventTy + '</td>';
            _content +=     '</tr>';
        });
        _content +=         '<tr>';
        _content +=             '<td>누적 운행시간 '+ sumry.drvTime + ' / 누적 운행거리 '+ sumry.drvDistance + ' / 최대 연속 운행시간 '+ sumry.maxDrvTime + '</td>';
        _content +=             '<td></td>';
        _content +=             '<td></td>';
        _content +=             '<td></td>';
        _content +=             '<td></td>';
        _content +=             '<td></td>';
        _content +=         '</tr>';
        _content +=     '</tbody>';
        _content += '</table>';

        _content = _content.replaceAll('#', '>');
        wOptions.content = _content;
        me.init(wOptions).open();

        var $grid = $('#' + _id);
        $grid.kendoGrid({
            scrollable: true,
            height: '95%',
            toolbar: [
                { name: "excel", text: '엑셀다운로드' }
            ],
            excel: {
                fileName: '차량운행이력상세_' + vrn + '_' + dlvyDe + '.xlsx',
                allPages: true,
                filterable: true
            },
            columns: [  // 엑셀다운로드 시 header 및 size 조정을 위해 입력
                { field: "statusNm", title: "진행상태", width: 150 },
                { field: "dlvyLcNm", title: "장소", width: 170 },
                { field: "eventNm", title: "구분", width: 170 },
                { field: "eventDt", title: "시간", width: 170 },
                { field: "remark", title: "비고", width: 170 },
                { field: "eventTy", title: "데이터타입", width: 170 }
            ]
        });

        var gridTitle = params.dlvyDe + ' / '+ sumry.fctryCd + ' / '+ params.vrn + ' / '+ sumry.vhcleTy + ' / '+ sumry.driverNm + '('+ sumry.mobileNo + ')';
        $grid.parent().siblings(".k-grid-toolbar").prepend("<div class='gridTitle'><h1>" + gridTitle + "</h1></div>");

        // colspan, rowspan 설정 후 grid화 할경우 css가 깨지므로, 이에 대해서는 jquery로 후처리 한다.
        $grid.find('tbody tr:first > td:first, tbody tr:last > td:first')
            .attr('colspan', '6')
            .addClass('text-center')
            .css({ 'background-color': '#eee', 'font-weight': 'bold' })
            .siblings()
            .remove();

            
        mergeCell();
        
        return;
        // return me;   // 객체 반환시 -> ie에서 <a href> 로 AIV_WINDOW.driveDtls() 사용시 버그발생

        // 일별 운행일지의 colspan에 대한 후처리 로직
        function mergeCell() {
            var $grid = $('#' + _id);
            for (var i = 0; i < 3; i++) {  // 좌측부터 3열까지만 병합 실행
                tdList = $grid.find('tr').find('td:eq(' + i + ')');
                var cIdx = 0;
                for (var nIdx = 1; nIdx < tdList.length - 1; nIdx++) {
                    var $curTd = $(tdList[cIdx]),
                        curVal = $curTd.html();
                    var $nextTd = $(tdList[nIdx]),
                        nextVal = $nextTd.html();

                    if (curVal == nextVal) {  // 아래칸과 값이 같을 경우
                        $curTd.attr('rowspan', (Number($curTd.attr('rowspan') || 1) + 1));    // 위 칸의 rowspan 을 +1
                        // $nextTd.remove(); // 아래칸은 hidden 처리 (제거시 칸 깨짐 현상 버그 존재)
                        $nextTd.hide(); // 아래칸은 hidden 처리 (제거시 칸 깨짐 현상 버그 존재)
                    } else {
                        cIdx = nIdx;
                    }
                }
            }
        }

    }


    /**
     * 차량관제 - 상세 정보 조회
     * @param {*} dlvyDe 
     * @param {*} vrn 
     */
    me.cntrlDtls = function (fctryCd, dlvyDe, vrn) {
        me.fn = {};
        me.params = {};
        me.btn = {};
        me.tmap = {};
        me.grid = {};

        // 파라미터 조회 함수
        me.fn.getParams = function () {
            return {
                vrn: vrn,
                dlvyDe: me.params.dlvyDe.val()
            }
        }

        // kendo window options
        var wOptions = {
            title: '차량상태정보',
            width: '90%',
            height: 700,
            templateType: 'ctrlDtls',
            resizable: true
        };
        // var _id = "driveDtls";
        var _smryId = "driveDtlsSmry";
        var _searchId = "driveDtlsSearch";
        var _mapId = "driveDtlsMap";
        var _eventId = "driveDtlsEvent";
        var _adresId = "driveDtlsAdres";

        me.init(wOptions).open();   // window open


        // 차량 기본 정보 조회
        AIV_COMM.ajax({
            method: "POST",
            url: AIV_COMM.HOST_URL + '/adm/anals/vhcleCntrl/rt',
            data: {
                fctryCd: fctryCd,
                vrn: vrn
            },
            success: function(responseData) {
                var data = responseData[0];
                $('#' + _smryId).html(data.vrn + ' / ' + data.driverNm + '(' + data.mobileNo + ') / 최종 데이터 수신시간 : ' + data.dataDt);
            }
        })

        // 파라미터 초기화
        me.params.dlvyDe = $('#' + _searchId + ' #dlvyDe').AIV_DATETIMEPICKER({
            type: 0,
            change: function () {
                var value = this.value().format('yyyy-mm-dd');
                dlvyDe = value;

                me.tmap.searchCoursTrace();
                me.grid.event.reload();
                // me.grid.adres.reload();

            }
        });
        me.params.dlvyDe.val(dlvyDe);   // 초기값 세팅
        me.params.vrn = vrn;   // 초기값 세팅



        // me.btn.coursTrace = $('#'+_searchId+ ' #btnCoursTrace').AIV_BUTTON({
        //     title: "경로추적",
        //     class: 'k-primary',
        //     icon: 'search',
        //     onClick: PAGE.fn.search,
        // });
        me.btn.excel = $('#' + _searchId + ' #btnExcel').AIV_BUTTON({
            title: "엑셀 다운로드",
            class: 'k-primary',
            icon: 'excel',
            onClick: function () {
                me.grid.adres.self.saveAsExcel()
            },
        });


        // 맵 생성
        me.tmap = $('#' + _mapId).AIV_TMAP({
            search: {       // 검색영역 옵션 정의
                url: AIV_COMM.HOST_URL + '/adm/anals/vhcleCntrl/hist',
                getParams: function () {
                    return {
                        dlvyDe: me.params.dlvyDe.val(),
                        vrn: me.params.vrn
                    }
                },
                after: function (datas) {
                    // PAGE.fn.setStatusCnt();
                    if ( datas.length > 0 ) {
                        var lastData = datas.pop();
                        me.tmap.map.setCenter(new Tmap.LonLat(lastData.lon, lastData.lat).transform("EPSG:4326", "EPSG:3857"), 15);
                    }
                    setTimeout(function(){
                        me.grid.adres.setData(datas);   // 주소 그리드에 값 입력
                    }, 1)
                }
            },
            marker: {       // 마커 옵션 정의
                // marker-mouseover event 발생시 실행 함수
                onMouseover: function (e) {
                    var marker = this;
                    var data = { regDt: marker.data.regDt };
                    syncWidgetFocus('marker', data);
                },
                // marker-mouseout event 발생시 실행 함수
                onMouseout: function (e) {
                    var marker = this;
                    marker.popup.hide();
                },
                // marker-click event 발생시 실행 함수
                onClick: function (e) {
                    var marker = this;
                }
            },
        });

        // 경로추적 검색
        me.tmap.searchCoursTrace();


        // 이벤트 이력 그리드 생성
        me.grid.event = $("#" + _eventId).AIV_GRID({
            apiUrl: "/adm/anals/vhcleCntrl/event/hist",
            getParams: function () {
                return {
                    dlvyDe: me.params.dlvyDe.val(),
                    vrn: me.params.vrn
                }
            },
            columns: [
                { kField: "eventDt" },
                { kField: "eventNm" },
                { kField: "tmsPlanDt" }
            ],
            height: '100%',
            pageable: false,
            dataBound: function (e) {
                var items = e.sender.dataSource.view();
                // PAGE.fn.dataBound(items);
                $(items).each(function (i, item) {
                    var row = $('tr[data-uid=' + item.uid + ']');
                    $(row).on('mouseover', function (e) {
                        var data = {
                            regDt: item.regDt
                        };
                        syncWidgetFocus('event', data);
                    })
                });
            },
            // fn_dataBound: PAGE.fn.dataBound
        });

        // 주소 이력 그리드 생성
        me.grid.adres = $("#" + _adresId).AIV_GRID({
            // apiUrl: "/datas/dummy",
            // getParams: PAGE.fn.getParams,
            columns: [
                { kField: "dataDt" },
                { kField: "adres", width: '' },
            ],
            excel: {
                fileName: '차량운행이력_' + vrn + '_' + me.fn.getParams().dlvyDe,
                allPages: true,
                filterable: true
            },
            height: '100%',
            pageable: false,
            // dataBound: function (e) {
            //     var items = e.sender.dataSource.view();
            //     // PAGE.fn.dataBound(items);
            //     $(items).each(function (i, item) {
            //         var row = $('tr[data-uid=' + item.uid + ']');
            //         $(row).on('mouseover', function (e) {
            //             var data = {
            //                 regDt: item.regDt
            //             };
            //             syncWidgetFocus('adres', data);
            //         })
            //     });
            // },
        });


        // tmap, event grid, adres 간 mouseover 이벤트 발생시 focus의 동기화를 맞추어 줌
        function syncWidgetFocus(type, data) {
            return;
            var marker = me.tmap.marker.getMarkerByData(data);
            if (type == 'marker') {
                me.tmap.marker.showPopup(marker);

                me.grid.adres.setScrollTopByData(data);

                me.grid.event.setScrollTopByData(data);

            } else if (type == 'adres') {
                me.tmap.marker.showPopup(marker);
                me.tmap.map.setCenter(marker.lonlat, 15);

                me.grid.event.setScrollTopByData(data);

            } else if (type == 'event') {
                me.tmap.marker.showPopup(marker);
                me.tmap.map.setCenter(marker.lonlat, 15);

                me.grid.adres.setScrollTopByData(data);

            }
        }


        // TODO:remove
        // setTimeout(function () {
        //     var eventData = [];
        //     for (var i = 0; i < 30; i++) {
        //         var time = 360;
        //         time += i * 15;
        //         var hour = Math.floor(time / 60) < 10 ? '0'+Math.floor(time / 60) : Math.floor(time / 60);
        //         var min = Math.floor(time % 60) < 10 ? '0'+Math.floor(time % 60) : Math.floor(time % 60);
        //         eventData.push({
        //             eventDt: (new Date('2018-07-19T' + hour + ':' + min)).format('hh:mi'),
        //             regDt: (new Date('2018-07-19T' + hour + ':' + min)).format('yyyy-mm-dd hh:mi:ss'),
        //             eventNm: '배송',
        //             expcTime: '-'
        //         })
        //     }
        //     me.grid.event.setData(eventData)

        // }, 1000)

        return me;

    }


    /**
     * 미도착 배송지 분석 - 상세 정보 조회
     * @param {*}
     *  dlvyDe : 배송일자
     *  routeNo : 노선번호
     *  dlvyLcNm : 배송지명
     *  dlvyLcCd : 배송지코드
     */
    me.noArvl = function (dataItems) {
        me.fn = {};
        me.params = {};
        me.btn = {};
        me.tmap = {};

        // 파라미터 getter fn
        me.fn.getParams = function () {
            // var params = me.params.getAllValue();
            var params = {};
            params.dlvyLcCd = dataItems.dlvyLcCd;
            params.dlvyDe = dataItems.dlvyDe;
            params.vrn = dataItems.vrn;
            return params;
        }

        // 파라미터 setter fn
        me.fn.setParams = function (lon, lat, adres) {
            me.params.lng.kValue(lon);
            me.params.lat.kValue(lat);
            // TODO: ajax
            me.params.adres.kValue(adres);
        }

        // 드래그 가능한 마커 및 Circle 생성
        me.fn.addDragableMarker = function (lon, lat) {

            // 맵 위치 설정
            me.tmap.map.setCenter(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), 17);

            // 상단 input 영역 값 조정
            me.params.lng.kValue(lon);
            me.params.lat.kValue(lat);

            // 주소 변환 후 상단 input에 등록
            var fn_success = function (adres) {
                me.params.adres.kValue(adres);
            }
            me.tmap.reverseGeo(lon, lat, fn_success);

            // circle 객체 생성 및 추가
            me.tmap.circle.add({
                lon: lon,
                lat: lat,
                strokeWidth: 2
            });

            // 지도 내 마커 추가
            me.tmap.marker.addDragable({ 
                lon: lon,
                lat: lat,
                labelText: dataItems.dlvyLcNm + '<br>(' + dataItems.dlvyLcCd + ')',
                event: {
                    onMouseover: function () {
                        // this.popup.show();
                        this.popup.hide();
                    },
                    onMouseout: function () {
                        this.popup.hide();
                    },
                    onMousedown: function () {
                        this.popup.hide();
                        me.tmap.map.ctrl_nav.dragPan.deactivate();//드래그 액션 비활성화
                    },
                    onMouseup: function (e) {
                        this.popup.show();
                        me.tmap.map.ctrl_nav.dragPan.activate();//드래그 액션 활성화
                        me.tmap.markerLayer.removeMarker(this); // 기존 마커 삭제

                        setTimeout(function () {  // e 객체 내 xy 프로퍼티 생성에 시간이 걸리므로 약간의 timeout 부여
                            //클릭 부분의 ViewPortPx를 LonLat 좌표로 변환합니다.
                            var lonlat = me.tmap.map.getLonLatFromViewPortPx(e.xy).transform("EPSG:3857", "EPSG:4326");
                            // 마커 추가(위치만 변경시 Marker Label위치를 변경할 수 없기 때문에 제거 후 재생성 함)
                            me.fn.addDragableMarker(lonlat.lon, lonlat.lat);
                        }, 1)
                    }
                }

            });
        }

        // kendo window options
        var wOptions = {
            title: '미도착배송지 상세 조회',
            width: '90%',
            height: 700,
            templateType: 'noArvl',
            resizable: true
        };

        // window open
        me.init(wOptions).open();

        // kendo window 내부 DOM id
        var _smryId = "noArvlSmry";
        var _searchId = "noArvlSearch";
        var _mapId = "noArvlMap";

        // 상단 요약 설정
        var summary = dataItems.dlvyDe + ' / ' + dataItems.routeNo + ' / ' + dataItems.dlvyLcNm + '(' + dataItems.dlvyLcCd + ')';
        $('#' + _smryId).html(summary)

        // 파라미터 영역 설정
        me.params = $('#' + _searchId).AIV_SEARCH_INIT();

        // 버튼 설정
        me.btn.btnApplc = $('#' + _searchId + ' #btnApplc').AIV_BUTTON({
            title: "적용",
            class: 'k-primary',
            icon: 'save',
            onClick: function () {

                var params = me.fn.getParams();
                params.adres = me.params.adres.kValue();
                params.lat = me.params.lat.kValue();
                params.lng = me.params.lng.kValue();

                AIV_ALERT.open("알림", "적용 하시겠습니까?", {
                    type: "2", // 1:확인, 2:확인,취소
                    onOK: function (e) {
                        AIV_COMM.ajax({
                            method: "POST",
                            url: AIV_COMM.HOST_URL + '/adm/anals/noArvlDlvyLc/update',
                            data: params,
                            success: function(responseData) {
                                // me.fn.setParams(126.98, 37.57, '초기 주소...');
                                if ( responseData == 1 ) {
                                    AIV_ALERT.open("알림", "변경되었습니다.", {
                                        type: "1", // 1:확인, 2:확인,취소
                                    });
                                } else {
                                    AIV_ALERT.open("알림", "변경에 실패하였습니다. 잠시 후 다시 시도해주세요.", {
                                        type: "1", // 1:확인, 2:확인,취소
                                    });
                                }
                            }
                        });

                    },
                    onCancel: function (e) {
                        AIV_SYS.log("onCancel");
                    }
                });
            },
        });

        // 맵 생성
        me.tmap = $('#' + _mapId).AIV_TMAP({
            search: {       // 검색영역 옵션 정의
                url: AIV_COMM.HOST_URL + '/adm/anals/vhcleCntrl/hist',
                getParams: function () {
                    var params = me.fn.getParams();
                    //TODO: remove test data
                    params.vrn = '서울86바1597'
                    params.dlvyDe = '20181023'
                    return params;
                },
                after: function () {
                    // PAGE.fn.setStatusCnt();
                    // 이동 마커 생성
                    AIV_COMM.ajax({
                        method: "POST",
                        url: AIV_COMM.HOST_URL + '/adm/anals/noArvlDlvyLc/select',
                        data: {
                            dlvyLcCd: dataItems.dlvyLcCd
                        },
                        success: function(responseData) {
                            console.log(responseData);
                            var lat = responseData.lat;
                            var lng = responseData.lng;
                            me.fn.addDragableMarker(lng, lat);
                            // me.fn.setParams(126.98, 37.57, '초기 주소...');
                        }
                    });

                }
            },
            marker: {       // 마커 옵션 정의
                // marker-mouseover event 발생시 실행 함수
                onMouseover: function (e) {
                    var marker = this;
                    marker.popup.show();
                },
                // marker-mouseout event 발생시 실행 함수
                onMouseout: function (e) {
                    var marker = this;
                    marker.popup.hide();
                },
                // marker-click event 발생시 실행 함수
                onClick: function (e) {
                    var marker = this;
                }
            }
        });

        // 경로추적 검색
        me.tmap.searchCoursTrace();

        

        return me;

    }




     


     /**
     * 배차관리 > 위생점검표 관리 > 월별 일반위생점검표 상세
     * @param {year} 년
     * @param {month} 월
     * @param {fctryCd} 공장코드
     * @param {fctryNm} 공장명
     * @param {trnsprtCmpny} 운수사명
     * @param {vhcleTy} 차종
     * @param {vrn} 차량번호
     * @param {driverCd} 기사코드
     * @param {driverNm} 기사명
     */
    me.snitatDtls = function (params) {


        // params: year(yyyy), month(MM), fctryCd, vrn
        // url: POST /adm/car/alc/snitatChckTableManage/detail
        
        AIV_COMM.ajax({
            method: "POST",
            async: false,
            url: AIV_COMM.HOST_URL + '/adm/car/alc/snitatChckTableManage/detail',
            data: {
                year: params.year,
                month: (params.month < 10 ? '0' + params.month : params.month),
                fctryCd: params.fctryCd,
                vrn: params.vrn
            },
            success: function(responseData) {
                var datas = [];
                $(responseData).each(function(i, data){
                    datas.push({
                        'regDe': data.regDe.substr(0,4) + '-' + data.regDe.substr(4,2) + '-' + data.regDe.substr(6,2),
                        'dayOfWeek': data.weekDayKr, 'dlvyTy': data.dlvyTy, 'dayoffTy': data.dayoffTy, 'dayoffCnt': data.dayoffCnt,
                        'item01': data.item01, 'item02': data.item02, 'item03': data.item03, 'item04': data.item04, 'item05': data.item05,
                        'item06': data.item06, 'item07': data.item07, 'item08': data.item08, 'item09': data.item09, 'item10': data.item10,
                        'rm': data.rm
                    });
                })
                
                var wOptions = {    // kendo window options
                    title: '위생점검표관리',
                    width: 1200,
                    height: 700
                };
        
                var divId = "aivWindowSnitatDtls";
                var leftId = divId + "Left";
                var rightId = divId + "Right";
                var mainId = divId + "Main";
        
                var _content = '';
                _content += '<div id="' + divId + '">';
                /* 상단 타이틀 */
                _content +=     '<button id="btnPDFDownload" class="aiv_btn_1x k-primary k-button k-button-icontext" style="position: absolute;right: 24px;"><span class="k-icon k-i-pdf"></span> PDF다운로드</button>';
                _content +=     '<div style="text-align: center;font-size: 32px;font-weight: bold;text-decoration: underline;">';
                _content +=         '일 반 위 생 점 검 표';
                _content +=     '</div>';
                
                /* 상단 좌측 정보 테이블 */
                _content +=     '<div class="row" style="margin: 0px;">';
                _content +=         '<div class="col-lg-4">';
                _content +=             '<table id="' + leftId + '">';
                _content +=                 '<colgroup>';
                _content +=                     '<col style="width: 150px">';
                _content +=                     '<col style="width: 170px">';
                _content +=                 '</colgroup>';
                _content +=                 '<thead>';
                _content +=                     '<tr>';
                _content +=                         '<th></td>';
                _content +=                         '<td></td>';
                _content +=                     '</tr>';
                _content +=                 '</thead>';
                _content +=                 '<tbody>';
                _content +=                     '<tr>';
                _content +=                         '<th class="text-center">년월</th>';
                _content +=                         '<td>' + params.year + '년 ' + params.month + '월</th>';
                _content +=                     '</tr>';
                _content +=                     '<tr>';
                _content +=                         '<th class="text-center">공장명</th>';
                _content +=                         '<td>' + params.fctryNm + '</td>';
                _content +=                     '</tr>';
                _content +=                     '<tr>';
                _content +=                         '<th class="text-center">운수회사</th>';
                _content +=                         '<td>' + params.trnsprtCmpny + '</td>';
                _content +=                     '</tr>';
                _content +=                 '</tbody>';
                _content +=             '</table>';
                _content +=         '</div>';
                /* 상단 테이블간 여백 */
                _content +=         '<div class="col-lg-4">';
                _content +=         '</div>';
                /* 상단 우측 정보 테이블 */
                _content +=         '<div class="col-lg-4">';
                _content +=             '<table id="' + rightId + '">';
                _content +=                 '<colgroup>';
                _content +=                     '<col style="width: 150px">';
                _content +=                     '<col style="width: 170px">';
                _content +=                 '</colgroup>';
                _content +=                 '<thead>';
                _content +=                     '<tr>';
                _content +=                         '<th></td>';
                _content +=                         '<td></td>';
                _content +=                     '</tr>';
                _content +=                 '</thead>';
                _content +=                 '<tbody>';
                _content +=                     '<tr>';
                _content +=                         '<th class="text-center">차종</th>';
                _content +=                         '<td>' + params.vhcleTy + '</td>';
                _content +=                     '</tr>';
                _content +=                     '<tr>';
                _content +=                         '<th class="text-center">차량번호</td>';
                _content +=                         '<td>' + params.vrn + '</td>';
                _content +=                     '</tr>';
                _content +=                     '<tr>';
                _content +=                         '<th class="text-center">기사명</th>';
                _content +=                         '<td>' + params.driverNm + '</td>';
                _content +=                     '</tr>';
                _content +=                 '</tbody>';
                _content +=             '</table>';
                _content +=         '</div>';
                /* 하단 메인 테이블 */
                _content +=         '<div class="col-lg-12" style="height: 1150px;padding-top: 20px;">';
                _content +=             '<div id="' + mainId + '"></div>';
                _content +=         '</div>';
                _content +=     '</div>';
                _content += '</div>';
        
        
                wOptions.content = _content;
                me.init(wOptions).open();
                
                /* 상단 좌우측 그리드 설정 */
                var $topGrid = $('#' + leftId +',#' + rightId);
                $topGrid.AIV_GRID_TABLE({
                    scrollable: false,
                    width: '100%',
                    height: '97%'
                });
                $topGrid.find('.k-grid-header').hide();
                $topGrid.find('tr th').css({
                    'background-color': '#fafafa',
                    'border-bottom': '1px solid #ddd'
                });
                $topGrid.find('tr td').css({
                    'border-bottom': 'solid 1px #ddd'
                });
                // $topGrid.find('tr:last th, tr:last td').css({
                //     'border-bottom': '0px'
                // });
                
                /* 하단 메인 그리드 설정 */
                var mainGrid = $('#'+mainId).AIV_GRID_TABLE({
                    scrollable: false,
                    height: '100%',
                    columns:[
                        { field: 'regDe', title: '일자', width: 120 },
                        { field: 'dayOfWeek', title: '요일', width: 50 },
                        { field: 'dlvyTy', title: '구분', width: 100,
                            template: function(dataItem){
                                var _el = '';
                                
                                // 휴가였으면
                                if ( dataItem.dayoffTy != null) {
                                    _el = dataItem.dayoffTy;
                                    // 휴가일수 있으면
                                    if ( dataItem.dayoffCnt != null) {
                                        _el += '(' + dataItem.dayoffCnt + ')';
                                    }

                                // 운행했으면
                                } else if ( dataItem.dlvyTy != null ) {
                                    _el = dataItem.dlvyTy;
                                } else {
                                    _el = '-';
                                }
                                return _el;
                            }
                        },
                        {
                            title: '개인위생',
                            columns:[
                                { field: 'item01', title: '두발, 수염<br>손톱 등<br>개인위생', width: 120 },
                                { field: 'item02', title: '피부병, 심한<br>감기 등 전염성<br>질병 감염', width: 120 },
                            ]
                        },
                        {
                            title: '운송 전 위생상태',
                            columns:[
                                { field: 'item03', title: '적재함<br>내부<br>세척', width: 120 },
                                { field: 'item04', title: '냉동기작동<br>온도기록지<br>온도기록기<br>부착상태', width: 120 },
                            ]
                        },
                        {
                            title: '운송 중 위생상태',
                            columns:[
                                { field: 'item05', title: '흡연, 음식물<br>섭취 잡담,<br>껌씹는 행위', width: 120 },
                                { field: 'item06', title: '영업장 및<br>화장실 출입 시<br>손 세척<br>및 신발소독', width: 120 },
                                { field: 'item07', title: '적재함<br>바닥<br>오염', width: 120 },
                                { field: 'item08', title: '적재함<br>적정온도<br>유지', width: 120 },
                            ]
                        },
                        {
                            title: '운송 후 위생상태',
                            columns:[
                                { field: 'item09', title: '온도 기록지<br>기록 보관', width: 120 },
                                { field: 'item10', title: '적재함<br>내부<br>세척', width: 120 },
                            ]
                        },
                        { field: 'rm', title: '비고', width: 50 }
                    ]
                })
        
                mainGrid.setData(datas);
        
                $('#btnPDFDownload').on('click', function(){
                    var selector = '#' + _windowContentId;
                    $('#btnPDFDownload').hide();
                    $(selector).css('overflow', 'visible');
                    AIV_UTIL.exportPDF(selector, '일반위생점검표_' + params.year + params.month + '_' + params.driverNm);
                    $(selector).css('overflow', 'auto');
                    $('#btnPDFDownload').show();
                });
        
                return me;

            }
        })
        
        
    }


     /**
     * 공상자 관리
     * @param {dlvyDe} 년원일
     * @param {driverCd} 기사코드
     * @param {routeNo} 노선번호
     */
    me.emptyboxDtls = function (params) {

        var wOptions = {    // kendo window options
            title: '공상자 관리',
            width: 1200,
            height: 500
        };

        var divId = "aivWindowEmptyboxDtls";

        var _content = '';
        _content += '<div>';
        _content +=     '<div class="row" style="margin: 0px;">';
        _content +=         '<div class="col-lg-12">';
        _content +=             '<div id="' + divId + '"></div>';
        _content +=         '</div>';
        _content +=     '</div>';
        _content += '</div>';

        wOptions.content = _content;
        me.init(wOptions).open();
        

        /* 하단 메인 그리드 설정 */
        me.emptyBoxGrid = $('#'+divId).AIV_GRID({
            toolbar: [
                'save'
            ],
            apiUrl: "/adm/empty/box",           // CRUD의 기본 URL 작성
            getParams: function(){return params;},     // 그리드 조회 시 실행될 파라미터 조회 함수 적용
            // scrollable: false,
            height: 400,
            columns:[
                { kField: 'id' },
                { kField: "selectable", editable: false,
                    template: function(dataItems){
                        console.log(dataItems)
                    }
                },
                { kField: "dlvyDe", editable: false },
                { kField: "routeNo", editable: false },
                { kField: "dlvyLoId", editable: false },
                { kField: "dlvyLoNm", editable: false },
                { kField: "squareBoxQty" },
                { kField: "triangleBoxQty" },
                { kField: "yodelryBoxQty" },
                { kField: "palletQty" },
                { kField: "cause", editable: false },
                { kField: "modifyCause", editable: false },
                { kField: "confirm", editable: false },
                { kField: "erpConfirmYn", editable: false },
                { kField: "regUser", editable: false },
                // { kField: "dlvyLcNm", editable: false },
            ],
            editable: true,
            dataBound: function(e) {
                var items = e.sender.dataSource.view();
                $(items).each(function(i, dataItem){
                    // var dataItem = items[i];
                    var $tr = $('tr[data-uid='+dataItem.uid+']');
                    // erp 에서 확정되었으면 수정불가능하도록 제어
                    if ( dataItem.erpConfirmYn == true || dataItem.readOnly == true ) {
                        $tr.find('.k-checkbox').attr('disabled', true);
                        $tr.find('.k-edit-cell').attr('disabled', true);
                    }
                });
            },
        })


        // mainGrid.setData(datas);

        return me;
    }


    /**
     * 물류비 분석
     * @deprecated
     * @description 소스양이 너무 많아 aiv_window.lgist.js 로 분기함. 181113. kdh
     */
    // me.lgistModelCreate = function (params) {
    // }

})(jQuery, window);