/**
 * Kendo TreeView
 */
(function ($) {

    // 기본 설정값
    // var _width = 50;
    // var _height = 25;

    // 세팅
    $.fn.AIV_TREEVIEW = function (options) {
        options = options || {};
        var me = this;

        defaultOptions = {
            dataSpriteCssClassField : 'sprite'
        }

        me.kendoTreeView($.extend(true, defaultOptions, options));
        me.self = me.data("kendoTreeView");

        me.setData = function(datas){
            me.self.setDataSource(new kendo.data.HierarchicalDataSource({
                data: datas
            }));
        }

        return me;
    };


}(jQuery));