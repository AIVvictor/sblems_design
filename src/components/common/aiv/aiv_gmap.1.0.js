/**
 * Google-Map
 */
(function ($) {

    window.initMap = function() {
        console.log('google init map');
    }
    
    // 지도 세팅
    $.fn.AIV_GMAP = function (options) {
        
        var me = this;
        var id = me.attr('id');
        
        // MAP 구조 초기화
        options = options || {};
        // me.appKey = 'AIzaSyAlgCaHHM_BZrMm02oONrlOIlUfLiamWw0'; // Google-MAP appkey  [SeoulMilk CVO]
        me.appKey = 'AIzaSyAvxFocHgqUNmOhhwg4PFjQwn65hckD1Wo'; // Google-MAP appkey  [AI.V ADAS]
        // me.appKey = 'AIzaSyDI5rF0kiWq8R5Ghyiw8NejhOZTFTK98iw '; // Google-MAP appkey    [AI.V kdh-private]
        me.map = {};            // 지도 관련 객체 및 함수모음
        
        // GMAP 전역객체
        // me.map = new google.maps.Map(document.getElementById(id), {
        //     center: {lat: -33.8688, lng: 151.2195},
        //     zoom: 13
        // });

        // 주소 자동완성 함수
        me.map.setAutoComplete = function(options){

            var input = document.getElementById(options.id);
            var autocomplete = new google.maps.places.Autocomplete(input);
            
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("위치정보가 없습니다.: '" + place.name + "'");
                    return;
                }
                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();


                options.success({
                    lat: lat,
                    lng: lng,
                    address: place.formatted_address
                })
                
                

            });

            me.setStyle(options.id)
        }

        me.setStyle = function (id){
            // 기존에 미등록되어있다면 추가
            if ( $('#gmap_custom_style').length < 1) {
                var style = '<style id="gmap_custom_style">';
                style += 'html, body { height: 100%; margin: 0; padding: 0; } ';
                style += '#description { font-family: Roboto; font-size: 15px; font-weight: 300; } ';
                style += '#infowindow-content .title { font-weight: bold; } ';
                style += '#infowindow-content { display: none; } ';
                style += '#map #infowindow-content { display: inline; } ';
                style += '.pac-card { margin: 10px 10px 0 0; border-radius: 2px 0 0 2px; box-sizing: border-box; -moz-box-sizing: border-box; outline: none; box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3); background-color: #fff; font-family: Roboto; } ';
                style += '.pac-container { padding-bottom: 12px; margin-right: 12px; z-index: 99999; display: inline-block; } ';
                style += '.pac-controls { display: inline-block; padding: 5px 11px; } ';
                style += '.pac-controls label { font-family: Roboto; font-size: 13px; font-weight: 300; } ';
                style += '#pac-input { background-color: #fff; font-family: Roboto; font-size: 15px; font-weight: 300; margin-left: 12px; padding: 0 11px 0 13px; text-overflow: ellipsis; width: 400px; } ';
                style += '#pac-input:focus { border-color: #4d90fe; } ';
                style += '#title { color: #fff; background-color: #4d90fe; font-size: 25px; font-weight: 500; padding: 6px 12px; } ';
                style += '</style>';
            }
            
            $('head').append(style);

        }
        return me;
    }




}(jQuery));