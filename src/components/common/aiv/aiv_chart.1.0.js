/**
 * 차트
 */

(function ($) {

    // am4core.useTheme(am4themes_material);
    // am4core.useTheme(am4themes_animated);

    $.fn.AIV_CHART = function (options) {
        var me = this;
        me.options = options;
        me.xAxes = [];
        me.yAxes = [];

        var fillColorList = [
            '#67b7dc', '#6794dc', '#6771dc', '#8067dc', '#a367dc', '#c767dc', '#dc67ce', '#dc67ab', '#dc6788',
            '#dc6967', '#dc8c67', '#dcaf67', '#dcd267', '#c3dc67', '#a0dc67', '#7ddc67', '#67dc75', '#67dc98'];
        var strokeColorList = fillColorList;
        // var defaultOptions = getDefaultOptions(options);
        var selfId = this.attr('id');
        // var chartOptions = $.extend(true, defaultOptions, options);

        var chartType = am4charts.XYChart;   // default;
        if (options.type == 'XYChart') {
            chartType = am4charts.XYChart;
        }
        var chart = am4core.create(selfId, chartType);

        /**
         * Create X/Y axes
         */
        $(options.xAxes).each(function (i, axis) {
            if (axis.type == 'CategoryAxis') {
                var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

                // $.extend(true, categoryAxis, axis);

                categoryAxis.renderer.fullWidthTooltip = true;
                categoryAxis.renderer.grid.template.disabled = true;

                categoryAxis.dataFields.category = axis.dataFields.category;
                categoryAxis.renderer.minGridDistance = axis.renderer && axis.renderer.minGridDistance || categoryAxis.renderer.minGridDistance;
                categoryAxis.renderer.cellStartLocation = axis.renderer && axis.renderer.cellStartLocation || 0.2;
                categoryAxis.renderer.cellEndLocation = axis.renderer && axis.renderer.cellEndLocation || 0.8;
                categoryAxis.renderer.labels.template.rotation = axis.renderer && axis.renderer.labels && axis.renderer.labels.template && axis.renderer.labels.template.rotation || categoryAxis.renderer.labels.template.rotation;
            }
        });
        $(options.yAxes).each(function (i, axis) {
            if (axis.type == 'ValueAxis') {
                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                valueAxis.min = axis.min || 0;
                valueAxis.max = axis.max || valueAxis.max;
                valueAxis.renderer.opposite = axis.renderer && axis.renderer.opposite;

                me.yAxes.push(valueAxis);
            }
        });

        /**
         *  Create Series
         */
        $(options.series).each(function (i, axis) {
            var _amFillColor = am4core.color(fillColorList[i]);
            var _amStrokeColor = am4core.color(strokeColorList[i]);

            if (axis.type == 'ColumnSeries') {
                var _series = chart.series.push(new am4charts.ColumnSeries());
                _series.name = axis.name;
                _series.dataFields.valueY = axis.dataFields.valueY;
                _series.dataFields.categoryX = axis.dataFields.categoryX;
                _series.columns.template.fill = _amFillColor;

                // options 전체에 지정된 tooltip 이 있을경우 series의 0번에 적용하며, 나머지는 제거함.
                if (typeof options.tooltipHTML !== 'undefined') {
                    if (i == 0) {
                        _series.tooltipHTML = options.tooltipHTML;
                    } else {
                        _series.tooltipHTML = '';
                    }

                    // options 전체에 지정된 tooltip 이 없을경우 series각각에 설정된 툴팁을 적용함. 없을 경우 기본 툴팁 적용.
                } else {
                    _series.tooltipHTML = axis.tooltipHTML || `{categoryX}: {valueY}`;  // default
                }

                // 시리즈 변경시마다 툴팁 컬러 변경되는것 방지
                _series.tooltip.getFillFromObject = false;
                _series.tooltip.background.fill = _amFillColor;
            }
        });

        /**
         * Chart Series를 나중에 넣어야 할때 사용
         */
        chart.pushSeries = function (options) {
            var _amFillColor = am4core.color(fillColorList[chart.series.length]);
            var _amStrokeColor = am4core.color(strokeColorList[chart.series.length]);

            var _series = null;
            if (options.type == 'LineSeries') {
                _series = chart.series.push(new am4charts.LineSeries());
                _series.name = options.name;
                _series.dataFields.categoryX = options.dataFields.categoryX;
                _series.dataFields.valueY = options.dataFields.valueY;
                _series.strokeWidth = 4;
                
                // yAxes축이 좌우에 있을 경우에 사용 가능함.
                if ( options.yAxis && typeof options.yAxis.index !== 'undefined' ) {
                    if ( typeof me.yAxes !== 'undefined' ) {
                        _series.yAxis = me.yAxes[options.yAxis.index];
                        _series.numberFormatter.numberFormat = '0';
                    }
                }

                if (typeof options.bullet !== 'undefined') {
                    _bullet = _series.bullets.push(new am4charts.CircleBullet());
                    _bullet.strokeWidth = options.bullet.strokeWidth || _bullet.strokeWidth;
                }

                _series.stroke = _amFillColor;
            } else if (options.type == 'ColumnSeries') {
                _series = chart.series.push(new am4charts.ColumnSeries());
                _series.name = options.name;
                _series.dataFields.categoryX = options.dataFields.categoryX;
                _series.dataFields.valueY = options.dataFields.valueY;
                _series.columns.template.width = am4core.percent(95);
                _series.strokeWidth = 1;

                _series.stroke = _amStrokeColor;
                _series.columns.template.fill = _amFillColor;
            }

            _series.tooltipHTML = (typeof options.tooltipHTML !== 'undefined') ? options.tooltipHTML : `{name}: {valueY}`;

            // 시리즈 변경시마다 툴팁 컬러 변경되는것 방지
            _series.tooltip.getFillFromObject = false;
            _series.tooltip.background.fill = _amFillColor;
        }


        // Add chart Scrollbar
        if (chart.scrollbarX == true) {
            chart.scrollbarX = new am4core.Scrollbar();
        }

        // Add chart Cursor
        chart.cursor = new am4charts.XYCursor();

        // Add chart Legend
        chart.legend = new am4charts.Legend();

        // set Number formatter
        chart.numberFormatter.numberFormat = "#,###.000";

        return chart;
    };


})(jQuery);