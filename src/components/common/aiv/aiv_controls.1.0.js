/**
 * Controls Common Plug-in
 */


(function ($) {

    var AIV_CONTROLS = AIV_CONTROLS || {};
    /**
     * get/set Value for Kendo-jQuery-Object 
     */
    $.fn.kValue = function (val) {
        var me = this;
        var dataRole = me.attr('data-role');
        var kendoType = '';

        // 체크박스
        if (me.attr('type') == 'checkbox') {
            if (AIV_UTIL.isNull(val)) { // get
                return me.is(':checked') ? 'Y' : 'N';
            } else { // set
                if (val == true || val == 'Y') {
                    me.prop('checked', true);
                } else {
                    me.prop('checked', false);
                }
            }

            // 달력
        } else if (dataRole == 'datepicker' || dataRole == 'datetimepicker' || dataRole == 'timepicker') {
            if (dataRole == 'datepicker') {
                kendoType = 'kendoDatePicker';
            } else if (dataRole == 'datetimepicker') {
                kendoType = 'kendoDateTimePicker';
            } else if (dataRole == 'timepicker') {
                kendoType = 'kendoTimePicker';
            }
            if (AIV_UTIL.isNull(val)) { // get
                return me.val();
            } else { // set
                me.data(kendoType).value(val);
                // $me.data(kendoType).trigger("change");
                return me;
            }

            // 그 외 textbox, select 등
        } else {
            if (dataRole == 'numerictextbox') {
                kendoType = 'kendoNumericTextBox';
            } else if (dataRole == 'maskedtextbox') {
                kendoType = 'kendoMaskedTextBox';
            } else if (dataRole == 'dropdownlist') {
                kendoType = 'kendoDropDownList';
            } else if (dataRole == 'dateinput') {
                kendoType = 'kendoDateInput';
            } else if (dataRole == 'multiselect') {
                kendoType = 'kendoMultiSelect';
            }
            if (AIV_UTIL.isNull(val)) { // get
                return me.data(kendoType).value();
            } else { // set
                me.data(kendoType).value(val);
                me.trigger("change");
                // $me.data(kendoType).trigger("change");
                return me;
            }

        }
    }

    $.fn.kText = function (val) {
        var me = this;
        var dataRole = me.attr('data-role');
        var kendoType = '';

        // 체크박스
        if (me.attr('type') == 'checkbox') {
            // 달력
        } else if (dataRole == 'datepicker' || dataRole == 'datetimepicker' || dataRole == 'timepicker') {
            // 그 외 textbox, select 등
        } else {
            if (dataRole == 'dropdownlist') {
                kendoType = 'kendoDropDownList';
            }
            return me.data(kendoType).text();
        }
    }

    /**
     * 지정 DOM 내 input 객체들에 대해 기본 생성을 함
     */
    $.fn.AIV_SEARCH_INIT = function (options) {
        var me = this;
        var divId = me.attr('id');
        options = options || {};
        var rtnParams = {};

        var sampleCode = {
            'ACU': [
                { key: '1', value: '년' },
                { key: '2', value: '월' },
                { key: '3', value: '일' },
            ],
            'EMI': [
                { key: '1001', value: '고체연료연소' },
                { key: '1002', value: '기체연료연소' },
                { key: '1003', value: '액체연료연소' },
                { key: '2001', value: '이동연소(항공)' },
                { key: '2002', value: '이동연소(도로)' },
                { key: '2003', value: '이동연소(철도)' },
                { key: '2004', value: '이동연소(선박)' },
                { key: '3001', value: '석탄의 채굴 및 처리' },
                { key: '3002', value: '원유 및 천연가스 시스템' },
                { key: '4001', value: '시멘트 생산' },
                { key: '4002', value: '석회 생산' },
                { key: '4003', value: '탄산염의 기타 공정사용' },
                { key: '4011', value: '석유정제활동(수소제조)' },
                { key: '4012', value: '석유정제활동(촉매재생)' },
                { key: '4013', value: '석유정제활동(코크스제조)' },
                { key: '4014', value: '암모니아 생산' },
                { key: '4015', value: '질산 생산' },
                { key: '4016', value: '아디프산 생산' },
                { key: '4017', value: '카바이드 생산' },
                { key: '4018', value: '소다회 생산' },
                { key: '4019', value: '석유화학제품생산' },
                { key: '4020', value: '불소화합물생산(HCFC-22 생산)' },
                { key: '4021', value: '철강생산' },
                { key: '4022', value: '합금철 생산' },
                { key: '4023', value: '아연생산' },
                { key: '4024', value: '납생산' },
                { key: '4025', value: '전자산업(반도체)' },
                { key: '4026', value: '전자산업(디스플레이)' },
                { key: '4027', value: '전자산업(광전지)' },
                { key: '4029', value: '오존층파괴물질의 대체물질 사용' },
                { key: '4030', value: '오존층파괴물질의 대체물질 사용(전기설비)' },
                { key: '4031', value: '이산화티탄 생산' },
                { key: '5001', value: '고형폐기물의 매립' },
                { key: '5002', value: '고형폐기물의 생물학적처리' },
                { key: '5003', value: '하수처리 및 배출' },
                { key: '5004', value: '폐수처리 및 배출' },
                { key: '5005', value: '폐기물의 소각' },
                { key: '6001', value: '간접배출(외부전기사용)' },
                { key: '6002', value: '간접배출(외부 열사용)' },
                { key: '7001', value: '기타 ' }
            ],
            'ENG': [
                { key: '1', value: '경유(ℓ)' },
                { key: '2', value: '휘발유(ℓ)' },
                { key: '3', value: 'LNG_도시가스(㎥)' },
                { key: '4', value: 'LPG_프로판(Kg)' },
                { key: '5', value: 'LPG_프로판(N㎥)' },
                { key: '6', value: '전기(Kwh)' },
                { key: '7', value: '스팀(톤)' },
                { key: '8', value: 'LPG_부탄(Kg)' },
                { key: '9', value: 'LPG_부탄(ℓ)' },
                { key: '10', value: '등유(ℓ)' },
                { key: '11', value: '온수(Gcal)' },
            ],
            'GASENG': [
                { key: 'GAS', value: '온실가스' },
                { key: 'ENG', value: '에너지' },
            ],
            'LOGI_MEA': [
                { key: '1', value: '계량형' },
                { key: '2', value: '비계량형' },
            ],
            'LOGI_PTY': [
                { key: '1', value: '민간제안사업' },
                { key: '2', value: '지정사업' },
                { key: '3', value: '효과검증사업' },
                { key: '4', value: '세방 녹색물류전환사업' },
            ],
            'LOGI_TAR': [
                { key: '1', value: '물류' },
                { key: '2', value: '제주도' },
                { key: '3', value: '집유' },
                { key: '4', value: '용차' },
            ],
            'LOGI_UNT': [
                { key: '1', value: '전체' },
                { key: '2', value: '차종(톤급)' },
            ],
            'MM': [
                { key: '01', value: '1' },
                { key: '02', value: '2' },
                { key: '03', value: '3' },
                { key: '04', value: '4' },
                { key: '05', value: '5' },
                { key: '06', value: '6' },
                { key: '07', value: '7' },
                { key: '08', value: '8' },
                { key: '09', value: '9' },
                { key: '10', value: '10' },
                { key: '11', value: '11' },
                { key: '12', value: '12' },
            ],
            'SCU': [
                { key: '1', value: '고유단위' },
                { key: '2', value: 'TJ' },
                { key: '3', value: 'TOE' },
                { key: '4', value: 'tCO2' },
                /* 임의추가값 */
                { key: '5', value: 'GJ' },
                { key: '6', value: 'CO2' },
                { key: '7', value: 'CH3' },
                { key: '8', value: 'N2O' },
                { key: '9', value: 'N2OtCO2eq' },
                { key: '10', value: 'TOE' },
            ],
            'YYYY': [
                // {key:'2020', value: '2020' },
                { key: '2019', value: '2019' },
                { key: '2018', value: '2018' },
                { key: '2017', value: '2017' },
                { key: '2016', value: '2016' },
                { key: '2015', value: '2015' },
                { key: '2014', value: '2014' },
            ],
            'UNT': [
                { key: 1, value: 'g' },
                { key: 2, value: 'Kg' },
                { key: 3, value: 'ton' },
                { key: 4, value: 'Gg' },
                { key: 5, value: 'Nm3' },
                { key: 6, value: '천m3' },
                { key: 8, value: 'cal' },
                { key: 9, value: 'kcal' },
                { key: 10, value: 'Gcal' },
                { key: 11, value: 'ℓ' },
                { key: 12, value: '㎘' },
                { key: 13, value: 'J' },
                { key: 14, value: 'GJ' },
                { key: 15, value: 'TJ' },
                { key: 16, value: 'Kwh' },
                { key: 17, value: 'Mwh' },
                { key: 18, value: 'Gwh' },
                { key: 19, value: 'Kg' },
                { key: 20, value: '시간' },
                { key: 21, value: '분' },
            ],
            'LOGI_YCT': [
                { key: '1', value: '정기' },
                { key: '2', value: '비정기' },
            ],
            // example code
            'SITE': [
                { key: '3', value: 'A사업부222' },
                { key: '4', value: 'F사업부' },
                { key: '5', value: '강남사업부' },
            ],
            'TRANS_TYPE': [
                { key: '1', value: '화물차' },
                { key: '2', value: '선박' },
                { key: '3', value: '철도' },
            ]
        };


        // 서버에서 공통코드를 가져와 변수에 저장함
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/common/keylist',
            async: false,
            data: {
                sUiId: AIV_MENU.sUiId    // 화면 ID
                // sUiId: AIV_MENU.sMenuId    // 화면 ID
            },
            success: function (responseData) {
                // 필요시 webSotrage에 저장할 수도 있음
                var codes = responseData.resultData;

                // TODO: remove
                if (AIV_UTIL.isEmpty(codes)) {
                    console.error('>> Codes is null <<');
                    codes = sampleCode;
                }

                AIV_CONTROLS.COMM_CODES = codes;
            }
        });





        /**
         * 해당 div 하위 input 에 대해 id에 맞추어 자동으로 입력칸을 생성한다.
         */
        $(me).find('input').each(function (i, input) {
            var $input = $(input);
            var inputId = $input.attr('id');

            /**
             * .AIV_DATETIMEPICKER: 달력 / 0:년-월-일, 1:년-월, 2:년, 3:일, 4:년-월-일 시, 5:년-월-일 시:분, 6:년-월-일 시:분:초
             */
            if (inputId == 'fromDe') {
                rtnParams.fromDe = $input.AIV_DATETIMEPICKER({ type: 2, title: "분석기간" });
            } else if (inputId == 'toDe') {
                rtnParams.toDe = $input.AIV_DATETIMEPICKER({ type: 2 });
            } else if (inputId == 'fromMonth') {
                rtnParams.fromMonth = $input.AIV_DATETIMEPICKER({ type: 1, title: "분석기간" });
            } else if (inputId == 'toMonth') {
                rtnParams.toMonth = $input.AIV_DATETIMEPICKER({ type: 1 });

                /**
                 * .AIV_DROPDOWNLIST: 콤보박스 / 0:공장,1:출발지,2:도착지
                 */
            } else if (inputId == 'cYear') {
                var extendOptions = $.extend(true, { type: 'cYear', title: "조회년도", allYn: false }, options.cYear);
                rtnParams.cYear = $input.AIV_DROPDOWNLIST(extendOptions);
            } else if (inputId == 'cYearEnd') {
                rtnParams.cYearEnd = $input.AIV_DROPDOWNLIST({ type: 'cYearEnd', allYn: false });
            } else if (inputId == 'fromYear') {
                // var extendOptions = $.extend(true, { type: 'fromYear', title: "조회년도", allYn: false }, options.fromYear);
                rtnParams.fromYear = $input.AIV_DROPDOWNLIST({ type: 'fromYear', title: "분석기간", allYn: false });
            } else if (inputId == 'toYear') {
                // var extendOptions = $.extend(true, { type: 'toYear', allYn: false }, options.toYear);
                rtnParams.toYear = $input.AIV_DROPDOWNLIST({ type: 'toYear', allYn: false });
            } else if (inputId == 'cMon') {
                rtnParams.cMon = $input.AIV_DROPDOWNLIST({ type: 'cMon' });
            } else if (inputId == 'cMonEnd') {
                rtnParams.cMonEnd = $input.AIV_DROPDOWNLIST({ type: 'cMonEnd', allYn: false });
            } else if (inputId == 'unit') {
                rtnParams.unit = $input.AIV_DROPDOWNLIST({ type: 'unit', title: "조회단위", allYn: false });
            } else if (inputId == 'sSiteId') {
                rtnParams.sSiteId = $input.AIV_DROPDOWNLIST({ type: 'sSiteId', title: "사업장", allYn: false });
            } else if (inputId == 'idSite') {
                rtnParams.idSite = $input.AIV_DROPDOWNLIST({ type: 'idSite', title: "사업장", allYn: false });
            } else if (inputId == 'sLineCd') {
                rtnParams.sLineCd = $input.AIV_DROPDOWNLIST({ type: 'sLineCd', title: "노선구분", allYn: false });
            } else if (inputId == 'sVhcleType') {
                rtnParams.sVhcleType = $input.AIV_DROPDOWNLIST({ type: 'sVhcleType', title: "운송수단구분", allYn: false });
            } else if (inputId == 'idTransCom') {
                rtnParams.idTransCom = $input.AIV_DROPDOWNLIST({ type: 'idTransCom', title: "운수회사", allYn: false });
            } else if (inputId == 'sBaseUnt') {
                rtnParams.sBaseUnt = $input.AIV_DROPDOWNLIST({ type: 'sBaseUnt', title: "원단위기준", allYn: false });
            } else if (inputId == 'emi') {
                rtnParams.emi = $input.AIV_DROPDOWNLIST({ type: 'emi', title: "연소", allYn: true });
            } else if (inputId == 'idEngCef') {
                rtnParams.idEngCef = $input.AIV_DROPDOWNLIST({ type: 'idEngCef', title: "에너지원", allYn: true });
            } else if (inputId == 'transTypeCd') {
                rtnParams.transTypeCd = $input.AIV_DROPDOWNLIST({ type: 'transTypeCd', title: "운송수단구분", allYn: false });
            } else if (inputId == 'sLogiWeiCd') {
                rtnParams.sLogiWeiCd = $input.AIV_DROPDOWNLIST({ type: 'sLogiWeiCd', title: "차종(톤급)", allYn: true });
            } else if (inputId == 'sCarType') {
                rtnParams.sCarType = $input.AIV_DROPDOWNLIST({ type: 'sCarType', title: "차량구분", allYn: true });
            } else if (inputId == 'acu') {
                rtnParams.acu = $input.AIV_DROPDOWNLIST({ type: 'acu', title: "조회단위", allYn: false });
            } else if (inputId == 'sLogiTypeCd') {
                rtnParams.sLogiTypeCd = $input.AIV_DROPDOWNLIST({ type: 'sLogiTypeCd', title: "조회구분" });
            } else if (inputId == 'auth') {
                rtnParams.auth = $input.AIV_DROPDOWNLIST({ type: 'auth', title: "권한" });


                /**
                 * .AIV_MULTI_DROPDOWNLIST
                 */
            } else if (inputId == 'years') {
                rtnParams.years = $input.AIV_MULTI_DROPDOWNLIST({ type: 'years', title: "조회년도" });
            } else if (inputId == 'idSites') {
                var extendOptions = $.extend(true, { type: 'idSites', title: "사업장" }, options.idSites);
                rtnParams.idSites = $input.AIV_MULTI_DROPDOWNLIST(extendOptions);
            } else if (inputId == 'idPoints') {
                var extendOptions = $.extend(true, { type: 'idPoints', title: "배출시설" }, options.idPoints);
                rtnParams.idPoints = $input.AIV_MULTI_DROPDOWNLIST(extendOptions);

                /**
                 * .AIV_CHECKBOX: 체크박스
                 */
            } else if (inputId == 'monthStack') {
                rtnParams.monthStack = $input.AIV_CHECKBOX({ label: '월누적조회' });
            } else if (inputId == 'sum') {
                rtnParams.sum = $input.AIV_CHECKBOX({ label: '합계그래프' });

                /**
                 * .AIV_TEXTBOX: 텍스트박스 / numeric:숫자, 그 외: 텍스트
                 */
            } else if (inputId == 'title') {
                rtnParams.title = $input.AIV_TEXTBOX({ type: 'text', title: "제목", onKeyup: options.onKeyup });
            } else if (inputId == 'sEmpId') {
                rtnParams.sEmpId = $input.AIV_TEXTBOX({ type: 'text', title: "사용자", onKeyup: options.onKeyup });
            } else if (inputId == 'user') {
                rtnParams.user = $input.AIV_TEXTBOX({ type: 'text', title: "사용자", onKeyup: options.onKeyup });
            } else if (inputId == 'code') {
                rtnParams.code = $input.AIV_TEXTBOX({ type: 'text', title: "코드구분", onKeyup: options.onKeyup });
            } else if (inputId == 'sVrn') {
                rtnParams.sVrn = $input.AIV_TEXTBOX({ type: 'text', title: "차량번호", onKeyup: options.onKeyup });
                // } else if (inputId == 'routeNo') {
                //     rtnParams.routeNo = $input.AIV_TEXTBOX({ type: 'text', title: "노선번호", onKeyup: options.onKeyup });
            }
        });

        setTimeout(function () {
            me.css('visibility', 'visible');
        }, 1000)
        // me.show();

        // URL에 parameter를 해당 input 내에 value로 지정한다.
        var urlParams = AIV_UTIL.getUrlParams();
        for (key in urlParams) {
            if (rtnParams[key] && rtnParams[key].kValue) {
                rtnParams[key].kValue(urlParams[key]);
            }
        }


        // input 별로 destroy 함수 생성
        for (key in rtnParams) {
            rtnParams[key].destroy = function () {
                var $input = $(this);
                var $kInput = $input.closest('.k-widget.k-header');
                $('<input id="' + $input.attr('id') + '" />').insertAfter($kInput);
                if ($input.self) {
                    $input.self.destroy();
                }
                if ($kInput.prev().hasClass('aiv_search_title')) {
                    $kInput.prev().remove();
                }
                $kInput.remove();
            }
        }

        // 함수를 제외한 모든 객체를 가져오는 함수
        rtnParams.getAllParam = function () {
            var _params = $.extend({}, this); // 객체 복사
            for (key in _params) {
                if (typeof _params[key] === 'function') { // 함수 타입 제거
                    delete _params[key];
                }
            }
            return _params;
        }

        // 객체 내 모든 파라미터 값을 담은 객체를 가져오는 함수
        rtnParams.getAllValue = function () {
            var _values = {};
            var _params = this;
            for (key in _params) {
                if (typeof _params[key] !== 'function') {
                    var _oid = $(_params[key]).attr('oid'); // original id
                    if (AIV_UTIL.isNotNull(_oid)) {
                        _values[_oid] = _params[key].kValue();
                    } else {
                        _values[key] = _params[key].kValue();
                    }
                }
            }
            return _values;
        }

        // 객체 내 모든 파라미터 값에 세팅하는 함수
        rtnParams.setAllValue = function (setParams) {
            var _params = this;
            for (key in _params) {
                if (typeof _params[key] !== 'function') {
                    _params[key].kValue(setParams[key]);
                }
            }
        }

        return rtnParams;
    }

    window.AIV_CONTROLS = AIV_CONTROLS || {};

})(jQuery);



/**
 * 달력 플러그인
 */
(function ($) {

    // 기본 설정값
    var _wMore = 20;
    var _width = 100 + _wMore;
    var _height = 0;

    // 세팅
    $.fn.AIV_DATEPICKER = function (options) {

        var me = this;
        me.wrap("<div class='aiv_search_input_wrap'></div>");

        // 타이틀 세팅
        var _title = "";
        me.attr('type', 'text');
        _title = (AIV_UTIL.isEmpty(options.title)) ? _title : options.title;

        // me.before("<div>");
        if (_title != "") {
            me.before("<div class='aiv_search_title'>" + _title + "</div>");
        }

        var today = new Date();

        _width = (AIV_UTIL.isEmpty(options.width)) ? _width : options.width;

        //_height = (AIV_UTIL.isEmpty(options.height)) ? _height : options.height;

        // 달력 포맷(타입: undefined:년-월-일, 0:년-월-일, 1:년-월, 2:년)
        if (AIV_UTIL.isEmpty(options.type)) {
            options.format = "yyyy-MM-dd"
        } else {
            if (options.type == 0) { options.format = "yyyy-MM-dd"; } else if (options.type == 1) {
                options.format = "yyyy-MM";
                _width = 85 + _wMore;
            } else if (options.type == 2) {
                options.format = "yyyy";
                _width = 65 + _wMore;
            } else if (options.type == 3) {
                options.format = "dd";
                _width = 50 + _wMore;
            }
        }

        $.extend(options, {
            value: today,
            format: options.format,
        });

        me.kendoDatePicker(options);

        me.closest("span.k-datepicker").width(_width);
        // me.closest("span.k-datepicker").css('font-size', '11px');

        return this;
    };


}(jQuery));



/**
 * 날짜시간 플러그인
 */
(function ($) {


    // 세팅
    $.fn.AIV_DATETIMEPICKER = function (options) {

        // 기본 설정값
        var _width = 170;
        var _height = 15;
        var _type = 0;

        var parent = this;
        var selfId = parent.attr('id') + "_" + "AIV_TIMEPICKER";
        var timelistId = parent.attr('id') + "_timeview";

        var _title = "";
        _title = (AIV_UTIL.isEmpty(options.title)) ? _title : options.title;

        // 타이틀 세팅
        if (_title != "") {
            parent.before("<div class='aiv_search_title'>" + _title + "</div>");
        }
        var me = this; //$("#" + selfId);
        var meId = me.attr('id')

        var today = new Date();

        _width = (AIV_UTIL.isEmpty(options.width)) ? _width : options.width;
        _height = (AIV_UTIL.isEmpty(options.height)) ? _height : options.height;
        _type = (AIV_UTIL.isEmpty(options.type)) ? _type : options.type;

        //_height = (AIV_UTIL.isEmpty(options.height)) ? _height : options.height;

        //  포맷(_type, 0:년-월-일, 1:년-월, 2:년, 3:일, 4:년-월-일 시, 5:년-월-일 시:분, 6:년-월-일 시:분:초, 7:시:분)
        switch (_type) {
            case 0:
                options.format = "yyyy-MM-dd";
                _width = options.width || 115;
                break;
            case 1:
                options.format = "yyyy-MM";
                _width = 100;
                $.extend(options, { start: "year", depth: "year" });
                break;
            case 2:
                options.format = "yyyy";
                _width = 80;
                $.extend(options, { start: "decade", depth: "decade" });
                break;
            case 3:
                options.format = "dd";
                _width = 65;
                break;
            case 4:
                options.format = "yyyy-MM-dd HH";
                _width = 160;
                break;
            case 5:
                options.format = "yyyy-MM-dd HH:mm";
                _width = 180;
                break;
            case 6:
                options.format = "yyyy-MM-dd HH:mm:ss";
                _width = 200;
                break;
            case 7:
                options.format = "HH:mm";
                _width = 70;
                break;
            case 'cYear':
                options.format = "yyyy";
                _width = 80;
                $.extend(options, { start: "decade", depth: "decade" });
                break;
            // case 'cYearEnd':
            //     options.format = "yyyy";
            //     _width = 80;
            //     $.extend(options, { start: "decade", depth: "decade" });
            //     break;
            case 'fromYear':
                options.format = "yyyy";
                _width = 80;
                $.extend(options, { start: "decade", depth: "decade" });
                break;
            case 'toYear':
                options.format = "yyyy";
                _width = 80;
                $.extend(options, { start: "decade", depth: "decade" });
                break;
            case 'cMon':
                options.format = "MM";
                _width = 60;
                $.extend(options, { start: "year", depth: "year" });
                break;
            // case 'cMonEnd':
            //     options.format = "MM";
            //     _width = 60;
            //     $.extend(options, { start: "year", depth: "year" });
            //     break;
        }

        // 팝업 캘린더 하단(Footer) 커스텀 세팅...
        me.setCustomFooter = function (baseId, obj, calObj) {
            var curObj = null;

            obj.one("open", function (e) {

                if (e.view == 'time') return;
                var footerTemplate = baseId + kendo.guid();
                var customButton01 = baseId + kendo.guid();
                var customButton02 = baseId + kendo.guid();
                var customButton03 = baseId + kendo.guid();
                var customButton04 = baseId + kendo.guid();
                // if (AIV_UTIL.isEmpty(curObj)) {

                var sHTML = '';
                sHTML = '<script id="' + footerTemplate + '" type="text/x-kendo-template">';
                sHTML += '  <button id="' + customButton01 + '" style="padding:2px 2px !important; margin:2px 1px !important;"></button>';
                sHTML += '  <button id="' + customButton02 + '" style="padding:2px 2px !important; margin:2px 1px !important;"></button>';
                sHTML += '  <button id="' + customButton03 + '" style="padding:2px 2px !important; margin:2px 1px !important;"></button>';
                sHTML += '  <button id="' + customButton04 + '" style="padding:2px 2px !important; margin:2px 1px !important;"></button>';
                sHTML += '  #=text#';
                sHTML += '</script>';
                $(sHTML).appendTo('#' + baseId);

                var templateHTML = kendo.template($("#" + footerTemplate).html());
                var dp = this;
                setTimeout(function () {
                    dp.dateView.popup.wrapper.find(".k-footer").append(templateHTML({ text: "&nbsp;" }));

                    var setCustomFooterButton = function (i, t, f) {
                        // AIV_SYS.log('baseId------------------------------------>>>>>' + i);
                        // 버튼
                        curObj = $("#" + i).AIV_BUTTON({
                            title: t,
                            onClick: f
                        });
                        // me.closest("button.k-button").width(_width);
                        curObj.closest("button.k-button").height(15);
                        curObj.closest("button.k-button").css('font-size', '11px');
                    };

                    var d = new Date();
                    setCustomFooterButton(customButton01, "전전월오늘", function () {
                        calObj.self.value(new Date(d.getFullYear(), d.getMonth() - 2, d.getDate()));
                        calObj.self.close("date");
                    });
                    setCustomFooterButton(customButton02, "전월오늘", function () {
                        calObj.self.value(new Date(d.getFullYear(), d.getMonth() - 1, d.getDate()));
                        calObj.self.close("date");
                    });
                    setCustomFooterButton(customButton03, "당월1일", function () {
                        calObj.self.value(new Date(d.getFullYear(), d.getMonth(), 1));
                        calObj.self.close("date");
                    });
                    setCustomFooterButton(customButton04, "당월말일", function () {
                        calObj.self.value(new Date(d.getFullYear(), d.getMonth() + 1, 0));
                        calObj.self.close("date");
                    });

                });
                // }
            });

            obj.one("close", function (e) {
                // AIV_SYS.log('close------------------------------------>>>>>');
                curObj = null;
            });
        };

        // 플러그인 세팅
        switch (_type) {
            case 0:
            case 1:
            case 2:
            case 3:
                // AIV_SYS.log("날짜세팅");    
                // 날짜 세팅
                $.extend(options, {
                    value: today,
                    format: options.format,
                    culture: "ko-KR"
                });


                me.kendoDatePicker(options);
                me.self = me.data("kendoDatePicker");

                me.closest("span.k-datepicker").width(_width);
                // me.closest("span.k-datepicker").css('font-size', '11px');        

                // me.setCustomFooter(parent.attr('id'), me.getKendoDatePicker(), me);

                break;
            case 4:
            case 5:
            case 6:
                // 날짜시간 세팅
                // AIV_SYS.log("날짜시간 세팅");    

                var d = new Date();
                $.extend(options, {
                    value: new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0),
                    interval: 30,
                    timeFormat: "HH:mm", //24 hours format
                    dateInput: true,
                    culture: "ko-KR"
                });

                me.kendoDateTimePicker(options);
                me.self = me.data("kendoDateTimePicker");

                me.closest("span.k-datetimepicker").width(_width);
                // me.closest("span.k-datetimepicker").height(_height);
                // me.closest("span.k-datetimepicker").css('font-size', '12px');
                // me.closest("span.k-datetimepicker").css('margin-top', '-6px');
                // me.closest("span.k-datetimepicker").css('padding', '0px');

                // me.setCustomFooter(parent.attr('id'), me.getKendoDateTimePicker(), me);

                break;
            case 7:
                var d = new Date();
                $.extend(options, {
                    value: new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0),
                    interval: 30,
                    timeFormat: "HH:mm", //24 hours format
                    dateInput: true,
                    culture: "ko-KR"
                });
                me.kendoTimePicker(options);
                me.closest("span.k-timepicker").width(_width).height(28);

                break;
            case 'cYear':
                $.extend(options, {
                    value: today,
                    format: options.format,
                    culture: "ko-KR"
                });
                me.kendoDatePicker(options);
                me.self = me.data("kendoDatePicker");
                me.closest("span.k-datepicker").width(_width);
                break;
            // case 'cYearEnd':
            //     $.extend(options, {
            //         value: today,
            //         format: options.format,
            //         culture: "ko-KR"
            //     });
            //     me.kendoDatePicker(options);
            //     me.self = me.data("kendoDatePicker");
            //     me.closest("span.k-datepicker").width(_width);
            //     break;
            case 'cMon':
                $.extend(options, {
                    value: today,
                    format: options.format,
                    culture: "ko-KR"
                });
                me.kendoDatePicker(options);
                me.self = me.data("kendoDatePicker");
                me.closest("span.k-datepicker").width(_width);
                break;
            // case 'cMonEnd':
            //     $.extend(options, {
            //         value: today,
            //         format: options.format,
            //         culture: "ko-KR"
            //     });
            //     me.kendoDatePicker(options);
            //     me.self = me.data("kendoDatePicker");
            //     me.closest("span.k-datepicker").width(_width);
            //     break;
        }

        // 시간일경우 마지막리스트 추가
        me.addLastTime = function () {
            var tmHTML = '';
            var hour = '';
            for (i = 0; i < 24; i++) {
                hour = i;
                if (i < 10) hour = '0' + i;
                if (i == 0) {
                    tmHTML = '<li tabindex="-1" role="option" class="k-item k-state-selected" unselectable="on" id="dttoDe_option_selected" aria-selected="true">00:00</li>';
                    tmHTML += '<li tabindex="-1" role="option" class="k-item" unselectable="on">' + hour + ':30</li>';
                } else {
                    tmHTML += '<li tabindex="-1" role="option" class="k-item" unselectable="on">' + hour + ':00</li>';
                    tmHTML += '<li tabindex="-1" role="option" class="k-item" unselectable="on">' + hour + ':30</li>';
                }
            }
            tmHTML += '<li tabindex="-1" role="option" class="k-item" unselectable="on">' + hour + ':59</li>';

            $("#" + timelistId + "").append(tmHTML);
            $("#" + timelistId + "").parents("div").css('height', '206px');
        };

        if ((_type == 5) || (_type == 6)) {
            me.addLastTime();
        }

        me.addDay = function (add) {
            // var datepicker = $("#datepicker").data("kendoDatePicker");
            var datepicker = me.self;
            var time = datepicker.value().getTime();
            var unit = 24 * 60 * 60 * 1000;
            datepicker.value(new Date(time + add * unit));
        }

        return me;
    };


}(jQuery));



/**
 * 콤보(DROPDOWNLIST) 플러그인
 */
(function ($) {

    // 기본 설정값
    var _width = 110;
    var _height = 0;
    var DATA_SET = {};

    // 세팅
    $.fn.AIV_DROPDOWNLIST = function (options) {
        var _index = 0;

        var me = this;
        if (options.nowrap) {
        } else {
            me.wrap("<div class='aiv_search_input_wrap'></div>");
        }

        // 타이틀 세팅
        var _title = "";
        _title = (AIV_UTIL.isEmpty(options.title)) ? _title : options.title;
        if (_title != "") {
            me.before("<div class='aiv_search_title'>" + _title + "</div>");
        }

        _width = (AIV_UTIL.isEmpty(options.width)) ? _width : options.width;
        //_height = (AIV_UTIL.isEmpty(options.height)) ? _height : options.height;

        options.data = [];
        // (전체) 포함 여부
        // var allYn = !!options.allYn;
        var allYn = (AIV_UTIL.isNull(options.allYn)) ? true : options.allYn;
        // if (allYn) {
        //     options.data.push({ text: "(전체)", value: "" });
        // }
        if (options.type == 'cYear') { // 조회년도
            _width = 70;
            var yyyy = new Date().format('yyyy');
            var codeList = AIV_CONTROLS.COMM_CODES.YYYY;
            $(codeList).each(function (i, code) {
                if (yyyy == code.key) {
                    _index = i;
                }
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'cMon') { // 월
            _width = 50;
            var mm = new Date().format('mm');
            allYn = false;
            me.closest('.aiv_search_input_wrap').css('margin-left', '6px');
            var codeList = AIV_CONTROLS.COMM_CODES.MM;
            $(codeList).each(function (i, code) {
                if (mm == code.key) {
                    _index = i;
                }
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'unit') { // 단위
            _width = 115;
            var codeList = AIV_CONTROLS.COMM_CODES.SCU;
            $(codeList).each(function (i, code) {
                if (i != 0) {   // {1: '고유단위'}
                    options.data.push({ text: code.value, value: code.key });
                }
            });

        } else if (options.type == 'sSiteId' || options.type == 'idSite') {
            _width = 115;
            allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.SITE;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'sLineCd') {
            _width = 115;
            allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.LOGI_YCT;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'sVhcleType') {
            _width = 115;
            allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.LOGI_YCT2;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'idTransCom') {
            _width = 115;
            allYn = true;
            // var codeList = AIV_CONTROLS.COMM_CODES.LOGI_YCT2;
            // $(codeList).each(function (i, code) {
            //     options.data.push({ text: code.value, value: code.key });
            // });

        } else if (options.type == 'sBaseUnt') {
            _width = 115;
            // allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.BASE_UNT;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'fromYear') {
            _width = 115;
            // allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.YYYY;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'toYear') {
            _width = 115;
            // allYn = true;
            me.closest('.aiv_search_input_wrap').css('margin-left', '6px');
            var codeList = AIV_CONTROLS.COMM_CODES.YYYY;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'emi') {
            _width = 115;
            // allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.EMI;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'idEngCef') {
            _width = 115;
            // allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.ENG;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'transTypeCd') {
            _width = 115;
            // allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.TRANS_TYPE;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'sLogiWeiCd') {
            _width = 115;
            // allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.LOGI_WEI;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'sCarType') {
            _width = 115;
            // allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.CAR_TYPE;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'acu') {
            _width = 115;
            // allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.ACU;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'cYearEnd') {
            _width = 70;
            me.closest('.aiv_search_input_wrap').css('margin-left', '6px');
            var yyyy = new Date().format('yyyy');
            var codeList = AIV_CONTROLS.COMM_CODES.YYYY;
            $(codeList).each(function (i, code) {
                if (yyyy == code.key) {
                    _index = i;
                }
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'cMonEnd') {
            _width = 50;
            var mm = new Date().format('mm');
            allYn = false;
            me.closest('.aiv_search_input_wrap').css('margin-left', '6px');
            var codeList = AIV_CONTROLS.COMM_CODES.MM;
            $(codeList).each(function (i, code) {
                if (mm == code.key) {
                    _index = i;
                }
                options.data.push({ text: code.value, value: code.key });
            });

        } else if (options.type == 'sLogiTypeCd') {
            _width = 115;
            allYn = true;
            var codeList = AIV_CONTROLS.COMM_CODES.LOGI_TYPE;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        }
        else if (options.type == 'auth') {
            _width = 115;
            allYn = false;
            var codeList = AIV_CONTROLS.COMM_CODES.AUTH;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
            options.onSelect = function (e) {
                console.log(e.dataItem);
                AIV_COMM.ajax({
                    method: "GET",
                    url: "/api/v1/common/menu/auth",
                    data: {
                        sAuthCd: e.dataItem.value
                    }
                })
            }
        } else if (options.type == 1) {
            _width = 115;
            options.data.push({ text: "전체", value: "전체" });
        }

        switch (options.type) {
            case 'cYear':
            case 'fromYear':
            case 'toYear':
            case 'cMon':
            case 'unit':
            case 'sSiteId':
            case 'idSite':
            case 'sLineCd':
            case 'sVhcleType':
            case 'idTransCom':
            case 'sBaseUnt':
            case 'emi':
            case 'idEngCef':
            case 'transTypeCd':
            case 'sLogiWeiCd':
            case 'sCarType':
            case 'acu':
            case 'cYearEnd':
            case 'cMonEnd':
            case 'sLogiTypeCd':
            case 'auth':

                // DropDownList 생성
                me.kendoDropDownList({
                    dataTextField: "text",
                    dataValueField: "value",
                    autoWidth: true, // 펼쳤을 때 LIST 내 TEXT 길이에 따라 자동적으로 가로길이를 조정해줌
                    optionLabel: allYn ? "전체" : false,
                    dataSource: options.data,
                    filter: options.filter,
                    change: options.onChange,
                    select: options.onSelect,
                    index: _index
                });

                me.closest("span.k-dropdown").width(_width);
                me.closest("span.k-dropdown").css('font-size', '13px');
                me.self = me.data("kendoDropDownList");

                break;
            // case 3:
            //     // DropDownTree 생성
            //     me.kendoDropDownTree({
            //         placeholder: "-창고-",
            //         height: "auto",
            //         change: onChange,
            //         dataSource: [{ text: "양주공장", value: "1D1", expanded: true, items: [{ text: "양주-창고01", value: "1D1-001" }, { text: "양주-창고02", value: "1D1-002" }, { text: "양주-창고03", value: "1D1-003" }] }, { text: "용인공장", value: "2D1", expanded: true, items: [{ text: "용인-창고01", value: "2D1-001" }, { text: "용인-창고02", value: "2D1-002" }, { text: "용인-창고03", value: "2D1-003" }] }, { text: "안산공장", value: "3D1", expanded: true, items: [{ text: "안산-창고01", value: "3D1-001" }, { text: "안산-창고02", value: "3D1-002" }, { text: "안산-창고03", value: "3D1-003" }] }, { text: "거창공장", value: "4D1", expanded: true, items: [{ text: "거창-창고01", value: "4D1-001" }, { text: "거창-창고02", value: "4D1-002" }, { text: "거창-창고03", value: "4D1-003" }] }]
            //     });
            //     me.closest("span.k-dropdowntree").width(_width);
            //     me.closest("span.k-dropdowntree").css('font-size', '13px');
            //     me.dataSource = me.data("kendoDropDownTree").dataSource
            //     break;
        }

        return me;
    };


}(jQuery));


/**
 * 멀티드롭다운(MULTI-DROPDOWNLIST) 플러그인
 */
(function ($) {

    // 기본 설정값
    var _width = 110;
    var _height = 0;
    var DATA_SET = {};

    // 세팅
    $.fn.AIV_MULTI_DROPDOWNLIST = function (options) {

        var me = this;
        var id = me.attr('id');

        if (options.nowrap) { } else {
            me.wrap("<div class='aiv_search_input_wrap'></div>");
        }

        me.attr('multiple', 'multiple');
        // <select id="required" multiple="multiple" data-placeholder="Select attendees..."></select>


        // 타이틀 세팅
        var _title = "";
        _title = (AIV_UTIL.isEmpty(options.title)) ? _title : options.title;
        if (_title != "") {
            me.before("<div class='aiv_search_title'>" + _title + "</div>");
        }

        // data setting
        options.data = [];
        _width = (AIV_UTIL.isEmpty(options.width)) ? _width : options.width;
        if (options.type == 'years') { // 년도
            _width = 245;
            var codeList = AIV_CONTROLS.COMM_CODES.YYYY
            $(codeList).each(function (i, code) {
                if (i != 0) {
                    options.data.push({ text: code.value, value: code.key });
                }
            });
        } else if (options.type == 'idSites') {
            _width = 250;
            var codeList = AIV_CONTROLS.COMM_CODES.SITE
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'idPoints') {
            _width = 350;
        }


        if (options.type == 'idSites') {
            /**
             * 1. "전체"라는 옵션값을 처음에 추가해줌
             * 2. 다른 선택값이 있을때 "전체" 선택시 다른 선택지가 해제됨
             * 3. "전체" 선택 중 다른선택값 선택시 
             */
            function contains(value, values) {
                for (var idx = 0; idx < values.length; idx++) {
                    if (values[idx] === value) {
                        return true;
                    }
                }
                return false;
            }

            options.data = [{ text: "전체", value: "" }].concat(options.data);

            me.kendoMultiSelect({
                dataTextField: "text",
                dataValueField: "value",
                autoWidth: true, // 펼쳤을 때 LIST 내 TEXT 길이에 따라 자동적으로 가로길이를 조정해줌
                // optionLabel: allYn ? "전체" : false,
                dataSource: options.data,
                // filter: options.filter,
                change: options.onChange,
                select: function (e) {
                    var dataItemValue = this.dataSource.view()[e.item.index()].value;
                    var values = this.value();

                    if (dataItemValue !== "" && contains(dataItemValue, values)) {  // 전체
                        return;
                    }

                    if (dataItemValue === "") {
                        values = [];
                    } else if (values.indexOf("") !== -1) {
                        values = $.grep(values, function (value) {
                            return value !== "";
                        });
                    }

                    values.push(dataItemValue);
                    this.value(values);
                    this.trigger("change"); //notify others for the updated values
                    e.preventDefault();
                },
                // index: 0, // index:0=default
                autoClose: false,
                placeholder: '선택해주세요',
            });

            // css height 늘어나는 현상 방어코드
            // me.siblings('.k-multiselect-wrap').css({'height': '28px'})
            // .parent().css({'height': '28px'});

            // } else if (true) {
        } else {
            var content = "";
            content += "<div class='row'>"
            content += "<div class='col-lg-12'>"
            content += "<button class='k-button' id='select'>전체 선택</button>"
            content += "<button class='k-button' id='deselect'>선택 해제</button>"
            content += "</div>"
            content += "</div>"

            me.kendoMultiSelect({
                dataTextField: "text",
                dataValueField: "value",
                autoWidth: true, // 펼쳤을 때 LIST 내 TEXT 길이에 따라 자동적으로 가로길이를 조정해줌
                // optionLabel: allYn ? "전체" : false,
                dataSource: options.data,
                // filter: options.filter,
                change: options.onChange,
                // index: 0, // index:0=default
                autoClose: false,
                placeholder: '선택해주세요',
                headerTemplate: content
            });
        }




        me.closest("div.k-multiselect").width(_width);  // wrapper 영역 크기 조절
        me.closest("div.k-multiselect").find('input.k-input').width('100%');    // placeholder 간격 조절
        me.self = me.data("kendoMultiSelect");

        // 전체선택 버튼 클릭이벤트 바인딩
        $(document).on('click', '#' + id + '-list' + ' #select', function () {
            selectAll();
        });
        // 전체해제 버튼 클릭이벤트 바인딩
        $(document).on('click', '#' + id + '-list' + ' #deselect', function () {
            deselectAll();
        });

        // 전체선택 기능 함수
        var selectAll = function () {
            var yearsValue = [];
            var allData = me.self.dataSource.data();
            $(allData).each(function (i, data) {
                yearsValue.push({
                    text: data.text,
                    value: data.value
                })
            });
            me.self.value(yearsValue);
            me.self.dataSource.filter({});
            me.self.trigger('change');
        }
        // 전체해제 기능 함수
        var deselectAll = function () {
            me.self.value("");
            me.self.dataSource.filter({});
            me.self.trigger('change');
        }

        me.selectAll = selectAll;
        me.deselectAll = deselectAll;

        return me;
    };


}(jQuery));



/**
 * 텍스트박스(TEXTBOX) 플러그인
 */
(function ($) {


    // 세팅
    $.fn.AIV_TEXTBOX = function (options) {
        // 기본 설정값
        var _width = 110;
        var _height = 0;

        var me = this;
        me.wrap("<div class='aiv_search_input_wrap'></div>");

        me.attr('type', 'text');
        var id = me.attr('id');

        var _title = "";
        _title = (AIV_UTIL.isEmpty(options.title)) ? _title : options.title;

        if (_title != "") {
            me.before("<div class='aiv_search_title'>" + _title + "</div>");
        }

        // 타이틀 세팅
        _width = (AIV_UTIL.isEmpty(options.width)) ? _width : options.width;
        //_height = (AIV_UTIL.isEmpty(options.height)) ? _height : options.height;

        options.data = {};

        if (id == 'routeNo') {
            _width = 150;
            me.kendoMaskedTextBox({
                mask: ""
            }).bind("keyup", function (e) {
                if (e.keyCode == 13 && options.onKeyup) {
                    options.onKeyup();
                }
            });
            me.self = me.data('kendoMaskedTextBox');

        } else if (id == 'sVrn') {
            me.kendoMaskedTextBox({
                mask: ""
            }).bind("keyup", function (e) {
                if (e.keyCode == 13 && options.onKeyup) {
                    options.onKeyup();
                }
            });
            me.self = me.data('kendoMaskedTextBox');

        } else if (id == 'driverNm') {
            me.kendoMaskedTextBox({
                mask: ""
            }).bind("keyup", function (e) {
                if (e.keyCode == 13 && options.onKeyup) {
                    options.onKeyup();
                }
            });
            me.self = me.data('kendoMaskedTextBox');

        } else if (id == 'mobileNo') {
            _width = 150;
            me.kendoMaskedTextBox({
                mask: ""
            }).bind("keyup", function (e) {
                if (e.keyCode == 13 && options.onKeyup) {
                    options.onKeyup();
                }
            });
            me.self = me.data('kendoMaskedTextBox');

        } else if (id == 'modelNm') {
            _width = 150;
            me.kendoMaskedTextBox({
                mask: ""
            }).bind("keyup", function (e) {
                if (e.keyCode == 13 && options.onKeyup) {
                    options.onKeyup();
                }
            });
            me.self = me.data('kendoMaskedTextBox');

        } else if (id == 'stdTimeHHMI') {
            _width = 70;
            me.kendoDateInput({
                format: "HH:mm",
            });
            me.self = me.data('kendoMaskedTextBox');

        } else if (options.type == 'numeric') {
            me.attr('type', 'numeric');
            me.kendoNumericTextBox({
                format: options.format || '#'
            });
            me.self = me.data('kendoNumericTextBox');
        } else {
            if (!AIV_UTIL.isEmpty(options.onKeyup)) {
                me.kendoMaskedTextBox({
                    mask: ""
                }).bind("keyup", function (e) {
                    if (e.keyCode == 13 && options.onKeyup) {
                        options.onKeyup();
                    }
                });
            } else {
                me.kendoMaskedTextBox({
                    mask: ""
                });
            }
            me.self = me.data('kendoMaskedTextBox');
        }
        me.closest("span.k-widget").width(_width);


        return this;
    };


}(jQuery));



/**
 * 체크박스(CHECKBOX) 플러그인
 */
(function ($) {

    // 세팅
    $.fn.AIV_CHECKBOX = function (options) {

        var me = this;

        me.attr("type", "checkbox")
            .addClass('k-checkbox');

        var _oid = me.attr('id');
        var _id = _oid + '_' + kendo.guid(); // label의 for가 해당 체크박스를 찾도록 하기 위해 유니크 값 부여
        me.attr('id', _id);
        me.attr('oid', _oid);
        var _title = (AIV_UTIL.isEmpty(options.title)) ? "" : options.title;
        var _label = (AIV_UTIL.isEmpty(options.label)) ? "" : options.label;

        if (_title != "") {
            me.before("<div class='aiv_search_title'>" + _title + "</div>");
        }
        if (_label != "") {
            me.after('<label class="k-checkbox-label" for="' + _id + '">' + _label + '</label>');
        }

        if (options.checked) {
            me.attr('checked', true);
        }

        me.afterHtml = function (text) {
            $label = $('label[for=' + _id + ']');
            $span = $label.find('span[name=cnt]');
            if ($span.length > 0) {
                $span.html(text)
            }
            return me;
        }

        me.labelCss = function (options) {
            $label = $('label[for=' + _id + ']');
            $label.css(options);
            return me;
        }

        if (options.onChange) {
            $(me).change(function () {
                options.onChange();
            })
        }

        return this;
    };


}(jQuery));



/**
 * 버튼(BUTTON) 플러그인
 */
(function ($) {

    // 기본 설정값
    var _width = 50;
    var _height = 25;

    // 세팅
    $.fn.AIV_BUTTON = function (options) {

        var me = this;

        var _title = "";
        _title = (AIV_UTIL.isEmpty(options.title)) ? _title : options.title;

        if (_title != "") {
            me.text(_title);
        }
        if (options.class) {
            me.addClass(options.class);
        }

        // 타이틀 세팅
        _width = (AIV_UTIL.isEmpty(options.width)) ? _width : options.width;
        _height = (AIV_UTIL.isEmpty(options.height)) ? _height : options.height;

        options.data = {};

        me.kendoButton({
            icon: (AIV_UTIL.isEmpty(options.icon)) ? null : options.icon,
            click: (AIV_UTIL.isEmpty(options.onClick)) ? console.log("버튼 클릭이벤트 미정의.") : options.onClick
        });

        // me.closest("button.k-button").width(_width);
        // me.closest("button.k-button").height(_height);
        // me.closest("button.k-button").css('font-size', '13px');

        return this;
    };


}(jQuery));


/**
 * 리스트박스(LISTBOX) 플러그인
 */
(function ($) {

    // 기본 설정값
    var _width = 50;
    var _height = 25;

    // 세팅
    $.fn.AIV_LISTBOX = function (options) {

        var me = this;

        var dataSource = new kendo.data.DataSource({
            data: [] // { text: "foo" }
        });

        var defaultOptions = {
            dataTextField: 'text',
            dataValueField: 'value',
            dataSource: dataSource,
            selectable: "multiple",
            // connectWith: "lgist_selected",
            // toolbar: {
            //     tools: ["transferTo", "transferFrom", "transferAllTo", "transferAllFrom"]
            // },
        }

        options = $.extend(defaultOptions, options);

        me.kendoListBox(options);

        me.setData = function (datas) {
            var _ds = new kendo.data.DataSource({ data: datas });
            me.self.setDataSource(_ds);
        }

        // 전체값을 Array 형태로 반환
        me.getAllValue = function () {
            var datas = [];
            var dataItems = me.self.dataItems();
            $(dataItems).each(function (i, dataItem) {
                datas.push(dataItem.text);
            })
            return datas;
        }

        me.clear = function () {
            me.self.setDataSource([]);
        }

        me.self = me.data("kendoListBox");

        return this;
    };


}(jQuery));



/**
 * 업로드(FILE-UPLOAD) 플러그인
 */
(function ($) {

    // 기본 설정값
    var _width = 50;
    var _height = 25;

    // 세팅
    $.fn.AIV_UPLOAD = function (options) {
        options = options || {};
        var me = this;

        me.attr('type', 'file');

        var defaultOptions = {
            multiple: false,
            showFileList: true,
        };

        options = $.extend(true, defaultOptions, options);
        me.kendoUpload(options);
        me.self = me.data("kendoUpload");


        // upload function 작성
        me.upload = function (options) {
            options = options || {};
            /**
             * FromData() 를 통하여 Ajax Upload를 구현함.
             * 1. Form 생성
             * 2. Kendo Upload의 input 객체를 찾아 Form에 추가
             * 3. sMenuId등 기타 parameter 들을 input 객체로 만들어 Form에 추가
             * 4. ajax 전송
             */

            if (me.self) {
                var form = document.createElement("form");
                form.name = "fileupload";
                form.enctype = "multipart/form-data";
                form.method = "post";

                // 파일 체크
                if (me.self.getFiles()[0].rawFile) {
                    var input = $(me)[0];
                    input.setAttribute("name", "file");
                    form.appendChild(input);
                }

                // 메뉴아이디 체크
                if (AIV_MENU.sMenuId) {
                    var input = document.createElement("input");
                    input.setAttribute("type", "text");
                    input.setAttribute("name", "sMenuId");
                    input.setAttribute("value", AIV_MENU.sMenuId);
                    form.appendChild(input);
                }

                // gId체크 (optional)
                if (options.gId) {
                    var input = document.createElement("input");
                    input.setAttribute("name", "gId");
                    input.setAttribute("value", options.gId);
                    form.appendChild(input);

                    var input2 = document.createElement("input");
                    input2.setAttribute("name", "gid");
                    input2.setAttribute("value", options.gId);
                    form.appendChild(input2);
                }

                // idFile체크 (optional)
                if (options.idFile) {
                    var input = document.createElement("input");
                    input.setAttribute("name", "idFile");
                    input.setAttribute("value", options.idFile);
                    form.appendChild(input);
                }

                AIV_COMM.fileUpload({
                    form: form,
                    success: options.success
                });
            }
        }


        return me;
    };


}(jQuery));