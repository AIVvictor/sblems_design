/**
 * 메뉴 관리
 */
(function (w) {
    var MENU = {};

    MENU.menuHTML = "";
    MENU.currentPageTitle = "";
    MENU.currentFileName = "";
    MENU.currentPageId = 0; // 미사용
    MENU.sUiId = 0;
    MENU.sMenuId = 0;
    MENU.pathInfo = "";

    /**
     * 초기화
     * @param {초기화 옵션} options 
     */
    MENU.init = function (options) {
        MENU.currentFileName = location.pathname.split('/').pop();   // ex: /v1/sum/111.html -> 111.html
        // 메뉴HTML 작성..
        // MENU.menuHTML = MENU.setMenu(MENU.defaultMenu);
        var platMenu = AIV_USER.getMenuInfo();
        var hierachyMenu = convertMenu( platMenu );
        MENU.menuHTML = MENU.setMenu( hierachyMenu );
    };
    
    
    // flat한 형태의 메뉴리스트를 hierachy 하게 바꿔준다
    var convertMenu = function(originalMenuList){
        var oMenus = originalMenuList;
        var cMenus = [];
        $(oMenus).each(function(i, oMenu){
            // if ( oMenu.nLev == 0 ) { // API Response내 nLev값이 내려오지 않으므로 취소. 191007. kdh.
            if ( oMenu.sMenuCat == oMenu.sMenuId ) {    // 메뉴카테고리가 자신의 메뉴 아이디면 최상위 메뉴로판단 하도록 변경. 191007. kdh.
                cMenus.push({    // 1depth menu
                    "id": oMenu.idMenuInfo, "title": oMenu.sMenuNm, "path": oMenu.sPagePath || '#', "iconClassName": null, "sMenuId": oMenu.sMenuId, "items": []
                })
            } else {
                $(cMenus).each(function(j, cMenu){
                    if ( cMenu.sMenuId === oMenu.sUMenuId ) {
                        cMenus[j].items.push({    // 2depth menu
                            "id": oMenu.idMenuInfo, "title": oMenu.sMenuNm, "path": oMenu.sPagePath || '#', "iconClassName": null, "sMenuId": oMenu.sMenuId, "items": []
                        })
                    }
                    $(cMenu.items).each(function(k, cMenuItem){
                        if ( cMenuItem.sMenuId === oMenu.sUMenuId ) {
                            cMenus[j].items[k].items.push({    // 3depth menu
                                "id": oMenu.idMenuInfo, "title": oMenu.sMenuNm, "path": oMenu.sPagePath || '#', "iconClassName": null, "sMenuId": oMenu.sMenuId, "items": []
                            })
                        }
                    });
                });
            }
        });
        return cMenus;
    };


    /**
     * 메뉴 HTML을 작성하여 리턴한다.
     * @param {메뉴 JSON} hierachyMenu 
     */
    MENU.setMenu = function (hierachyMenu) {

        var retHTML = '';

        $(hierachyMenu).each(function (mIdx1, menu1) {


            if (menu1.path.indexOf(MENU.currentFileName) >= 0) {
                bfindCurPage = true;
                MENU.sUiId = menu1.sUiId;
                MENU.sMenuId = menu1.sMenuId;
                // MENU.pathInfo = menu1.title;
                MENU.pathInfo = '';
                MENU.currentPageTitle = menu1.title;
            }


            retHTML += '<li class="">';
            if (menu1.items.length > 0) {
                retHTML += '    <a href="javascript:void(0);"><span class="title">' + menu1.title + '</span><span class="arrow"></span></a>';
            } else {
                retHTML += '    <a href="' + menu1.path + '">' + menu1.title + '</a>';
            }
            retHTML += '    <span class="icon-thumbnail">' + menu1.title.substring(0, 2) + '</span>';

            
            // retHTML += '        <a href="#">';
            // retHTML += '          <span class="title">' + menu1.title + '</span>';
            // if ( menu1.items.length != 0 ) {
            //     retHTML += '          <span class=" arrow"></span>';
            // }
            // retHTML += '        </a>';
            // retHTML += '        <span class="icon-thumbnail ">' + menu1.title.substring(0, 2) + '</span>';

            // 하위 메뉴가 있다면....
            if (menu1.items.length > 0) {
                retHTML += '        <ul class="sub-menu">';
                $(menu1.items).each(function (mIdx2, menu2) {

                    if (menu2.path.indexOf(MENU.currentFileName) >= 0) {
                        bfindCurPage = true;
                        MENU.sUiId = menu2.sUiId;
                        MENU.sMenuId = menu2.sMenuId;
                        MENU.pathInfo = menu1.title + ">>" + menu2.title;
                        MENU.currentPageTitle = menu2.title;
                    }

                    retHTML += '          <li mid="' + menu2.sMenuId + '">';
                    if (menu2.items.length > 0) {
                        retHTML += '            <a href="javascript:void(0);"><span class="title">' + menu2.title + '</span><span class="arrow"></span></a>';
                    } else {
                        retHTML += '            <a href="' + menu2.path + '">' + menu2.title + '</a>';
                    }
                    retHTML += '            <span class="icon-thumbnail">' + menu2.title.substring(0, 2) + '</span>';

                    // 하위 메뉴가 있다면....
                    if (menu2.items.length > 0) {
                        retHTML += '     <ul class="sub-menu">';

                        $(menu2.items).each(function (mIdx3, menu3) {

                            if (menu3.path.indexOf(MENU.currentFileName) >= 0) {
                                bfindCurPage = true;
                                MENU.sUiId = menu3.sUiId;
                                MENU.sMenuId = menu3.sMenuId;
                                MENU.pathInfo = menu1.title + ">>" + menu2.title + ">>" + menu3.title;
                                MENU.currentPageTitle = menu3.title;
                            }

                            retHTML += '        <li mid="' + menu3.sMenuId + '">';
                            retHTML += '            <a href="' + menu3.path + '">' + menu3.title + '</a>';
                            // retHTML += '            <a href="#" onClick="javascript:MENU.fnMoveScreen(\'' + menu3.sMenuId + '\')">' + menu3.title + '</a>';
                            retHTML += '            <span class="icon-thumbnail">' + menu3.title.substring(0, 2) + '</span>';
                            retHTML += '        </li>';
                        });
                        retHTML += '     </ul>';
                    }
                    retHTML += '          </li>';
                });
                retHTML += '        </ul>';
            }
            retHTML += '      </li>';
        });

        return retHTML;
    };


    /**
     * Deprecated
     */
    MENU.fnMoveScreen = function (menuID) {
        var url = MENU.getUrl(menuID);
        location.href = url;
    };

    /**
     * Deprecated
     */
    MENU.getUrl = function (menuID) {

        var retUrl = "";

        $.each(MENU.defaultMenu, function (idx, menu1) {

            // 하위 메뉴가 있다면....
            if (menu1.items.length > 0) {
                $.each(menu1.items, function (idx, menu2) {

                    // 하위 메뉴가 있다면....
                    if (menu2.items.length > 0) {
                        $.each(menu2.items, function (idx, menu3) {
                            // 메뉴아이디를 찾는다
                            if (menu3.sMenuId.indexOf(menuID) >= 0) {
                                retUrl = menu3.path;
                                return false;
                            }
                        });
                    } else {
                        // 메뉴아이디를 찾는다
                        if (menu2.sMenuId.indexOf(menuID) >= 0) {
                            retUrl = menu2.path;
                            return false;
                        }
                    }
                });
            } else {
                // 메뉴아이디를 찾는다
                if (menu1.sMenuId.indexOf(menuID) >= 0) {
                    retUrl = menu1.path;
                    return false;
                }
            }
        });

        return retUrl;
    };

    MENU.init();
    w.AIV_MENU = {
        currentPageId: MENU.currentPageId,
        sUiId: MENU.sUiId,
        sMenuId: MENU.sMenuId,
        currentPageTitle: MENU.currentPageTitle,
        pathInfo: MENU.pathInfo,
        menuHTML: MENU.menuHTML,
        sUiId: location.pathname.split('/').pop().split('.')[0]  // "/v1/sum/111.html" -> "111"
    };
}(window));