/**
 * 팝업 윈도우
 */
(function ($, W) {

    var me = W.AIV_WINDOW || {};

    /**
     * 이고 모델 불러오기
     */
    me.lgistEgoModelLoad = function (modelId) {
        AIV_COMM.ajax({
            method: "POST",
            url: AIV_COMM.HOST_URL + '/adm/3pl/model/dtls',
            data: modelId,
            async: false,
            success: function(responseData) {
                // 모델 정보 세팅
                me.lgistEgoModelSetting(responseData);
            }
        });
    }

    /**
     * 배송 모델 불러오기
     */
    me.lgistModelLoad = function (modelId) {
        AIV_COMM.ajax({
            method: "POST",
            url: AIV_COMM.HOST_URL + '/adm/3pl/model/dtls',
            data: modelId,
            async: false,
            success: function(responseData) {
                // 모델 정보 세팅
                me.lgistModelSetting(responseData);
            }
        });
    }

    // 이고 모델정보 화면에 입력
    me.lgistEgoModelSetting = function (model) {

        // 탭1) 거점위치 선택
        me.tab1.params.pointNm.kValue( model.pointNm );
        // 탭1) 선택노선정보 세팅
        var routes = []
        $(model.models).each(function(i, model){
            routes.push({text: model.routeNo});
        })
        me.tab1.params.lgistSelected.setData(routes);


        // 탭2) 분석대상 시작일자
        me.tab2.params.startDe.kValue( model.startDe );
        // 탭2) 분석대상 종료일자
        me.tab2.params.endDe.kValue( model.endDe );

        // 탭2) 요일 설정 입력
        me.tab2.params.day_use1.kValue( model.dayUse1 == "true" );
        me.tab2.params.day_use2.kValue( model.dayUse2 == "true" );
        me.tab2.params.day_use3.kValue( model.dayUse3 == "true" );
        me.tab2.params.day_use4.kValue( model.dayUse4 == "true" );
        me.tab2.params.day_use5.kValue( model.dayUse5 == "true" );
        me.tab2.params.day_use6.kValue( model.dayUse6 == "true" );
        me.tab2.params.day_use7.kValue( model.dayUse7 == "true" );
        // 탭2) 타겟공장 (선택사항)
        me.tab2.params.fctryTarget.kValue( model.fctryTarget );
        // 탭2) 거점명입력
        me.tab2.params.pointAddrNm.kValue();
        // 탭2) 거점명입력 (거점좌표,주소입력)
        me.tab2.params.pointLng.kValue();
        me.tab2.params.pointLat.kValue();
        me.tab2.params.pointAddr.kValue();

        // 탭3) 그리드 설정
        var divId = "aivWindowLgistEgoModelDtls";
        $('#'+divId).find('.nav-tabs a').eq(2).click(); // 3번탭을 눌러 그리드 생성
        me.tab3.grid.main.setData(model.products);      // 그리드 내 데이터 셋
        $('#'+divId).find('#tab3_grid').css({'height': '460px','overflow-y': 'auto'});  // css 조정
        $('#'+divId).find('.nav-tabs a').eq(0).click(); // 다시 처음 화면으로

        // 탭3) 모델명 설정
        me.tab3.params.modelNm.kValue(model.modelNm);
        
    }

    // 배송 모델정보 화면에 입력
    me.lgistModelSetting = function (model) {
        // 탭1) 등록 거점명 입력
        me.tab1.params.pointNm.kValue( model.pointNm );
        // 탭1) 등록 거점 위치로 마커 이동
        me.tab1.tmap.marker.removeAll();    // 기존 마커 삭제
        me.tab1.fn.addDragableMarker(model.lng, model.lat);
        // me.tab1.tmap.addDragableMarker(model);

        // 탭2) 회전율 설정
        me.tab2.params.idLgistRuleMstrFk.kValue(model.idLgistRuleMstrFk.id);
        me.tab2.params.idLgistRuleMstrFk.self.trigger('change');

        // 탭3) 요일 설정 입력
        me.tab3.params.day_config1.kValue( model.dayConfig1 );
        me.tab3.params.day_config2.kValue( model.dayConfig2 );
        me.tab3.params.day_config3.kValue( model.dayConfig3 );
        me.tab3.params.day_config4.kValue( model.dayConfig4 );
        me.tab3.params.day_config5.kValue( model.dayConfig5 );
        me.tab3.params.day_config6.kValue( model.dayConfig6 );
        me.tab3.params.day_config7.kValue( model.dayConfig7 );

        // 탭4) 분석유형 입력
        me.tab4.params.analsTy.kValue( model.analsTy );
        me.tab4.params.startDe.kValue( model.startDe );
        me.tab4.params.endDe.kValue( model.endDe );
        me.tab4.params.fctryCd.kValue( model.fctryCd );
        // me.tab4.params.caralcTy.kValue( model.caralcTy );
        me.tab4.params.day_use1.kValue( model.dayUse1 == "true" );
        me.tab4.params.day_use2.kValue( model.dayUse2 == "true" );
        me.tab4.params.day_use3.kValue( model.dayUse3 == "true" );
        me.tab4.params.day_use4.kValue( model.dayUse4 == "true" );
        me.tab4.params.day_use5.kValue( model.dayUse5 == "true" );
        me.tab4.params.day_use6.kValue( model.dayUse6 == "true" );
        me.tab4.params.day_use7.kValue( model.dayUse7 == "true" );
        
        me.tab4.params.lgistOptional.clear();
        me.tab4.params.lgistSelected.clear();

        var datas = [];
        $(model.models).each(function(i, models){
            datas.push({text: models.routeNo});
        })
        me.tab4.params.lgistSelected.setData(datas);
    }

    // 이고 모델 내 입력 아이템들의 enable/disable 상태를 결정한다
    me.lgistEgoModelEnable = function( isEnable ){

        // 탭1) 거점위치
        me.tab1.params.pointNm.data("kendoDropDownList").enable( isEnable );
        // 탭1) 배차유형
        me.tab1.params.caralcTy.data("kendoDropDownList").enable( isEnable );
        // 탭1) 선택노선정보
        $(me.tab1.params.lgistSelected.self.dataSource.view()).each(function(i, row){
            me.tab1.params.lgistSelected.self.enable( $('li[data-uid=' + row.uid + ']'), isEnable );
        })
        

        // 탭2) 분석대상 시작일자
        me.tab2.params.startDe.self.enable( isEnable );
        // 탭2) 분석대상 종료일자
        me.tab2.params.endDe.self.enable( isEnable );
        // 탭2) 요일설정
        me.tab2.params.day_use1.attr('disabled', !isEnable);
        me.tab2.params.day_use2.attr('disabled', !isEnable);
        me.tab2.params.day_use3.attr('disabled', !isEnable);
        me.tab2.params.day_use4.attr('disabled', !isEnable);
        me.tab2.params.day_use5.attr('disabled', !isEnable);
        me.tab2.params.day_use6.attr('disabled', !isEnable);
        me.tab2.params.day_use7.attr('disabled', !isEnable);

        // 탭2) 타겟공장 (선택사항)
        me.tab2.params.fctryTarget.self.enable( isEnable );
        // 탭2) 거점명
        me.tab2.params.pointAddrNm.self.enable( isEnable );
        
        // 탭2) 모델명
        me.tab3.params.modelNm.self.enable( isEnable );

        // 등록 버튼 제거
        $('#lgist_ego_model_save').remove();
    }

    // 배송 모델 내 입력 아이템들의 enable/disable 상태를 결정한다
    me.lgistModelEnable = function( isEnable ){
        // 탭1) 등록 거점명
        me.tab1.params.pointNm.self.enable( isEnable );
        // 탭1) 등록 거점 검색 주소
        me.tab1.params.adres.self.enable( isEnable );

        // 탭2) 회전율 설정 ajax 통신 등 시간이 필요하므로 약간의 인터벌 부여
        setTimeout(function(){
            me.tab2.params.idLgistRuleMstrFk.self.enable( isEnable );
        }, 1000)

        // 탭3) 요일 설정
        me.tab3.params.day_config1.self.enable( isEnable );
        me.tab3.params.day_config2.self.enable( isEnable );
        me.tab3.params.day_config3.self.enable( isEnable );
        me.tab3.params.day_config4.self.enable( isEnable );
        me.tab3.params.day_config5.self.enable( isEnable );
        me.tab3.params.day_config6.self.enable( isEnable );
        me.tab3.params.day_config7.self.enable( isEnable );

        // 탭4) 분석유형
        me.tab4.params.analsTy.self.enable( isEnable );
        me.tab4.params.startDe.self.enable( isEnable );
        me.tab4.params.endDe.self.enable( isEnable );
        me.tab4.params.fctryCd.self.enable( isEnable );
        // me.tab4.params.caralcTy.kValue( model.caralcTy );
        me.tab4.params.day_use1.attr('disabled', !isEnable);
        me.tab4.params.day_use2.attr('disabled', !isEnable);
        me.tab4.params.day_use3.attr('disabled', !isEnable);
        me.tab4.params.day_use4.attr('disabled', !isEnable);
        me.tab4.params.day_use5.attr('disabled', !isEnable);
        me.tab4.params.day_use6.attr('disabled', !isEnable);
        me.tab4.params.day_use7.attr('disabled', !isEnable);
        
        me.tab4.params.lgistOptional.self.enable( isEnable );
        me.tab4.params.lgistSelected.self.enable( isEnable );

        me.tab4.params.modelNm.self.enable( isEnable );

        // 등록 버튼 제거
        $('#lgist_model_save').remove();
    }

    /**
     * 이고 모델 입력 창 생성
     */
    me.lgistEgoModelCreate = function (params, disableFlag) {
        var wOptions = {    // kendo window options
            title: '이고 모델 정보 입력',
            width: 1200,
            height: 700
        };

        var divId = "aivWindowLgistEgoModelDtls";
        var $divId = $('#'+divId);

        
        var _content = '';
        _content += '<div>';
        _content +=     '<div class="row" style="margin: 0px;">';
        _content +=         '<div class="col-lg-12" id="' + divId + '">';
        _content +=         '</div>';
        _content +=     '</div>';
        _content += '</div>';
        
        
        wOptions.content = _content;
        window.lgistEgoModelWindow = me.init(wOptions).open();

        // window 내 HTML DOM 작성
        (function setContent(){
            var _content = '';
            _content += '<div class="lgist-model-load" onclick="AIV_WINDOW.lgistEgoModelLoad()">불러오기</div>';
            _content += '<ul class="nav nav-tabs nav-tabs-fillup hidden-sm-down" role="tablist" data-init-reponsive-tabs="dropdownfx">';
            _content +=     '<li class="nav-item">';
            _content +=         '<a class="active" data-toggle="tab" role="tab" data-target="#lgist_tab1" href="#" aria-expanded="true">1. 거점위치 선택 》</a>';
            _content +=     '</li>';
            _content +=     '<li class="nav-item">';
            _content +=         '<a href="#" data-toggle="tab" role="tab" data-target="#lgist_tab2" class="" aria-expanded="false">2. 분석범위 설정 》</a>';
            _content +=     '</li>';
            _content +=     '<li class="nav-item">';
            _content +=         '<a href="#" data-toggle="tab" role="tab" data-target="#lgist_tab3" class="" aria-expanded="false">3. 제품이관 설정 》</a>';
            _content +=     '</li>';
            _content += '</ul>';
            _content += '<div class="tab-content">';
    //      _content +=     '<!-- 탭1 -->';
            _content +=     '<div class="tab-pane active" id="lgist_tab1" aria-expanded="true">';
    //      _content +=     '  <!-- 조회영역 -->';
            _content +=         '<div id="search_area1" class="row aiv_search_area" style="visibility: visible;" >';
            _content +=             '<div class="col-lg-12">';
            _content +=                 '<div class="row">';
            _content +=                     '<div class="col-lg-2 col-sm-2 mb-2">';
            _content +=                         '<div class="lgist-table-title">거점선택</div>';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10 mb-2">';
            _content +=                         '<input id="pointNm" />';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=                 '<hr>';
            _content +=                 '<div class="row">';
            _content +=                     '<div class="col-lg-2 col-sm-2">';
            _content +=                         '<div class="lgist-table-title">배차유형</div>';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10">';
            _content +=                         '<input id="caralcTy" />';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=                 '<hr>';
            _content +=                 '<div class="row">';
            _content +=                     '<div class="col-lg-2 col-sm-2">';
            _content +=                         '<div class="lgist-table-title">노선</div>';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10">';
            _content +=                         '<label for="lgist_optional" style="width: 225px;">미선택 노선정보</label>';
            _content +=                         '<label for="lgist_selected">선택 노선정보</label>';
            _content +=                         '<br />';
            _content +=                         '<select id="lgist_optional" >';
            _content +=                         '</select>';
            _content +=                         '<select id="lgist_selected"></select>';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=             '</div>';
            _content +=         '</div>';
    //      _content +=         '<!-- /조회영역 -->';
            _content +=     '</div>';
    //      _content +=     '<!-- /탭1 -->';
    //      _content +=     '<!-- 탭2 -->';
            _content +=     '<div class="tab-pane" id="lgist_tab2" aria-expanded="false">';
    //      _content +=         '<!-- 조회영역 -->';
            _content +=         '<div id="search_area2" class="row aiv_search_area" style="visibility: visible;">';
            _content +=             '<div class="col-lg-12">';
            _content +=                 '<div class="row">';
            _content +=                     '<div class="col-lg-2 col-sm-2 mb-2">';
            _content +=                         '<div class="lgist-table-title">분석대상 시작일자</div>';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10 mb-2">';
            _content +=                         '<input id="startDe" />';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=                 '<hr>';
            _content +=                 '<div class="row">';
            _content +=                     '<div class="col-lg-2 col-sm-2 mb-2">';
            _content +=                         '<div class="lgist-table-title">분석대상 종료일자</div>';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10 mb-2">';
            _content +=                         '<input id="endDe" />';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=                 '<hr>';
            _content +=                 '<div class="row">';
            _content +=                     '<div class="col-lg-2 col-sm-2">';
            _content +=                         '<div class="lgist-table-title">요일</div>';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10">';
            _content +=                         '<input id="day_use1" />';
            _content +=                         '<input id="day_use2" />';
            _content +=                         '<input id="day_use3" />';
            _content +=                         '<input id="day_use4" />';
            _content +=                         '<input id="day_use5" />';
            _content +=                         '<input id="day_use6" />';
            _content +=                         '<input id="day_use7" />';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=                 '<hr>';
            _content +=                 '<div class="row">';
            _content +=                     '<div class="col-lg-2 col-sm-2">';
            _content +=                         '<div class="lgist-table-title">타겟공장 (선택사항)</div>';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10">';
            _content +=                         '<input id="fctryTarget" />';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=                 '<hr>';
            _content +=                 '<div class="row">';
            _content +=                     '<div class="col-lg-2 col-sm-2">';
            _content +=                         '<div class="lgist-table-title">거점명입력 (선택사항)</div>';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10">';
            _content +=                         '<input id="pointAddrNm" />';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-2 col-sm-2">';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10">';
            _content +=                         '<input id="pointAddrKeyword" />';
            // _content +=                         '<button id="pointAddrBtn" class="aiv_btn_1x k-button k-button-icontext" data-role="button" role="button" ></button>';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-2 col-sm-2">';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10">';
            _content +=                         '<input id="lng" style="width: 150px;" disabled  />';
            _content +=                         '<input id="lat" style="width: 150px;" disabled  />';
            _content +=                         '<input id="addr" style="width: 300px;" disabled />';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=                 '<hr>';
            _content +=                 '<div class="row">';
            _content +=                     '<div class="col-lg-2 col-sm-2" style="height: 290px;" >';
            _content +=                     '</div>';
            _content +=                     '<div class="col-lg-10 col-sm-10" style="width: 900px;height: 290px;" >';
            _content +=                         '<div id="lgistEgoTmap" style="width: 900px;"></div>';
            // _content +=                         '<div id="lgistEgoGmap" style="display: none;"></div>';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=             '</div>';
            _content +=         '</div>';
    //      _content +=         '<!-- /조회영역 -->';
            _content +=     '</div>';
    //      _content +=     '<!-- /탭2 -->';
    //      _content +=     '<!-- 탭3 -->';
            _content +=     '<div class="tab-pane" id="lgist_tab3" aria-expanded="false">';
    //      _content +=         '<!-- 조회영역 -->';
            _content +=         '<div id="search_area3" class="row aiv_search_area" style="visibility: visible;">';
            _content +=             '<div class="col-lg-12 mb-2">';
            _content +=                 '<div class="row mb-4">';
            _content +=                     '<div class="col-lg-12">';
            _content +=                         '<div id="tab3_grid"></div>';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=                 '<div class="row">';
            _content +=                     '<div class="col-lg-12" style="text-align: right;">';
            _content +=                         '<input id="model_nm" />';
            _content +=                         '<button id="lgist_ego_model_save" class="aiv_btn_1x k-primary k-button k-button-icontext" data-role="button" role="button"><span class="k-icon k-i-save"></span>최종 저장하기</button>';
            _content +=                     '</div>';
            _content +=                 '</div>';
            _content +=             '</div>';
            _content +=         '</div>';
    //      _content +=         '<!-- /조회영역 -->';
            _content +=     '</div>';
    //      _content +=     '<!-- /탭3 -->';
            _content += '</div>';

            $('#'+divId).html(_content);
        })();
        // 불러오기 버튼 css 조정
        
        // 불러오기 영억 바로 실행
        (function(){
    
            var options = {
                data: [],
                onChange: function(e){
                    if ( this.value() != '' ) {
                        me.lgistEgoModelLoad(this.value());
                    }
                }
            };

            me.lgstModelDropDownList = $('#'+divId).find('.lgist-model-load').AIV_DROPDOWNLIST({
                type: 1010,
                optionLabel: "이전 모델 불러오기",
                autoWidth: true,    // 펼쳤을 때 LIST 내 TEXT 길이에 따라 자동적으로 가로길이를 조정해줌
                onChange: options.onChange,
                index: 0, // index:0=default
                dataBound: function(e){
                    $(this.element).closest("span.k-dropdown").css({
                        'position': 'absolute',
                        'right': '20px',
                        'width': '180px',
                        'text-align': 'center',
                        'height': '28px',
                        'z-index': 999
                    });
                }
            });
    
        })();


        // tab1 content 생성
        function contentTab1(){
            // tab 객체 초기화
            me.tab1 = { fn:{}, params:{}, btn:{}, tmap:{} };

            // 노선정보 입력(분석유형, 시작일자, 종료일자, 공장, 배차유형, 요일 선택에 따라 새로 갱신)
            me.tab1.fn.setRouteNoList = function(){
                var params = {
                    pointNm: me.tab1.params.pointNm.kValue(),
                    fctryCd: me.tab1.params.pointNm.kValue(),
                    caralcTy: me.tab1.params.caralcTy.kValue()
                }

                var routeNoList = [];
                AIV_COMM.ajax({
                    method: "POST",
                    url: AIV_COMM.HOST_URL + '/adm/3pl/find/route/combo',
                    async: false,
                    data: params,
                    success: function(responseData) {
                        console.log(responseData);
                        $(responseData).each(function(i, data){
                            routeNoList.push({ text:data.key });
                        })
                    }
                });

                // 노선정보
                // $("#lgist_optional, #lgist_selected").empty();
                $("#lgist_optional").empty();
                me.tab1.params.lgistOptional.clear();
                // me.tab1.params.lgistSelected.clear();

                me.tab1.params.lgistOptional.setData(routeNoList);
            }


             /**
              * tab parameter 초기화
              */
             // 거점위치 선택
             me.tab1.params.pointNm = $('#' + divId).find('#pointNm').kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                autoWidth: true,
                optionLabel: '거점위치',
                dataSource: [{
                    text: '용인', value: '2D1'
                }],
                change: me.tab1.fn.setRouteNoList
            });

            // 배차유형
            me.tab1.params.caralcTy = $('#'+divId).find('#search_area1 #caralcTy').AIV_DROPDOWNLIST({ type: 1004, title: "", onChange: me.tab1.fn.setRouteNoList });

            me.tab1.params.lgistOptional = $('#'+divId).find("#lgist_optional").AIV_LISTBOX({
                connectWith: "lgist_selected",
                toolbar: {
                    tools: ["transferTo", "transferFrom", "transferAllTo", "transferAllFrom"]
                }
            });
            me.tab1.params.lgistSelected = $('#'+divId).find("#lgist_selected").AIV_LISTBOX();

        }
        contentTab1();
        

        // tab2 content생성
        function contentTab2(){
            // tab 객체 초기화
            me.tab2 = { fn:{}, params:{}, map:{}, btn:{}, grid:{} };
            // 분석대상 시작일자
            me.tab2.params.startDe = $('#'+divId).find('#search_area2 #startDe').AIV_DATETIMEPICKER({ type: 0, title: "", width: 150 });
            //분석대상 종료일자
            me.tab2.params.endDe = $('#'+divId).find('#search_area2 #endDe').AIV_DATETIMEPICKER({ type: 0, title: "", width: 150 });

            // 월요일~일요일
            me.tab2.params.day_use1 = $('#'+divId).find('#search_area2 #day_use1').AIV_CHECKBOX({ label: '월&nbsp;', checked: true, onChange: me.tab2.fn.setRouteNoList });
            me.tab2.params.day_use2 = $('#'+divId).find('#search_area2 #day_use2').AIV_CHECKBOX({ label: '화&nbsp;', checked: true, onChange: me.tab2.fn.setRouteNoList });
            me.tab2.params.day_use3 = $('#'+divId).find('#search_area2 #day_use3').AIV_CHECKBOX({ label: '수&nbsp;', checked: true, onChange: me.tab2.fn.setRouteNoList });
            me.tab2.params.day_use4 = $('#'+divId).find('#search_area2 #day_use4').AIV_CHECKBOX({ label: '목&nbsp;', checked: true, onChange: me.tab2.fn.setRouteNoList });
            me.tab2.params.day_use5 = $('#'+divId).find('#search_area2 #day_use5').AIV_CHECKBOX({ label: '금&nbsp;', checked: true, onChange: me.tab2.fn.setRouteNoList });
            me.tab2.params.day_use6 = $('#'+divId).find('#search_area2 #day_use6').AIV_CHECKBOX({ label: '토&nbsp;', checked: true, onChange: me.tab2.fn.setRouteNoList });
            me.tab2.params.day_use7 = $('#'+divId).find('#search_area2 #day_use7').AIV_CHECKBOX({ label: '일', checked: true, onChange: me.tab2.fn.setRouteNoList });

            me.tab2.params.fctryTarget = $('#'+divId).find('#search_area2 #fctryTarget').AIV_DROPDOWNLIST({ type: 1000, allYn: true });

            me.tab2.params.pointAddrNm = $('#'+divId).find('#search_area2 #pointAddrNm').AIV_TEXTBOX({ type: 'text', title: '거점명입력', width:200 });
            me.tab2.params.pointAddrKeyword = $('#'+divId).find('#search_area2 #pointAddrKeyword').AIV_TEXTBOX({ type: 'text', width:400, title: '검색 주소 입력'/*, onKeyup: function(){ me.tab2.btn.pointAddrBtn.click(); } */});
            me.tab2.params.pointLng = $('#'+divId).find('#search_area2 #lng').AIV_TEXTBOX({ type: 'text', title: '경도' });
            me.tab2.params.pointLat = $('#'+divId).find('#search_area2 #lat').AIV_TEXTBOX({ type: 'text', title: '위도' });
            me.tab2.params.pointAddr = $('#'+divId).find('#search_area2 #addr').AIV_TEXTBOX({ type: 'text', width: 400, title: '주소' });
            /*
            me.tab2.btn.pointAddrBtn = $('#'+divId).find('#search_area2 #pointAddrBtn').AIV_BUTTON({
                title: '주소 조회',
                class: 'k-primary',
                icon: 'search',
                onClick:function(){
                    var text = me.tab2.params.pointAddrKeyword.kValue();
                    me.tab2.map.lgistEgoTmap.fullAddrGeo({
                        search: {
                            addr: text
                        },
                        result: {
                            addrSelector: '#search_area2 #addr',
                            latSelector: '#search_area2 #lat',
                            lngSelector: '#search_area2 #lng'
                        }
                    })
                }
            });
            */
           /**
            * 주소검색 - Google Map 으로 변경하였음.
            */
            $('#lgistEgoGmap').AIV_GMAP().map.setAutoComplete({
                id:'pointAddrKeyword',  // input id
                success: function(options){ // callback function
                    options.lngSelector = '#search_area2 #lng';
                    options.latSelector = '#search_area2 #lat';
                    options.addrSelector = '#search_area2 #addr';
                    me.tab2.map.lgistEgoTmap.addDragableMarker(options);    // input 값 입력 및 마커 이동은 기존의 티맵 사용
                }
            })

            // 페이지 div width 동기화를 위해 클릭시 티맵을 그려줌(한번만)
            $('#'+divId).find('.nav-tabs a').eq(1).one('click', function(){
                setTimeout(function(){
                    me.tab2.map.lgistEgoTmap = $('#'+divId).find('#search_area2 #lgistEgoTmap').AIV_TMAP();
                }, 1);
            })
            // setTimeout(function(){
                // me.tab2.map.lgistEgoTmap = $('#'+divId).find('#search_area2 #lgistEgoTmap').AIV_TMAP();
            // }, 2000);
            
        }
        contentTab2();


        // tab3 content생성
        function contentTab3(){
            // tab 객체 초기화
            me.tab3 = { fn:{}, params:{}, grid:{} };
            // tab parameter 초기화


            me.tab3.grid.editor = function(container, options){
                $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:' + options.field + '" />')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        decimals: 2,
                        min: 0,
                        max: 1,
                        step: 0.01
                    });
            };



            $('#'+divId).find('.nav-tabs a').eq(2).on('click', function(){

                me.tab3.grid.main = $('#'+divId).find('#tab3_grid').AIV_GRID_TABLE({
                    // apiUrl: "/adm/3pl/find/product/item",           // CRUD의 기본 URL 작성
                    // getParams: me.tab3.fn.getParams,                           // 그리드 조회 시 실행될 파라미터 조회 함수 적용
                    height: '70%',
                    columns: [{
                        kField: 'itemNm',
                        editable: false
                    },{
                        kField: 'itemCd',
                        editable: false
                    },{
                        kField: 'packQty', width: 80,
                        editable: false,
                        // attributes: { 'class': 'text-center text-warning'}
                    // },{
                    //     kField: 'outturn',
                    //     editable: false
                    },{
                        title: '용인',
                        columns:[{
                            kField: 'p2', editor: me.tab3.grid.editor
                        }]
                    },{
                        title: '양주',
                        columns:[{
                            kField: 'p1', editor: me.tab3.grid.editor
                        }]
                    },{
                        title: '안산',
                        columns:[{
                            kField: 'p3', editor: me.tab3.grid.editor
                        }]
                    },{
                        title: '거창',
                        columns:[{
                            kField: 'p4', editor: me.tab3.grid.editor
                        }]
                    },{
                        title: '거점(출하량)',
                        field: 'point', width: 80
                        , template: function(dataItem){
                            // 거점명(거점선택), 주소(거점선택), 위도(거점선택), 경도(거점선택)
                            if ( me.tab2.params.pointAddrNm.kValue() != ''
                                && me.tab2.params.pointAddr.kValue() != ''
                                && me.tab2.params.pointLat.kValue() != ''
                                && me.tab2.params.pointLng.kValue() != '' ) {
                                return dataItem.packQty;
                            } else {
                                return '-';
                            }
                        }
                        , editable: function(){ return false; }
                    }],
                    editable: true,
                    dataBound: function(e){
                        var rows = e.sender.dataSource.view();
                        console.log('rows', rows)
                        $(rows).each(function(i, row){
                            var sum = Number(row.p1) + Number(row.p2) + Number(row.p3) + Number(row.p4);
                            if ( sum != 1 ) {
                                $('tr[data-uid=' + row.uid + ']').find('td:eq(2)').addClass('text-danger');
                            } else {
                                $('tr[data-uid=' + row.uid + ']').find('td:eq(2)').removeClass('text-danger');
                            }
                            e.sender.dataSource.view()[i].useYn = true;
                        })

                        $('#'+divId).find('#tab3_grid').css({'height': '460px','overflow-y': 'auto'});
                    },
                    change: function(e) {
                        var selectedRows = this.select();

                        var selectedDataItems = [];
                        for (var i = 0; i < selectedRows.length; i++) {
                            var dataItem = this.dataItem(selectedRows[i]);
                            var p1 = dataItem.p1;
                            var p2 = dataItem.p2;
                            var p3 = dataItem.p3;
                            var p4 = dataItem.p4;
                            var sum = Number(p1) + Number(p2) + Number(p3) + Number(p4);

                            // if ( sum != dataItem.boxUnstoringCnt ) {
                            if ( sum != 1 ) {
                                $(selectedRows[0]).find('td:eq(2)').addClass('text-danger');
                            } else {
                                $(selectedRows[0]).find('td:eq(2)').removeClass('text-danger');
                            }
                        //   selectedDataItems.push(dataItem);
                            $(selectedRows[i]).removeClass('k-state-selected');
                        }
                        // selectedDataItems contains all selected data items
                    },
                    height: '100%',
                    autoBind: false
                });

                var params = {
                    fromDe: me.tab2.params.startDe.kValue(),
                    toDe: me.tab2.params.endDe.kValue(),
                    fctryCd: me.tab1.params.pointNm.kValue(),
                    routeNos: me.tab1.params.lgistSelected.getAllValue()
                }

                // 입력이 아닌 상세보기에서는 AJAX 통신을 안하도록 플래그를 두어 처리함
                if(disableFlag != true) {
                    AIV_COMM.ajax({
                        method: "POST",
                        url: AIV_COMM.HOST_URL + '/adm/3pl/find/product/item',
                        async: false,
                        data: params,
                        success: function(responseData) {
                            var fctryTarget = me.tab2.params.fctryTarget.kValue();
                            if ( fctryTarget != '' ) {
                                $(responseData).each(function(i, obj){
                                    if ( fctryTarget == '1D1' ) {
                                        fctryTarget = 'p1';
                                    } else if ( fctryTarget == '2D1' ) {
                                        fctryTarget = 'p2';
                                    } else if ( fctryTarget == '3D1' ) {
                                        fctryTarget = 'p3';
                                    } else if ( fctryTarget == '4D1' ) {
                                        fctryTarget = 'p4';
                                    }
                                    responseData[i].p1 = 0;
                                    responseData[i].p2 = 0;
                                    responseData[i].p3 = 0;
                                    responseData[i].p4 = 0;
                                    responseData[i][fctryTarget] = 1;
                                });
                            }
                            me.tab3.grid.main.setData(responseData);
                        }
                    });
                }

            });



            // 모델명
            me.tab3.params.modelNm = $('#'+divId).find('#search_area3 #model_nm').AIV_TEXTBOX({ type: 'text', title: "모델 명", width: 300 });
            

            // 저장 버튼 클릭 시
            $('#lgist_ego_model_save').click(function(){
                var model = {};
                model.models = [];

                model.lgistTy = 'TRANSPORT';  // 이고 모델 입력

                model.pointNm = me.tab1.params.pointNm.kValue();        // 거점 선택
                model.fctryCd = me.tab1.params.pointNm.kValue();        // == 공장선택
                model.caralcTy = me.tab1.params.caralcTy.kValue();      // 배차유형
                model.routeNoList = [];                                 // 노선정보
                var routeNoList = me.tab1.params.lgistSelected.getAllValue();
                $( routeNoList ).each(function(i, routeNo){
                    model.models.push({
                        routeNo: routeNo
                    });
                });
                model.startDe = me.tab2.params.startDe.kValue();        // 분석대상 시작일자
                model.endDe = me.tab2.params.endDe.kValue();            // 분석대상 종료일자
                model.dayUse1 = ( me.tab2.params.day_use1.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 월요일   true || false
                model.dayUse2 = ( me.tab2.params.day_use2.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 화요일   true || false
                model.dayUse3 = ( me.tab2.params.day_use3.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 수요일   true || false
                model.dayUse4 = ( me.tab2.params.day_use4.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 목요일   true || false
                model.dayUse5 = ( me.tab2.params.day_use5.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 금요일   true || false
                model.dayUse6 = ( me.tab2.params.day_use6.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 토요일   true || false
                model.dayUse7 = ( me.tab2.params.day_use7.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 일요일   true || false
                model.fctryTarget = me.tab2.params.fctryTarget.kValue();    // 타겟공장
                model.pointAddrNm = me.tab2.params.pointAddrNm.kValue();        // 거점명(거점선택)
                model.addr = me.tab2.params.pointAddr.kValue();         // 주소(거점선택)
                model.lat = me.tab2.params.pointLat.kValue();           // 위도(거점선택)
                model.lng = me.tab2.params.pointLng.kValue();           // 경도(거점선택)
                // model.configList = me.tab3.grid.main.self.dataSource.view();
                model.products = me.tab3.grid.main.self.dataSource.view();   // 제품 출하량 그리드
                model.modelNm = me.tab3.params.modelNm.kValue();            // 모델명
                model.useYn = true; // 사용여부
                
                // validation check
                // if ( AIV_UTIL.isEmpty(model.pointNm) ) {
                //     $('#'+divId).find('.nav-tabs a').eq(0).tab('show');
                //     $(me.tab1.params.pointNm).focus();
                //     alert("거점명을 입력해 주세요.");
                //     return;
                // } else if ( AIV_UTIL.isEmpty( model.models ) ) {
                //     $('#'+divId).find('.nav-tabs a').eq(3).tab('show');
                //     $(me.tab2.params.idLgistRuleMstrFk).focus();
                //     alert("노선을 선택해 주세요.");
                //     return;
                // }


                // **INSERT MODEL**
                AIV_COMM.ajax({
                    method: "POST",
                    url: AIV_COMM.HOST_URL + '/adm/3pl/model/save',
                    data: [model],
                    success: function(responseData) {
                        alert('정상적으로 입력되었습니다.');
                        me.self.close();
                        location.reload();  // 메인화면의 dropdownlist 갱신 겸하여 새로고침
                    }
                });



            });

        }
        contentTab3();


        function setStyle(){
            $('#'+divId).find('.k-checkbox-label').css({'margin-right': '10px'})
            $('#'+divId).find('.lgist-table-title').css({
                'width': '100%', 'text-align': 'center', 'margin':'0px 6px 0px 0px', 'padding':'5px 15px', 'vertical-align':'top', 'font-size':'13px',
                'font-family':'Nanum Gothic', 'font-weight':'bold', 'background-color':'rgb(68, 74, 88)', 'color':'#ffffff', 'display':'inline-block'
            });
            // css 영역
            $('#'+divId).find('.lgist-table-title').css({
                'width': '100%', 'text-align': 'center', 'margin':'0px 6px 0px 0px', 'padding':'5px 15px', 'vertical-align':'top', 'font-size':'13px',
                'font-family':'Nanum Gothic', 'font-weight':'bold', 'background-color':'rgb(68, 74, 88)', 'color':'#ffffff', 'display':'inline-block'
            });
            $('#'+divId).find('label').css({ 'margin-bottom': '5px', 'font-weight': 'bold', 'display': 'inline-block' });
            $('#'+divId).find('.k-listbox').css({ 'width': '220px', 'height': '200px' })
            $('#'+divId).find('.k-listbox:first-of-type').css({ 'width': '220px', 'margin-right': '1px' })
            $('#'+divId).find('.k-listbox:last-of-type').css({ 'width': '180px', 'margin-left': '5px' })

            $('#'+divId).find('.k-textbox').css('border', '1px solid #ddd');
        }
        setStyle();

        AIV_WINDOW.lgistEgoModelCreate.me = me;
        return me;


        // tab1 content 생성
        // function contentTab1(){

    }

    /**
     * 배송 모델 입력 창 생성
     */
    me.lgistModelCreate = function (params) {

        var wOptions = {    // kendo window options
            title: '모델 정보 입력',
            width: 1200,
            height: 700
        };

        var divId = "aivWindowLgistModelDtls";
        var $divId = $('#'+divId);

        var _content = '';
        _content += '<div>';
        _content +=     '<div class="row" style="margin: 0px;">';
        _content +=         '<div class="col-lg-12" id="' + divId + '">';
        _content +=         '</div>';
        _content +=     '</div>';
        _content += '</div>';
        
        
        wOptions.content = _content;
        me.init(wOptions).open();
        
        
        _content = '';
        _content += '<div class="lgist-model-load" onclick="AIV_WINDOW.lgistModelLoad()">불러오기</div>';
        _content += '<ul class="nav nav-tabs nav-tabs-fillup hidden-sm-down" role="tablist" data-init-reponsive-tabs="dropdownfx">';
        _content +=     '<li class="nav-item">';
        _content +=         '<a class="active" data-toggle="tab" role="tab" data-target="#lgist_tab1" href="#" aria-expanded="true">1. 거점위치 선택 》</a>';
        _content +=     '</li>';
        _content +=     '<li class="nav-item">';
        _content +=         '<a href="#" data-toggle="tab" role="tab" data-target="#lgist_tab2" class="" aria-expanded="false">2. 회전율 설정 》</a>';
        _content +=     '</li>';
        _content +=     '<li class="nav-item">';
        _content +=         '<a href="#" data-toggle="tab" role="tab" data-target="#lgist_tab3" class="" aria-expanded="false">3. 요일 설정 》</a>';
        _content +=     '</li>';
        _content +=     '<li class="nav-item">';
        _content +=         '<a href="#" data-toggle="tab" role="tab" data-target="#lgist_tab4" class="" aria-expanded="false">4. 분석범위 설정</a>';
        _content +=     '</li>';
        _content += '</ul>';
        _content += '<div class="tab-content">';
//      _content +=     '<!-- 탭1 -->';
        _content +=     '<div class="tab-pane active" id="lgist_tab1" aria-expanded="true">';
//      _content +=     '  <!-- 조회영역 -->';
        _content +=         '<div id="search_area1" class="row aiv_search_area" >';
        _content +=             '<div class="col-lg-12 mb-2">';
        _content +=                 '<input id="pointNm" />';
        _content +=             '</div>';
        _content +=             '<div class="col-lg-12 mb-2">';
        _content +=                 '<input id="pointAdres" />';
        // _content +=                 '<button id="pointAdresSearch" class="aiv_btn_1x k-primary k-button k-button-icontext" data-role="button" role="button"><span class="k-icon k-i-search"></span>조회하기</button>';
        _content +=             '</div>';
        _content +=             '<div class="col-lg-12 mb-2">';
        _content +=                 '<input id="lng" disabled />';
        _content +=                 '<input id="lat" disabled />';
        _content +=                 '<input id="adres" disabled />';
        _content +=             '</div>';
        _content +=         '</div>';
//      _content +=         '<!-- /조회영역 -->';
//      _content +=         '<!-- 그리드영역 -->';
        _content +=         '<div class="row">';
        _content +=             '<div class="col-lg-12" style="height:420px; background-color: lightgray;">';
        _content +=                 '<div id="tab1_map"></div>';
        // _content +=                 '<div id="lgistEgoGmap"></div>';
        _content +=             '</div>';
        _content +=         '</div>';
//      _content +=         '<!-- /그리드영역 -->';
        _content +=     '</div>';
//      _content +=     '<!-- /탭1 -->';
//      _content +=     '<!-- 탭2 -->';
        _content +=     '<div class="tab-pane" id="lgist_tab2" aria-expanded="false">';
//      _content +=         '<!-- 조회영역 -->';
        _content +=         '<div id="search_area2" class="row aiv_search_area" style="visibility: visible;">';
        _content +=             '<div class="col-lg-2 mb-2">';
        _content +=                 '<div class="lgist-table-title">회전율 설정</div>';
        _content +=             '</div>';
        _content +=             '<div class="col-lg-10 mb-2">';
        _content +=                 '<input id="id_lgist_rule_mstr_fk" />';
        _content +=             '</div>';
        _content +=         '</div>';
//      _content +=         '<!-- /조회영역 -->';
//      _content +=         '<!-- 그리드영역 -->';
        _content +=         '<div class="row">';
        _content +=             '<div class="col-lg-12" style="height:520px;">';
        _content +=                 '<div id="tab2_grid"></div>';
        _content +=             '</div>';
        _content +=         '</div>';
//      _content +=         '<!-- /그리드영역 -->';
        _content +=     '</div>';
//      _content +=     '<!-- /탭2 -->';
//      _content +=     '<!-- 탭3 -->';
        _content +=     '<div class="tab-pane" id="lgist_tab3" aria-expanded="false">';
//      _content +=         '<!-- 조회영역 -->';
        _content +=         '<div id="search_area3" class="row aiv_search_area" style="visibility: visible;">';
        _content +=             '<div class="col-lg-12 mb-2">';
        _content +=                 '<input id="day_config1">';
        _content +=                 '<input id="day_config2">';
        _content +=                 '<input id="day_config3">';
        _content +=                 '<input id="day_config4">';
        _content +=                 '<input id="day_config5">';
        _content +=                 '<input id="day_config6">';
        _content +=                 '<input id="day_config7">';
        _content +=             '</div>';
        _content +=         '</div>';
//      _content +=         '<!-- /조회영역 -->';
        _content +=         '<div class="row mt-3">';
        _content +=             '<div class="col-lg-12" style="text-align: right;">';
        _content +=                 '※특정 요일의 물량을 다른 요일로 변경하여 물류비를 산출할 수 있습니다.';
        _content +=             '</div>';
        _content +=         '</div>';
        _content +=     '</div>';
//      _content +=     '<!-- /탭3 -->';
//      _content +=     '<!-- 탭4 -->';
        _content +=     '<div class="tab-pane" id="lgist_tab4" aria-expanded="false">';
        _content +=         '<div id="search_area4" class="row aiv_search_area" style="visibility: visible;">';
        _content +=             '<div class="col-lg-12">';
        _content +=                 '<div class="row">';
        _content +=                     '<div class="col-lg-2 col-sm-2">';
        _content +=                         '<div class="lgist-table-title">분석유형</div>';
        _content +=                     '</div>';
        _content +=                     '<div class="col-lg-10 col-sm-10">';
        _content +=                         '<input id="analsTy" />';
        _content +=                     '</div>';
        _content +=                 '</div>';
        _content +=                 '<hr>';
        _content +=                 '<div class="row">';
        _content +=                     '<div class="col-lg-2 col-sm-2">';
        _content +=                         '<div class="lgist-table-title">분석대상 시작일자</div>';
        _content +=                     '</div>';
        _content +=                     '<div class="col-lg-10 col-sm-10">';
        _content +=                         '<input id="startDe" />';
        _content +=                     '</div>';
        _content +=                 '</div>';
        _content +=                 '<hr>';
        _content +=                 '<div class="row">';
        _content +=                     '<div class="col-lg-2 col-sm-2">';
        _content +=                         '<div class="lgist-table-title">분석대상 종료일자</div>';
        _content +=                     '</div>';
        _content +=                     '<div class="col-lg-10 col-sm-10">';
        _content +=                         '<input id="endDe" />';
        _content +=                     '</div>';
        _content +=                 '</div>';
        _content +=                 '<hr>';
        _content +=                 '<div class="row">';
        _content +=                     '<div class="col-lg-2 col-sm-2">';
        _content +=                         '<div class="lgist-table-title">공장선택</div>';
        _content +=                     '</div>';
        _content +=                     '<div class="col-lg-10 col-sm-10">';
        _content +=                         '<input id="fctryCd" />';
        _content +=                     '</div>';
        _content +=                 '</div>';
        _content +=                 '<hr>';
        _content +=                 '<div class="row">';
        _content +=                     '<div class="col-lg-2 col-sm-2">';
        _content +=                         '<div class="lgist-table-title">배차유형</div>';
        _content +=                     '</div>';
        _content +=                     '<div class="col-lg-10 col-sm-10">';
        _content +=                         '<input id="caralcTy" />';
        _content +=                     '</div>';
        _content +=                 '</div>';
        _content +=                 '<hr>';
        _content +=                 '<div class="row">';
        _content +=                     '<div class="col-lg-2 col-sm-2">';
        _content +=                         '<div class="lgist-table-title">요일</div>';
        _content +=                     '</div>';
        _content +=                     '<div class="col-lg-10 col-sm-10">';
        _content +=                         '<input id="day_use1" />';
        _content +=                         '<input id="day_use2" />';
        _content +=                         '<input id="day_use3" />';
        _content +=                         '<input id="day_use4" />';
        _content +=                         '<input id="day_use5" />';
        _content +=                         '<input id="day_use6" />';
        _content +=                         '<input id="day_use7" />';
        _content +=                     '</div>';
        _content +=                 '</div>';
        _content +=                 '<hr>';
        _content +=                 '<div class="row">';
        _content +=                     '<div class="col-lg-2 col-sm-2">';
        _content +=                         '<div class="lgist-table-title">노선</div>';
        _content +=                     '</div>';
        _content +=                     '<div class="col-lg-10 col-sm-10">';
        _content +=                         '<label for="lgist_optional" style="width: 225px;">미선택 노선정보</label>';
        _content +=                         '<label for="lgist_selected">선택 노선정보</label>';
        _content +=                         '<br />';
        _content +=                         '<select id="lgist_optional" >';
        _content +=                         '</select>';
        _content +=                         '<select id="lgist_selected"></select>';
        _content +=                     '</div>';
        _content +=                 '</div>';
        _content +=                 '<hr>';
        _content +=                 '<div class="row">';
        _content +=                     '<div class="col-lg-12" style="text-align: right;">';
        _content +=                         '<input id="model_nm" />';
        _content +=                         '<button id="lgist_model_save" class="aiv_btn_1x k-primary k-button k-button-icontext" data-role="button" role="button"><span class="k-icon k-i-save"></span>최종 저장하기</button>';
        _content +=                     '</div>';
        _content +=                 '</div>';
        _content +=             '</div>';
        _content +=         '</div>';
        _content +=     '</div>';
//      _content +=     '<!-- /탭4 -->';
        _content += '</div>';

        $('#'+divId).html(_content);
        // 불러오기 버튼 css 조정
        
        // 불러오기 영억 바로 실행
        (function(){
    
            var options = {
                data: [],
                onChange: function(e){
                    if ( this.value() != '' ) {
                        me.lgistModelLoad(this.value());
                    }
                }
            };

            me.lgstModelDropDownList = $('#'+divId).find('.lgist-model-load').AIV_DROPDOWNLIST({
                type: 1010,
                optionLabel: "이전 모델 불러오기",
                autoWidth: true,    // 펼쳤을 때 LIST 내 TEXT 길이에 따라 자동적으로 가로길이를 조정해줌
                onChange: options.onChange,
                index: 0, // index:0=default
                dataBound: function(e){
                    $(this.element).closest("span.k-dropdown").css({
                        'position': 'absolute',
                        'right': '20px',
                        'width': '180px',
                        'text-align': 'center',
                        'height': '28px',
                        'z-index': 999
                    });
                }
            });
    
        })();


        // tab1 content 생성
        function contentTab1(){
            // tab 객체 초기화
            me.tab1 = { fn:{}, params:{}, btn:{}, tmap:{} };
             // tab parameter 초기화
            me.tab1.params = $('#aivWindowLgistModelDtls').find('#search_area1').AIV_SEARCH_INIT({
                onKeyup: function(){ me.tab1.btn.pointAddrBtn.click() }
            });
            // me.tab1.params = $('#aivWindowLgistModelDtls #search_area1').AIV_SEARCH_INIT();
            // me.tab1.params = $('#'+divId).find('#search_area1').AIV_SEARCH_INIT();

            
            // tab fn 초기화
            // 파라미터 getter fn
            me.tab1.fn.getParams = function () {
                var params = me.params.getAllValue();
                params.dlvyLcCd = dataItems.dlvyLcCd;
                params.dlvyDe = dataItems.dlvyDe;
                return params;
            }

            // 파라미터 setter fn
            me.tab1.fn.setParams = function (lon, lat, adres) {
                me.tab1.params.lng.kValue(lon);
                me.tab1.params.lat.kValue(lat);
                me.tab1.params.adres.kValue(adres);
            }

            // 거점주소 정보를 입력받아 검색
            /*
            me.tab1.btn.pointAddrBtn = $('#'+divId).find('#search_area1 #pointAdresSearch').AIV_BUTTON({
                title: '주소 조회',
                class: 'k-primary',
                icon: 'search',
                onClick:function(){
                    var text = me.tab1.params.pointAdres.kValue();
                    me.tab1.tmap.fullAddrGeo({
                        search: {
                            addr: text
                        },
                        result: {
                            addrSelector: '#search_area1 #adres',
                            latSelector: '#search_area1 #lat',
                            lngSelector: '#search_area1 #lng'
                        }
                    })
                }
            });
            */
            /**
             * 주소검색 - Google Map 으로 변경하였음.
             */
            $('#lgistEgoGmap').AIV_GMAP().map.setAutoComplete({
                id:'pointAdres',  // input id
                success: function(options){ // callback function
                    options.lngSelector = '#search_area1 #lng';
                    options.latSelector = '#search_area1 #lat';
                    options.addrSelector = '#search_area1 #adres';
                    me.tab1.tmap.addDragableMarker(options);    // input 값 입력 및 마커 이동은 기존의 티맵 사용
                }
            })



            // // 드래그 가능한 마커 및 Circle 생성
            me.tab1.fn.addDragableMarker = function (lon, lat) {

                // 맵 위치 설정
                me.tab1.tmap.map.setCenter(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), 17);

                // 상단 input 영역 값 조정
                me.tab1.params.lng.kValue(lon);
                me.tab1.params.lat.kValue(lat);

                // 주소 변환 후 상단 input에 등록
                var fn_success = function (adres) {
                    me.tab1.params.adres.kValue(adres);
                }
                me.tab1.tmap.reverseGeo(lon, lat, fn_success);

                // // circle 객체 생성 및 추가
                // me.tab1.tmap.circle.add({
                //     lon: lon,
                //     lat: lat,
                //     strokeWidth: 2
                // });

                // 지도 내 마커 추가
                me.tab1.tmap.marker.addDragable({ 
                    lon: lon,
                    lat: lat,
                    labelText: '원하시는 거점 위치로 마커를 이동 후,<br>↖ 좌측 상단의 거점명을 입력해주세요.',
                    event: {
                        onMouseover: function () {
                            this.popup.hide();
                        },
                        onMouseout: function () {
                            // this.popup.show();
                        },
                        onMousedown: function () {
                            me.tab1.tmap.map.ctrl_nav.dragPan.deactivate();//드래그 액션 비활성화
                            this.popup.hide();
                        },
                        onMouseup: function (e) {
                            this.popup.show();
                            me.tab1.tmap.map.ctrl_nav.dragPan.activate();//드래그 액션 활성화
                            me.tab1.tmap.markerLayer.removeMarker(this); // 기존 마커 삭제

                            setTimeout(function () {  // e 객체 내 xy 프로퍼티 생성에 시간이 걸리므로 약간의 timeout 부여
                                //클릭 부분의 ViewPortPx를 LonLat 좌표로 변환합니다.
                                var lonlat = me.tab1.tmap.map.getLonLatFromViewPortPx(e.xy).transform("EPSG:3857", "EPSG:4326");
                                // 마커 추가(위치만 변경시 Marker Label위치를 변경할 수 없기 때문에 제거 후 재생성 함)
                                me.tab1.fn.addDragableMarker(lonlat.lon, lonlat.lat);
                            }, 1)
                        }
                    }

                });
            }

            // 맵 생성
            me.tab1.tmap = $('#tab1_map').AIV_TMAP({
                search: {       // 검색영역 옵션 정의
                    url: '',
                    getParams: function () {
                        var params = me.tab1.fn.getParams();
                        return params;
                    },
                    after: function () {
                        // PAGE.fn.setStatusCnt();
                    }
                },
                marker: {       // 마커 옵션 정의
                    // marker-mouseover event 발생시 실행 함수
                    onMouseover: function (e) {
                        var marker = this;
                        marker.popup.show();
                    },
                    // marker-mouseout event 발생시 실행 함수
                    onMouseout: function (e) {
                        var marker = this;
                        marker.popup.hide();
                    },
                    // marker-click event 발생시 실행 함수
                    onClick: function (e) {
                        var marker = this;
                    }
                }
            });

            // 경로추적 검색
            // me.tab1.tmap.searchCoursTrace();

            // 이동 마커 생성
            // TODO: ajax call
            // me.tab1.fn.addDragableMarker(126.98, 37.57);
        }
        contentTab1();
        

        // tab2 content생성
        function contentTab2(){
            // tab 객체 초기화
            me.tab2 = { fn:{}, params:{}, grid:{} };
            // tab parameter 초기화
            me.tab2.params = $('#'+divId).find('#search_area2').AIV_SEARCH_INIT();

            (function createParams(){
                // id_lgist_rule_mstr_fk
                // rule_nm
                var options = {
                    data: [],
                    onChange: function(){
                        me.tab2.grid.reload();
                    }
                };

                AIV_COMM.ajax({
                    method: "POST",
                    url: AIV_COMM.HOST_URL + '/adm/3pl/find/rule/combo',
                    async: false,
                    success: function(responseData) {
                        $(responseData).each(function(i, data){
                            options.data.push({ text: data.value, value: data.key });
                        })

                        me.tab2.params.idLgistRuleMstrFk = $('#'+divId).find('#search_area2 #id_lgist_rule_mstr_fk').kendoDropDownList({
                            dataTextField: "text",
                            dataValueField: "value",
                            autoWidth: true,    // 펼쳤을 때 LIST 내 TEXT 길이에 따라 자동적으로 가로길이를 조정해줌
                            optionLabel: "선택해주세요.",
                            dataSource: options.data,
                            change: options.onChange,
                            index: 0, // index:0=default
                            dataBound: function(e){
                                $(this.element)
                                    .closest("span.k-dropdown")
                                    .css({width: '300px', 'font-size': '13px'});
                            }
                        });
                        me.tab2.params.idLgistRuleMstrFk.self = me.tab2.params.idLgistRuleMstrFk.data('kendoDropDownList');
                    }
                });
                
            })();   // 즉시 실행




            // tab fn 초기화
            me.tab2.fn.getParams = function() {
                return Number(me.tab2.params.getAllValue().idLgistRuleMstrFk);
            }
            // tab grid 초기화

            me.tab2.grid = $('#'+divId).find("#tab2_grid").AIV_GRID_TABLE({
                apiUrl: "/adm/3pl/rule/dtls",           // CRUD의 기본 URL 작성
                getParams: me.tab2.fn.getParams,                           // 그리드 조회 시 실행될 파라미터 조회 함수 적용
                columns: [{
                    title: '이동 거리', width: 200,
                    template: function(dataItems){
                        return dataItems.minDstnc + '~' + dataItems.maxDstnc + 'km';
                    }
                },{
                    title: '소요 시간', width: 200,
                    template: function(dataItems){
                        return dataItems.minTime + '~' + dataItems.maxTime + '시';
                    }
                },{
                    title: '배송지 수', width: 200,
                    template: function(dataItems){
                        return dataItems.minDlvyCnt + '~' + dataItems.maxDlvyCnt;
                    }
                },{
                    title: '회전율', width: 200,
                    template: function(dataItems){
                        return dataItems.rtateRate;
                    }
                }],
                autoBind: false
            });
        }
        contentTab2();


        // tab3 content생성
        function contentTab3(){
            // tab 객체 초기화
            me.tab3 = { fn:{}, params:{} };
            // tab parameter 초기화
            me.tab3.params.day_config1 = $('#'+divId).find('#day_config1').AIV_DROPDOWNLIST({ title: '월', type: 92, allYn: false });
            me.tab3.params.day_config2 = $('#'+divId).find('#day_config2').AIV_DROPDOWNLIST({ title: '화', type: 92, allYn: false });
            me.tab3.params.day_config3 = $('#'+divId).find('#day_config3').AIV_DROPDOWNLIST({ title: '수', type: 92, allYn: false });
            me.tab3.params.day_config4 = $('#'+divId).find('#day_config4').AIV_DROPDOWNLIST({ title: '목', type: 92, allYn: false });
            me.tab3.params.day_config5 = $('#'+divId).find('#day_config5').AIV_DROPDOWNLIST({ title: '금', type: 92, allYn: false });
            me.tab3.params.day_config6 = $('#'+divId).find('#day_config6').AIV_DROPDOWNLIST({ title: '토', type: 92, allYn: false });
            me.tab3.params.day_config7 = $('#'+divId).find('#day_config7').AIV_DROPDOWNLIST({ title: '일', type: 92, allYn: false });

            me.tab3.params.day_config1.kValue(1);
            me.tab3.params.day_config2.kValue(2);
            me.tab3.params.day_config3.kValue(3);
            me.tab3.params.day_config4.kValue(4);
            me.tab3.params.day_config5.kValue(5);
            me.tab3.params.day_config6.kValue(6);
            me.tab3.params.day_config7.kValue(7);
        }
        contentTab3();


        // tab4 content생성
        function contentTab4(){
            var _inputWidth = 150;
            // tab 객체 초기화
            me.tab4 = { fn:{}, params:{} };

            // 노선정보 입력(분석유형, 시작일자, 종료일자, 공장, 배차유형, 요일 선택에 따라 새로 갱신)
            // 노선정보 입력(분석유형, 시작일자, 종료일자, 공장, 배차유형, 요일 선택에 따라 새로 갱신)
            me.tab4.fn.setRouteNoList = function(){

                var params = {
                    fctryCd: me.tab4.params.fctryCd.kValue(),
                    caralcTy: me.tab4.params.caralcTy.kValue(),
                    fromDe: me.tab4.params.startDe.kValue(),
                    toDe: me.tab4.params.endDe.kValue()
                }
                console.log ( '-----------------------------------' );
                console.log ( 'me.tab4.fn.setRouteNoList()', params );
                console.log ( '-----------------------------------' );
                var datas = [];
                AIV_COMM.ajax({
                    method: "POST",
                    url: AIV_COMM.HOST_URL + '/adm/3pl/find/route/combo',
                    async: false,
                    data: params,
                    success: function(responseData) {
                        $(responseData).each(function(i, data){
                            datas.push({text:data.value});
                            // options.data.push({ text: data.value, value: data.key });
                        })
                        console.log('route/combo', 'callbak-function')
                    }
                });

                // TODO: remove
                // var routeNoList = [
                //     '11AA001', '11AA002', '11AA003', '11AA004', '11AA005', '11AA006', '11AA007', '11AA008', '11AA009', '11AA010',
                //     '11AA011', '11AA012', '11AA013', '11AA014', '11AA015', '11AA016', '11AA017', '11AA018', '11AA019', '11AA020',
                //     '11AA021', '11AA022', '11AA023', '11AA024', '11AA025', '11AA026', '11AA027', '11AA028', '11AA029', '11AA030'
                // ]
                
                // 노선정보
                $("#lgist_optional, #lgist_selected").empty();
                me.tab4.params.lgistOptional.clear();
                me.tab4.params.lgistSelected.clear();

                // var datas = [];
                // $(routeNoList).each(function(i, routeNo){
                //     if ( Math.random() < 0.3 ) {
                //         datas.push({text:routeNo});
                //     }
                // })
                me.tab4.params.lgistOptional.setData(datas);
            }

            // tab parameter 초기화
            // 분석유형
            me.tab4.params.analsTy = $('#'+divId).find('#analsTy').kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                autoWidth: true,
                optionLabel: false,
                dataSource: [
                    { text: '배송', value: 'dlvy' },
                    { text: '이고', value: 'ego' }
                ],
                // filter: options.filter,
                change: function(){
                    me.tab4.fn.setRouteNoList();
                },
                dataBound: function(e){
                    $(this.element)
                        .closest("span.k-dropdown")
                        .css({width: _inputWidth, 'font-size': '13px'});
                }
            });
            me.tab4.params.analsTy.self = me.tab4.params.analsTy.data("kendoDropDownList");


            // 분석대상 시작일자
            me.tab4.params.startDe = $('#'+divId).find('#search_area4 #startDe').AIV_DATETIMEPICKER({ type: 0, title: "", width: _inputWidth, change: me.tab4.fn.setRouteNoList });
            //분석대상 종료일자
            me.tab4.params.endDe = $('#'+divId).find('#search_area4 #endDe').AIV_DATETIMEPICKER({ type: 0, title: "", width: _inputWidth, change: me.tab4.fn.setRouteNoList });
            // 공장선택
            me.tab4.params.fctryCd = $('#'+divId).find('#search_area4 #fctryCd').AIV_DROPDOWNLIST({ type: 1000, title: "", onChange: me.tab4.fn.setRouteNoList });
            // 배차유형
            me.tab4.params.caralcTy = $('#'+divId).find('#search_area4 #caralcTy').AIV_DROPDOWNLIST({ type: 1004, title: "", onChange: me.tab4.fn.setRouteNoList });
            
            // 월요일~일요일
            me.tab4.params.day_use1 = $('#'+divId).find('#search_area4 #day_use1').AIV_CHECKBOX({ label: '월&nbsp;', checked: true, onChange: me.tab4.fn.setRouteNoList });
            me.tab4.params.day_use2 = $('#'+divId).find('#search_area4 #day_use2').AIV_CHECKBOX({ label: '화&nbsp;', checked: true, onChange: me.tab4.fn.setRouteNoList });
            me.tab4.params.day_use3 = $('#'+divId).find('#search_area4 #day_use3').AIV_CHECKBOX({ label: '수&nbsp;', checked: true, onChange: me.tab4.fn.setRouteNoList });
            me.tab4.params.day_use4 = $('#'+divId).find('#search_area4 #day_use4').AIV_CHECKBOX({ label: '목&nbsp;', checked: true, onChange: me.tab4.fn.setRouteNoList });
            me.tab4.params.day_use5 = $('#'+divId).find('#search_area4 #day_use5').AIV_CHECKBOX({ label: '금&nbsp;', checked: true, onChange: me.tab4.fn.setRouteNoList });
            me.tab4.params.day_use6 = $('#'+divId).find('#search_area4 #day_use6').AIV_CHECKBOX({ label: '토&nbsp;', checked: true, onChange: me.tab4.fn.setRouteNoList });
            me.tab4.params.day_use7 = $('#'+divId).find('#search_area4 #day_use7').AIV_CHECKBOX({ label: '일', checked: true, onChange: me.tab4.fn.setRouteNoList });
            // 모델명
            me.tab4.params.modelNm = $('#'+divId).find('#search_area4 #model_nm').AIV_TEXTBOX({ type: 'text', title: "모델 명", width: 300 });
            // me.tab4.params.routeNoList = $('#'+divId).find('#routeNoList').AIV_DROPDOWNLIST({ title: '월', type: 92, allYn: false })  // 노선
            
            

            me.tab4.params.lgistOptional = $('#'+divId).find("#lgist_optional").AIV_LISTBOX({
                connectWith: "lgist_selected",
                toolbar: {
                    tools: ["transferTo", "transferFrom", "transferAllTo", "transferAllFrom"]
                }
            });
            me.tab4.params.lgistSelected = $('#'+divId).find("#lgist_selected").AIV_LISTBOX();
            

            // 노선선택 최초 실행
            me.tab4.fn.setRouteNoList();


            $('#lgist_model_save').click(function(){
                var model = {};
                model.models = [];

                // 데이터 수집
                model.pointNm = me.tab1.params.pointNm.kValue();                // 거점명
                model.adres = me.tab1.params.adres.kValue();                    // 주소
                model.lat = me.tab1.params.lat.kValue();                        // 위도
                model.lng = me.tab1.params.lng.kValue();                        // 경도

                model.idLgistRuleMstrFk = { id: me.tab2.params.idLgistRuleMstrFk.kValue() } // 룰 마스터 id fk

                model.dayConfig1 = me.tab3.params.day_config1.kValue();         // 요일설정 - 월요일
                model.dayConfig2 = me.tab3.params.day_config2.kValue();         // 요일설정 - 화요일
                model.dayConfig3 = me.tab3.params.day_config3.kValue();         // 요일설정 - 수요일
                model.dayConfig4 = me.tab3.params.day_config4.kValue();         // 요일설정 - 목요일
                model.dayConfig5 = me.tab3.params.day_config5.kValue();         // 요일설정 - 금요일
                model.dayConfig6 = me.tab3.params.day_config6.kValue();         // 요일설정 - 토요일
                model.dayConfig7 = me.tab3.params.day_config7.kValue();         // 요일설정 - 일요일

                model.analsTy = me.tab4.params.analsTy.kValue();                // 분석유형
                model.startDe = me.tab4.params.startDe.kValue();                // 분석 시작 대상일
                model.endDe = me.tab4.params.endDe.kValue();                    // 분석 종료 대상일
                model.fctryCd = me.tab4.params.fctryCd.kValue();                // 공장코드
                model.caralcTy = me.tab4.params.caralcTy.kValue();              // 배차유형
                model.dayUse1 = ( me.tab4.params.day_use1.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 월요일   true || false
                model.dayUse2 = ( me.tab4.params.day_use2.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 화요일   true || false
                model.dayUse3 = ( me.tab4.params.day_use3.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 수요일   true || false
                model.dayUse4 = ( me.tab4.params.day_use4.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 목요일   true || false
                model.dayUse5 = ( me.tab4.params.day_use5.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 금요일   true || false
                model.dayUse6 = ( me.tab4.params.day_use6.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 토요일   true || false
                model.dayUse7 = ( me.tab4.params.day_use7.kValue() == 'Y' ? 'true' : 'false' );    // 요일 사용여부 - 일요일   true || false
                model.modelNm = me.tab4.params.modelNm.kValue();
                model.matrixes = [];
                model.useYn = true;
                model.sttus = 0;

                var routeNoList = me.tab4.params.lgistSelected.getAllValue();

                $( routeNoList ).each(function(i, routeNo){
                    model.models.push({
                        // idLgistRuleMstrFk: me.tab2.params.idLgistRuleMstrFk.kValue(),
                        routeNo: routeNo
                    });
                })

                
                // validation check
                if ( AIV_UTIL.isEmpty(model.pointNm) ) {
                    $('#'+divId).find('.nav-tabs a').eq(0).tab('show');
                    $(me.tab1.params.pointNm).focus();
                    alert("거점명을 입력해 주세요.");
                    return;
                } else if ( AIV_UTIL.isEmpty(model.idLgistRuleMstrFk ) ) {
                    $('#'+divId).find('.nav-tabs a').eq(1).tab('show');
                    $(me.tab2.params.idLgistRuleMstrFk).focus();
                    alert("회전율 룰셋을 선택해 주세요.");
                    return;
                } else if ( AIV_UTIL.isEmpty( model.models ) ) {
                    $('#'+divId).find('.nav-tabs a').eq(3).tab('show');
                    $(me.tab2.params.idLgistRuleMstrFk).focus();
                    alert("노선을 선택해 주세요.");
                    return;
                }


                // **INSERT MODEL**
                AIV_COMM.ajax({
                    method: "POST",
                    url: AIV_COMM.HOST_URL + '/adm/3pl/model/save',
                    data: [model],
                    success: function(responseData) {
                        alert('정상적으로 입력되었습니다.');
                        me.self.close();
                        location.reload();  // 메인화면의 dropdownlist 갱신 겸하여 새로고침
                    }
                });



            });
            
            
            // css 영역
            $('#'+divId).find('.k-checkbox-label').css({'margin-right': '10px'})
            $('#'+divId).find('.lgist-table-title').css({
                'width': '100%', 'text-align': 'center', 'margin':'0px 6px 0px 0px', 'padding':'5px 15px', 'vertical-align':'top', 'font-size':'13px',
                'font-family':'Nanum Gothic', 'font-weight':'bold', 'background-color':'rgb(68, 74, 88)', 'color':'#ffffff', 'display':'inline-block'
            });
            $('#'+divId).find('label').css({ 'margin-bottom': '5px', 'font-weight': 'bold', 'display': 'inline-block' });
            $('#'+divId).find('.k-listbox').css({ 'width': '220px', 'height': '200px' })
            $('#'+divId).find('.k-listbox:first-of-type').css({ 'width': '220px', 'margin-right': '1px' })
            $('#'+divId).find('.k-listbox:last-of-type').css({ 'width': '180px', 'margin-left': '5px' })
        }
        contentTab4();


        AIV_WINDOW.lgistModelCreate.me = me;
        return me;
    }

    /**
     * 이고 모델정보 확인 창 생성
     * @param {*} modelId 
     */
    me.lgistEgoModelView = function (modelId) {
        // 1. 이고 모델 입력창 생성
        me.lgistEgoModelCreate({}, true);
        // 2. 이고 모델불러오기 적용
        me.lgistEgoModelLoad(modelId);
        // 3. 이고 모델 수정불가 적용
        me.lgistEgoModelEnable(false);
    }

    /**
     * 배송 모델정보 확인 창 생성
     * @param {*} modelId 
     */
    me.lgistModelView = function (modelId) {
        // 1. 배송 모델 입력창 생성
        me.lgistModelCreate();
        // 2. 배송 모델불러오기 적용
        me.lgistModelLoad(modelId);
        // 3. 배송 모델 수정불가 적용
        me.lgistModelEnable(false);
    }


    /**
     * 룰셋정보 입력 window
     */
    me.lgistRuleCreate = function () {

        var wOptions = {    // kendo window options
            title: '룰 정보 입력',
            width: 1200,
            height: 700
        };

        var divId = "aivWindowLgistRuleDtls";
        var $divId = $('#'+divId);

        var _content = '';
        _content += '<div>';
        _content +=     '<div class="row" style="margin: 0px;">';
        _content +=         '<div class="col-lg-12" id="' + divId + '">';
        _content +=         '</div>';
        _content +=     '</div>';
        _content += '</div>';
        
        
        wOptions.content = _content;
        me.init(wOptions).open();
        
        
        _content = '';
//      _content += '<!-- 조회영역 -->';
        _content += '<div id="search_area" class="row aiv_search_area" style="visibility: visible;">';
        _content +=     '<div class="col-lg-12 mb-2">';
        _content +=         '<input id="ruleNmCreate" />';
        _content +=     '</div>';
        _content += '</div>';
//      _content += '<!-- /조회영역 -->';
//      _content += '<!-- 그리드영역 -->';
        _content += '<div class="row">';
        _content +=     '<div class="col-lg-12" style="height:520px;">';
        _content +=         '<div id="grid"></div>';
        _content +=     '</div>';
        _content += '</div>';
//      _content += '<!-- /그리드영역 -->';

        $('#'+divId).html(_content);

        // tab 객체 초기화
        me.content = { fn:{}, params:{}, grid:{} };
        // tab parameter 초기화
        me.content.params.ruleNm = $('#'+divId).find('#ruleNmCreate').AIV_TEXTBOX({ type: 'text', title: "룰셋 명" });

        var digitEditor = function(field, step) {
            var editor = function(container, options) {
                $('<input data-bind="value:' + field + '" data-step="' + step + '"/>')
                    .appendTo(container)
                    .kendoNumericTextBox({});
                }
            return editor;
        }

        // tab grid 초기화
        me.content.grid = $('#'+divId).find("#grid").AIV_GRID({
            toolbar: [
                'add', 'lgist-rule-save', 'cancel'    // 행 추가, 룰 저장, 행 삭제
            ],
            // apiUrl: "/datas/dummy",           // CRUD의 기본 URL 작성
            getParams: function(){ return {} },                           // 그리드 조회 시 실행될 파라미터 조회 함수 적용
            columns: [{
                kField: "selectable", editable: false
            },{
                title: '최소거리<br>(km)', width: 100, field: 'minDstnc', editor: digitEditor('minDstnc', 1)
            },{
                title: '최대거리<br>(km)', width: 100, field: 'maxDstnc', editor: digitEditor('maxDstnc', 1)
            },{
                title: '최소시간<br>(시)', width: 100, field: 'minTime', editor: digitEditor('minTime', 1)
            },{
                title: '최대시간<br>(시)', width: 100, field: 'maxTime', editor: digitEditor('maxTime', 1)
            },{
                title: '최소착지<br>(수)', width: 100, field: 'minDlvyCnt', editor: digitEditor('minDlvyCnt', 1)
            },{
                title: '최대착지<br>(수)', width: 100, field: 'maxDlvyCnt', editor: digitEditor('maxDlvyCnt', 1)
            },{
                title: '회전율', width: 100, field: 'rtateRate', editor: digitEditor('rtateRate', 0.01)
            }],
            editable: true,

        });

        AIV_WINDOW.lgistRuleCreate.me = me;

        return me;
    }

    /**
     * 룰셋정보 조회 window
     * @param {*} ruleId 
     * @param {*} ruleNm 
     */
    me.lgistRuleView = function (ruleId, ruleNm){
        var wOptions = {    // kendo window options
            title: '룰셋 정보 조회',
            width: 1200,
            height: 700
        };

        var divId = "aivWindowLgistRuleDtls";
        var $divId = $('#'+divId);

        var _content = '';
        _content += '<div>';
        _content +=     '<div class="row" style="margin: 0px;">';
        _content +=         '<div class="col-lg-12" id="' + divId + '">';
        _content +=         '</div>';
        _content +=     '</div>';
        _content += '</div>';
        
        wOptions.content = _content;
        me.init(wOptions).open();
        
        
        _content = '';
//      _content += '<!-- 조회영역 -->';
        _content += '<div id="search_area" class="row aiv_search_area" style="visibility: visible;">';
        _content +=     '<div class="col-lg-12 mb-2">';
        _content +=         '<input id="ruleNmCreate" />';
        _content +=     '</div>';
        _content += '</div>';
//      _content += '<!-- /조회영역 -->';
//      _content += '<!-- 그리드영역 -->';
        _content += '<div class="row">';
        _content +=     '<div class="col-lg-12" style="height:520px;">';
        _content +=         '<div id="grid"></div>';
        _content +=     '</div>';
        _content += '</div>';
//      _content += '<!-- /그리드영역 -->';

        $('#'+divId).html(_content);

        // tab 객체 초기화
        me.content = { fn:{}, params:{}, grid:{} };
        // tab parameter 초기화
        me.content.params.ruleNm = $('#'+divId).find('#ruleNmCreate').AIV_TEXTBOX({ type: 'text', title: "룰셋 명" });
        me.content.params.ruleNm.kValue(ruleNm)
        $(me.content.params.ruleNm).attr('disabled', true);


        // tab grid 초기화
        me.content.grid = $('#'+divId).find("#grid").AIV_GRID_TABLE({
            apiUrl: "/adm/3pl/rule/dtls",           // CRUD의 기본 URL 작성
            getParams: function() { return Number(ruleId); },                           // 그리드 조회 시 실행될 파라미터 조회 함수 적용
            columns: [{
                title: '이동 거리', width: 200,
                template: function(dataItems){
                    return dataItems.minDstnc + '~' + dataItems.maxDstnc + 'km';
                }
            },{
                title: '소요 시간', width: 200,
                template: function(dataItems){
                    return dataItems.minTime + '~' + dataItems.maxTime + '시';
                }
            },{
                title: '배송지 수', width: 200,
                template: function(dataItems){
                    return dataItems.minDlvyCnt + '~' + dataItems.maxDlvyCnt;
                }
            },{
                title: '회전율', width: 200,
                template: function(dataItems){
                    return dataItems.rtateRate;
                }
            }],
            autoBind: true
        });

        AIV_WINDOW.lgistRuleView.me = me;


    }


    /**
     * 운영 > 노선궤적조회
     * @param {id} 저장된 궤적정보의 row id (T_ROUTE_PATH_MATRIX6)
     * @param {id} : ex) 2754
     * @param {routeNo} : ex) "22AA003"
     * @param {vrn} : ex) "경기91자5081"
     * @param {dlvyDe} : ex) "20170301"
     * @param {startPos} : ex) "거점1"
     * @param {endPos} : ex) "거점1"
     * @param {tmsDistance} : ex) 521
     * @param {newDistance} : ex) 485
     * @param {tmsTollCost} : ex) 38800
     * @param {newTollCost} : ex) 36000
     */
    me.routePathInfo = function (dataItem) {
        me.fn = {};
        me.params = {};
        me.btn = {};
        me.tmap = {};

        var wOptions = {    // kendo window options
            title: '노선 궤적 조회',
            width: 1200,
            height: 700
        };

        var divId = "aivWindowLgistModelDtls";
        var $divId = $('#'+divId);

        var _content = '';
        _content += '<div>';
        _content +=     '<div class="row" style="margin: 0px;">';
        _content +=         '<div class="col-lg-12" id="' + divId + '" style="height: 550px;">';
        _content +=             '<div id="routePathInfoWindow" style="width: 100%;height: 100%;" >';
        _content +=                 '<h5 id="routePathInfoSmry" style="text-align: center;font-weight: bold;background-color: rgb(238,238,238);"></h5>';
        _content +=                 '<div id="routePathInfoMap" style="height: 450px;"></div>';
        _content +=             '</div>';
        _content +=         '</div>';
        _content +=     '</div>';
        _content += '</div>';
        
        wOptions.content = _content;
        me.init(wOptions).open();
        

        // 맵 생성
        // me.tmap = $('#' + windowTemplateDiv).AIV_TMAP({});
        me.tmap = $('#routePathInfoMap').AIV_TMAP({});

        // return;
        // me.tmap.map.setCenter(new Tmap.LonLat(lastData.lon, lastData.lat).transform("EPSG:4326", "EPSG:3857"), 15);

        AIV_COMM.ajax({
            method: 'POST',
            async: false,
            url: AIV_COMM.HOST_URL + '/adm/3pl/matrix/select',
            data: {
                matrixId: dataItem.id
            },
            success: function(responseData){
                var jsonData = JSON.parse(responseData.jsonData);
                try {
                    var linkUrl = 'http://sdctms.seoulmilk.co.kr/repRouteMap2.aspx?'
                        + '&SR_CENTERCODE=' + dataItem.routeNo.substr(0,1) + 'D1'
                        + '&SR_DELDATE=' + dataItem.dlvyDe
                        + '&SR_BATCHNO=' + dataItem.routeNo.substr(1,2)
                        + '&SR_CARNO=' + dataItem.routeNo
                        + '&SR_GUBUN=UriClick'
                        + '&USER_ID=215071'
                    var total = jsonData.features[0].properties;
                    var bDistance = "총 거리 : " + (dataItem.tmsDistance).toFixed(1) + "km / ";
                    var bFare = " 총 요금 : " + dataItem.tmsTollCost + "원";
                    var linkBtn = " <button><a href='" + linkUrl + "' target='_blank' >TMS 노선 보기</a></button>";

    
                    var tDistance = "총 거리 : " + (total.totalDistance/1000).toFixed(1) + "km / ";
                    var tTime = " 총 시간 : " + (total.totalTime/60).toFixed(0) + "분 / ";	
                    var tFare = " 총 요금 : " + total.totalFare + "원";	
                    $("#routePathInfoSmry").html(dataItem.dlvyDe + ' / ' + dataItem.routeNo +'<br>(TMS)'+bDistance + bFare + linkBtn + '<br>(변경)' + tDistance + tTime + tFare); 
                } catch (error) {
                    console.error(error);
                }


                var markerList = [];
                var linearPointList = [];

                // 마커 데이터 정리
                $(responseData.tmsPlan).each(function(i, plan){
                    var gpsx = plan.lng;
                    var gpsy = plan.lat;
                    
                    // var idx = i;
                    var idx = (markerList.length);
                    var size = new Tmap.Size(24, 38);//아이콘 크기 설정
                    var offset = new Tmap.Pixel(-(size.w / 2), -size.h);//아이콘 중심점 설정
                    var labelText = plan.dlvyLoNm + '.<br>(' + plan.addr + ')';
                    var label = new Tmap.Label('<div><ul><li>' + labelText + '</li></ul></div>');
                    switch (idx) {
                        case 10: idx = 'a'; break;
                        case 11: idx = 'b'; break; case 12: idx = 'c'; break; case 13: idx = 'd'; break; case 14: idx = 'e'; break; case 15: idx = 'f'; break;
                        case 16: idx = 'g'; break; case 17: idx = 'h'; break; case 18: idx = 'i'; break; case 19: idx = 'j'; break; case 20: idx = 'k'; break;
                        case 21: idx = 'l'; break; case 22: idx = 'm'; break; case 23: idx = 'n'; break; case 24: idx = 'o'; break; case 25: idx = 'p'; break;
                        case 26: idx = 'q'; break; case 27: idx = 'r'; break; case 28: idx = 's'; break; case 29: idx = 't'; break; case 30: idx = 'u'; break;
                        case 31: idx = 'v'; break; case 32: idx = 'w'; break; case 33: idx = 'x'; break; case 34: idx = 'y'; break; case 35: idx = 'z'; break;
                    }
                    var icon = new Tmap.IconHtml('<img src=http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_' + idx + '.png />', size, offset);
                    // if(i==0 ) {
                    //     icon = new Tmap.IconHtml('<img src=http://tmapapis.sktelecom.com/upload/tmap/marker/pin_r_m_a.png />', size, offset);
                    // }
                    var marker = new Tmap.Markers(new Tmap.LonLat(gpsx, gpsy).transform("EPSG:4326", "EPSG:3857"), icon, label);
                    marker.events.register("mouseover", marker, function(){
                        this.popup.show();
                    });
                    marker.events.register("mouseout", marker, function(){
                        this.popup.hide();
                    })
                    markerList.push(marker);
                    
                });

                
                // 라인 데이터  정리
                $(jsonData.features).each(function(i, feature){
                    var geometry = feature.geometry;
                    var properties = feature.properties;

                    if ( geometry.type == 'LineString' ) {
                        if ( properties.description != '경유지와 연결된 가상의 라인입니다') {
                            $(geometry.coordinates).each(function(j, coordinate){
                                var gpsx = coordinate[0];
                                var gpsy = coordinate[1];
                                // linearPointList.push(new Tmap.Geometry.Point(gpsx, gpsy).transform("EPSG:4326", "EPSG:3857"));
                                linearPointList.push(new Tmap.Geometry.Point(gpsx, gpsy));
                            })
                        }
                    }

                })

                drawMarker(dataItem);
                drawLine();
                
                
                // 마커 그리기
                function drawMarker(dataItem){
                    var fctryCd = dataItem.routeNo.substr(0,1); // 노선정보의 시작 숫자를 근거로 공장선택

                    var size = new Tmap.Size(24, 38);//아이콘 크기 설정
                    var offset = new Tmap.Pixel(-(size.w / 2), -size.h);//아이콘 중심점 설정
                    var icon = new Tmap.IconHtml('<img src=http://tmapapis.sktelecom.com/upload/tmap/marker/pin_g_m_f.png />', size, offset);
                    var label = null;
                    var fctryLon = '';
                    var fctryLat = '';
                    if ( fctryCd == '1' ) {
                        fctryLon = 127.059763030966;
                        fctryLat = 37.8205560353999;
                        label = new Tmap.Label('<div><ul><li>양주공장</li></ul></div>');
                    } else if ( fctryCd == '2' ) {
                        fctryLon = 127.111362237;
                        fctryLat = 37.296220411;
                        label = new Tmap.Label('<div><ul><li>용인공장</li></ul></div>');
                    } else if ( fctryCd == '3' ) {
                        fctryLon = 126.757383;
                        fctryLat = 37.322633;
                        label = new Tmap.Label('<div><ul><li>안산공장</li></ul></div>');
                    } else if ( fctryCd == '4' ) {
                        fctryLon = 127.926573029565;
                        fctryLat = 35.6706711519532;
                        label = new Tmap.Label('<div><ul><li>거창공장</li></ul></div>');
                    }
                    var marker_f = new Tmap.Markers(new Tmap.LonLat(fctryLon, fctryLat).transform("EPSG:4326", "EPSG:3857"), icon, label);//설정한 좌표를 "EPSG:3857"로 좌표변환한 좌표값으로 설정합니다.
                    marker_f.events.register("mouseover", marker_f, function(){
                        this.popup.show();
                    });
                    marker_f.events.register("mouseout", marker_f, function(){
                        this.popup.hide();
                    })
                    me.tmap.markerLayer.addMarker(marker_f); // 예전 공장 위치 마커 추가
                    
                    
                    // 이동할 거점 마커 생성
                    var moveLon = dataItem.startLng;
                    var moveLat = dataItem.startLat;
                    var moveLabel = new Tmap.Label('<div><ul><li>' + dataItem.startPos + '</li></ul></div>');
                    var moveIcon = new Tmap.IconHtml('<img src=http://tmapapis.sktelecom.com/upload/tmap/marker/pin_g_m_p.png />', size, offset);
                    
                    

                    var marker_m = new Tmap.Markers(new Tmap.LonLat(moveLon, moveLat).transform("EPSG:4326", "EPSG:3857"), moveIcon, moveLabel);//설정한 좌표를 "EPSG:3857"로 좌표변환한 좌표값으로 설정합니다.
                    marker_m.events.register("mouseover", marker_m, function(){
                        this.popup.show();
                    });
                    marker_m.events.register("mouseout", marker_m, function(){
                        this.popup.hide();
                    })
                    me.tmap.markerLayer.addMarker(marker_m); // 거점 위치 마커 추가


                    $(markerList).each(function(i, marker){
                        if ( i==0 || i==markerList.length-1 ) { // 처음과 마지막 마커는 패스
                        } else {
                            me.tmap.markerLayer.addMarker(marker); // 경유지 위치 마커 추가
                        }
                    })

                    me.tmap.map.setCenter(new Tmap.LonLat(fctryLon, fctryLat).transform("EPSG:4326", "EPSG:3857"), 9);
                    // var icon = new Tmap.IconHtml('<img src=http://tmapapis.sktelecom.com/upload/tmap/marker/pin_r_m_e.png />', size, offset);
                }
                
                // 라인 그리기
                function drawLine(){
                    var lineString = new Tmap.Geometry.LineString(linearPointList); // 라인 스트링 생성
                    var lineCollection = new Tmap.Geometry.Collection(lineString);
                    var style_red = {
                        strokeColor: "#FF0000",
                        strokeWidth: 2,
                        strokeDashstyle: "solid",
                    };
                    //vector feature 객체화
                    var lineFeature = new Tmap.Feature.Vector(lineCollection, null, style_red);
                    
                    //벡터 레이어에 등록
                    me.tmap.vectorLayer.addFeatures(lineFeature);
                }

                
            }
        })
    }
})(jQuery, window);