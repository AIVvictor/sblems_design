/**
 * T-Map
 */
(function ($) {


    // 지도 세팅
    $.fn.AIV_TMAP = function (options) {
        var me = this;
        var id = me.attr('id');

        // TMAP 전역객체
        var map;

        // MAP 구조 초기화
        options = options || {};
        me.appKey = 'ea8ff587-6ade-4287-88af-7ad7ac2d60aa'; // T-MAP appkey
        me.map = {};            // 지도 관련 객체 및 함수모음
        me.markerLayer = {};    // 마커레이어 관련 객체 및 함수모음
        me.marker = {};         // 마커 관련 객체 및 함수모음
        me.marker.event = {};   // 마커 이벤트 관련 객체 및 함수모음
        me.vectorLayer = {};    // 벡터레이어 관련 객체 및 함수모음
        // me.point = {};         // 포인터 관련 객체 및 함수모음
        me.line = {};         // 라인 관련 객체 및 함수모음
        me.circle = {};         // 원 관련 객체 및 함수모음


        // 맵 생성
        me.map = new Tmap.Map({
            div: id,
            width: '100%',
            height: '100%'
        });

        // service 관련 초기화
        initService();
        // vector 관련 초기화
        initVector();
        // marker 관련 초기화
        initMarker();
        return me;




        /**
         * Web Service 관련 함수 정의
         * > reverseGeo
         */

        // 참고: http://tmapapi.sktelecom.com/main.html#webservice/sample/WebSampleReverseGeocoding
        function initService() {
            // Reverse GeoCoding
            me.reverseGeo = function (lon, lat, fn_success) {
                $.ajax({
                    method: "GET",
                    url: "https://api2.sktelecom.com/tmap/geo/reversegeocoding?version=1&format=json&callback=result", // ReverseGeocoding api 요청 url입니다.
                    async: false,
                    data: {
                        "coordType": "WGS84GEO", //지구 위의 위치를 나타내는 좌표 타입
                        "addressType": "A10",  // 주소 타입 입니다.
                        "lon": lon,
                        "lat": lat,
                        "appKey": me.appKey, //실행을 위한 키 입니다. 발급받으신 AppKey를 입력하세요.
                    },
                    //데이터 로드가 성공적으로 완료되었을 때 발생하는 함수입니다.
                    success: function (response) {
                        var adres = response.addressInfo.fullAddress.split(',').pop();
                        fn_success(adres);
                    },
                    //요청 실패시 콘솔창에서 에러 내용을 확인할 수 있습니다.
                    error: function (request, status, error) {
                        console.log("code:" + request.status + "\n" + "message:" + request.responseText + "\n" + "error:" + error);
                    }
                });
            }

            // 주소로 좌표 검색
            me.fullAddrGeo = function (option) {
                var addrSelector = option.result.addrSelector;
                var latSelector = option.result.latSelector;
                var lngSelector = option.result.lngSelector;
                $.ajax({
                    method:"GET",
                    url:"https://api2.sktelecom.com/tmap/geo/fullAddrGeo?version=1&format=json&callback=result", //FullTextGeocoding api 요청 url입니다.
                    async: false,
                    data:{
                        "coordType" : "WGS84GEO", //지구 위의 위치를 나타내는 좌표 타입입니다.
                        "fullAddr" : option.search.addr, //주소 정보 입니다, 도로명 주소 표준 표기 방법을 지원합니다.  
                        "page" : "1", //페이지 번호 입니다.
                        "count" : "20", //페이지당 출력 갯수 입니다.
                        "appKey" : me.appKey, //실행을 위한 키 입니다. 발급받으신 AppKey를 입력하세요.
                    },
                    //데이터 로드가 성공적으로 완료되었을 때 발생하는 함수입니다.
                    success:function(response){
                        var coordinateInfo = response.coordinateInfo;

                        //검색 결과 정보가 없을 때 처리
                        if ( coordinateInfo == undefined || coordinateInfo.coordinate==undefined || coordinateInfo.coordinate.length == 0 ) {
                            //예외처리를 위한 파싱 데이터
                            error = response.error;
                            // 주소가 올바르지 않을 경우 예외처리
                            alert("code: " + error.code +"\nid: " + error.id + "\nmessage: " + error.message);
                            return;
                        }
                        
                        var coord = coordinateInfo.coordinate[0];
                        // 3. 마커 찍기
                        var lon, lat;
                        if (coord.lon != '') { // 구주소
                            lon = coord.lon;
                            lat = coord.lat;
                        } else { // 신주소
                            lon = coord.newLon;
                            lat = coord.newLat;
                        }
                        $(lngSelector).val(lon);
                        $(latSelector).val(lat);
                        
                        // var size = new Tmap.Size(24, 38); // 아이콘 크기 설정
                        // var offset = new Tmap.Pixel(-(size.w / 2), -size.h); // 아이콘 중심점 설정
                        // var icon = new Tmap.IconHtml("<img src='http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_m_p.png' />", size, offset); // 마커 아이콘 설정
                        // var marker_s = new Tmap.Marker(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), icon); // 설정한 좌표를 "EPSG:3857"로 좌표변환한 좌표값으로 설정합니다.
                        
                        // me.marker.removeAll();    // 기존 마커 삭제
                        // me.markerLayer.addMarker(marker_s); // 마커 레이어에 마커 추가
                        // me.map.setCenter(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), 15); // 설정한 좌표를 "EPSG:3857"로 좌표변환한 좌표값으로 즁심점을 설정합니다.
                        


                        // 드래그 가능한 마커 및 Circle 생성
                        function addDragableMarker (lon, lat) {

                            me.marker.removeAll();  // 기존 마커 삭제
                            // 맵 위치 설정
                            me.map.setCenter(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), 15);

                            // 지도 내 마커 추가
                            me.marker.addDragable({
                                lon: lon,
                                lat: lat,
                                labelText: '원하시는 거점 위치로 마커를 이동 후,<br>↖ 좌측 상단의 거점명을 입력해주세요.',
                                event: {
                                    onMouseover: function () {
                                        this.popup.hide();
                                    },
                                    onMouseout: function () {
                                        this.popup.show();
                                    },
                                    onMousedown: function () {
                                        me.map.ctrl_nav.dragPan.deactivate();//드래그 액션 비활성화
                                        this.popup.hide();
                                    },
                                    onMouseup: function (e) {
                                        this.popup.show();
                                        me.map.ctrl_nav.dragPan.activate();//드래그 액션 활성화
                                        me.markerLayer.removeMarker(this); // 기존 마커 삭제

                                        setTimeout(function () {  // e 객체 내 xy 프로퍼티 생성에 시간이 걸리므로 약간의 timeout 부여
                                            //클릭 부분의 ViewPortPx를 LonLat 좌표로 변환합니다.
                                            var lonlat = me.map.getLonLatFromViewPortPx(e.xy).transform("EPSG:3857", "EPSG:4326");
                                            // 마커 추가(위치만 변경시 Marker Label위치를 변경할 수 없기 때문에 제거 후 재생성 함)
                                            addDragableMarker(lonlat.lon, lonlat.lat);

                                            $(lngSelector).val(lon);
                                            $(latSelector).val(lat);
                                            me.reverseGeo(lonlat.lon, lonlat.lat, function(address){
                                                $(addrSelector).val(address);
                                            });
                                        }, 1)
                                    }
                                }

                            });
                        }
                        addDragableMarker(lon, lat);





                        // 검색 결과 표출
                        var matchFlag, newMatchFlag;
                        //검색 결과 주소를 담을 변수
                        var address = '', newAddress = '';
                        // var city, gu_gun, eup_myun, legalDong, adminDong, ri, bunji;
                        // var buildingName, buildingDong, newRoadName, newBuildingIndex, newBuildingName, newBuildingDong;
                        //새주소일 때 검색 결과 표출
                        //새주소인 경우 newMatchFlag가 응닶값으로 온다
                        if(coord.newMatchFlag != ''){
                            // 새(도로명) 주소 좌표 매칭 구분 코드
                            newMatchFlag = coord.newMatchFlag;
                            
                            if (coord.city_do != '')          { newAddress += " " + coord.city_do; }          // 시/도 명칭
                            if (coord.gu_gun != '')           { newAddress += " " + coord.gu_gun; }           // 군/구 명칭
                            if (coord.eup_myun != '')         { newAddress += " " + coord.eup_myun; }         // 읍면동 명칭
                            if (coord.legalDong != '')        { newAddress += " " + coord.legalDong; }        // 출력 좌표에 해당하는 법정동 명칭
                            if (coord.adminDong != '')        { newAddress += " " + coord.adminDong; }        // 출력 좌표에 해당하는 법정동 명칭
                            if (coord.ri != '')               { newAddress += " " + coord.ri; }               // 출력 좌표에 해당하는 리 명칭
                            if (coord.bunji != '')            { newAddress += " " + coord.bunji; }            // 출력 좌표에 해당하는 지번 명칭
                            if (coord.newRoadName != '')      { newAddress += " " + coord.newRoadName; }      // 새(도로명) 주소 매칭을 한 경우, 길 이름을 반환
                            if (coord.newBuildingIndex != '') { newAddress += " " + coord.newBuildingIndex; } // 새(도로명) 주소 매칭을 한 경우, 건물 번호를 반환
                            if (coord.newBuildingName != '')  { newAddress += " " + coord.newBuildingName; }  // 새(도로명) 주소 건물명 매칭을 한 경우, 건물 이름을 반환
                            if (coord.newBuildingDong != '')  { newAddress += " " + coord.newBuildingDong; }  // 새주소 건물을 매칭한 경우 새주소 건물 동을 반환
                            
                            // 검색 결과 표출
                            $(addrSelector).val(newAddress);
                        }
                        
                        //구주소일 때 검색 결과 표출
                        //구주소인 경우 MatchFlag가 응닶값으로 온다
                        if(coord.matchFlag != ''){
                            // 매칭 구분 코드
                            matchFlag = coord.matchFlag;
                            if (coord.city_do != '')      { address += " " + coord.city_do; }      // 시/도 명칭
                            if (coord.gu_gun != '')       { address += " " + coord.gu_gun; }       // 군/구 명칭
                            if (coord.eup_myun != '')     { address += " " + coord.eup_myun; }     // 읍면동 명칭
                            if (coord.legalDong != '')    { address += " " + coord.legalDong; }    // 출력 좌표에 해당하는 법정동 명칭
                            if (coord.adminDong != '')    { address += " " + coord.adminDong; }    // 출력 좌표에 해당하는 법정동 명칭
                            if (coord.ri != '')           { address += " " + coord.ri; }           // 출력 좌표에 해당하는 리 명칭
                            if (coord.bunji != '')        { address += " " + coord.bunji; }        // 출력 좌표에 해당하는 지번 명칭
                            if (coord.buildingName != '') { address += " " + coord.buildingName; } // 출력 좌표에 해당하는 지번 명칭
                            if (coord.buildingDong != '') { address += " " + coord.buildingDong; } // 출력 좌표에 해당하는 지번 명칭
                            
                            // 검색 결과 표출
                            $(addrSelector).val(address);
                        }
                    },
                    //요청 실패시 콘솔창에서 에러 내용을 확인할 수 있습니다.
                    error:function(request,status,error){
                        // console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
                        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
                    }
                });
            }

            // 드래그 가능한 마커 및 Circle 생성
            me.addDragableMarker = function(options) {
                var address = options.address;
                var lon = options.lng;
                var lat = options.lat;
                var addrSelector = options.addrSelector;
                var lngSelector = options.lngSelector;
                var latSelector = options.latSelector;

                if ( addrSelector != undefined ) {
                    $(addrSelector).val(address);
                }
                if ( lngSelector != undefined ) {
                    $(lngSelector).val(lon);
                }
                if ( latSelector != undefined ) {
                    $(latSelector).val(lat);
                }

                me.marker.removeAll();  // 기존 마커 삭제
                // 맵 위치 설정
                me.map.setCenter(new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857"), 15);

                // 지도 내 마커 추가
                me.marker.addDragable({
                    lon: lon,
                    lat: lat,
                    labelText: '원하시는 거점 위치로 마커를 이동 후,<br>↖ 좌측 상단의 거점명을 입력해주세요.',
                    event: {
                        onMouseover: function () {
                            this.popup.hide();
                        },
                        onMouseout: function () {
                            // this.popup.show();
                        },
                        onMousedown: function () {
                            me.map.ctrl_nav.dragPan.deactivate();//드래그 액션 비활성화
                            this.popup.hide();
                        },
                        onMouseup: function (e) {
                            this.popup.show();
                            me.map.ctrl_nav.dragPan.activate();//드래그 액션 활성화
                            me.markerLayer.removeMarker(this); // 기존 마커 삭제

                            setTimeout(function () {  // e 객체 내 xy 프로퍼티 생성에 시간이 걸리므로 약간의 timeout 부여
                                //클릭 부분의 ViewPortPx를 LonLat 좌표로 변환합니다.
                                var lonlat = me.map.getLonLatFromViewPortPx(e.xy).transform("EPSG:3857", "EPSG:4326");
                                // 마커 추가(위치만 변경시 Marker Label위치를 변경할 수 없기 때문에 제거 후 재생성 함)
                                options.lng = lonlat.lon;
                                options.lat = lonlat.lat;

                                me.addDragableMarker(options);

                                $(lngSelector).val(lonlat.lon);
                                $(latSelector).val(lonlat.lat);
                                me.reverseGeo(lonlat.lon, lonlat.lat, function(address){
                                    $(addrSelector).val(address);
                                });
                            }, 1)
                        }
                    }

                });
            }
        }






        /********************************
         * Vector 관련 함수 정의
         * > me.vectorLayer
         * 
         * > me.searchCoursTrace
         * > me.line.addList
         * > me.line.add
         * > me.removeFeature
         * > me.removeAllFeatures
         * 
         * > me.circle.add
         ********************************/
        function initVector() {
            // 벡터 레이어 생성
            me.vectorLayer = new Tmap.Layer.Vector("Tmap_Line_Vector");
            // 벡터 레이어 추가
            me.map.addLayer(me.vectorLayer);

            // 검색영역 조건에 맞춰 경로추적 리스트 검색
            me.searchCoursTrace = function () {

                // 파라미터 조회 함수 존재시 참조하여 조회 실행
                var params = (options.search.getParams && options.search.getParams()) || {};
                var url = options.search.url;
                kendo.ui.progress(me.parent(), true);
                window.ddd1 = me;
                console.log(me)

                // 차량 기본 정보 조회
                AIV_COMM.ajax({
                    method: "POST",
                    url: url,
                    data: params,
                    success: function(responseData) {
                        kendo.ui.progress(me.parent(), false);
                        window.ddd2 = me;
                        console.log(me)
                        var datas = [];
                        $(responseData).each(function(i, data){
                            var adres = data.adres;
                            adres = adres.split(' ');
                            adres.shift();
                            adres = adres.join(' ');    // '대한민국' 문구 제거

                            datas.push({
                                icon: '',
                                lon: data.lng,
                                lat: data.lat,
                                vrn: data.vrn,
                                driverNm: data.driverCd,
                                adres: data.adres,
                                state: '',
                                // state: '상차[상차종료]',
                                az: data.dgree,   // 방위각
                                dataDt: (new Date(data.dataDate)).format('yyyy-mm-dd hh:mi:ss')
                            });
                        })
                        
                        
                        // 마커 전체 생성
                        me.marker.addRouteList(datas);
                        // 라인 전체 생성
                        me.line.addList(datas);
                        // me.map.setCenter(getLonlat(126.986072, 37.570028), 15);
                        options.search.after(datas);
    
                    }
                })


                // //TODO: ajax
                // var text = 'helloworld';
                // for (var i = 0; i < 100; i++) {
                //     var time = 360;
                //     time += i * 5;
                //     var hour = Math.floor(time / 60) < 10 ? '0' + Math.floor(time / 60) : Math.floor(time / 60);
                //     var min = Math.floor(time % 60) < 10 ? '0' + Math.floor(time % 60) : Math.floor(time % 60);
                //     var dataDt = (new Date('2018-07-19T' + hour + ':' + min)).format('yyyy-mm-dd hh:mi:ss');
                //     datas.push({
                //         icon: text[i % 10],
                //         lon: 126.986072 - 0.003 + 0.001 * i,
                //         lat: 37.57002 + Math.random() / 100 / 2 - Math.random() / 100 / 2,
                //         vrn: '강원80바100' + i,
                //         driverNm: '김기사' + i,
                //         adres: '서울시 강남구 ' + i + '번지',
                //         state: '상차[상차종료]',
                //         az: 15 * i,   // 방위각
                //         dataDt: dataDt,
                //         adres: '경기도 양주시 회천' + i + '동'
                //     })
                // }


            }

            // 라인 객체 - 리스트 생성 및 추가
            me.line.addList = function (datas) {
                // 라인 전체 삭제
                me.removeAllFeatures();

                //polyline 좌표 배열
                var pointList = [];

                $(datas).each(function (i, data) {
                    pointList.push(me.line.add(data));
                });

                //좌표 배열 객체화
                var lineString = new Tmap.Geometry.LineString(pointList);

                //polyline(Collection) 객체 정의
                var lineCollection = new Tmap.Geometry.Collection(lineString);

                var style_red = {
                    strokeColor: "#FF0000",
                    strokeWidth: 2,
                    strokeDashstyle: "solid",

                    // 이하 적용 안됨
                    pointRadius: 60,
                    title: "this is a red line",
                    // externalGraphic: '/ui/comm/img/truck_icon_gr.png',
                    externalGraphic: 'http://topopen.tmap.co.kr/imgs/point.png',
                    graphicWidth: 20,
                    graphicHeight: 20,
                    graphicXOffset: 20,
                    graphicYOffset: 20,
                    graphicOpacity: 1,
                    rotation: 45
                };

                //vector feature 객체화
                var lineFeature = new Tmap.Feature.Vector(lineCollection, null, style_red);

                //벡터 레이어에 등록
                me.vectorLayer.addFeatures(lineFeature);
            }

            // 라인 객체 - 생성 및 추가
            me.line.add = function (data) {
                var pointer = getPointer(data.lon, data.lat);   // 포인터 생성
                pointer.data = data;     // 마커 객체 내 해당 마커 데이터 저장
                // me.markerLayer.addMarker(marker); //마커 레이어에 마커 추가
                // $(marker.icon.imageDiv).css('cursor', 'pointer')    // 마커 DOM의 css 변경

                return pointer;
            }

            // 원 객체 - 생성 및 추가
            me.circle.add = function (datas) {
                me.circle.removeAllFeatures();
                // if ( AIV_UTIL.isNull( me.circleLayer ) ) {
                //     // 벡터 레이어 추가
                //     me.circleLayer = new Tmap.Layer.Vector("Tmap_Circle_Vector");
                //     me.map.addLayer(me.circleLayer);
                // } else {
                //     // 원 객체 삭제
                //     me.circleLayer.removeAllFeatures();
                // }

                var coord = getLonlat(datas.lon, datas.lat);
                var circle = new Tmap.Geometry.Circle(coord.lon, coord.lat, 50, { unit: "m" }); // 원 생성

                // 지도상에 그려질 스타일을 설정합니다
                var style_red = {
                    fillColor: "#FF0000",
                    fillOpacity: 0.2,
                    strokeColor: "#FF0000",
                    strokeWidth: datas.strokeWidth || 3,
                    strokeDashstyle: "dash",
                    pointRadius: 30,
                    type: 'circle'
                };

                var circleFeature = new Tmap.Feature.Vector(circle, null, style_red); // 원 백터 생성
                // me.circleLayer.addFeatures([circleFeature]); // 원 백터 를 백터 레이어에 추가
                me.vectorLayer.addFeatures([circleFeature]); // 원 백터 를 백터 레이어에 추가
            }

            // 벡터 객체 단일 삭제
            me.circle.removeAllFeatures = function () {
                $(me.vectorLayer.features).each(function (i, f) {
                    if (f.style.type == 'circle') {
                        me.removeFeature(f);
                    }
                });
            }

            // 벡터 객체 단일 삭제
            me.removeFeature = function (f) {
                me.vectorLayer.removeFeatures([f]);
            }

            // 벡터 객체 전체 삭제
            me.removeAllFeatures = function () {
                me.vectorLayer.removeAllFeatures();
            }
        }


















        /*********************
         * Marker 관련 객체 및 함수 정의
         * > me.markerLayer
         * 
         * 
         * > me.searchMarkers
         * > me.marker.addList
         * > me.marker.add
         * > me.marker.addDragable
         * > me.marker.getLabel
         * 
         * > me.searchRoutes
         * > me.marker.addRouteList
         * > me.marker.addRoute
         * > me.marker.getLabelRoute
         * 
         * > me.marker.showPopup
         * > me.marker.getMarkerByData
         * > me.marker.removeAll
         * > me.marker.event.onMouseover
         * > me.marker.event.onMouseout
         * > me.marker.event.onClick
         *********************/
        function initMarker() {

            // 마커 레이어 생성
            me.markerLayer = new Tmap.Layer.Markers();
            // 마커 레이어 추가
            me.map.addLayer(me.markerLayer);

            // 검색영역 조건에 맞춰 마커 리스트 검색
            me.searchMarkers = function () {

                // 파라미터 조회 함수 존재시 참조하여 조회 실행
                var params = (options.search.getParams && options.search.getParams()) || {};
                var url = options.search.url;

                AIV_COMM.ajax({
                    method: "POST",
                    url: url,
                    data: params,
                    success: function(datas){
                        $(datas).each(function(i, data){
                            data.lon = data.lng;
                            if ( data.lng != null ) { /* console.log(data) */ }
                        })
                        // 마커 전체 생성
                        me.marker.addList(datas);
                        // me.map.setCenter(getLonlat(126.986072, 37.570028), 15);
                        if ( options.search && typeof options.search.after == 'function' ) {
                            options.search.after(datas);
                        }
                    }
                });
                // //TODO: ajax
                // var text = 'helloworld';
                // var datas = [];
                // for (var i = 0; i < text.length; i++) {
                //     datas.push({
                //         icon: text[i],
                //         lon: 126.986072 - 0.003 + 0.001 * i,
                //         lat: 37.57002,
                //         vrn: '강원80바100' + i,
                //         driverNm: '김기사' + i,
                //         adres: '서울시 강남구 ' + i + '번지',
                //         state: '상차[상차종료]',
                //         dataDt: (new Date()).format('yyyy-mm-ddThh:mi:ss')
                //     })
                // }
            }

            // 마커 객체 - 리스트 생성 및 추가
            me.marker.addList = function (datas) {
                // 마커 전체 삭제
                me.marker.removeAll();
                $(datas).each(function (i, data) {
                    me.marker.add(data);
                })
            }

            // 마커 객체 - 생성 및 추가
            me.marker.add = function (data) {
                var size = new Tmap.Size(25, 30);//아이콘 크기
                var offset = new Tmap.Pixel(-(size.w / 2), -(size.h));//아이콘 중심점
                var icon = new Tmap.Icon('/ui/comm/img/truck_icon_gr.png', size, offset);//아이콘 설정
                var lonlat = getLonlat(data.lon, data.lat);//좌표값
                var label = me.marker.getLabel(data) //라벨 생성

                var marker = new Tmap.Markers(lonlat, icon, label);//마커 생성
                marker.events.register("mouseover", marker, me.marker.event.onMouseover);
                marker.events.register("mouseout", marker, me.marker.event.onMouseout);
                marker.events.register("click", marker, me.marker.event.onClick);
                marker.data = data;     // 마커 객체 내 해당 마커 데이터 저장

                me.markerLayer.addMarker(marker); //마커 레이어에 마커 추가
                $(marker.icon.imageDiv).css('cursor', 'pointer')    // 마커 DOM의 css 변경
            }

            // 이동 가능한 마커 생성
            /**
             * 
             * @param {*} data 
             * @param lon:String 
             * @param lat:String 
             * @param labelText:String 
             * @param onMouseover:function
             * @param onMouseout:function
             * @param onMousedown:function
             * @param onMouseup:function
             * @param onClick:function 
             */
            me.marker.addDragable = function (data) {
                // var size = new Tmap.Size(48, 76);//아이콘 크기
                var size = new Tmap.Size(24, 38);   //아이콘 크기
                var offset = new Tmap.Pixel(-(size.w / 2), -(size.h));//아이콘 중심점
                var icon = new Tmap.Icon('http://tmapapis.sktelecom.com/upload/tmap/marker/pin_b_b_p.png', size, offset);//아이콘 설정
                var lonlat = getLonlat(data.lon, data.lat);//좌표값
                var label = me.marker.getLabel(data) //라벨 생성

                var marker = new Tmap.Markers(lonlat, icon, label);//마커 생성
                marker.data = data;     // 마커 객체 내 해당 마커 데이터 저장
                me.markerLayer.addMarker(marker); //마커 레이어에 마커 추가
                $(marker.icon.imageDiv).css('cursor', 'pointer')    // 마커 DOM의 css 변경
                $("#" + marker.icon.imageDiv.id).draggable(); //드래그 이벤트

                marker.events.register("mouseover", marker, data.event.onMouseover);
                marker.events.register("mouseout", marker, data.event.onMouseout);
                marker.events.register("mousedown", marker, data.event.onMousedown);//마우스 버튼을 눌렀을때 발생하는 이벤트 등록
                marker.events.register("mouseup", marker, data.event.onMouseup);//마우스 버튼을 떼었을때 발생하는 이벤트 등록

                if ( marker.popup != undefined && typeof(marker.popup.show) === 'function' ) {
                    marker.popup.show();
                }
                me.map.setCenter(getLonlat(data.lon, data.lat));

            }

            // 마커 객체 - 경로 형식 마커 라벨 생성
            me.marker.getLabel = function (data) {
                var _el = '';
                if (AIV_UTIL.isNotNull(data.labelText)) {
                    _el = data.labelText;
                } else {
                    _el = '<div>'
                        + ' <ul>'
                        + '     <li>차량번호: ' + data.vrn + '</li>'
                        + '     <li>운전자명: ' + data.driverNm + '</li>'
                        + '     <li>주소: ' + data.adres + '</li>'
                        + '     <li>상태: ' + data.state + '</li>'
                        + '     <li>일시: ' + (new Date(data.dataDt)).format('yyyy-mm-dd hh:mi:ss') + '</li>'
                        + ' </ul>'
                        // + ' <div class="text-center">[금일 이력보기]</div>'
                        + '</div>';
                }
                return new Tmap.Label(_el);
            }

            // 검색영역 조건에 맞춰 경로 형식 마커 리스트 검색
            me.searchRoutes = function () {

                // 파라미터 조회 함수 존재시 참조하여 조회 실행
                var params = (options.search.getParams && options.search.getParams()) || {};
                var url = options.search.url;

                //TODO: ajax
                var text = 'helloworld';
                var datas = [];
                for (var i = 0; i < 100; i++) {
                    var time = 360;
                    time += i * 5;
                    datas.push({
                        icon: text[i % 10],
                        lon: 126.986072 - 0.003 + 0.001 * i,
                        lat: 37.57002 + Math.random() / 100 / 2 - Math.random() / 100 / 2,
                        vrn: '강원80바100' + i,
                        driverNm: '김기사' + i,
                        adres: '서울시 강남구 ' + i + '번지',
                        state: '상차[상차종료]',
                        az: 15 * i,   // 방위각
                        dataDt: (new Date('2018-07-19T' + Math.floor(time / 60) + ':' + Math.floor(time % 60))).format('yyyy-mm-dd hh:mi:ss'),
                        adres: '경기도 양주시 회천' + i + '동'
                    })
                }
                // 마커 전체 생성
                me.marker.addRouteList(datas);
                // me.map.setCenter(getLonlat(126.986072, 37.570028), 15);
                options.search.after(datas);
            }

            // 마커 객체 - 경로 형식 마커 리스트 생성 및 추가
            me.marker.addRouteList = function (datas) {
                // 마커 전체 삭제
                me.marker.removeAll();
                $(datas).each(function (i, data) {
                    if(i==0 || i==datas.length-1) {
                        var size = new Tmap.Size(25, 30);//아이콘 크기
                        var offset = new Tmap.Pixel(-(size.w / 2), -(size.h));//아이콘 중심점
                        var icon = new Tmap.Icon('/ui/comm/img/truck_icon_gr.png', size, offset);//아이콘 설정
                        me.marker.addRoute(data, icon);
                    } else {
                        me.marker.addRoute(data);
                    }
                })
            }
            // 마커 객체 - 경로 형식 마커 생성 및 추가
            me.marker.addRoute = function (data, icon) {
                if ( icon == undefined ) {
                    var size = new Tmap.Size(20, 20);//아이콘 크기
                    var offset = new Tmap.Pixel(-(size.w / 2), -(size.h / 2));//아이콘 중심점
                    var deg = Math.floor((data.az % 360) / 22.5);
                    icon = new Tmap.Icon('/ui/comm/img/map_dir_' + deg + '.png', size, offset);//아이콘 설정
                }
                var lonlat = getLonlat(data.lon, data.lat);//좌표값
                var label = me.marker.getLabelRoute(data) //라벨 생성

                var marker = new Tmap.Markers(lonlat, icon, label);//마커 생성
                marker.events.register("mouseover", marker, me.marker.event.onMouseover);
                marker.events.register("mouseout", marker, me.marker.event.onMouseout);
                // marker.events.register("click", marker, me.marker.event.onClick);
                marker.data = data;     // 마커 객체 내 해당 마커 데이터 저장

                me.markerLayer.addMarker(marker); //마커 레이어에 마커 추가
                $(marker.icon.imageDiv).css('cursor', 'pointer')    // 마커 DOM의 css 변경
            }

            // 마커 객체 - 경로 형식 마커 라벨 생성
            me.marker.getLabelRoute = function (data) {
                var _el =
                    '<div>'
                    + ' <ul>'
                    + '     <li>주소: ' + data.adres + '</li>'
                    + '     <li>상태: ' + data.state + '</li>'
                    + '     <li>일시: ' + data.dataDt + '</li>'
                    + ' </ul>'
                    + '</div>';
                return new Tmap.Label(_el);
            }


            // 입력된 마커만 popup-show 하며 그 외 등록된 marker들의 popup은 hide 처리.
            me.marker.showPopup = function (marker) {
                if (AIV_UTIL.isNotNull(marker)) {
                    $(me.markerLayer.markers).each(function () {
                        this.popup.hide();
                    })
                    marker.popup.show();
                }
            }

            // 입력된 data와 일치하는 key-value 값을 가진 marker를 반환
            me.marker.getMarkerByData = function (data) {
                var markers = me.markerLayer.markers;
                var marker = $.grep(markers, function (map) {
                    var _key = Object.keys(data)[0];
                    // var _value = Object.values(data)[0];
                    var _value = data[_key];
                    return map.data[_key] === _value;
                });
                return marker[0];
            }

            // 마커 객체 - 전체 삭제
            me.marker.removeAll = function () {
                var markers = me.markerLayer.markers;
                $(markers).each(function (i, marker) {
                    me.markerLayer.removeMarker(marker);
                })
            }

            if (AIV_UTIL.isNotEmpty(options.marker)) {
                // marker-mouseover event 발생시 실행 함수
                me.marker.event.onMouseover = options.marker.onMouseover || onMouseoverMarker;
                // marker-mouseout event 발생시 실행 함수
                me.marker.event.onMouseout = options.marker.onMouseout || onMouseoutMarker;
                // marker-click event 발생시 실행 함수
                me.marker.event.onClick = options.marker.onClick || onClickMarker;
            }
        }

    }




    // 좌표 객체 - 생성
    function getLonlat(lon, lat) {
        return new Tmap.LonLat(lon, lat).transform("EPSG:4326", "EPSG:3857");
    }

    // 포인터 객체 - 생성
    function getPointer(lon, lat) {
        return new Tmap.Geometry.Point(lon, lat).transform("EPSG:4326", "EPSG:3857");
    }

    // marker-mouseover event 발생시 실행 함수
    function onMouseoverMarker(e) {
        // me.map.setCenter(this.lonlat, 15);
        this.popup.show();
    }
    // marker-mouseout event 발생시 실행 함수
    function onMouseoutMarker(e) {
        this.popup.hide();
    }
    // marker-click event 발생시 실행 함수
    function onClickMarker(e) {
        var marker = this;
    }


}(jQuery));