/**
 * 알럿 윈도우
 */
var AIV_ALERT = {};
AIV_ALERT.sHTML = "";
AIV_ALERT.dialog = "";


AIV_ALERT.options = {};
AIV_ALERT.type = "1";
AIV_ALERT.title = "세방 물류에너지 관리시스템";
AIV_ALERT.content = "기본메시지";
AIV_ALERT.modal = true;
AIV_ALERT.closable = false;


// AIV_ALERT.resizable = false;
// AIV_ALERT.width = 300;
// AIV_ALERT.height = 500;

AIV_ALERT.KendoId = "";
AIV_ALERT.init = function(options) {
    
    // 이전생성한 객체가 있다면 릴리즈...
    if (!AIV_UTIL.isEmpty(AIV_ALERT.KendoId)) {
        AIV_SYS.log("이전객체=" + AIV_ALERT.KendoId);
        $('#' + AIV_ALERT.KendoId).remove();
    }

    var guid = kendo.guid();

    AIV_ALERT.KendoId = "dialog-" + guid;
    AIV_ALERT.sHTML = '';
    AIV_ALERT.sHTML += '<div id="' + AIV_ALERT.KendoId + '"></div>';
    $(AIV_ALERT.sHTML).appendTo("body");

    AIV_ALERT.dialog = $('#' + AIV_ALERT.KendoId);
    AIV_ALERT.dialog.kendoDialog({
        width: "320px",
        title: options.title,
        closable: options.closable,
        modal: options.modal,
        content: options.content,
        actions: options.actions,
        close: options.onClose
    });
};




AIV_ALERT.open = function(title, content, options) {
    // 옵션확인
    options = (AIV_UTIL.isEmpty(options)) ? AIV_ALERT.options : options;

    // 버튼 설정
    var actions = [];
    var type = (AIV_UTIL.isEmpty(options.type)) ? AIV_ALERT.type : options.type;
    if (type == "1") {
        actions.push({text : "확인", action : options.onOK });
    } else if (type == "2") {
        actions.push({text : "확인", action : options.onOK, primary: true });
        actions.push({text : "취소", action : options.onCancel });
    }

    
    // 옵션설정
    $.extend(options, {
        title: (AIV_UTIL.isEmpty(title)) ? AIV_ALERT.title : title,
        content: (AIV_UTIL.isEmpty(content)) ? AIV_ALERT.content : content,
        modal: (AIV_UTIL.isEmpty(options.modal)) ? AIV_ALERT.modal : options.modal,
        closable: (AIV_UTIL.isEmpty(options.closable)) ? AIV_ALERT.closable : options.closable,
        actions : actions
    });

    // AIV_SYS.log(options);
    
    AIV_ALERT.init(options);

    AIV_ALERT.dialog.data("kendoDialog").open();
};