/**
 * 로그통합관리, 추후 로그레벨 부여 또는 별도 서버전송등의 활용방안으로 확장 관리한다.
 */
var AIV_SYS = {};
AIV_SYS.logStr = "";
AIV_SYS.level = "";


AIV_SYS.initLog = function(){
    AIV_SYS.logStr = "";
    //AIV_SYS.level = "PRODUCT";
    AIV_SYS.level = "DEBUG";
};


AIV_SYS.log = function(strLog){
	try {
		if (typeof(strLog) == "object") strLog = JSON.stringify(strLog);
		if (window.console) {
			var dt = new Date();
			var str = dt.getHours() +":"+dt.getMinutes() +":"+dt.getSeconds() +"."+dt.getMilliseconds() + " ";
			str += " " + strLog;
			console.log(str);
		}
	} catch(e) {
		;
	}
};
AIV_SYS.initLog();
