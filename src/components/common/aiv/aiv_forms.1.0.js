/**
 * Form Common Plug-in
 */


(function ($) {
    /**
     * get/set Value for Kendo-jQuery-Object 
     */
    $.fn.kFormValue = function (val) {
        var me = this;
        var id = me.attr('id');
        var dataRole = me.attr('data-role');
        var $originText = $(me).closest('td:not(.k-editable-area)').find('#aiv_forms_orgin_text_' + id);    // editor는 기본적으로 'td'로 감싸고 있기 때문에 td.k-editable-area는 예외로 한다.
        var kendoType = '';

        // 체크박스
        if (me.attr('type') == 'checkbox') {
            if (AIV_UTIL.isNull(val)) { // get
                return me.is(':checked') ? 'Y' : 'N';
            } else { // set
                if (val == true || val == 'Y') {
                    me.prop('checked', true);
                } else {
                    me.prop('checked', false);
                }
            }

            // 달력
        } else if (dataRole == 'datepicker' || dataRole == 'datetimepicker' || dataRole == 'timepicker') {
            if (dataRole == 'datepicker') {
                kendoType = 'kendoDatePicker';
            } else if (dataRole == 'datetimepicker') {
                kendoType = 'kendoDateTimePicker';
            } else if (dataRole == 'timepicker') {
                kendoType = 'kendoTimePicker';
            }
            if (AIV_UTIL.isNull(val)) { // get
                return me.val();
            } else { // set
                $originText.text(val);
                me.data(kendoType).value(val);
                // $me.data(kendoType).trigger("change");
                return me;
            }

            // 파일업로드
        } else if (dataRole == 'upload') {
            var $uploadedFile = me.closest('.k-upload').siblings('.aiv_uploaded_file');
            var $fileLink = $uploadedFile.find('a');

            if (AIV_UTIL.isNull(val)) { // get
                return {
                    idFile: $uploadedFile.attr('id_file'),
                    gFileId: $uploadedFile.attr('g_file_id'),
                    cmdTag: $uploadedFile.attr('cmd_tag')
                };

            } else {    // set
                var idFile = val.idFile;
                var gFileId = val.gFileId;
                var sFileNm = val.sFileNm;
                // 업로드된 파일 보여주는 영역 show
                $uploadedFile.show();
                $uploadedFile.attr('id_file', idFile);
                $uploadedFile.attr('g_file_id', gFileId);
                $uploadedFile.attr('cmd_tag', 'R');
                $fileLink.attr('onClick', "AIV_COMM.fileDownload('" + AIV_MENU.sMenuId + "','" + gFileId + "','" + idFile + "','" + sFileNm + "')");
                $fileLink.text(sFileNm);

                // 읽기모드가 별도로 있을 경우 css 변경, 아닐 경우 삭제 가능한 버튼 생성
                if (val.readMode == true) {
                    $uploadedFile.addClass('aiv_uploaded_file_read');
                } else {
                    $fileLink.after('<button class="k-button k-primary ml-3" onclick="removeUploadedFile(event)">삭제</button>');
                    // 파일 삭제 버튼 눌렀을때 실행 함수 생성
                    window.removeUploadedFile = function (e) {
                        var $div = $(e.target).parent().empty();
                        $div.empty();
                        $div.attr('cmd_tag', 'D');
                        $div.hide();
                    }
                }


            }

            // 파일다운로드링크
        } else if (dataRole == 'aiv_download_link') {
            if (typeof val === 'undefined') { // get
                return me.text();
            } else {    // set
                // me.parent().attr('onClick', "AIV_COMM.fileDownload('" + val.sScreenId + "','" + val.gId + "','" + val.sFileNm + "')");
                me.parent().attr('onClick', "AIV_COMM.fileDownload('" + AIV_MENU.sMenuId + "','" + val.gFileId + "','" + val.idFile + "','" + val.sFileNm + "')");
                me.parent().text(val.sFileNm);
            }

            //  에디터
        } else if (dataRole == 'editor') {
            if (typeof val === 'undefined') { // get
                return me.self.value();
            } else {    // set
                $originText.html(val);
                return me.self.value(val);
            }

            // 그 외 textbox, select 등
        } else {
            if (dataRole == 'numerictextbox') {
                kendoType = 'kendoNumericTextBox';
            } else if (dataRole == 'maskedtextbox') {
                kendoType = 'kendoMaskedTextBox';
            } else if (dataRole == 'dropdownlist') {
                kendoType = 'kendoDropDownList';
            } else if (dataRole == 'dateinput') {
                kendoType = 'kendoDateInput';
            } else if (dataRole == 'upload') {
                kendoType = 'kendoUpload';
            }
            if (AIV_UTIL.isNull(val)) { // get
                return me.data(kendoType).value();
            } else { // set

                // kendo input 외 read 모드일때 text 영역일때도 값을 넣는다.
                $originText.text(val);

                me.data(kendoType).value(val);
                me.trigger("change");
                return me;
            }

        }
    }

    $.fn.kFormMode = function (mode) {
        var me = this;
        var id = me.attr('id');

        var dataRole = me.attr('data-role');    // data-role 에 따라 kendo의 형태가 다르기 때문에 구분하여 처리 한다.
        var $originText = $(me).closest('td:not(.k-editable-area)').find('#aiv_forms_orgin_text_' + id);

        if (mode == 'read') {
            // read영역 show
            $originText.show();

            // edit영역 hidden
            if (dataRole == 'datepicker' || dataRole == 'datetimepicker' || dataRole == 'timepicker') {
                me.parent().parent().hide();
            } else if (dataRole == 'maskedtextbox') {
                me.parent().hide();
                me.hide();
            } else if (dataRole == 'numerictextbox') {
                me.parent().parent().hide();
                me.hide();
            } else if (dataRole == 'editor') {
                me.closest('table.k-editor').hide();
            } else {
                me.parent().hide();
            }
            // 취소했을경우 원래의 값으로 input내 값을 복구 시킨다.
            var originValue = $originText.text();
            $(me).val(originValue);

        } else if (mode == 'edit') {
            // read영역 hidden
            $originText.hide();

            // edit영역 show
            if (dataRole == 'datepicker' || dataRole == 'datetimepicker' || dataRole == 'timepicker') {
                me.parent().parent().show();
            } else if (dataRole == 'maskedtextbox') {
                me.parent().show();
                me.show();
            } else if (dataRole == 'numerictextbox') {
                me.parent().parent().show();
                me.show();
            } else if (dataRole == 'editor') {
                me.closest('table.k-editor').show();
            } else {
                me.parent().show();
            }
        }

    }

    // $.fn.kText = function (val) {
    // }

    /**
     * 지정 DOM 내 input 객체들에 대해 기본 생성을 함
     */
    $.fn.AIV_FORM_INIT = function (options) {
        var me = this;
        var divId = me.attr('id');
        var rtnParams = {};
        options = options || {};

        /**
         * 해당 div 하위 input 에 대해 id에 맞추어 자동으로 입력칸을 생성한다.
         */
        $(me).find('input, textarea').each(function (_i, input) {
            var $input = $(input);
            var inputId = $input.attr('id');

            /**
             * $.fn.AIV_FORM_TEXTBOX()
             */
            if (inputId == 'sCorpNm') {
                rtnParams.sCorpNm = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "법인명" });
            } else if (inputId == 'sOwner') {
                rtnParams.sOwner = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "대표자명" });
            } else if (inputId == 'sCorpCat') {
                rtnParams.sCorpCat = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "대표업종" });
            } else if (inputId == 'sCorpRegNum') {
                rtnParams.sCorpRegNum = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "법인등록번호" });
            } else if (inputId == 'sCorpTel') {
                rtnParams.sCorpTel = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "대표전화번호" });
            } else if (inputId == 'sCorpAddr') {
                rtnParams.sCorpAddr = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "법인소재지" });
            } else if (inputId == 'sPicNm') {
                rtnParams.sPicNm = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "이름" });
            } else if (inputId == 'idErpDept') {
                rtnParams.idErpDept = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "부서" });
            } else if (inputId == 'sPicPos') {
                rtnParams.sPicPos = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "직위" });
            } else if (inputId == 'sPicTel') {
                rtnParams.sPicTel = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "전화번호" });
            } else if (inputId == 'sPicHp') {
                rtnParams.sPicHp = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "휴대전화" });
            } else if (inputId == 'sPicMail') {
                rtnParams.sPicMail = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "이메일" });
            } else if (inputId == 'sMainPrd') {
                rtnParams.sMainPrd = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "주요생산제품" });
            } else if (inputId == 'sCertPro') {
                rtnParams.sCertPro = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "품질경영시스템인증" });
            } else if (inputId == 'idSite') {
                rtnParams.idSite = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "사업장일련번호" });
            } else if (inputId == 'nOrder') {
                rtnParams.nOrder = $input.AIV_FORM_TEXTBOX({ type: 'numeric', title: "정렬순서", format: "#" });
            } else if (inputId == 'sSiteId') {
                rtnParams.sSiteId = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "사업장구분" });
            } else if (inputId == 'sSiteNm') {
                rtnParams.sSiteNm = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "사업장명" });
            } else if (inputId == 'sSiteRegNum') {
                rtnParams.sSiteRegNum = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "사업자등록번호" });
            } else if (inputId == 'sSiteAddr') {
                rtnParams.sSiteAddr = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "사업장소재지" });
            } else if (inputId == 'sSiteTel') {
                rtnParams.sSiteTel = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "사업장대표전화" });
            } else if (inputId == 'sSiteCat') {
                rtnParams.sSiteCat = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "업종" });
            } else if (inputId == 'sPicFax') {
                rtnParams.sPicFax = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "팩스" });
            } else if (inputId == 'sPointNm') {
                rtnParams.sPointNm = $input.AIV_FORM_TEXTBOX({ type: 'text', title: '배출시설명' });
            } else if (inputId == 'sPointId') {
                rtnParams.sPointId = $input.AIV_FORM_TEXTBOX({ type: 'text', title: '배출시설 ID' });
            } else if (inputId == 'sInvSeq') {
                rtnParams.sInvSeq = $input.AIV_FORM_TEXTBOX({ type: 'text', title: '배출시설 일련번호' });
            } else if (inputId == 'nCapa') {
                rtnParams.nCapa = $input.AIV_FORM_TEXTBOX({ type: 'numeric', title: '시설용량' });
            } else if (inputId == 'nCapa2') {
                rtnParams.nCapa2 = $input.AIV_FORM_TEXTBOX({ type: 'numeric', title: '세부시설용량' });
            } else if (inputId == 'cSyear') {
                rtnParams.cSyear = $input.AIV_FORM_TEXTBOX({ type: 'text', title: '시작년도' });
            } else if (inputId == 'sCefId') {
                rtnParams.sCefId = $input.AIV_FORM_TEXTBOX({ type: 'text', title: '배출원' });
            } else if (inputId == 'sTitle') {
                rtnParams.sTitle = $input.AIV_FORM_TEXTBOX({ type: 'text', title: '제목' });
            } else if (inputId == 'sBaseVal') {
                rtnParams.sBaseVal = $input.AIV_FORM_TEXTBOX({ type: 'numeric', title: '원단위 산정 기준 값', format: "#.000" });
            } else if (inputId == 'sFuelEff') {
                rtnParams.sFuelEff = $input.AIV_FORM_TEXTBOX({ type: 'text', title: '공인연비' });
            } else if (inputId == 'sCertEnv') {
                rtnParams.sCertEnv = $input.AIV_FORM_TEXTBOX({ type: 'text', title: "환경경영시스템인증" });
            }

            /**
             * $.fn.AIV_DATETIMEPICKER()
             */
            if (inputId == 'cYm') {
                rtnParams.cYm = $input.AIV_FORM_DATETIMEPICKER({ type: 'yyyy-MM', title: '년월' });
            } else if (inputId == 'cYear') {
                rtnParams.cYear = $input.AIV_FORM_DATETIMEPICKER({ type: 'yyyy', title: '연도' });
            }

            /**
             * $.fn.AIV_FORM_DROPDOWNLIST()
             */
            if (inputId == 'sSiteNmList') {
                rtnParams.sSiteNmList = $input.AIV_FORM_DROPDOWNLIST({ type: 'sSiteNmList', title: '사업장' });
            } else if (inputId == 'sEngCd') {
                rtnParams.sEngCd = $input.AIV_FORM_DROPDOWNLIST({ type: 'sEngCd', title: '배출원' });
            } else if (inputId == 'cSYear') {
                rtnParams.cSYear = $input.AIV_FORM_DROPDOWNLIST({ type: 'cSYear', title: '시작년도' });
            } else if (inputId == 'cEYear') {
                rtnParams.cEYear = $input.AIV_FORM_DROPDOWNLIST({ type: 'cEYear', title: '종료년도' });
            } else if (inputId == 'sUntCapa') {
                rtnParams.sUntCapa = $input.AIV_FORM_DROPDOWNLIST({ type: 'sUntCapa', title: '시설용량단위' });
            } else if (inputId == 'nUntCapa2') {
                rtnParams.nUntCapa2 = $input.AIV_FORM_DROPDOWNLIST({ type: 'nUntCapa2', title: '세부시설용량단위' });
            } else if (inputId == 'sBaseUnt') {
                rtnParams.sBaseUnt = $input.AIV_FORM_DROPDOWNLIST({ type: 'sBaseUnt', title: '기준단위' });
            } else if (inputId == 'sLogiTypeCd') {
                rtnParams.sLogiTypeCd = $input.AIV_FORM_DROPDOWNLIST({ type: 'sLogiTypeCd', title: '물류구분' });
            } else if (inputId == 'sInvCd') {
                rtnParams.sInvCd = $input.AIV_FORM_DROPDOWNLIST({ type: 'sInvCd', title: '배출시설 구분' });
            } else if (inputId == 'sEngCd') {
                rtnParams.sEngCd = $input.AIV_FORM_DROPDOWNLIST({ type: 'sEngCd', title: '에너지원' });
            } else if (inputId == 'sSitePartyCd') {
                rtnParams.sSitePartyCd = $input.AIV_FORM_DROPDOWNLIST({ type: 'sSitePartyCd', title: "사업장구분" });
            } else if (inputId == 'cCloseYear') {
                rtnParams.cCloseYear = $input.AIV_FORM_DROPDOWNLIST({ type: 'cCloseYear', title: "폐쇄연도" });
            } else if (inputId == 'sPicDept') {
                rtnParams.sPicDept = $input.AIV_FORM_DROPDOWNLIST({ type: 'sPicDept', title: "부서" });
            } else if (inputId == 'sSmlEmiYn') {
                rtnParams.sSmlEmiYn = $input.AIV_FORM_DROPDOWNLIST({ type: 'sSmlEmiYn', title: "소량배출사업장" });
            }

            /**
             * $.fn.AIV_FORM_UPLOAD()
             */
            if (inputId == 'sFileNm') {
                // var formOption = $.extend(true, { type: 'sFileNm', title: '파일업로드' }, options.sFileNm);
                // rtnParams.sFileNm = $input.AIV_FORM_UPLOAD(formOption);
                rtnParams.sFileNm = $input.AIV_FORM_UPLOAD({ type: 'sFileNm', title: '파일업로드' });
            } else if (inputId == 'sFileNm1') {
                rtnParams.sFileNm1 = $input.AIV_FORM_UPLOAD({ type: 'sFileNm1', title: '파일업로드' });
            } else if (inputId == 'sFileNm2') {
                rtnParams.sFileNm2 = $input.AIV_FORM_UPLOAD({ type: 'sFileNm2', title: '파일업로드' });
            } else if (inputId == 'sFileNm3') {
                rtnParams.sFileNm3 = $input.AIV_FORM_UPLOAD({ type: 'sFileNm3', title: '파일업로드' });
            } else if (inputId == 'sFileNm4') {
                rtnParams.sFileNm4 = $input.AIV_FORM_UPLOAD({ type: 'sFileNm4', title: '파일업로드' });
            } else if (inputId == 'sFileNm5') {
                rtnParams.sFileNm5 = $input.AIV_FORM_UPLOAD({ type: 'sFileNm5', title: '파일업로드' });
            } else if (inputId == 'sFileNmOld') {
                rtnParams.sFileNmOld = $input.AIV_FORM_UPLOAD({ type: 'sFileNmOld', title: '파일다운로드' });
            } else if (inputId == 'gMap') {
                rtnParams.gMap = $input.AIV_FORM_UPLOAD({ type: 'gMap', title: "약도" });
            } else if (inputId == 'gPhoto') {
                rtnParams.gPhoto = $input.AIV_FORM_UPLOAD({ type: 'gPhoto', title: "사진" });
            } else if (inputId == 'gArr') {
                rtnParams.gArr = $input.AIV_FORM_UPLOAD({ type: 'gArr', title: "시설배치도" });
            } else if (inputId == 'gPro') {
                rtnParams.gPro = $input.AIV_FORM_UPLOAD({ type: 'gPro', title: "공정도" });
            }

            /**
             * EDITOR
             */
            if (inputId == 'sContent') {
                rtnParams.sContent = $input.AIV_FORM_EDITOR({ type: 'sContent', title: '내용' });
            }

            /**
             * checkbox
             */
        });

        // input 별로 destroy 함수 생성
        for (key in rtnParams) {
            rtnParams[key].destroy = function () {
                var $input = $(this);
                var $kInput = $input.closest('.k-widget.k-header');
                $('<input id="' + $input.attr('id') + '" />').insertAfter($kInput);
                if ($input.self) {
                    $input.self.destroy();
                }
                if ($kInput.prev().hasClass('aiv_search_title')) {
                    $kInput.prev().remove();
                }
                $kInput.remove();
            }
        }

        // 함수를 제외한 모든 객체를 가져오는 함수
        rtnParams.getAllParam = function () {
            var _params = $.extend({}, this); // 객체 복사
            for (key in _params) {
                if (typeof _params[key] === 'function') { // 함수 타입 제거
                    delete _params[key];
                }
            }
            return _params;
        }

        // 객체 내 모든 파라미터 값을 담은 객체를 가져오는 함수
        rtnParams.getAllValue = function () {
            var _values = {};
            var _params = this;
            for (key in _params) {
                if (typeof _params[key] !== 'function') {
                    var _oid = $(_params[key]).attr('oid'); // original id
                    if (AIV_UTIL.isNotNull(_oid)) {
                        _values[_oid] = _params[key].kFormValue();
                    } else {
                        _values[key] = _params[key].kFormValue();
                    }
                }
            }
            return _values;
        }

        // 객체 내 모든 파라미터 값에 세팅하는 함수
        rtnParams.setAllValue = function (setParams) {
            var _params = this;
            for (key in _params) {
                if (typeof _params[key] !== 'function') {
                    // console.log(key, _params[key], setParams[key])
                    _params[key].kFormValue(setParams[key]);
                }
            }
        }

        // 객체 내 모든 파라미터의 값을 clear 하는 함수
        rtnParams.setEmpty = function () {
            var _params = this;
            for (key in _params) {
                if (typeof _params[key] !== 'function') {
                    // console.log(key, _params[key], setParams[key])
                    _params[key].kFormValue('');
                }
            }
        }

        // Form의 Mode를 바꾼다. Mode: 'read', 'edit'
        rtnParams.setMode = function (mode) {
            mode = (mode === 'edit') ? 'edit' : 'read';
            var _params = this;
            for (key in _params) {
                if (typeof _params[key] !== 'function') {
                    _params[key].kFormMode(mode);
                }
            }
        }

        // Form의 validator 를 생성한다.
        var validator = me.kendoValidator().data("kendoValidator");

        // Form의 input들이 isValid 한지 체크 한다.
        rtnParams.isValid = function () {
            // me.kendoValidator();
            var _flag = true;
            var _params = this;
            for (key in _params) {
                if (typeof _params[key] !== 'function') {
                    var _param = _params[key];
                    if (!validator.validateInput(_param)) {
                        // var title = $(_param).attr('title');
                        // alert(title + " 이(가) 유효하지 않습니다.");
                        _flag = false;
                    }
                }
            }
            return _flag;
        }



        return rtnParams;
    }

})(jQuery);



/**
 * 텍스트박스(TEXTBOX) 플러그인
 */
(function ($) {

    // 세팅
    $.fn.AIV_FORM_TEXTBOX = function (options) {
        // 기본 설정값
        var _default = {
            // width: 150,
            width: '100%',
            height: 0
        };

        var me = this;
        // me.wrap("<div class='form-group'></div>");
        me.attr('type', 'text');
        var id = me.attr('id');
        var title = $.AIV.kFieldModel[id] && $.AIV.kFieldModel[id].title || id;

        // me.addClass("k-textbox");
        me.attr('title', title);
        // me.after('<span class="k-invalid-msg" data-for="' + id + '"></span>');
        me.attr('validationMessage', title + '이(가) 필요합니다.');
        // me.attr('data-validmask-msg', id + '가 필요합니다.');
        var isRequired = me.attr('required') == 'required' ? true : false;
        var isEdit = me.attr('edit') == 'edit' ? true : false;

        // 초기 모드가 수정이 아닐 경우, input 은 숨김처리 한다.
        if (!isEdit) {
            me.hide();
            // me.parent().hide();
        }

        // Text 세팅 (mode:read)
        var _text = '';
        _text = '<span id="aiv_forms_orgin_text_' + id + '"></span>';
        me.before(_text);

        // Width 조정
        var _width = options.width ? options.width : _default.width;



        options.data = {};

        if (id == 'name') {
            _width = 150;
        } else {
        }


        me.parent().after('<span class="k-invalid-msg" data-for="' + id + '"></span>');

        if (options.type == 'numeric') {
            me.attr('type', 'numeric');
            var _step = 1;
            var _decimals = 0;
            if (options.format == '#.000') {
                _step = 0.001;
                _decimals = 3;
            }
            me.kendoNumericTextBox({    // 숫자
                format: options.format || '#',
                step: _step,
                decimals: _decimals,
                width: '100%',
            });
            me.self = me.data('kendoNumericTextBox');

        } else {
            me.kendoMaskedTextBox({ // 문자
                mask: ""
            });
            me.self = me.data('kendoMaskedTextBox');
        }

        if (!AIV_UTIL.isEmpty(options.onKeyup)) {
            me.bind("keyup", function (e) {
                if (e.keyCode == 13 && options.onKeyup) {
                    options.onKeyup();
                }
            });
        }


        me.closest("span.k-widget").width(_width);

        // 초기 모드가 수정이 아닐 경우, input 은 숨김처리 한다.
        var isEdit = me.attr('edit') == 'edit' ? true : false;
        if (!isEdit) {
            me.hide();
            me.siblings('input').hide();
            me.parents('.k-widget').hide();
        }

        return this;
    };
}(jQuery));



/**
 * 콤보(DROPDOWNLIST) 플러그인
 */
(function ($) {

    // 세팅
    $.fn.AIV_FORM_DROPDOWNLIST = function (options) {

        // 기본 설정값
        var _default = {
            // width: 150,
            width: '100%',
            height: 0
        };
        var _index = 0;

        var me = this;
        // me.wrap("<div class='form-group'></div>");
        // me.attr('type', 'text');

        var id = me.attr('id');
        var title = $.AIV.kFieldModel[id] && $.AIV.kFieldModel[id].title;
        me.attr('title', title);
        me.after('<span class="k-invalid-msg" data-for="' + id + '"></span>');

        // me.attr('data-validmask-msg', id + '가 필요합니다.');
        me.attr('validationMessage', title + '이(가) 필요합니다.');
        var isRequired = me.attr('required') == 'required' ? true : false;

        // Text 세팅 (mode:read)
        var _text = '';
        _text = '<span id="aiv_forms_orgin_text_' + id + '" ></span>';
        me.before(_text);

        // Width 조정
        var _width = options.width ? options.width : _default.width;


        options.data = [];
        var allYn = (AIV_UTIL.isNull(options.allYn)) ? true : options.allYn;
        if (options.type == 'sSiteNmList') { // 사업장
            options.data.push({ text: "A 사업장", value: "A_SITE_CODE" });
            options.data.push({ text: "B 사업장", value: "B_SITE_CODE" });
            options.data.push({ text: "C 사업장", value: "C_SITE_CODE" });
        } else if (options.type == 'sEngCd') { // 배출원
            allYn = false;
            var codeList = AIV_CONTROLS.COMM_CODES.ENG;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'cSYear') { // 시작년도
            allYn = false;
            var yyyy = new Date().format('yyyy');
            var codeList = AIV_CONTROLS.COMM_CODES.YYYY;
            $(codeList).each(function (i, code) {
                if (yyyy == code.key) {
                    _index = i;
                }
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'cEYear') { // 종료년도
            allYn = false;
            var yyyy = new Date().format('yyyy');
            var codeList = AIV_CONTROLS.COMM_CODES.YYYY;
            $(codeList).each(function (i, code) {
                if (yyyy == code.key) {
                    _index = i;
                }
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'sUntCapa') { // 시설용량단위
            allYn = false;
            var codeList = AIV_CONTROLS.COMM_CODES.UNT;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'nUntCapa2') { // 시설용량단위2
            allYn = false;
            var codeList = AIV_CONTROLS.COMM_CODES.UNT;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'sBaseUnt') { // 에너지원 기준단위
            allYn = false;
            var codeList = AIV_CONTROLS.COMM_CODES.BASE_UNT;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'sLogiTypeCd') { // 물류구분
            allYn = false;
            var codeList = AIV_CONTROLS.COMM_CODES.LOGI_TYPE;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'sInvCd') { // 배출시설 구분
            allYn = false;
            var codeList = AIV_CONTROLS.COMM_CODES.INV;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'sEngCd') { // 에너지원
            allYn = false;
            var codeList = AIV_CONTROLS.COMM_CODES.ENG;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'sSitePartyCd') { // 사업장구분
            allYn = false;
            var codeList = AIV_CONTROLS.COMM_CODES.SITE_PARTY;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
        } else if (options.type == 'cCloseYear') { // 폐쇄연도
            allYn = false;
            var codeList = AIV_CONTROLS.COMM_CODES.YYYY;
            $(codeList).each(function (i, code) {
                options.data.push({ text: code.value, value: code.key });
            });
            // 서버에서 받아온 연도에 4년을 추가로 옵션에 넣어준다.
            var cYear = new Date().getFullYear();
            for (var i = 2; i < 6; i++) {
                options.data.push({ text: (cYear + i).toString(), value: (cYear + i).toString() });
            }
            // 초기값을 가장 마지막 연도로 설정
            $(options.data).each(function (i, row) {
                _index = i
            })
        } else if (options.type == 'sCertEnv') {
            allYn = false;
        } else if (options.type == 'sPicDept') {    // 부서
            allYn = false;
            var codesList = AIV_CONTROLS.COMM_CODES.ERP_DEPT_CODES;
            // 최초 로드시 ajax를 통해 부서정보를 가져옴
            if (codesList && codesList.length > 0) {
            } else {
                codesList = [];
                AIV_COMM.ajax({
                    method: 'GET',
                    async: false,
                    url: '/api/v1/common/erp/dept/ls',
                    data: {
                        sDeptId: ENV_PROP.sDeptId // 전체 조직도
                    },
                    success: function (responseData) {
                        var deptList = responseData.resultData;
                        $(deptList).each(function (i, dept) {
                            // codesList.push({ 'text': dept.sFullDeptNm, 'value': dept.idErpDept });
                            codesList.push({ 'text': dept.sFullDeptNm, 'value': dept.sFullDeptNm });
                        });
                        
                        AIV_CONTROLS.COMM_CODES.ERP_DEPT_CODES = codesList; // 재사용 가능하도록 저장
                    }
                });
            }
            options.data = codesList;

            // $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:idErpDept" />')
            //     .appendTo(container)
            //     .kendoDropDownList({
            //         dataValueField: "key",
            //         dataTextField: "value",
            //         optionLabel: false,
            //         filter: 'contains',
            //         // autoWidth: true,
            //         dataSource: {
            //             data: codes
            //         },
            //         // index: 0,
            //         change: function (e) {
            //             var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
            //             var row = $(e.sender.element).closest("tr");
            //             var dataItem = grid.dataItem(row);
            //             dataItem.idErpDept = this.value();
            //             // dataItem.sDeptId = this.value();
            //             dataItem.sDeptNm = this.text();
            //         },
            //     });
        } else if (options.type == 'sSmlEmiYn'){
            allYn = false;
            options.data = [{
                text: '예', value: 'Y'
            },{
                text: '아니오', value: 'N'
            }]
        }


        // DropDownList 생성
        me.kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            autoWidth: true, // 펼쳤을 때 LIST 내 TEXT 길이에 따라 자동적으로 가로길이를 조정해줌
            optionLabel: allYn ? "전체" : false,
            dataSource: options.data,
            filter: options.filter,
            change: options.onChange,
            index: _index // index:0=default
        });

        me.closest("span.k-dropdown").width(_width);
        me.closest("span.k-dropdown").css('font-size', '13px');
        me.self = me.data("kendoDropDownList");


        // 초기 모드가 수정이 아닐 경우, input 은 숨김처리 한다.
        var isEdit = me.attr('edit') == 'edit' ? true : false;
        if (!isEdit) {
            // me.hide();
            me.parents('.k-widget.k-dropdown').hide();
        }

        return me;
    };


}(jQuery));


/**
 * 날짜시간 플러그인
 */
(function ($) {


    // 세팅
    $.fn.AIV_FORM_DATETIMEPICKER = function (options) {

        // 기본 설정값
        var _default = {
            // width: 150,
            width: '100%',
            height: 0
        };

        var me = this;
        me.attr('type', 'text');
        var id = me.attr('id');
        var title = $.AIV.kFieldModel[id] && $.AIV.kFieldModel[id].title;
        me.attr('title', title);
        me.after('<span class="k-invalid-msg" data-for="' + id + '"></span>');

        // me.attr('data-validmask-msg', id + '가 필요합니다.');
        me.attr('validationMessage', title + '이(가) 필요합니다.');
        var isRequired = me.attr('required') == 'required' ? true : false;

        // Text 세팅 (mode:read)
        var _text = '';
        _text = '<span id="aiv_forms_orgin_text_' + id + '"></span>';
        me.before(_text);

        // Width 조정
        var _width = options.width ? options.width : _default.width;

        var type = options.type;
        var today = new Date();

        //  포맷(type, yyyy:년 MM:월 dd:일 HH:시간24 mm:분 ss:초)
        switch (type) {
            case 'yyyy-MM-dd':
                options.format = "yyyy-MM-dd";
                _width = options.width || 115;
                break;
            case 'yyyy-MM':
                options.format = "yyyy-MM";
                _width = 100;
                $.extend(options, { start: "year", depth: "year" });
                break;
            case 'yyyy':
                options.format = "yyyy";
                _width = 80;
                $.extend(options, { start: "decade", depth: "decade" });
                break;
            case 'dd':
                options.format = "dd";
                _width = 65;
                break;
            case 'yyyy-MM-dd HH':
                options.format = "yyyy-MM-dd HH";
                _width = 160;
                break;
            case 'yyyy-MM-dd HH:mm':
                options.format = "yyyy-MM-dd HH:mm";
                _width = 180;
                break;
            case 'yyyy-MM-dd HH:mm:ss':
                options.format = "yyyy-MM-dd HH:mm:ss";
                _width = 200;
                break;
            case 'HH:mm':
                options.format = "HH:mm";
                _width = 70;
                break;
        }


        // 플러그인 세팅
        switch (type) {
            case 'yyyy-MM-dd':
            case 'yyyy-MM':
            case 'yyyy':
            case 'dd':
                // 날짜 세팅
                $.extend(options, {
                    value: today,
                    format: options.format,
                    culture: "ko-KR"
                });


                me.kendoDatePicker(options);
                me.self = me.data("kendoDatePicker");

                me.closest("span.k-datepicker").width(_width);

                break;
            case 'yyyy-MM-dd HH':
            case 'yyyy-MM-dd HH:mm':
            case 'yyyy-MM-dd HH:mm:ss':
                // 날짜시간 세팅
                // AIV_SYS.log("날짜시간 세팅");    

                var d = new Date();
                $.extend(options, {
                    value: new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0),
                    interval: 30,
                    timeFormat: "HH:mm", //24 hours format
                    dateInput: true,
                    culture: "ko-KR"
                });

                me.kendoDateTimePicker(options);
                me.self = me.data("kendoDateTimePicker");

                me.closest("span.k-datetimepicker").width(_width);

                break;
            case 'HH:mm':
                var d = new Date();
                $.extend(options, {
                    value: new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0),
                    interval: 30,
                    timeFormat: "HH:mm", //24 hours format
                    dateInput: true,
                    culture: "ko-KR"
                });
                me.kendoTimePicker(options);
                me.closest("span.k-timepicker").width(_width).height(28);

                break;
        }

        // 시간일경우 마지막리스트 추가
        me.addLastTime = function () {
            var tmHTML = '';
            var hour = '';
            for (i = 0; i < 24; i++) {
                hour = i;
                if (i < 10) hour = '0' + i;
                if (i == 0) {
                    tmHTML = '<li tabindex="-1" role="option" class="k-item k-state-selected" unselectable="on" id="dttoDe_option_selected" aria-selected="true">00:00</li>';
                    tmHTML += '<li tabindex="-1" role="option" class="k-item" unselectable="on">' + hour + ':30</li>';
                } else {
                    tmHTML += '<li tabindex="-1" role="option" class="k-item" unselectable="on">' + hour + ':00</li>';
                    tmHTML += '<li tabindex="-1" role="option" class="k-item" unselectable="on">' + hour + ':30</li>';
                }
            }
            tmHTML += '<li tabindex="-1" role="option" class="k-item" unselectable="on">' + hour + ':59</li>';

            $("#" + timelistId + "").append(tmHTML);
            $("#" + timelistId + "").parents("div").css('height', '206px');
        };

        if ((type == 5) || (type == 6)) {
            me.addLastTime();
        }

        // 일자 더하기 함수
        me.addDay = function (add) {
            var datepicker = me.self;
            var time = datepicker.value().getTime();
            var unit = 24 * 60 * 60 * 1000;
            datepicker.value(new Date(time + add * unit));
        }

        // 초기 모드가 수정이 아닐 경우, input 은 숨김처리 한다.
        var isEdit = me.attr('edit') == 'edit' ? true : false;
        if (!isEdit) {
            // me.hide();
            me.parents('.k-widget.k-datepicker').hide();
        }

        return me;
    };


}(jQuery));



/**
 * 파일업로드 플러그인
 */
(function ($) {

    // 세팅
    $.fn.AIV_FORM_UPLOAD = function (options) {
        options = options || {};
        var me = this;

        me.attr('type', 'file');

        me.before('<div id="' + options.type + '_uploaded_file" id_file="" g_file_id="" class="aiv_uploaded_file" cmd_tag=""><a href="javascript:void(0);"></a></div>');

        // 기본 설정값
        var defaultOptions = {
            width: '100%',
            multiple: false,
            showFileList: true,
            clear: function (a, b, c) {
                console.log('clear', a, b, c);
            },
            cancel: function (a, b, c) {
                console.log('cancel', a, b, c);
            },
            select: function (files) {
                var $uploadedFile = $(this.element).closest('.k-upload').siblings('.aiv_uploaded_file');
                $uploadedFile.attr('cmd_tag', 'U');
                $uploadedFile.empty();
                $uploadedFile.hide();
            }
        };
        options = $.extend(true, defaultOptions, options);

        // var title = $.AIV.kFieldModel[id] && $.AIV.kFieldModel[id].title;
        // me.attr('title', title);
        // me.after('<span class="k-invalid-msg" data-for="' + id + '"></span>');
        // me.attr('validationMessage', title + '이(가) 필요합니다.');
        // var isRequired = me.attr('required') == 'required' ? true : false;

        switch (options.type) {
            case 'sFileNm':
                // Upload 생성
                me.kendoUpload(options);
                me.self = me.data("kendoUpload");

                // upload function 작성
                me.upload = function (uploadOptions) {
                    uploadOptions = uploadOptions || {};

                    var form = AIV_UTIL.createForms({
                        input: $(me)[0],
                        gId: uploadOptions.gId,
                        gid: uploadOptions.gid,
                        idFile: uploadOptions.idFile,
                        cmdTag: uploadOptions.cmdTag || 'U',
                    });

                    AIV_COMM.fileUpload({
                        form: form,
                        success: uploadOptions.success
                    });
                }

                break;
            case 'sFileNm1':
            case 'sFileNm2':
            case 'sFileNm3':
            case 'sFileNm4':
            case 'sFileNm5':
            case 'gMap':
            case 'gPhoto':
            case 'gArr':
            case 'gPro':
                // Upload 생성
                me.kendoUpload(options);
                me.self = me.data("kendoUpload");

                me.getCmdTag = function () {
                    var id = me.attr('id');
                    var cmdTag = $('#' + id + '_uploaded_file').attr('cmd_tag');
                    return cmdTag;
                }

                // upload function 작성
                me.upload = function (uploadOptions) {
                    uploadOptions = uploadOptions || {};

                    var id = me.attr('id');
                    var idFile = $('#' + id + '_uploaded_file').attr('id_file') || uploadOptions.idFile;
                    var gFileId = $('#' + id + '_uploaded_file').attr('g_file_id') || uploadOptions.gFileId || uploadOptions.gId;
                    var cmdTag = $('#' + id + '_uploaded_file').attr('cmd_tag') || uploadOptions.cmdTag;

                    // 파일을 포함한 input정보들을 upload 가능한 form 형태로 변환
                    var form = AIV_UTIL.createForms({
                        input: $(me)[0],
                        cmdTag: cmdTag, // 'U':insert/update, 'D':delete, 'R':read(업로드할 필요 없음, 화면 필요에 따라 명명)
                        idFile: idFile,
                        gId: gFileId
                    });

                    // 파일 업로드 AJAX통신 실행
                    AIV_COMM.fileUpload({
                        form: form,
                        success: uploadOptions.success
                    });
                }

                break;
            case 'sFileNmOld':
                // Download Tag 생성
                me.wrap('<a href="#" style="text-decoration: underline;"></a>');    // input 대신 a tag로 대체하기 위하여 감쌈
                me.attr('data-role', 'aiv_download_link');      // get,set 시 분기처리가능하도록 data-role 지정
                me.css({ 'visibility': 'hidden', 'width': '0' });   // input tag 감추기
                break;

        }

        // 초기 모드가 수정이 아닐 경우, input 은 숨김처리 한다.
        // var isEdit = me.attr('edit') == 'edit' ? true : false;
        // if (!isEdit) {
        //     me.hide();
        //     me.parent('.k-widget.k-dropdown').hide();
        // }

        return me;
    };


}(jQuery));


/**
 * WYSIWYG editor 플러그인
 */
(function ($) {

    // 세팅
    $.fn.AIV_FORM_EDITOR = function (options) {
        options = options || {};
        var me = this;
        var id = me.attr('id');

        var height = '200px';
        me.css('height', options.height || height);

        var title = ($.AIV.kFieldModel[id] && $.AIV.kFieldModel[id].title);
        me.attr('title', title);
        // me.after('<span class="k-invalid-msg" data-for="' + id + '"></span>');

        me.attr('validationMessage', title + '이(가) 필요합니다.');
        // var isRequired = me.attr('required') == 'required' ? true : false;

        // Text 세팅 (mode:read)
        var _text = '';
        _text = '<div id="aiv_forms_orgin_text_' + id + '" ></div>';
        me.before(_text);

        // 기본 설정값
        var defaultOptions = {
            // width: '100%',
            // height: '400px',
            resizable: {
                content: true,
                toolbar: true
            }
        };
        options = $.extend(true, defaultOptions, options);

        switch (options.type) {
            case 'sContent':
                // Editor 생성
                me.kendoEditor(options);

                me.self = me.data("kendoEditor");

                break;
        }

        return me;
    };


}(jQuery));



/**
 * 체크박스(CHECKBOX) 플러그인
 */
(function ($) {

    // 세팅
    $.fn.AIV_CHECKBOX = function (options) {


        options = options || {};
        var me = this;
        var id = me.attr('id');
        me.attr("type", "checkbox")
            .addClass('k-checkbox');

        if (options.checked) {
            me.attr('checked', true);
        }

        me.afterHtml = function (text) {
            $label = $('label[for=' + _id + ']');
            $span = $label.find('span[name=cnt]');
            if ($span.length > 0) {
                $span.html(text)
            }
            return me;
        }

        me.labelCss = function (options) {
            $label = $('label[for=' + _id + ']');
            $label.css(options);
            return me;
        }

        if (options.onChange) {
            $(me).change(function () {
                options.onChange();
            })
        }

        var title = ($.AIV.kFieldModel[id] && $.AIV.kFieldModel[id].title);
        me.attr('title', title);
        // me.after('<span class="k-invalid-msg" data-for="' + id + '"></span>');

        me.attr('validationMessage', title + '이(가) 필요합니다.');
        // var isRequired = me.attr('required') == 'required' ? true : false;

        // Text 세팅 (mode:read)
        var _text = '';
        _text = '<div id="aiv_forms_orgin_text_' + id + '" ></div>';
        me.before(_text);

        return me;


        var me = this;

        me.attr("type", "checkbox")
            .addClass('k-checkbox');

        var _oid = me.attr('id');
        var _id = _oid + '_' + kendo.guid(); // label의 for가 해당 체크박스를 찾도록 하기 위해 유니크 값 부여
        me.attr('id', _id);
        me.attr('oid', _oid);
        var _title = (AIV_UTIL.isEmpty(options.title)) ? "" : options.title;
        var _label = (AIV_UTIL.isEmpty(options.label)) ? "" : options.label;

        if (_title != "") {
            me.before("<div class='aiv_search_title'>" + _title + "</div>");
        }
        if (_label != "") {
            me.after('<label class="k-checkbox-label" for="' + _id + '">' + _label + '</label>');
        }

        if (options.checked) {
            me.attr('checked', true);
        }

        me.afterHtml = function (text) {
            $label = $('label[for=' + _id + ']');
            $span = $label.find('span[name=cnt]');
            if ($span.length > 0) {
                $span.html(text)
            }
            return me;
        }

        me.labelCss = function (options) {
            $label = $('label[for=' + _id + ']');
            $label.css(options);
            return me;
        }

        if (options.onChange) {
            $(me).change(function () {
                options.onChange();
            })
        }

        return this;
    };


}(jQuery));