/**
 * 그리드
 */
(function ($) {



    window.AIV_GRID = {};
    window.AIV_GRID.hasGrid = [];
    $.AIV_GRID = {};
    $.AIV_GRID.editor = {};

    var _isBindToolbarBtnClickEvent = false;    // 툴바 이벤트 바인딩 중복을 막기 위한 플래그 값.

    // 기본 설정값
    var _scrollable = true;
    var _sortable = true;
    var _filterable = {
        mode: 'row',
        operators: {
            string: {
                contains: '포함'
            },
            number: {
                gte: '이상',
                lte: '이하'
            }
        }
    };
    var _editable = false;
    var _groupable = false;
    var _columnMenu = false;
    var _pageable = {
        input: true,
        numeric: false,
        refresh: true
    };
    var _fileNameExcel = AIV_MENU.currentPageTitle + '.xlsx' || "2ch_export";
    var _fileNamePdf = AIV_MENU.currentPageTitle + '.pdf' || "2ch_export";
    var _serverPaging = true;
    var _serverFiltering = true;
    var _serverSorting = true;
    var _pageSize = 20;
    var _pageUnlimitSize = 50000;
    var _height_diff = 152;
    var _width_diff = 152;




    // 탭바꿀때마다 window를 trigger resize하여 grid resize 이벤트 함수를 실행하도록 한다.
    $(document).on('click', 'ul.nav-tabs > li.nav-item', function (e) {
        $(window).trigger('resize');
    })


    // aiv_model.js내 정의 된 kField 기본 모델 객체와 사용자 정의 객체를 병합하여 반환
    function getKFieldModel(kField, colObj) {
        var _kFieldModel = $.extend({}, $.AIV.kFieldModel[kField]); // kFieldModel 객체 복사
        var _extendsModel = $.extend(true, _kFieldModel, colObj);


        // 그리드에서 validation 중 required가 true상태면
        if (_extendsModel.validation &&
            _extendsModel.validation.required &&
            _extendsModel.validation.required.required == false) {
            _extendsModel.validation.required = undefined
        }
        return $.extend(true, _kFieldModel, colObj); // 병합하여 반환
    }

    // Grid 내 datasource{url, schema} 등 정의
    function generateDataSource(me, options) {
        var _fields = {};

        _fields['rowId'] = { editable: false, nullable: true };
        $(options.columns).each(function (i, colObj) {
            _fields[colObj.kField] = getKFieldModel(colObj.kField, colObj);
        })

        var _token = AIV_USER.getUserInfo().token;
        var _baseUrl = AIV_COMM.HOST_URL + options.apiUrl;

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: {
                    type: options.method || "POST",
                    url: _baseUrl + "/ls",
                    dataType: "json",
                    beforeSend: function (req) {
                        // req.setRequestHeader("Content-Type", "application/json");
                        // req.setRequestHeader('authorization', _token);
                        req.setRequestHeader('Accept', 'application/json');
                        req.setRequestHeader('Content-Type', 'application/json');
                        req.setRequestHeader('jwt-header', _token);
                    },
                    complete: function (xhr, status) {

                        // 반환되는 값이 없으면 success 로 가지 않기 때문에 별도로 생성해주어야 함
                        if (xhr.status == 200 && status == 'success') {
                            me.self.selectedDataItems = [];
                            // options.dataSource.read();
                        }
                    }
                },
                create: {},
                /* Deprecated */
                update: {},
                /* Deprecated */
                // destroy: {}, /* Deprecated */
                default: {
                    url: _baseUrl
                },
                /**
                 * 전송 전 파라미터를 제너레이션
                 */
                parameterMap: function (dsOptions, operation) {
                    if (operation === "read" && options.getParams) {
                        // var ds = options.dataSource;
                        var params = options.getParams();

                        // params['pageNo'] = ds._currentRangeStart || 0;
                        // params['pageSize'] = ds._pageSize || _pageSize;
                        // if ( params.reSearch == true ) {
                        // dsOptions.page= 1;
                        // }
                        if (options.method == 'GET') {
                            return $.param(params);
                        } else if (options.method == 'POST') {
                            params['pageNo'] = dsOptions.page - 1 || 0;
                            params['pageSize'] = dsOptions.pageSize || _pageSize;

                            if (dsOptions.sort) {
                                params.sort = dsOptions.sort;
                            }
                            return kendo.stringify(params);
                        }

                        // } else if (operation !== "read" && dsOptions.models) {
                        //     return kendo.stringify(dsOptions.models);
                    } else if (operation !== "read" && me.self.selectedDataItems) {
                        return kendo.stringify(me.self.selectedDataItems);
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: _fields
                },
                total: function (response) {
                    return response.pageable ? (response.totalElements || 0) : response.length;
                },
                data: function (response) {
                    // return response.pageable ? (response.content || []) : response;
                    return response.resultData; // response 된 Json 객체중 데이터 영역을 지정해줌
                }
            },
            batch: true,
            serverPaging: (options.pageable == false) ? false : true, // _serverPaging
            serverFiltering: (options.pageable == false) ? false : true, // _serverFiltering
            serverSorting: (options.pageable == false) ? false : true, // _serverSorting
            pageSize: (options.pageable != false) ? (options.pageSize || _pageSize) : _pageUnlimitSize,

            // datasource 내 data 변경시 이벤트 발생
            change: function (e) {
                // 해당 row를 찾아내어 select() 해줌
                var model = e.items[0];
                if (AIV_UTIL.isNotNull(model)) { // .AIV_GRID_TABLE에서는 model이 없을 가능성 존재
                    var tr = $('tr[data-uid=' + model.uid + ']');
                    me.self.select(tr);
                }
            },
            sort: (options.dataSource && options.dataSource.sort) || undefined,

            group: (options.dataSource && options.dataSource.group) || undefined,
            aggregate: (options.dataSource && options.dataSource.aggregate) || undefined,
            // columns: (options.dataSource && options.dataSource.columns) || undefined,
        });


        options.dataSource = dataSource;
    }

    /*
     * column width auto fit
     */
    var setAutoFitColumn = function (me, options) {
        $(options.columns).each(function (i, row) {
            if (options.autoFitColumn || row.autoFitColumn) {
                me.data("kendoGrid").autoFitColumn(row.field || i);
            }
            $(row.columns).each(function (j, row2) {
                if (options.autoFitColumn || row2.autoFitColumn) {
                    me.data("kendoGrid").autoFitColumn(row2.field || j);
                }
            })
        });
    }



    // 그리드 세팅
    $.fn.AIV_GRID = function (options) {

        var me = this;
        me.me = me;
        
        var selfId = me.attr('id');
        // AIV_GRID.hasGrid.push(selfId);

        me.options = options;
        me.originalOptions = $.extend(true, {}, options);
        me.options = options;
        me.generateDataSource = generateDataSource;

        /**
         * 읽기/쓰기 모드에 따라 edit 기능 on/off
         */
        if (options.mode == 'read' || typeof options.mode === 'undefined') {
            options.editable = false;
            $(options.columns).each(function (i, col) {
                if (col.kField == 'selectable') {
                    options.columns.splice(i, 1);    // 컬럼리스트에서 selectable 컬럼 제거
                }
            });
        } else if (options.mode == 'edit') {
            options.editable = true;
        }

        // 컬럼 정보 보완 세팅
        generateColumnList(options);

        // 데이터소스 정보 세팅
        generateDataSource(me, options);

        // filterable 값이 true이면 default값 적용
        options.filterable = options.filterable ? _filterable : false;


        // POPUP 수정옵션 확인
        if (!AIV_UTIL.isEmpty(options.popedit)) {
            if (options.popedit) {
                options.columns.push({
                    command: ["edit"],
                    title: "&nbsp;",
                    width: "50px"
                });
                options.editable = "popup";
            }
        }

        // POPUP 삭제옵션 확인
        if (!AIV_UTIL.isEmpty(options.popdelete)) {
            if (options.popdelete) {
                options.columns.push({
                    command: ["destroy"],
                    title: "&nbsp;",
                    width: "80px"
                });
                options.editable = "popup";
            }
        }

        // console.log(options.height)
        var defaultOptions = {
            // height: options.height || '90%',
            // height: undefined,
            autoBind: options.autoBind === true ? true : false,
            excel: {
                fileName: options.fileName || _fileNameExcel,
                allPages: true,
                filterable: true
            },
            excelExport: function (e) {
                var grid = this;
                $(grid.tbody[0].rows[0]).removeClass('k-state-selected'); // 엑셀 다운로드 시 첫째 항이 선택되는 버그 수정
                var sheet = e.workbook.sheets[0];

                var origin = {
                    sheet: e.workbook.sheets[0], // 원본 0번째 시트
                    columns: this.columns,
                    sender: e.sender,
                    workbook: e.workbook,
                    data: e.data
                };

                /**
                 * 컬럼 수, n-rows 컬럼, template등 적용되지 않는 문제들 해결하기 위해
                 * 아예 새롭게 sheet object 내용을 처음부터 만듦
                 * 1. columns 정의
                 * 2. header 정의
                 * 3. cell 정의
                 * 4. cell-value-template 정의
                 */

                var isDoubleHeader = false;

                // 1. column 재정의
                function reCreateColumn(_columns) {
                    var reColumns = [];
                    $(_columns).each(function (i, col1) {
                        if (col1.columns == undefined) {
                            reColumns.push({
                                field: col1.field,
                                width: col1.width || 50,
                                autoWidth: false,
                                template: col1.template,
                            });
                        } else {
                            isDoubleHeader = true;
                            // } else if (col1.columns instanceof Array) {
                            $(col1.columns).each(function (j, col2) { // 2depth-header까지 가용
                                reColumns.push({
                                    field: col2.field,
                                    width: col2.width || 50,
                                    autoWidth: false,
                                    template: col2.template,
                                });
                            });
                        }
                    });
                    return reColumns;
                };


                // 2. header 정의
                // 2중컬럼이 반영되지 않으므로, 새롭게 생성
                function reCreateHeader(_isDoubleHeader, _columns) {
                    var reHeaders = [];
                    var header1 = { type: "header", cells: [] };
                    var header2 = { type: "header", cells: [] };

                    $(_columns).each(function (i, col1) {
                        // 첫번째 헤더 cell 세팅
                        header1.cells.push({
                            background: "#7a7a7a",
                            color: "#fff",
                            value: col1.title,
                            colSpan: (col1.columns && col1.columns.length) || 1, // 하위 column이 있으면 갯수만큼 가로행 증가
                            rowSpan: (_isDoubleHeader && col1.columns == undefined) ? 2 : 1 // 이중컬럼 상태 && 하위컬럼이 없을때
                        });

                        // 두번째 헤더 cell 세팅
                        if (col1.columns == undefined) {
                            // header2.cells.push({ background: "#7a7a7a", color: "#fff", value: "", colSpan: 1, rowSpan: 1 }); // rowSpan을 2로 해주었으므로 미필요
                        } else {
                            $(col1.columns).each(function (j, col2) { // 2depth-header까지 가용
                                header2.cells.push({ background: "#7a7a7a", color: "#fff", value: col2.title, colSpan: 1, rowSpan: 1 });
                            });
                        }
                    });

                    reHeaders.push(header1);
                    if (_isDoubleHeader) {
                        reHeaders.push(header2);
                    }

                    return reHeaders;
                };

                // 3. data-cells 정의
                // template이 있는 컬럼까지 적용되도록 kendo.template 이용
                function reCreateDataCells(_datas, _columns) {

                    var rows = [];
                    for (var rowIdx = 0; rowIdx < _datas.length; rowIdx++) { // grid data-row 수만큼 loop
                        var datas = _datas[rowIdx];
                        var row = { cells: [], type: 'data' };

                        for (var colIdx = 0; colIdx < _columns.length; colIdx++) { // column 수만큼 loop
                            var $tr = $('tr[data-uid="' + grid._data[rowIdx].uid + '"]'),
                                $td = $tr.find('td:eq(' + colIdx + ')'),
                                $div = $td.find('div'),
                                $span = $td.find('span'),
                                $a = $td.find('a');
                            // 글자 색 추출 후 엑셀 글자 색으로 지정. <a> <span> <div> <td> 순으로 지정. 기본값: #000000
                            var _color = AIV_UTIL.rgb2hex($a.css('color') || $span.css('color') || $div.css('color') || $td.css('color') || 'rgba(0, 0, 0, 0)');
                            row.cells.color = _color;

                            var column = _columns[colIdx]; // 해당 셀에 해당하는 컬럼
                            var template = column.template; // 해당 컬럼에 해당하는 템플릿
                            var cellValue = datas[column.field]; // 해당 셀에 해당하는 값
                            if (template != undefined) { // 템플릿이 존재시 템플릿 적용
                                if ( column.field == 'no') {
                                    // 템플릿 적용시 내부 value인 recode를 다시 이용해서 계산하므로 최종 row+1 부터 시작함 191105. kdh.
                                    cellValue = (rowIdx+1);
                                } else {
                                    cellValue = kendo.template(template)(datas);
                                }
                                if ( typeof cellValue === 'string' ) {
                                    // template 으로 인해 html 태그 존재시 제거 실행
                                    // cellValue.replace(/(<([^>]+)>)/ig,""); // 전체 태그 제거
                                    cellValue =
                                        cellValue.replace(/<(\/span|span)([^>]*)>/gi,"")
                                            .replace(/<(\/a|a)([^>]*)>/gi,"")
                                            .replace(/<(\/div|div)([^>]*)>/gi,""); // 특정 태그 제거
                                }
                            }
                            row.cells.push({ value: cellValue });

                        }
                        rows.push(row);
                    }
                    return rows;
                };

                // 컬럼 재정의 실행
                sheet.columns = reCreateColumn(origin.columns);
                // 헤더 재정의 실행
                var headerRows = reCreateHeader(isDoubleHeader, origin.columns);
                // 셀 데이터 재정의 실행 후 추가
                var dataRows = reCreateDataCells(origin.data, sheet.columns);

                sheet.rows = headerRows.concat(dataRows);

                console.log('excel sheet > ', sheet)

            },

            pdf: {
                fileName: options.fileName || _fileNamePdf,
            },
            // dataSource: {},   // generateDataSource() 에서 별도 설정
            scrollable: _scrollable,
            sortable: _sortable,
            filterable: _filterable,
            editable: _editable,
            groupable: _groupable,
            columnMenu: _columnMenu,
            resizable: true,
            pageable: _pageable,
            columns: options.columns,

            // 0. grid.change: 그리드 내 row가 선택 될때마다 발생
            // 1. 그리드 내 데이터 변경시 dataSource.change에서 row를 선택하는 함수 실행
            // 2. 1의 선택 이벤트 이후 이하 이벤트 trigger 실행
            change: function (e) {
                var selectedRows = this.select(); // 선택된 항목 배열
                var selectedDataItems = [];
                for (var i = 0; i < selectedRows.length; i++) {
                    var dataItem = this.dataItem(selectedRows[i]);
                    selectedDataItems.push(dataItem);
                }
                me.self.selectedRows = selectedRows; // 선택된 항목을 저장
                me.self.selectedDataItems = selectedDataItems; // 선택된 항목을 dataItems 형식으로 변환한것을 저장
            },

            beforeEdit: function (e) { // 그리드 내 아이템 내용 변경전 event 발생
            },

            edit: function (e) { // 그리드 내 아이템 내용 변경시 event 발생
                var grid = this;
                if (!e.model.isNew()) {
                    $(e.sender.columns).each(function (i, column) {
                        if (column.addOnly == true) { // addOnly 일 경우 해당 input을 제거하여 수정할 수 없도록 함
                            var input = e.container.find("input[name=" + column.field + "]");
                            $(input).remove();
                        }
                    })
                }
                e.preventDefault();
                return;

            },
            dataBinding: function () {
                // No. 컬럼 순서값 바인딩
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();

                // data-bound 해제하여 memory leak 방지
                var rows = this.tbody.children();
                for (var i = 0; i < rows.length; i++) {
                    kendo.unbind(rows[i]);
                }
            }
        };

        // Toolbar 아이콘 및 클릭이벤트 설정
        var setToolbarOptions = function (options) {
            // var me = this;
            if (AIV_UTIL.isNotNull(options.toolbar)) {
                options.readToolbar = options.toolbar;  // toolbar == readToolbar의 의미이며, 미리 저장해 놓음.

                // Toolbar 아이콘 템플릿 설정
                var _toolbar = [];
                var applyToolbar = [];
                if (options.mode == 'read') {
                    applyToolbar = options.readToolbar;
                } else if (options.mode == 'edit') {
                    applyToolbar = options.editToolbar;
                } else {    // default
                    applyToolbar = options.readToolbar;
                }
                $(applyToolbar).each(function (i, tool) {
                    if (typeof tool === 'string') {
                        var name = tool;
                        if (name == 'cancel') {
                            _toolbar.push({ name: name, text: "취소" });
                        } else if (name == 'add') {
                            _toolbar.push({ name: name, text: "신규", iconClass: 'k-icon k-i-plus' });
                        } else if (name == 'edit') {
                            _toolbar.push({ name: name, text: "편집", iconClass: 'k-icon k-i-edit' });
                            // } else if (name == 'create') {
                            //     _toolbar.push({ name: name, text: "신규" });
                        } else if (name == 'save') {
                            // _toolbar.push({ name: name, text: "저장" });	// name:'save'로 저장시 kendo 기본 save 이벤트가 실행되므로 주석처리함
                            _toolbar.push({ name: 'ksave', text: "저장", iconClass: 'k-icon k-i-save' });
                        } else if (name == 'excel') {
                            _toolbar.push({ name: name, text: "엑셀다운로드" });
                        } else if (name == 'pdf') {
                            _toolbar.push({ name: name, text: "PDF다운로드" });
                        } else if (name == 'destroy') {
                            _toolbar.push({ name: name, text: "삭제" });
                        }
                    } else if (typeof tool === 'object') {
                        _toolbar.push({ name: tool.name, text: tool.text });
                    }
                });
                options.toolbar = _toolbar;


                // 툴바 이벤트 바인딩 중복을 막기 위한 플래그 값. document.on 이기 때문에 최초 한번만 해준다.
                // 이미 생성된 그리드면 실행을 막는다.
                _isBindToolbarBtnClickEvent = AIV_GRID.hasGrid.find(function(g){ return g == selfId }) == undefined ? false : true;
                if (_isBindToolbarBtnClickEvent) {
                    return;
                } else {
                    // _isBindToolbarBtnClickEvent = true;
                    AIV_GRID.hasGrid.push(selfId);

                    // Toolbar 클릭 이벤트 설정
                    $(document).on('click', '.k-grid .k-button-icontext', function (e) {
                        e.preventDefault();

                        // GRID가 여러개일 경우 > 대상 GRID와 클릭한 대상이 속한 GRID가 다르면 실행하지 않음
                        if (me.attr('id') != $(e.currentTarget).closest('.k-grid').attr('id')) {
                            return;
                        }

                        // GRID내 validation 체크
                        var validator = $(me).kendoValidator().data("kendoValidator");
                        if (me.editable && validator != undefined && !validator.validate()) {
                            return;
                        }

                        var isEditEvent = $(this).hasClass('k-grid-edit'); // 편집
                        // var isCreateEvent = $(this).hasClass('k-grid-create');
                        var isAddEvent = $(this).hasClass('k-grid-add'); // 신규
                        var isSaveEvent = $(this).hasClass('k-grid-ksave'); // 저장
                        var isCancelEvent = $(this).hasClass('k-grid-cancel-changes'); //취소
                        var isDeleteEvent = $(this).hasClass('k-grid-delete'); // 삭제


                        var baseUrl = me.self.dataSource.transport.options.default.url; // 저장된 페이지
                        var hasDirtyRow = $.grep(me.self.dataSource.view(), function (e) { return e.dirty === true; }); // 데이터 수정이 된 row
                        var allRow = me.self.dataSource.view() // 전체 row
                        // var hasCheckRow = $.grep(me.self.dataSource.view(), function (e) { return e.gridCheck === true; }); // 체크박스 체크가 된 row
                        var hasCheckRow = me.self.selectedDataItems // 체크박스 체크가 된 row
                        // var dirtyParams = kendo.stringify(hasDirtyRow);
                        // var checkParams = kendo.stringify(hasCheckRow);

                        if (isEditEvent) {    // 편집버튼 클릭 시
                            // options.readToolbar = options.toolbar;  // 현재 툴바를 읽기툴바에 저장
                            // options.toolbar = options.editToolbar;  // 편집 툴바를 현재툴바에 저장
                            var selector = me.selector; // me(grid)를 삭제할 것이므로 selector를 미리 저장


                            // 미리 지정한 event 관련 함수가 있을 경우 실행시켜줌
                            if (me.originalOptions.events) {

                                // 툴바의 편집버튼 클릭시
                                if ( me.originalOptions.events.editToolbarOnClick ) {
                                    var isCancel = me.originalOptions.events.editToolbarOnClick();  // 실행 후 true가 리턴될 경우 이후 동작을 멈춤.
                                    if ( isCancel ) {
                                        return;
                                    }
                                }
                            }

                            var datas = [];
                            $(me.self.dataSource.data()).each(function (i, data) {
                                datas.push(data.toJSON())   // data 객체 안에 value 값들 외에 다양한 kendo-grid-datasource 의 값이 있으므로, toJSON을 통해 필요 속성만 가져온다.
                            });

                            var editOptions = $.extend(true, {}, me.originalOptions);   // 기존 옵션은 그리드를 생성하면서 customizing 되었으므로, 기본 옵션을 가져온다. 
                            editOptions.mode = 'edit';

                            $(selector).kendoGrid('destroy').empty();   // 기존 그리드 삭제
                            me.me.self = null;

                            me.me = $(selector).AIV_GRID(editOptions);  // 최초  옵션으로 그리드 다시 생성
                            me.self = me.data("kendoGrid");
                            me.setData(datas);                          // 가지고 있던 데이터 입력


                        } else if (isCancelEvent) {    /** 취소버튼 클릭 시 */
                            var selector = me.selector; // me(grid)를 삭제할 것이므로 selector를 미리 저장

                            var datas = [];
                            $(me.self.dataSource.data()).each(function (i, data) {
                                datas.push(data.toJSON())   // data 객체 안에 value 값들 외에 다양한 kendo-grid-datasource 의 값이 있으므로, toJSON을 통해 필요 속성만 가져온다.
                            });

                            var readOptions = $.extend(true, {}, me.originalOptions);   // 기존 옵션은 그리드를 생성하면서 customizing 되었으므로, 기본 옵션을 가져온다. 
                            readOptions.mode = 'read';

                            $(selector).kendoGrid('destroy').empty();   // 기존 그리드 삭제
                            me.me.self = null;

                            me.me = $(selector).AIV_GRID(readOptions);  // 최초  옵션으로 그리드 다시 생성
                            me.self = me.data("kendoGrid");
                            me.setData(datas);                          // 가지고 있던 데이터 입력

                        } else if (isAddEvent) {    /** 신규버튼 클릭 시 */
                            // 신규 버튼 클릭 시 기존에 선택되어있던 항목들이 사라지는 현상이 존재하므로
                            // 기저장되어 있던 항목들을 불러내어 다시 선택함
                            var rows = me.self.selectedRows;
                            if (AIV_UTIL.isNotEmpty(rows)) {
                                for (var i = 0; i < rows.length; i++) {
                                    var uid = $(rows[i]).attr('data-uid');
                                    var row = $('tr[data-uid=' + uid + ']');
                                    me.self.select(row);
                                }
                            }

                        } else if (isSaveEvent) {   /** 저장버튼 클릭 시 */

                            if (options.api && options.api.save) {
                                var saveOptions = options.api.save;

                                if (typeof saveOptions.beforeSend == 'function') {
                                    var saveDatas = saveOptions.beforeSend(hasCheckRow);
                                    if (saveDatas.length == 0) { // 전처리의 결과로 넣을 데이터가 없어지거나 하다면 저장을 취소함
                                        return;
                                    }
                                }

                                var multiSaveType = saveOptions.url.split('/')[saveOptions.url.split('/').length - 1];  // 'ls' || 'dtl'
                                var isMultiSave = true;
                                if (multiSaveType == 'ls') { // 여러건을 한번에 저장
                                    isMultiSave = true;
                                } else if (multiSaveType == 'dtl') { // 단건을 여러번에 저장
                                    isMultiSave = false;
                                }

                                // n건 저장: method=POST, url: */ls
                                if (isMultiSave) {
                                    AIV_COMM.ajax({
                                        method: saveOptions.method || 'POST',
                                        url: saveOptions.url,
                                        data: saveDatas,
                                        async: false,
                                        success: function (responseData) {
                                            // if ( responseData.resultCode == '000' ) {
                                            alert('저장을 성공하였습니다.');
                                            if (typeof saveOptions.success == 'function') {
                                                saveOptions.success(responseData);
                                            } else {
                                                me.reload();
                                            }
                                            // }
                                        }
                                    });

                                // 1건 저장: method=POST, url: */dtl
                                } else {
                                    var saveResponseDatas = [];
                                    $(saveDatas).each(function (i, saveData) {

                                        /**
                                         *  file upload 할 대상이 존재시,
                                         *  file upload 후 응답받은 file id 등을 기존의 save 할 데이터에 담는다
                                         */
                                        if ( saveData.file ) {
                                            saveData.file.upload({
                                                gId: saveData.gId,  // gId가 기존에 있을 경우 해당 gid에 추가하게 됨
                                                gid: saveData.gId,  // gId가 기존에 있을 경우 해당 gid에 추가하게 됨
                                                idFile: saveData.idFile,  // gId와 idFile이 기존에 있을 경우 해당 id로 update하게 됨
                                                cmdTag: saveData.cmdTag || 'U',
                                                success:function(jsonData){
                                                    saveData.idFile = saveData.idFile || jsonData.idFile;
                                                    saveData.gId = saveData.gId || jsonData.gid;
                                                    saveData.sFileId = jsonData.sFileId;
                                                    saveData.gMrvFile = jsonData.gid;
                                                }
                                            })
                                        }

                                        AIV_COMM.ajax({
                                            method: saveOptions.method || 'POST',
                                            url: saveOptions.url,
                                            data: saveData,
                                            async: false,
                                            success: function (responseData) {
                                                saveResponseDatas.push(responseData);
                                            }
                                        });
                                    });
                                    alert('저장을 성공하였습니다.');
                                    if (typeof saveOptions.success == 'function') {
                                        saveOptions.success(saveResponseDatas); // 여러번 수행했으므로 배열로 리턴함
                                    }
                                }
                            }


                        } else if (isDeleteEvent) { /** 삭제버튼 클릭 시 -> 직접 삭제로 기능을 위해 별도로 로직 작성 */
                            if (hasCheckRow) {
                                if (confirm('삭제하시겠습니까?')) {
                                    if (options.api && options.api.delete) {
                                        var deleteOptions = options.api.delete;

                                        if (typeof deleteOptions.beforeSend == 'function') {
                                            var deleteDatas = deleteOptions.beforeSend(hasCheckRow);
                                            if (deleteDatas.length == 0) { // 전처리의 결과로 넣을 데이터가 없어지거나 하다면 삭제를 취소함
                                                return;
                                            }
                                        }

                                        var multiDeleteType = deleteOptions.url.split('/')[deleteOptions.url.split('/').length - 1];
                                        console.log('multiDeleteType', multiDeleteType)
                                        var isMultiDelete = true;
                                        if (multiDeleteType == 'ls') { // 여러건을 한번에 저장
                                            isMultiDelete = true;
                                        } else if (multiDeleteType == 'dtl') { // 단건을 여러번에 저장
                                            isMultiDelete = false;
                                        }

                                        // n건 저장: method=POST, url: */ls
                                        if (isMultiDelete) {
                                            AIV_COMM.ajax({
                                                method: deleteOptions.method,
                                                url: deleteOptions.url || options.url + '/ls',
                                                data: deleteDatas,
                                                success: function (responseData) {
                                                    // if ( responseData.resultCode == '000' ) {
                                                    alert('삭제를 성공하였습니다.');
                                                    if (typeof deleteOptions.success == 'function') {
                                                        deleteOptions.success(responseData);
                                                    } else {
                                                        me.reload();
                                                    }
                                                    // }
                                                }
                                            });
                                            // 1건 저장: method=POST, url: */dtl
                                        } else {
                                            var deleteResponseDatas = [];
                                            $(deleteDatas).each(function (i, deleteData) {
                                                AIV_COMM.ajax({
                                                    method: deleteOptions.method,
                                                    url: deleteOptions.url,
                                                    data: deleteData,
                                                    async: false,
                                                    success: function (responseData) {
                                                        deleteResponseDatas.push(responseData);
                                                    }
                                                });
                                            });
                                            alert('저장을 성공하였습니다.');
                                            if (typeof deleteOptions.success == 'function') {
                                                deleteOptions.success(deleteResponseDatas); // 여러번 수행했으므로 배열로 리턴함
                                            }
                                        }
                                    }
                                }
                            } else {
                                alert('삭제할 행을 선택하십시오.');
                            }
                        }
                    });
                }
            }
            return options;
        }

        setToolbarOptions(options);

        /**
         * grid row-span-merge
         */
        var rowSpanMerge = function (e) {
            var items = e.sender.dataSource.view();
            var groupCell = [];
            // var rows = items
            var rows = (items[0] && items[0].items) ? items[0].items : items;   // groupTemplate 넣을 경우 items가 한번 더 감싸짐

            // items를 돌면서 수행
            $(rows).each(function (i, item) {
                // items을 tr 객체로 변환
                var row = $('tr[data-uid=' + item.uid + ']');

                // tr 내 td 객체를 돌면서 수행
                $(row).find('td').each(function (cIdx, td) {

                    // 해당 td가 rowspan 속성이 있는지 확인
                    if ($(td).hasClass('table-cell-rowspan')) {

                        // 이미 저장한 index가 있는지 확인
                        var hasColumn = groupCell.find(function (g) {
                            return g.columnIndex == cIdx;
                        });

                        if (hasColumn) {  // 있을 경우 해당 컬럼 내 리스트에 추가
                            hasColumn.cells.push(td);
                        } else {            // 없을 경우 신규 컬럼 추가
                            groupCell.push({ 'columnIndex': cIdx, 'cells': [td] });
                        }
                    }
                })
            });

            $(groupCell).each(function (i, group) {
                var headCell = null;
                // var columnIndex = group.columnIndex;
                var length = 0;
                $(group.cells).each(function (rIdx, cell) {
                    if (rIdx == 0) {  // row index 가 0이면 해당 셀을 headcell로 지정.
                        headCell = cell;
                        $(headCell).attr('rowspan', ++length);
                    } else {
                        if (headCell.innerHTML == cell.innerHTML) {    // 바로 전 cell과 비교하여 값이 같으면 merge && hide
                            $(headCell).attr('rowspan', ++length);
                            $(cell).css('display', 'none');
                        } else {    // 바로 전 cell과 비교하여 값이 다르면 head cell을 다시 생성
                            headCell = cell;
                            length = 0;
                            $(headCell).attr('rowspan', ++length);
                        }
                    }
                });
            });
        };

        // 기본설정옵션과 사용자정의 옵션을 merge한다.
        var mergeOptions = $.extend(true, defaultOptions, options);

        // 직접 설정한 dataBound 함수를 실행한 후에 span merge를 실행하도록 dataBound를 변경해준다.
        mergeOptions.dataBound = function (e) {
            rowSpanMerge(e);
            // me.self = me.data('kendoGrid');
            if (me.options.height == undefined) {
                me.gridResize();
            }
            if (typeof options.dataBound === 'function') {
                options.dataBound(e);
            }
        }

        // 기본옵션값 및 사용자 옵션값 병합 + 그리드 세팅
        me.kendoGrid(mergeOptions);
        me.self = me.data("kendoGrid");




        // 현재 그리드가 편집모드면 true
        me.isEditMode = function(){
            return $(me).find('thead tr:first th:first input[type=checkbox]').length > 0 ? true : false;  // 전체선택체크박스가 있는 체크
        }

        // 그리드 크기 조절..
        me.gridResize = function () {

            // 스크롤이 생겼을때만 조정해줌
            // if ( me.find('.k-grid-content').hasScrollBar() ) {
            if ($(document).height() > $(window).height()) {
                _height_diff = me.offset().top + 20;
                _width_diff = me.offset().left + 200;

                // 높이만 조정해준다.
                var resize_height = AIV_UTIL.getClientSize().height - _height_diff;
                var resize_width = AIV_UTIL.getClientSize().width - _width_diff;

                var opts = me.self.getOptions();
                opts.height = resize_height;

                // me.self.setOptions(opts);
                me.height(resize_height);
                me.self.resize();
            }
        };

        /**
         *  엑셀 저장요청
         */
        me.excel = function (options) {
            // 엑셀 세팅..
            me.self.options.excel = {
                fileName: options.fileName || _fileNameExcel,
                allPages: true,
                filterable: true
            };

            me.self.saveAsExcel();
        };

        // 데이터 업데이트
        me.setData = function (list) {
            // me.data('kendoGrid').dataSource.data([]);
            // me.data('kendoGrid').dataSource.add(list);
            me.data('kendoGrid').dataSource.data(list);
        }

        // 데이터 리로드
        me.reload = function (list) {
            me.data('kendoGrid').dataSource.read();
        }

        // set grid column after initialize
        me.setColumns = function (columns) {
            var _options = { columns: columns }
            generateColumnList(_options);
            var _columns = _options.columns; // kField generate
            var grid = me.self;
            var options = grid.getOptions();
            options.columns = _columns;
            grid.setOptions(options);
        }

        /*
         * 항목 전체 선택 체크박스 이벤트 세팅
         */
        me.checkAllBtnEvent = function () {
            $gridCheck = me.find('input[aria-label="Select all rows"]');

            if ($gridCheck.length > 0) {
                $gridCheck.on('click', function (e, me) {
                    // e.preventDefault();
                    checkboxChange(e, me, true);
                    return;
                })
            }

        }

        // --------------------------------------------------------------------------------------------------------------
        // 그리드 공통 초기 실행 작업
        // --------------------------------------------------------------------------------------------------------------
        if (AIV_UTIL.isNull(options.height)) {
            // 초기 그리드크기조절 함수 호출
            // me.gridResize();
            // 윈도우 사이즈가 변경되었을때...
            $(window).resize(function () {
                me.gridResize();
            });
        }

        // Column 사이즈 세팅
        setAutoFitColumn(me, options);

        // 항목 전체 선택 체크박스 이벤트 세팅
        me.checkAllBtnEvent();

        // 항목 선택 체크박스 이벤트 세팅
        // 선택시: aria-label="Deselect row"
        // 해제시: aria-label="Select row"
        $(document).on('change', 'input[aria-label="Select row"],input[aria-label="Deselect row"]', function (e) {
            // e.preventDefault();
            checkboxChange(e, me);
        });

        me.setScrollTopByData = setScrollTopByData;
        me.setTestData = setTestData;


        if (kendo.pdf != undefined) {
            kendo.pdf.defineFont({
                // "Nanum Gothic"             : "/assets/font/NanumGothic.ttf",
                "Nanum Gothic": "//themes.googleusercontent.com/static/fonts/earlyaccess/nanumgothic/v4/NanumGothic-Regular.ttf",
                "Nanum Gothic|Bold": "//themes.googleusercontent.com/static/fonts/earlyaccess/nanumgothic/v4/NanumGothic-Bold.ttf",
                // "Nanum Gothic|Bold|Italic" : "",
                // "Nanum Gothic|Italic"      : "//themes.googleusercontent.com/static/fonts/earlyaccess/nanumgothic/v4/NanumGothic-ExtraBold.ttf",
                // "WebComponentsIcons"      : "f"
            });
        }

        return me;
    };

    /**
     *  그리드 세팅
     */
    $.fn.AIV_GRID_TABLE = function (options) {
        var me = this;
        me.options = options;
        me.generateDataSource = generateDataSource;

        var defaultOptions = {
            dataSource: {
                type: 'json',
                pageable: false
            },
            // height: options.height || '90%',
            sortable: false,
            // autoBind: true
            autoBind: options.apiUrl == undefined ? false : true // API URL이 존재시 autuBind true, 없으면 false 가 default
        };


        // 컬럼 정보 보완 세팅
        options = $.extend(true, defaultOptions, options);
        generateColumnList(options);

        // 데이터소스 정보 세팅
        generateDataSource(me, options);

        me.kendoGrid(options);

        me.self = me.data("kendoGrid");

        setAutoFitColumn(me, options);

        // set grid column after initialize
        me.setColumns = function (columns) {
            var _options = { columns: columns }
            generateColumnList(_options);
            var _columns = _options.columns; // kField generate
            var grid = me.self;
            var options = grid.getOptions();
            options.columns = _columns;
            grid.setOptions(options);
        }

        me.setData = function (list) {
            me.data('kendoGrid').dataSource.data(list);
        }

        // 데이터 리로드
        me.reload = function (list) {
            me.data('kendoGrid').dataSource.read();
        }

        me.setScrollTopByData = setScrollTopByData;
        me.setTestData = setTestData;

        if (kendo.pdf != undefined) {
            kendo.pdf.defineFont({
                // "Nanum Gothic"             : "/assets/font/NanumGothic.ttf",
                "Nanum Gothic": "//themes.googleusercontent.com/static/fonts/earlyaccess/nanumgothic/v4/NanumGothic-Regular.ttf",
                "Nanum Gothic|Bold": "//themes.googleusercontent.com/static/fonts/earlyaccess/nanumgothic/v4/NanumGothic-Bold.ttf",
                // "Nanum Gothic|Bold|Italic" : "",
                // "Nanum Gothic|Italic"      : "//themes.googleusercontent.com/static/fonts/earlyaccess/nanumgothic/v4/NanumGothic-ExtraBold.ttf",
                // "WebComponentsIcons"      : "f"
            });
        }

        return me;
    }

    /**
     * 컬럼정의 리뉴얼
     */
    function generateColumnList(options) {
        /**
         * kField 정리
         */
        $.each(options.columns, function (index, colObj) {
            // 기존에 정의되어 있던 컬럼의 경우 자동으로 속성을 넣어줌
            var depth1 = options.columns[index];
            if (AIV_UTIL.isNotNull(colObj.kField)) {
                options.columns[index] = getKFieldModel(colObj.kField, colObj);
            }
            setTextAlignClass(options.columns[index]);
            if (options.columns[index].editable == true || (options.editable == true && options.columns[index].editable == undefined)) {
                if (options.columns[index].addOnly == true) { // 입력가능 + 수정불가
                } else {
                    $.extend(options.columns[index].attributes, { // 입력가능 + 수정가능
                        'class': 'editable-cell' + ' ' + options.columns[index].attributes.class
                    });
                }
            } else if (typeof options.columns[index].editable === 'function') {
                $.extend(options.columns[index].attributes, { // 입력가능 + 수정가능
                    'class': 'editable-cell' + ' ' + options.columns[index].attributes.class
                });
            } else {
                options.columns[index].editable = function () { return false; }
            }

            $.each(colObj.columns, function (index2, colObj2) {
                var depth2 = options.columns[index].columns[index2];
                if (AIV_UTIL.isNotNull(colObj2.kField)) {
                    options.columns[index].columns[index2] = getKFieldModel(colObj2.kField, colObj2);
                }
                setTextAlignClass(options.columns[index].columns[index2]);
                if (options.columns[index].columns[index2].editable == true || (options.editable == true && options.columns[index].columns[index2].editable == undefined)) {
                    if (options.columns[index].columns[index2].addOnly == true) { // 입력가능 + 수정불가
                    } else {
                        $.extend(options.columns[index].columns[index2].attributes, { // 입력가능 + 수정가능
                            'class': 'editable-cell' + ' ' + options.columns[index].columns[index2].attributes.class
                        });
                    }
                } else if (typeof options.columns[index].editable === 'function') {
                    $.extend(options.columns[index].attributes, { // 입력가능 + 수정가능
                        'class': 'editable-cell' + ' ' + options.columns[index].attributes.class
                    });
                } else {
                    options.columns[index].columns[index2].editable = function () { return false; }
                }
            });

            // 명령형 컬럼이 있을경우...
            if (typeof (colObj.command) != "undefined") {
                var command = colObj.command;
                if (typeof (command.type) != "undefined") {
                    if (command.type == "_POPEDIT") {
                        colObj.command = ["edit"];

                    } else if (command.type == "_BTN_CUSTOM") {
                        colObj.command = { text: command.title, click: command.click };
                    }
                }
            }
        });

        // 기본 text-align-class 설정
        function setTextAlignClass(col) {
            col.attributes = col.attributes || {};
            if (col.type == 'number') {
                col.attributes.class = col.attributes.class || "text-right";
            } else {
                col.attributes.class = col.attributes.class || "text-center";
            }
            if (col.rowspan == true) {
                col.attributes.class += ' table-cell-rowspan';
            }
        }
    }

    /**
     * 체크박스 클릭시 동작 설정 함수
     * @param {Click-Event} e
     * @param {Kendo-Grid-Object} me 
     * @param {All-checkbox-boolean} allFlag 
     */
    function checkboxChange(e, me, allFlag) {
        // e.preventDefault();
        var time = (new Date()).getTime();
        var isChecked = $(e.target).is(':checked');

        // case: 전체 체크박스
        if (allFlag) {

            // case: 미 체크 -> 체크
            if (isChecked) {
                // var me = $(e.target).closest('div.k-grid');
                // me.self = me.data('kendoGrid');

                // 1. datasource 내 전체 데이터에 grid Check 를 true로 설정
                // 2. grid row 내 체크 박스 모두 체크 설정
                // var views = me.self.dataSource.view();
                // $(views).each(function(i, view){
                //     view.gridCheck = true;          // 1
                //     $('tr[data-uid='+view.uid+']')  // 2
                //         .find('input[name=gridCheck]')
                //         .prop('checked', true);
                // });

                // case: 체크 -> 체크해제
            } else {
                var me = $(e.target).closest('div.k-grid');
                me.self = me.data('kendoGrid');
                var dataSource = me.self.dataSource;

                // dirty 가 true 인 row 만 필터
                var hasDirtyRow = $.grep(dataSource.view(), function (e) { return e.dirty === true; });

                // dirty(변경된) row에 대해서 되돌림
                $(hasDirtyRow).each(function (i, view) {
                    var dataItems = me.self.dataItem($('tr[data-uid=' + view.uid + ']'));
                    dataSource.cancelChanges(dataItems); // 원래 데이터 값으로 복구
                    var target = $('tr[data-uid=' + view.uid + ']');
                    kRowCancelChange(target, me); // 원래 템플릿으로 복구
                });

                // me.self.dataSource.fetch();    // data bound 되어 있는 경우 실행
                var fn_dataBound = me.self.options.fn_dataBound
                if (typeof fn_dataBound === 'function') {
                    fn_dataBound(hasDirtyRow); // fetch()의 경우 전체 실행되므로 dirty였던 행만 실행
                }

                // 1. datasource 내 전체 데이터에 grid Check 를 false로 설정
                // 2. grid row 내 체크 박스 모두 체크 해제 설정
                // var views = me.self.dataSource.view();
                // $(views).each(function(i, view){
                //     view.gridCheck = false;          // 1
                //     $('tr[data-uid='+view.uid+']')  // 2
                //         .find('input[name=gridCheck]')
                //         .prop('checked', false);
                // });
                me.self.clearSelection(); // 2
            }

            // case: row 체크박스
        } else {
            var me = $(e.target).closest('div.k-grid');
            me.self = me.data('kendoGrid');
            
            // 해당 row의 dataItems
            var dataSource = me.self.dataSource;
            // var row = $(e.currentTarget).closest("tr");
            var row = $(e.currentTarget).closest("tr");
            var dataItems = me.self.dataItem(row);

            // case: 미 체크 -> 체크
            if (isChecked) {
                // $(e.target).prop('checked', true);
                // dataItems.dirty = true;
                // dataItems.gridCheck = true;

                // case: 체크 -> 체크해제
            } else {

                // 신규였던경우 -> 제거
                if (AIV_UTIL.isEmpty(dataItems.id)) {
                    // me.self.removeRow( row );

                    // 수정되었던경우 -> 복구
                } else if (dataItems.dirty) {
                    dataSource.cancelChanges(dataItems); // 원래 데이터 값으로 복구
                    kRowCancelChange(e.currentTarget, me); // 원래 템플릿으로 복구
                    // me.self.dataSource.fetch();    // data bound 되어 있는 경우 실행(전체row)
                    var fn_dataBound = me.self.options.fn_dataBound
                    if (typeof fn_dataBound === 'function') {
                        fn_dataBound([dataItems]); // fetch()의 경우 전체 실행되므로 dirty였던 행만 실행
                    }

                    // 수정안되었던경우 -> gridCheck 만 다시 false로 해제
                } else {
                    // dataItems.gridCheck = false;
                }
            }
        }
    }

    /**
     * grid 내 한 row만 초기화 함 ( checkbox 연동 싱크의 문제로 작성됨 )
     */
    function kRowCancelChange(target, me) {
        var grid = me.getKendoGrid();
        var rowIndex = $(target).closest("tr").index();
        var row = me.getKendoGrid().items().eq(rowIndex);
        var dataItem = grid.dataItem(row);

        var rowChildren = $(row).children('td[role="gridcell"]');

        for (var i = 0; i < grid.columns.length; i++) {

            var column = grid.columns[i];
            var template = column.template;
            var cell = rowChildren.eq(i);

            if (template !== undefined) {
                var kendoTemplate = kendo.template(template);

                // Render using template
                cell.html(kendoTemplate(dataItem));
            } else {
                var fieldValue = dataItem[column.field];

                var format = column.format;
                var values = column.values;

                if (values !== undefined && values != null) {
                    // use the text value mappings (for enums)
                    for (var j = 0; j < values.length; j++) {
                        var value = values[j];
                        if (value.value == fieldValue) {
                            cell.html(value.text);
                            break;
                        }
                    }
                } else if (format !== undefined) {
                    // use the format
                    cell.html(kendo.format(format, fieldValue));
                } else {
                    // Just dump the plain old value
                    cell.html(fieldValue);
                }
            }
        }

    }


    /**
     * data 와 같은 데이터 속성을 가지고 있는 row를 찾아 scroll을 이동함
     * @param {*} data : ex) {eventDt: 2018-07-19}
     */
    function setScrollTopByData(data) {
        // offset.top의 div 내 상대 위치를 찾기 위해 position:relative css속성 부여
        $('.k-grid-content table').css('position', 'relative');

        var me = this;
        var grid = me.self;
        var views = grid.dataSource.view();
        var view = $.grep(views, function (map) {
            var _key = Object.keys(data)[0];
            // var _value = Object.values(data)[0];
            var _value = data[_key];
            return map[_key] === _value;
        })


        if (view.length > 0) {
            var row = $('tr[data-uid=' + view[0].uid + ']');
            var top = row.position().top;

            grid.element.find(".k-grid-content").animate({ // use $('html, body') if you want to scroll the body and not the k-grid-content div
                scrollTop: top
            }, 100);

            $(row).css('background-color', 'rgba(0,0,0,0.15)') // 해당 row의 bg-color 변경
                .siblings().css('background-color', ''); // 다른 row의 bg-color 초기화
        }
    }

    /**
     * Deprecated
     * @param {*} type : ex) routeNo(직접 참조시) 혹은 routes.routeNo(배열 속 참조시)
     * @param {*} dataSources: 그리드의 dataSources. ex) dataBound: function(e) { e.sender.dataSource.view() } 혹은 me.self.dataSource.view()
     */
    function setDropDownList(type, dataSources) {
        var datas = [];
        var ds = [];
        if (type.split('.').length > 1) {
            var upper = type.split('.')[0];
            var lower = type.split('.')[1];
            $(dataSources).each(function (i, row) {
                $(row[upper]).each(function (j, arr) {
                    var value = arr[lower];
                    setDatas(datas, value)
                })
            })
            ds = getDataSources(datas);
            PAGE.params[lower].self.setDataSource(ds);
        } else {
            $(dataSources).each(function (i, row) {
                var value = row[type];
                setDatas(datas, value)
            })
            ds = getDataSources(datas);
            PAGE.params[type].self.setDataSource(ds);
        }

        function setDatas(datas, value) {
            if (AIV_UTIL.isNotEmpty(value) && datas.indexOf(value) < 0) {
                datas.push(value);
            }
        }

        function getDataSources(datas) {
            var _ds = [];
            $(datas).each(function (i, value) {
                _ds.push({ text: value, value: value })
            })
            return _ds;
        }
    }

    /**
     * 테스트용 데이터 세팅 임시 함수
     * @param {*} data : ex) {eventDt: 2018-07-19}
     */
    function setTestData(testSize) {
        grid.dataSource.data([]);
    }

}(jQuery));