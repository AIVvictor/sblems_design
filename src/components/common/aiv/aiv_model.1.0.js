/**
 * 그리드
 */
(function ($) {

    $.AIV = $.AIV || {};
    var AIV_MODEL = AIV_MODEL || {};
    //에딧상태에서 날짜 텍스트 드롭다운 등등
    var editor = (function () {

        return {
            // Y or N
            yn: function (container, options) {

                $('<input name="name" data-text-field="text" data-required-msg="필수 값 입니다." data-value-field="value" data-bind="value:' + options.field + '" required="required" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        optionLabel: false,
                        dataSource: {
                            data: [
                                { text: "Y", value: "Y" },
                                { text: "N", value: "N" }
                            ]
                        },
                        index: 0
                    });
                $('<span class="k-invalid-msg" data-for="name"></span>').appendTo(container);
            },

            // date picker
            datepicker: function (container, options) {
                $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:' + options.field + '" required="required" />')
                    .appendTo(container)
                    .AIV_DATETIMEPICKER({
                        type: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            var de = new Date(this.value()).format('yyyy-mm-dd');
                            dataItem[options.field] = de;
                            // dataItem.fctryCd = this.value();
                            // dataItem.fctryNm = this.text();
                        }
                    });

                return;
            },

            // factory dropdownlist
            fctry: function (container, options) {
                $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:fctryCd" required="required" />')
                    .appendTo(container)
                    .AIV_DROPDOWNLIST({
                        type: 0,
                        nowrap: true,
                        width: '100%',
                        allYn: false,
                        onChange: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            dataItem.fctryCd = this.value();
                            dataItem.fctryNm = this.text();
                        }
                    });

                return;
            },

            // vhcleTy dropdowntree
            vhcleTy: function (container, options) {
                $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:vhcleTy" required="required" />')
                    .appendTo(container)
                    .AIV_DROPDOWNLIST({
                        type: 5,
                        nowrap: true,
                        width: '90%',
                        allYn: false,
                        onChange: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            dataItem.vhcleTy = this.value();
                        }
                    });
            },

            // caralcTy dropdowntree
            caralcTy: function (container, options) {
                $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:caralcTy" required="required" />')
                    .appendTo(container)
                    .AIV_DROPDOWNLIST({
                        type: 4,
                        nowrap: true,
                        width: '90%',
                        allYn: false,
                        onChange: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            dataItem.caralcTy = this.value();
                            dataItem.caralcTyNm = this.text();
                        }
                    });
            },

            // 약관구분 dropdownlist
            stplatGb: function (container, options) {
                $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:stplatGb" required="required" />')
                    .appendTo(container)
                    .AIV_DROPDOWNLIST({
                        type: 97,
                        nowrap: true,
                        width: '98%',
                        allYn: false,
                        onChange: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            dataItem.stplatGb = this.value();
                            dataItem.stplatGbNm = this.text();
                        }
                    });

                return;
            },
            // 약관구분 dropdownlist
            regType: function (container, options) {
                $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:regType" required="required" />')
                    .appendTo(container)
                    .AIV_DROPDOWNLIST({
                        type: 97,
                        nowrap: true,
                        width: '98%',
                        allYn: false,
                        onChange: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            dataItem.regType = this.value();
                            dataItem.regTypeNm = this.text();
                        }
                    });

                return;
            },

            // warehouse dropdowntree
            wrhous: function (container, options) {


                $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:setupLc" required="required" />')
                    // $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:wrhousCd" required="required" />')
                    .appendTo(container)
                    .AIV_DROPDOWNLIST({
                        type: 3,
                        nowrap: true,
                        width: '98%',
                        allYn: false,
                        onChange: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            // dataItem.wrhousCd = this.value();
                            dataItem.setupLc = this.value();
                            dataItem.wrhousNm = this.text();
                        },
                    });
            },

            // 지연사유 dropdownlist
            delayResn: function (container, options) {

                var reasonDataList = [
                    { text: '상차완료(공장대기)', value: '상차완료(공장대기)' },
                    { text: '관제오류(통문정상)', value: '관제오류(통문정상)' },
                    { text: '다회전 차량 출발시간 오류', value: '다회전 차량 출발시간 오류' },
                    { text: '접착시각 지연(기사)', value: '접착시각 지연(기사)' },
                    { text: '이고지연(양주)', value: '이고지연(양주)' },
                    { text: '이고지연(용인)', value: '이고지연(용인)' },
                    { text: '이고지연(안산)', value: '이고지연(안산)' },
                    { text: '이고지연(거창)', value: '이고지연(거창)' },
                    { text: '특수거래처 실제 상차시간 변경', value: '특수거래처 실제 상차시간 변경' },
                    { text: '일반 및 급식 혼용출하', value: '일반 및 급식 혼용출하' },
                    { text: '물량증가(상차지연)', value: '물량증가(상차지연)' },
                    { text: '물량감소(고객센터 증가)', value: '물량감소(고객센터 증가)' },
                    { text: '출발예정시간 오류(밴더출하)', value: '출발예정시간 오류(밴더출하)' },
                    { text: '거래명세서 출력지연', value: '거래명세서 출력지연' },
                    { text: '재고조사', value: '재고조사' },
                    { text: '기타', value: '기타' },
                    { text: '물류자동화설비 고장(거창)', value: '물류자동화설비 고장(거창)' },
                    { text: '차순 내 노선 증가(거창)', value: '차순 내 노선 증가(거창)' },
                    { text: '입력 오류', value: '입력 오류' },
                    { text: '차량고장(결행)', value: '차량고장(결행)' },
                ];

                $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:delayResn" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "value",
                        optionLabel: false,
                        autoWidth: true,
                        dataSource: {
                            data: reasonDataList
                        },
                        index: 0
                    });
                $('<span class="k-invalid-msg" data-for="name"></span>').appendTo(container);
            },

            stdTime: function (container, options) {
                // me.attr('type', 'numeric');
                $('<input name="name" data-text-field="text" data-value-field="value" data-bind="value:' + options.field + '" />')
                    .appendTo(container)
                    .kendoNumericTextBox();
            },

            // Y or N
            nYearGasGoalSumMonth: function (container, options) {
                $('<input name="name" data-text-field="number" data-value-field="value" data-bind="value:' + options.field + '" />')
                    .appendTo(container)
                    .kendoNumericTextBox({
                        format: '#.000',
                        step: 0.001,
                        decimals: 3,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);

                            // dataItem.wrhousCd = this.value();
                            var sum = Number(dataItem.m1) + Number(dataItem.m2) + Number(dataItem.m3) + Number(dataItem.m4) + Number(dataItem.m5) + Number(dataItem.m6)
                                + Number(dataItem.m7) + Number(dataItem.m8) + Number(dataItem.m9) + Number(dataItem.m10) + Number(dataItem.m11) + Number(dataItem.m12);
                            sum = (sum).toFixed(3)
                            dataItem.nYearGasGoalSum = sum;
                        },
                    });
            },

            // no used
            /*
            // File Upload
            sFileNm: function (container, options) {
                // $('<input name="file" type="file" data-bind="value:' + options.field + '" />')
                $('<input name="file" type="file" data-bind="value:sFileId" />')
                    .appendTo(container)
                    .AIV_UPLOAD();
            },
            */

            nEngEtcUntCd: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.UNT;

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:nEngEtcUntCd" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log(e, this, dataItem)
                            dataItem.nEngEtcUntCd = this.value();
                            dataItem.nEngEtcUntCdNm = this.text();
                        },
                    });
                $('<span class="k-invalid-msg" data-for="name"></span>').appendTo(container);
            },

            sEngCdNm: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.ENG;

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:sEngCd" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log(e, this, dataItem)
                            dataItem.sEngCd = this.value();
                            dataItem.sEngCdNm = this.text();
                        },
                    });
                $('<span class="k-invalid-msg" data-for="name"></span>').appendTo(container);
            },

            sMrvengCdNm: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.MRVENG;

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:sMrvengCd" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log(e, this, dataItem)
                            dataItem.sMrvengCd = this.value();
                            dataItem.sMrvengCdNm = this.text();
                        },
                    });
                $('<span class="k-invalid-msg" data-for="name"></span>').appendTo(container);
            },

            sInUntCdNm: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.UNT;
                if (options && options.emptyValue == true) {
                    codes = [{ key: '', value: '', }].concat(codes);
                }

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:sInUntCd" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log(e, this, dataItem)
                            dataItem.sInUntCd = this.value();
                            dataItem.sInUntCdNm = this.text();
                        },
                    });
                $('<span class="k-invalid-msg" data-for="name"></span>').appendTo(container);
            },

            sCalUntCdNm: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.UNT;

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:sCalUntCd" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log(e, this, dataItem)
                            dataItem.sCalUntCd = this.value();
                            dataItem.sCalUntCdNm = this.text();
                        },
                    });
                $('<span class="k-invalid-msg" data-for="name"></span>').appendTo(container);
            },

            sEmiCdNm: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.EMI;

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:sEmiCd" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            dataItem.sEmiCd = this.value();
                            dataItem.sEmiNm = this.text();
                            dataItem.sEmiCdNm = this.text();
                        },
                    });
                $('<span class="k-invalid-msg" data-for="name"></span>').appendTo(container);
            },

            sGhgCd: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.GHG;

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:sGhgCd" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            dataItem.sGhgCd = this.value();
                            dataItem.sGhgCdNm = this.text();
                        },
                    });
                $('<span class="k-invalid-msg" data-for="name"></span>').appendTo(container);
            },

            cYear: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.YYYY;

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:cYear" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log(e, this, dataItem)
                            dataItem.cYear = this.value();
                            dataItem.cYear = this.text();
                        },
                    });
                $('<span class="k-invalid-msg" data-for="name"></span>').appendTo(container);
            },

            cYm: function (container, options) {
                var yyyy = AIV_CONTROLS.COMM_CODES.YYYY;
                var mm = AIV_CONTROLS.COMM_CODES.MM;
                var codes = [];
                for (var i = 0; i < yyyy.length; i++) {
                    for (var k = 0; k < mm.length; k++) {
                        if (mm[k].value.length == 1) {
                            mm[k].value = '0' + mm[k].value;
                        }
                        codes.push(
                            { key: yyyy[i].value + '-' + mm[k].value, value: yyyy[i].value + '-' + mm[k].value }
                        )
                    }
                }
                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:cYm" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        // autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log(e, this, dataItem)
                            dataItem.cYm = this.value();
                            dataItem.cYm = this.text();
                        },
                    });
            },
            sAuthType: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.AUTH;

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:sAuthType" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        // autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log(e, this, dataItem)
                            dataItem.sAuthType = this.value();
                            dataItem.sAuthType = this.text();
                        },
                    });
            },
            sSiteNm: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.SITE;

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:idSite" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        // autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log(e, this, dataItem)
                            dataItem.idSite = this.value();
                            dataItem.sSiteNm = this.text();
                        },
                    });
            },
            sDeptNm: function (container, options) {
                /*
                // dropdown-tree list
                var codes = AIV_MODEL.ERP_DEPT_CODES;
                if ( codes && codes.length > 0 ) {

                } else {
                    codes = [];
                    var deptList = [];
                    AIV_COMM.ajax({
                        method: 'GET',
                        async: false,
                        url: '/api/v1/common/erp/dept/ls',
                        data: {
                            sDeptId: '' // 전체 조직도
                        },
                        success: function (responseData) {
                            deptList = responseData.resultData;
                            AIV_CONTROLS.COMM_CODES.DEPT_LIST = deptList;
                        }
                    });

                    function setDept(codes, idErpDept, dept, deptArrIdx){
                        // console.log(codes, idErpDept, deptArr, deptArrIdx);
                        var deptArr = dept.split(' > ');
                        var deptArrLen = deptArr.length;
                        var deptNm = deptArr[deptArrIdx];
                        var hasCodes = codes.find(function(c){ return c.value == deptNm; });
                        if (hasCodes) {
                            setDept(hasCodes.items, idErpDept, dept, ++deptArrIdx);
                        } else if ( deptArrIdx < deptArrLen-1 ) {
                            hasCodes = { 'value':deptNm, expanded: true, 'items':[] };
                            codes.push(hasCodes);
                            setDept(hasCodes.items, idErpDept, dept, ++deptArrIdx);
                        } else {
                            hasCodes = { 'key':idErpDept, 'value':deptNm };
                            codes.push(hasCodes);
                        }
                    };
                    
                    $(deptList).each(function(i, dept){
                        setDept(codes, i, dept, 0)
                    });
                    window.AIV_MODEL.ERP_DEPT_CODES = codes;
                }

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:idErpDept" />')
                    .appendTo(container)
                    .kendoDropDownTree({
                    // .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        filter: 'contains',
                        // autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            dataItem.idErpDept = this.value();
                            dataItem.sDeptNm = this.text();
                        },
                    });
                */

                // dropdown list
                var codes = AIV_MODEL.ERP_DEPT_CODES;
                if (codes && codes.length > 0) {
                } else {
                    codes = [];
                    AIV_COMM.ajax({
                        method: 'GET',
                        async: false,
                        url: '/api/v1/common/erp/dept/ls',
                        data: {
                            sDeptId: ENV_PROP.sDeptId // 전체 조직도
                        },
                        success: function (responseData) {
                            var deptList = responseData.resultData;
                            $(deptList).each(function (i, dept) {
                                codes.push({ 'key': dept.idErpDept, 'value': dept.sFullDeptNm });
                            });
                            AIV_MODEL.ERP_DEPT_CODES = codes;
                        }
                    });
                }
                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:idErpDept" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        filter: 'contains',
                        // autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            dataItem.idErpDept = this.value();
                            // dataItem.sDeptId = this.value();
                            dataItem.sDeptNm = this.text();
                        },
                    });

            },
            sMenuNm: function (container, options) {
                var codes = [];
                AIV_COMM.ajax({
                    method: "GET",
                    url: "/api/v1/common/menu/all",
                    success: function (responseData) {
                        $(responseData.resultData).each(function (i, row) {
                            codes.push({ key: row.sMenuNm, value: row.sMenuNm });
                        })
                    }
                })
                console.log('codes', codes);

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:sMenuNm" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        // autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log(e, this, dataItem)
                            dataItem.sMenuNm = this.value();
                            dataItem.sMenuNm = this.text();
                        },
                    });
            },
            sStatusNm: function (container, options) {
                var codes = AIV_CONTROLS.COMM_CODES.EMP_STAT;

                $('<input name="name" data-text-field="value" data-value-field="key" data-bind="value:sStatusCd" />')
                    .appendTo(container)
                    .kendoDropDownList({
                        dataValueField: "key",
                        dataTextField: "value",
                        optionLabel: false,
                        filter: 'contains',
                        // autoWidth: true,
                        dataSource: {
                            data: codes
                        },
                        // index: 0,
                        change: function (e) {
                            var grid = $(e.sender.element).closest('.k-grid').data('kendoGrid');
                            var row = $(e.sender.element).closest("tr");
                            var dataItem = grid.dataItem(row);
                            console.log('dataItem', dataItem);
                            dataItem.sStatusCd = this.value();
                            dataItem.sStatusNm = this.text();
                        },
                    });
            },
        }
    })();
    //포맷에 맞게 변경 ex6000초를 시분으로
    var template = (function () {
        return {
            integer: function (kField) {
                return function (dataItem) {
                    var dt = dataItem[kField];
                    if (dt == undefined || dt == '') {
                        return '';
                    } else {
                        return kendo.toString(dt, "n0");;
                    }
                }
            },
            float: function (kField) {
                return function (dataItem) {
                    var dt = dataItem[kField];
                    if (dt == undefined || dt == '') {
                        return '';
                    } else {
                        return kendo.toString(dt, "n3");;
                    }
                }
            },
            yyyymmddhhmiss: function (kField) {
                return function (dataItem) {
                    var dt = dataItem[kField];
                    if (dt == undefined || dt == '') {
                        return '';
                    } else {
                        dt = dt.replace('+0000', '+0900');  // GMT 보정
                        return (new Date(dt)).format('yyyy-mm-dd hh:mi:ss');
                    }
                }
            },
            /*
            yyyymmddhhmi: function(kField) {
                return function(dataItem) {
                    var dt = dataItem[kField];
                    if (dt == undefined || dt == '') {
                        return '';
                    } else {
                        return (new Date(dt)).format('yyyy-mm-dd hh:mi');
                    }
                }
            },
            hhmi: function(kField) {
                return function(dataItem) {
                    var dt = dataItem[kField];
                    if (dt == undefined || dt == '') {
                        return '';
                    } else {
                        return (new Date(dt)).format('hh:mi');
                    }
                }
            },
            miss_kr: function(kField) {
                return function(dataItem) {
                    var dt = dataItem[kField];
                    if (dt == undefined || dt == '' || dt == 0) {
                        return '00분 00초';
                    } else {
                        var sign = (dt < 0) ? '-' : '';
                        var min = Math.abs(Math.floor(dt / 60));
                        var sec = Math.abs(Math.floor(dt % 60));
    
    
                        min = (min >= 0 && min < 10) ? '0' + min : min;
                        sec = (sec >= 0 && sec < 10) ? '0' + sec : sec;
    
                        return sign + min + '분 ' + sec + '초';
                    }
                }
            },
    
            hhmi_kr: function(kField) {
                return function(dataItem) {
                    var dt = dataItem[kField];
                    if (dt == undefined || dt == '' || dt == 0 || isNaN(dt)) {
                        return '00시간 00분';
                    } else {
                        var field = dataItem[kField];
                        var sign = (field < 0) ? '-' : '';
                        var hour = Math.abs(Math.floor(field / 60));
                        var min = Math.abs(Math.floor(field % 60));
    
    
                        hour = (hour >= 0 && hour < 10) ? '0' + hour : hour;
                        min = (min >= 0 && min < 10) ? '0' + min : min;
    
                        return sign + hour + '시간 ' + min + '분';
                    }
                }
            },
    
            hhmi_kr_by_sec: function(kField) {
                return function(dataItem) {
                    var dt = dataItem[kField];
                    if (dt == undefined || dt == '' || dt == 0 || isNaN(dt)) {
                        return '00시간 00분';
                    } else {
                        var field = dataItem[kField] / 60; // sec -> min
                        var sign = (field < 0) ? '-' : '';
                        var hour = Math.abs(Math.floor(field / 60));
                        var min = Math.abs(Math.floor(field % 60));
    
    
                        hour = (hour >= 0 && hour < 10) ? '0' + hour : hour;
                        min = (min >= 0 && min < 10) ? '0' + min : min;
    
                        return sign + hour + '시간 ' + min + '분';
                    }
                }
            },
    
            tempt: function(kField) {
                return function(dataItem) {
                    var tempt = dataItem[kField];
                    if (tempt == undefined || tempt == '') {
                        return '-';
                    } else if (tempt == -5555 || tempt == -9999) {
                        return '-'
                    } else {
                        return kendo.toString(tempt, 'n1');
                    }
                }
            },
    
            caralcTy: function(dataItem) {
    
                var cd = dataItem['caralcTy'];
                if (cd == undefined || cd == '') {
                    return '';
                } else {
                    var cdNm = '';
                    if (cd == '1A') {
                        cdNm = '우유:일반';
                    } else if (cd == '1B') {
                        cdNm = '우유:학교';
                    } else if (cd == '1C') {
                        cdNm = '우유:군';
                    } else if (cd == '1D') {
                        cdNm = '우유:직판';
                    } else if (cd == '1F') {
                        cdNm = '우유:특수';
                    } else if (cd == '1K') {
                        cdNm = '우유:자가';
                    } else if (cd == '2A') {
                        cdNm = '가공품:일반';
                    } else if (cd == '2F') {
                        cdNm = '가공품:특수';
                    } else if (cd == '2K') {
                        cdNm = '가공품:자가';
                    } else if (cd == '9E') {
                        cdNm = '공통:공장내이고';
                    } else if (cd == '9N') {
                        cdNm = '공통:이고';
                    } else if (cd == '9P') {
                        cdNm = '공통:경유지이고';
                    } else if (cd == '9S') {
                        cdNm = '공통:대기';
                    } else if (cd == '9T') {
                        cdNm = '공통:공장간이고';
                    }
                    return cdNm;
                }
            }
            */
        }
    })();
    //유효성검증
    var validation = (function () {
        return {
            default: function (fieldNm) {
                var msg = '필수 값입니다.';
                if (fieldNm != undefined) {
                    msg = fieldNm + "은(는) " + msg;
                }
                return {
                    // pattern: {
                    //     value: "^[A-Z]{4}$",
                    //     message: "Brand Code must be 4 upper case characters."
                    // },
                    required: {
                        required: false,
                        message: msg
                    }
                }
            }
        }
    })()


    /**
     * kendo grid - default field model
     */
    /**
     * {kFieldKey} :{
     *   field: any
     *   title: any
     *   type: 'String' || 'number'
     *   attributes: { "class" : "", "style" : "" }
     *   [, sortable]: true || false
     *   [, width]: css-value
     *   [, autoFitColumn]: true || false,
     *   [, headerTemplate]: 'String' || function,
     *   [, template]: 'String' || function,
     * }
     */

    var kFieldModel = {
        "sEmpId": { field: 'sEmpId', title: "사번", type: "string" },
        "cYear": { field: 'cYear', title: "연도", type: "string", editor: editor.cYear },
        "cMon": { field: 'cMon', title: "월", type: "string" },
        "sUntCd": { field: 'sUntCd', title: "단위", type: "string" },
        "sEngType": { field: 'sEngType', title: "구분", type: "string" },
        "site": { field: 'site', title: "사업장", type: "string" },
        "engPonumber": { field: 'engPonumber', title: "배출시설", type: "string" },
        "legend": { field: 'legend', title: "범례", type: "string" },
        "sEngNm": { field: 'sEngNm', title: "에너지원", type: "string" },
        "sumUnt": { field: 'sumUnt', title: "집계단위", type: "string" },
        "arrUseAmt": { field: 'arrUseAmt', title: "월별 데이터", type: "string" },
        "arrTco2": { field: 'arrTco2', title: "온실가스 월별 데이터", type: "string" },
        "arrTonKm": { field: 'arrTonKm', title: "톤킬로 월별데이터", type: "string" },
        "arrGco2": { field: 'arrGco2', title: "원단위 월별 데이터", type: "string" },
        "prYearAvg": { field: 'prYearAvg', title: "전년도원단위 활용", type: "number", format: "{0:n0}" },
        "twYearAvg": { field: 'twYearAvg', title: "2년평균원단위 활용", type: "number", format: "{0:n0}" },
        "trYearAvg": { field: 'trYearAvg', title: "3년평균원단위 활용", type: "number", format: "{0:n0}" },
        "foYearAvg": { field: 'foYearAvg', title: "4년평균원단위 활용", type: "number", format: "{0:n0}" },
        "fvYearAvg": { field: 'fvYearAvg', title: "5년평균원단위 활용", type: "number", format: "{0:n0}" },
        "nAmt": { field: 'nAmt', title: "실적", type: "number", format: "{0:n0}" },
        "nYearGasGoalSum": { field: 'nYearGasGoalSum', title: "합계", type: "number", format: "{0:n0}" },
        "nYearEngGoalSum": { field: 'nYearEngGoalSum', title: "에너지(TJ)", type: "number", format: "{0:n0}" },
        "sFileNm": { field: 'sFileNm', title: "첨부파일", type: "string", width: 300 },
        "sfilePath": { field: 'sfilePath', title: "감축목표통보파일경로", type: "string" },
        "sSiteNm": { field: 'sSiteNm', title: "사업장", type: "string", editor: editor.sSiteNm },
        "sSiteId": { field: 'sSiteId', title: "사업장ID", type: "string" },
        "sCefId": { field: 'sCefId', title: "에너지원ID", type: "string" },
        "arrEngPlan": { field: 'arrEngPlan', title: "월별목표(GJ)", type: "number" },
        "arrGhgPlan": { field: 'arrGhgPlan', title: "월별목표(tCO2)", type: "number" },
        "cYm": { field: 'cYm', title: "년월", type: "string", editor: editor.cYm },
        // "cYm": { field: 'cYm', title: "년월", type: "string" },
        "sPointId": { field: 'sPointId', title: "배출시설ID", type: "string" },
        "sPointNm": { field: 'sPointNm', title: "배출시설명", type: "string" },
        "nUseAmt": { field: 'nUseAmt', title: "사용량", type: "number", format: "{0:n3}" },
        "nCost": { field: 'nCost', title: "사용금", type: "number", format: "{0:n3}" },
        "engBillYn": { field: 'engBillYn', title: "고지서등록여부", type: "string" },
        "sCarNo": { field: 'sCarNo', title: "차량번호", type: "string" },
        "sDepId": { field: 'sDepId', title: "조직ID", type: "string" },
        "sDepNm": { field: 'sDepNm', title: "조직명", type: "string" },
        "sDeptId": { field: 'sDeptId', title: "조직ID", type: "string" },
        "sDeptNm": { field: 'sDeptNm', title: "조직명", type: "string", editor: editor.sDeptNm },
        "cCloseYn": { field: 'cCloseYn', title: "마감여부", type: "string" },
        "sModstring": { field: 'sModstring', title: "실적수정 종료일", type: "string" },
        "nTj": { field: 'nTj', title: "TJ", type: "string" },
        "nToe": { field: 'nToe', title: "TOE", type: "string" },
        "nTco2Smly": { field: 'nTco2Smly', title: "전년동월 tCO2", type: "string" },
        "tCO2ChangeRate": { field: 'tCO2ChangeRate', title: "tCO2 증감률(%)", type: "string" },
        "diesel": { field: 'diesel', title: "경유", type: "string" },
        "lng": { field: 'lng', title: "LNG 도시가스", type: "string" },
        "lpgProPaneKg": { field: 'lpgProPaneKg', title: "LPG 프로판(Kg)", type: "string" },
        "lpgProPaneNm": { field: 'lpgProPaneNm', title: "LPG 프로판(Nm3)", type: "string" },
        "kerosene": { field: 'kerosene', title: "등유(리터)", type: "string" },
        "lpgButaneKg": { field: 'lpgButaneKg', title: "LPG 부탄(리터)", type: "string" },
        "gasoline": { field: 'gasoline', title: "휘발유", type: "string" },
        "lpgButaneL": { field: 'lpgButaneL', title: "LPG 부탄(리터)", type: "string" },
        "steam": { field: 'steam', title: "스팀", type: "number", format: "{0:n3}" },
        "mnths": { field: 'mnths', title: "월별 배출량", type: "string" },
        "nGoal": { field: 'nGoal', title: "목표", type: "number", format: "{0:n3}" },
        "nRate": { field: 'nRate', title: "달성률(%)", type: "number", format: "{0:n3}" },
        "nTonKm": { field: 'nTonKm', title: "생산량", type: "number" },
        "sCorpId": { field: 'sCorpId', title: "관리업체코드", type: "string" },
        "sCorpNm": { field: 'sCorpNm', title: "법인명", type: "string" },
        "sCorpRegNum": { field: 'sCorpRegNum', title: "법인등록번호", type: "string" },
        "sOwner": { field: 'sOwner', title: "대표자명", type: "string" },
        "sCorpTel": { field: 'sCorpTel', title: "대표전화번호", type: "string" },
        "sCorpCat": { field: 'sCorpCat', title: "대표업종", type: "string" },
        "sCorpAddr": { field: 'sCorpAddr', title: "법인소재지", type: "string" },
        "sPicNm": { field: 'sPicNm', title: "이름", type: "string" },
        "sPicDept": { field: 'sPicDept', title: "부서", type: "string" },
        "idErpDept": { field: 'idErpDept', title: "부서", type: "string", editor: editor.sDeptNm },
        "sPicPos": { field: 'sPicPos', title: "직위", type: "string" },
        "sPicTel": { field: 'sPicTel', title: "전화번호", type: "string" },
        "sPicHp": { field: 'sPicHp', title: "휴대전화", type: "string" },
        "sPicMail": { field: 'sPicMail', title: "이메일", type: "string" },
        "sPic": { field: 'sPic', title: "담당자", type: "string" },
        "sMainPrd": { field: 'sMainPrd', title: "주요생산제품", type: "string" },
        "sSmlYn": { field: 'sSmlYn', title: "중소기업여부", type: "string" },
        "sCertPro": { field: 'sCertPro', title: "품질경영시스템인증", type: "string" },
        "sCertEnv": { field: 'sCertEnv', title: "환경경영시스템인증", type: "string" },
        "sNgmsCorp": { field: 'sNgmsCorp', title: "정부보고 관리업체코드", type: "string" },
        "nOrder": { field: 'nOrder', title: "정렬순서", type: "number" },
        "sSiteRegNum": { field: 'sSiteRegNum', title: "사업자등록번호", type: "string" },
        "sSiteCat": { field: 'sSiteCat', title: "업종", type: "string" },
        "sSiteAddr": { field: 'sSiteAddr', title: "사업장소재지", type: "string" },
        "sSiteTel": { field: 'sSiteTel', title: "사업장대표전화", type: "string" },
        "sParty": { field: 'sParty', title: "사업장구분", type: "string" },
        "cCloseYear": { field: 'cCloseYear', title: "폐쇄연도", type: "string" },
        "sPicFax": { field: 'sPicFax', title: "담당자팩스", type: "string" },
        "sSmlEmiYn": { field: 'sSmlEmiYn', title: "소량배출사업장여부", type: "string" },
        "sRegId": { field: 'sRegId', title: "등록자", type: "string" },
        "dRegDate": { field: 'dRegDate', title: "등록일", type: "string", template: template.yyyymmddhhmiss('dRegDate') },
        "dRegstring": { field: 'dRegstring', title: "등록일", type: "string" },
        "sModId": { field: 'sModId', title: "변경자", type: "string" },
        "dModstring": { field: 'dModstring', title: "변경일", type: "string" },
        "cDelYn": { field: 'cDelYn', title: "삭제여부", type: "string" },
        "sErpCd": { field: 'sErpCd', title: "ERP CD", type: "string" },
        "nPrdAmt": { field: 'nPrdAmt', title: "생산량", type: "number" },
        "nMember": { field: 'nMember', title: "상시 종업원수", type: "number" },
        "nSales": { field: 'nSales', title: "매출액", type: "number" },
        "nEngCost": { field: 'nEngCost', title: "에너지비용", type: "number" },
        "nCapital": { field: 'nCapital', title: "자본금", type: "number" },
        "memo": { field: 'memo', title: "비고", type: "string" },
        "sEmpNm": { field: 'sEmpNm', title: "이름", type: "string" },
        "sDept": { field: 'sDept', title: "주관부서", type: "string" },
        "sPrjNm": { field: 'sPrjNm', title: "전환사업명", type: "string" },
        "sAuthType": { field: 'sAuthType', title: "사용자 권한", type: "string", editor: editor.sAuthType },
        "sCefType": { field: 'sCefType', title: "활동구분", type: "string" },
        "sEngCd": { field: 'sEngCd', title: "사용 에너지원", type: "string" },
        "sEngCd2": { field: 'sEngCd2', title: "정부보고 에너지원", type: "string" },
        "sUntCd2": { field: 'sUntCd2', title: "계산단위", type: "string" },
        "nFactor": { field: 'nFactor', title: "환산계수", type: "number" },
        "nOxyVal": { field: 'nOxyVal', title: "산화율", type: "number" },
        "sActvCd": { field: 'sActvCd', title: "배출활동구분", type: "string" },
        "cTier": { field: 'cTier', title: "계산식Tier", type: "string" },
        "cActvTier": { field: 'cActvTier', title: "활동도Tier", type: "string" },
        "cHeatTier": { field: 'cHeatTier', title: "발열량Tier", type: "string" },
        "cCefTier": { field: 'cCefTier', title: "배출계수Tier", type: "string" },
        "cOxyTier": { field: 'cOxyTier', title: "산화율Tier", type: "string" },
        "nHeat": { field: 'nHeat', title: "순발열량", type: "number" },
        "nTotHeat": { field: 'nTotHeat', title: "총발열량", type: "number" },
        "nCo2Cef": { field: 'nCo2Cef', title: "CO2배출계수", type: "number" },
        "nCh4Cef": { field: 'nCh4Cef', title: "CH2배출계수", type: "number" },
        "nN2oCef": { field: 'nN2oCef', title: "N2O배출계수", type: "number" },
        "sUponumberId": { field: 'sUponumberId', title: "", type: "string" },
        "sPonumberId": { field: 'sPonumberId', title: "배출시설ID", type: "string" },
        "sPonumberNm": { field: 'sPonumberNm', title: "배출시설명", type: "string" },
        "sInvCd": { field: 'sInvCd', title: "배출시설구분", type: "string" },
        "nCapa": { field: 'nCapa', title: "용량", type: "number" },
        "sUntCapa": { field: 'sUntCapa', title: "용량단위", type: "string" },
        "nCapa2": { field: 'nCapa2', title: "세부시설용량", type: "number" },
        "nUntCapa2": { field: 'nUntCapa2', title: "세부시설용량단위", type: "number" },
        "cSYear": { field: 'cSYear', title: "시작년도", type: "string" },
        "cEYear": { field: 'cEYear', title: "종료년도", type: "string" },
        "sCat": { field: 'sCat', title: "코드", type: "string" },
        "sCd": { field: 'sCd', title: "코드값", type: "string" },
        "nLev": { field: 'nLev', title: "레벨", type: "number" },
        "sUCd": { field: 'sUCd', title: "부모코드", type: "string" },
        "sDesc": { field: 'sDesc', title: '코드명', type: "string" },
        "sExp": { field: 'sExp', title: "코드설명", type: "number" },
        "nBoardId": { field: 'nBoardId', title: "게시판ID", type: "number" },
        "nBoardType": { field: 'nBoardType', title: "게시판종류", type: "number" },
        "sTitle": { field: 'sTitle', title: "제목", type: "string" },
        "sFilePath": { field: 'sFilePath', title: "파일경로", type: "string" },
        "sContent": { field: 'sContent', title: "내용", type: "string" },
        "sReply": { field: 'sReply', title: "답변", type: "string" },
        "sRegEmpId": { field: 'sRegEmpId', title: "등록자", type: "string" },
        "indirEmission_electricPower": { field: 'indirEmission_electricPower', title: "전기", type: "number", format: "{0:n3}" },
        "indirEmission_steam": { field: 'indirEmission_steam', title: "스팀", type: "number", format: "{0:n3}" },
        "indirEmission_hotWater": { field: 'indirEmission_hotWater', title: "온수", type: "number", format: "{0:n3}" },
        "fixCmbst_value": { field: "fixCmbst_value", title: "고정연소", type: "number", format: '{0:n3}' },
        "mvngCmbst_value": { field: "mvngCmbst_value", title: "이동연소", type: "number", format: '{0:n3}' },
        "etcEmission_value": { field: 'etcEmission_value', title: "기타배출", type: "number", format: '{0:n3}' },
        "nTco2Eq": { field: 'nTco2Eq', title: "원단위", type: "number" },
        "nID": { field: 'nID', title: "ID", type: "number" },
        "cDate": { field: 'cDate', title: "날짜", type: "string" },
        "sLineCd": { field: 'sLineCd', title: "노선구분", type: "string" },
        "sTransComp": { field: 'sTransComp', title: "운수회사", type: "string" },
        "sCarTon": { field: 'sCarTon', title: "톤수", type: "string" },
        "sStartPnt": { field: 'sStartPnt', title: "출발지", type: "string" },
        "sDestPnt": { field: 'sDestPnt', title: "도착지", type: "string" },
        "sPrdCd": { field: 'sPrdCd', title: "제품코드", type: "string" },
        "sPrdNm": { field: 'sPrdNm', title: "제품명", type: "string" },
        "nWeight": { field: 'nWeight', title: "총중량(kg)", type: "number" },
        "nDistance": { field: 'nDistance', title: "운행거리(km)", type: "number" },
        "nFuelEff": { field: 'nFuelEff', title: "연비(L/KM)", type: "number" },
        "nEng": { field: 'nEng', title: "연료사용량(L)", type: "number" },
        "nMj": { field: 'nMj', title: "에너지사용량(MJ)", type: "number" },
        "nTco2": { field: 'nTco2', title: "온실가스배출량(gCO2eq)", type: "number" },
        "m1": { field: "m1", title: "1월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m2": { field: "m2", title: "2월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m3": { field: "m3", title: "3월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m4": { field: "m4", title: "4월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m5": { field: "m5", title: "5월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m6": { field: "m6", title: "6월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m7": { field: "m7", title: "7월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m8": { field: "m8", title: "8월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m9": { field: "m9", title: "9월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m10": { field: "m10", title: "10월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m11": { field: "m11", title: "11월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "m12": { field: "m12", title: "12월", type: "number", format: "{0:n3}", editor: editor.nYearGasGoalSumMonth },
        "temporary": { field: "temporary", title: "구분", type: "string" },
        "idSite": { field: "idSite", title: "일련번호", type: "number" },
        "sMenuNm": { field: "sMenuNm", title: "메뉴명", type: "string", editor: editor.sMenuNm },
        "sModDate": { field: "sModDate", title: "사업장 실적 수정 종료일", type: "number" },
        "engNm": { field: "engNm", title: "배출원", type: "string" },
        "sMrvengCdNm": { field: "sMrvengCdNm", title: "정부보고 에너지", type: "string", editor: editor.sMrvengCdNm },
        "sInUntCdNm": { field: "sInUntCdNm", title: "입력 단위", type: "string", editor: editor.sInUntCdNm },
        "sCalUntCdNm": { field: "sCalUntCdNm", title: "계산 단위", type: "string", editor: editor.sCalUntCdNm },
        "sEmiCd": { field: "sEmiCd", title: "배출활동 구분", type: "string", editor: editor.sEmiCdNm },
        "sEmiCdNm": { field: "sEmiCdNm", title: "배출활동 구분", type: "string", editor: editor.sEmiCdNm },
        "nYearEngGoal": { field: "nYearEngGoal", title: "GJ", type: "number", format: "{0:n3}" },
        "nYearGasGoal": { field: "nYearGasGoal", title: "tCO2", type: "number", format: "{0:n3}" },
        "nEngGoal": { field: "nEngGoal", title: "에너지(GJ)", type: "number", format: "{0:n3}" },
        "nGasGoal": { field: "nGasGoal", title: "온실가스", type: "number", format: "{0:n3}" },
        "nEngGoalSum": { field: "nEngGoalSum", title: "GJ", type: "number", format: "{0:n3}" },
        "nGasGoalSum": { field: "nGasGoalSum", title: "tCO2", type: "number", format: "{0:n3}" },
        "sUntCdNm": { field: "sUntCdNm", title: "단위", type: "string" },
        "calcUntNm": { field: "calcUntNm", title: "단위", type: "string" },
        "sum": { field: "sum", title: "합계", type: "number", format: '{0:n3}' },
        "nPower": { field: "nPower", title: "발전량(kw)", type: "number", format: "{0:n0}" },
        "nPoint": { field: "nPoint", title: "위치", type: "string" },
        "sEmtCdNm": { field: "sEmtCdNm", title: "배출시설구분", type: "string" },
        "sEngCdNm": { field: "sEngCdNm", title: "사용 에너지", type: "string", editor: editor.sEngCdNm },
        "sDelYn": { field: "sDelYn", title: "삭제여부", type: "string" },
        "nEngEtc": { field: "nEngEtc", title: "기타사용량", type: "number", format: "{0:n3}" },
        "nEngEtcUntCd": { field: "nEngEtcUntCd", title: "기타사용량 단위", type: "string", editor: editor.nEngEtcUntCd },
        "nEngEtcUntCdNm": { field: "nEngEtcUntCdNm", title: "기타사용량 단위 명", type: "string" },
        "t1GjSum": { field: "t1GjSum", title: "GJ", type: "number", format: "{0:n3}" },
        "t1ToeSum": { field: "t1ToeSum", title: "TOE", type: "number", format: "{0:n3}" },
        "t2Tco2eqSum": { field: "t2Tco2eqSum", title: "전년 동월 tCO2", type: "number", format: "{0:n3}" },
        "t1Tco2eqSum": { field: "t1Tco2eqSum", title: "tCO2", type: "number", format: "{0:n3}" },
        "tco2eqSumDeltaRate": { field: "tco2eqSumDeltaRate", title: "tCO2 증감율(%)", type: "number", format: "{0:n2}" },
        "cefNm": { field: "cefNm", title: "배출구분", type: "string" },
        "entNm": { field: "entNm", title: "배출원", type: "string" },
        "sInUntNm": { field: "sInUntNm", title: "단위", type: "string" },
        "dateMon": {
            field: "dateMon", title: "년월", type: "string"
            , template: function (dataItem) {
                if (dataItem.dateMon.length == 6) {
                    return dataItem.dateMon.substr(0, 4) + '-' + dataItem.dateMon.substr(4, 2);
                } else {
                    return dataItem.dateMon;
                }
            }
        },
        "sVrn": { field: "sVrn", title: "차량번호", type: "string" },
        "transTypeNm": { field: "transTypeNm", title: "운송수단<br>구분", type: "string" },
        "lineNm": { field: "lineNm", title: "노선구분", type: "string" },
        "sTransComNm": { field: "sTransComNm", title: "운수회사", type: "string" },
        "tonNm": { field: "tonNm", title: "톤수", type: "string" },
        "sLogiWeiCd": { field: "sLogiWeiCd", title: "톤수", type: "string" },
        "selfvrnNm": { field: "selfvrnNm", title: "자차여부", type: "string" },
        "loadageSum": { field: "loadageSum", title: "적재량<br>(kg)", type: "number", format: "{0:n0}" },
        "distanceSum": { field: "distanceSum", title: "운송거리<br>(km)", type: "number", format: "{0:n0}" },
        "toeSum": { field: "toeSum", title: "환산톤<br>(TOE)", type: "number", format: "{0:n0}" },
        // "toeSum": { field: "toeSum", title: "공인연비", type: "number", format: "{0:n0}" },
        "engVolSum": { field: "engVolSum", title: "연료사용량<br>(L)", type: "number", format: "{0:n0}" },
        "mjSum": { field: "mjSum", title: "에너지사용량<br>(MJ)", type: "number", format: "{0:n0}" },
        "gco2Sum": { field: "gco2Sum", title: "온실가스배출량<br>(gCO2eq)", type: "number", format: "{0:n0}" },
        "basUnt": { field: "basUnt", title: "온실가스원단위<br>(gCO2eq/ton-km)", type: "number", format: "{0:n0}" },
        "fareSum": { field: "fareSum", title: "운임료", type: "number", format: "{0:n0}" },
        "nModCal": { field: "nModCal", title: "산출계수", type: "number", format: "{0:n3}" },
        "sOthCefNm": { field: "sOthCefNm", title: "기타 온실가스 배출명", type: "string" },
        "sOthPurpose": { field: "sOthPurpose", title: "사용목적", type: "string" },
        "sGhgCd": { field: "sGhgCd", title: "온실가스", type: "string", editor: editor.sGhgCd },
        "cPer": { field: "cPer", title: "구성비율", type: "number", format: '{0:n3}' },
        "sLogiTypeCd": { field: "sLogiTypeCd", title: "구분", type: "string" },
        "sEmpMail": { field: "sEmpMail", title: "이메일", type: "string" },
        "sInvSeq": { field: "sInvSeq", title: "배출시설 일련번호", type: "string" },
        "sFuelEff": { field: "sFuelEff", title: "공인연비", type: "string" },
        "sLogiTypeCd": { field: "sLogiTypeCd", title: "물류구분", type: "string" },
        "sBaseVal": { field: "sBaseVal", title: "원단위 산정 기준 값", type: "string" },
        "sBaseUnt": { field: "sBaseUnt", title: "기준 단위", type: "string" },
        "selectable": {
            selectable: true,
            editable: false,
            width: '37px',
            headerAttributes: { "class": "text-left", "style": "text-align: left;padding: 7px 10px 2px 10px;", "disabled": "disabled" },
            attributes: { "class": "text-left", "style": "padding: 8px 10px 2px 10px;" },
        },
        "logiWeiNm": { field: "logiWeiNm", title: "톤수", type: "string" },
        "sCarType": { field: "sCarType", title: "차종", type: "string" },
        "sVin": { field: "sVin", title: "차대번호", type: "string" },
        "fuelTypeNm": { field: "fuelTypeNm", title: "유종", type: "string" },
        "sCarMade": { field: "sCarMade", title: "연식", type: "string" },
        "useTypeNm": { field: "useTypeNm", title: "용도", type: "string" },
        "carStatusNm": { field: "carStatusNm", title: "상태", type: "string" },
        "sCarCompany": { field: "sBassCarCompanyeUnt", title: "제조자", type: "string" },
        "sCarModel": { field: "sCarModel", title: "차량모델", type: "string" },
        "dModDate": { field: "dModDate", title: "수정일시", type: "string" },
        "sCarLoanDate": { field: "sCarLoanDate", title: "대차일시", type: "string" },
        "sFuelEff": { field: "sFuelEff", title: "공인연비", type: "string" },
        "sSelfVrnNm": { field: "sSelfVrnNm", title: "자차여부", type: "string" },
        "gCo2Sum": { field: "gCo2Sum", title: "온실가스배출량", type: "string" },
        "sStatusNm": { field: "sStatusNm", title: "상태", type: "string", editor: editor.sStatusNm },
    }


    $.AIV.kFieldModel = kFieldModel;

    AIV_MODEL.editor = editor;
    window.AIV_MODEL = AIV_MODEL;


}(jQuery));