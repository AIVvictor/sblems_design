/**
 * 유틸리티
 */
var AIV_UTIL = {
    /**
     * 화면 사이즈 확인
     */
    getScreenSize: function () {
        var size = {
            width: screen.availWidth,
            height: screen.availHeight
        };
        return size;
    },
    /**
     * 클라이언트 사이즈 확인
     */
    getClientSize: function () {
        var size = {
            width: document.body.clientWidth,
            height: document.body.clientHeight
        };
        return size;
    },
    /**
     * 윈도우 사이즈 확인
     */
    getInnerSize: function () {
        var size = {
            width: window.innerWidth,
            height: window.innerHeight
        };
        return size;
    },
    /**
     * 지정 ID하위의 입력값 추출. (AIV 플러그인 기준- '_'구분자를 활용함. aiv_controls 플러그인 작성기준)
     */
    getDivData: function (id) {
        var ret = [];
        var jsonstring = '{';
        $.each($("#" + id).find('input'), function (a, b) {
            var k = b.getAttribute("id").split('_')[0] + "";
            var v = b.value;
            v = v.replace(/-/g, ''); // 하이픈(-) 제거
            v = v.replace(/ /g, ''); // 공백 제거
            v = v.replace(/:/g, ''); // 콜론(:) 제거
            //console.log(k + "/" + v);
            ret.push(JSON.parse('{"' + k + '":"' + v + '"}'));
            jsonstring += '"' + k + '":"' + v + '",';
        });
        jsonstring = jsonstring.substring(0, jsonstring.length - 1) + '}';

        return jsonstring;
        //return JSON.stringify(ret);
    },

    /**
     * 지정 ID하위의 입력값 추출.
     */
    getDivDataJson: function (id) {
        var ret = {};
        $.each($("#" + id).find('input'), function (i, obj) {
            var k = $(obj).attr('id');
            var v = null;
            if ($(obj).attr('type') == 'checkbox') {
                v = $(obj).is(':checked') ? 'Y' : 'N'; // true:'Y', false:'N'
            } else {
                v = $(obj).val();
            }
            v = $.trim(v);
            // if( typeof v == 'string') {
            // 	v = v.replaceAll('-', '')	// 하이픈(-) 제거
            // 		.replaceAll(' ', '')	// 공백 제거
            // 		.replaceAll(':', '');	// 콜론(:) 제거
            // }
            ret[k] = v;
        });

        return ret;
        //return JSON.stringify(ret);
    },
    /**
     * 널(null) 확인
     */
    isNull: function (obj) {
        // console.log("[AIV_UTIL - isNull]");
        if ((obj != null) && (typeof (obj) != "undefined")) {
            return false;
        }
        return true;
    },
    isNotNull: function (obj) {
        return !this.isNull(obj);
    },
    /**
     * 빈값 확인
     */
    isEmpty: function (obj) {
        // console.log("[AIV_UTIL - isEmpty]");
        if (this.isNull(obj)) {
            return true;
        }
        if (obj == "") {
            return true;
        }
        return false;
    },
    isNotEmpty: function (obj) {
        return !this.isEmpty(obj);
    },
    /**
     * WebStorage 체크
     */
    checkWebStorage: function () {
        var ret = false;
        if (storageAvailable('localStorage')) {
            //AIV_SYS.log('야호!~ 우리는 localStorage를 사용할 수 있습니다.');
            ret = true;
        } else {
            //AIV_SYS.log('슬픈소식, localStorage를 사용할 수 없습니다.');
            ret = false;
        }

        if (storageAvailable('sessionStorage')) {
            //AIV_SYS.log('야호! 우리는 sessionStorage 사용할 수 있습니다.');
            ret = true;
        } else {
            //AIV_SYS.log('슬픈소식, sessionStorage 사용할 수 없습니다.');
            ret = false;
        }
        return ret;
    },
    setPopPage: function () {
        var popupYN = getParam('popupYN');
        var menuShowYN = getParam('menuShowYN');
        if (popupYN == "Y") {
            $(".content").css("padding-top", "0px");
            $(".header").css("display", "none");
            $(".page-sidebar").css("display", "none");
            $(".page-container").css("padding-left", "0px");
        }
    },

    date: {
        addMinutes: function (date, minutes) {
            return new Date(date.getTime() + minutes * 60 * 1000);
        },
        addDays: function (date, days) {
            return new Date(date.getTime() + days * 24 * 60 * 60 * 1000);
        },
        miToHhmi: function (minute, separator1, separator2) {
            var sign = '';
            if (minute < 0) {
                sign = '-';
                minute = Math.abs(minute);
            }

            separator1 = (separator1 != undefined) ? separator1 : "";
            separator2 = (separator2 != undefined) ? separator2 : "";

            var stdHour = Math.floor(minute / 60);
            var stdMin = Math.floor(minute % 60);
            stdHour = stdHour < 10 ? '0' + stdHour : stdHour;
            stdMin = stdMin < 10 ? '0' + stdMin : stdMin;

            return sign + stdHour + separator1 + stdMin + separator2;
        }
    },

    /**
     * 1. 브라우저 타이틀 입력
     * 2. 페이지 타이틀 입력
     * 3. 페이지 경로 입력
     */
    initPage: function () {
        $(document).prop('title', '세방 물류에너지 관리시스템: ' + AIV_MENU.currentPageTitle);
        $("#pageTitle").html(AIV_MENU.currentPageTitle);

        var pathInfo = AIV_MENU.pathInfo.replaceAll('>>', '<span class="k-icon k-i-arrow-chevron-right"></span>')
        $("#pathInfo").html(pathInfo);
    },

    /**
     * 페이지 기본 객체 생성
     */
    initPageObj: function () {
        return new Object({
            fn: {},
            params: {},
            btn: {},
            grid: {},
            chart: {},
            form: {},
            treeview: {},
        });
    },

    /**
     * 브라우저 유형 확인
     */
    checkBroswer: function () {

        var agent = navigator.userAgent.toLowerCase(),
            name = navigator.appName,
            browser = '';

        // MS 계열 브라우저를 구분
        if (name === 'Microsoft Internet Explorer' || agent.indexOf('trident') > -1 || agent.indexOf('edge/') > -1) {
            browser = 'ie';
            /*
            if(name === 'Microsoft Internet Explorer') { // IE old version (IE 10 or Lower)
            	agent = /msie ([0-9]{1,}[\.0-9]{0,})/.exec(agent);
            	browser += parseInt(agent[1]);
            } else { // IE 11+
            	if(agent.indexOf('trident') > -1) { // IE 11
            		browser += 11;
            	} else if(agent.indexOf('edge/') > -1) { // Edge
            		browser = 'edge';
            	}
            }
            */
        } else if (agent.indexOf('safari') > -1) { // Chrome or Safari
            if (agent.indexOf('opr') > -1) { // Opera
                browser = 'opera';
            } else if (agent.indexOf('chrome') > -1) { // Chrome
                browser = 'chrome';
            } else { // Safari
                browser = 'safari';
            }
        } else if (agent.indexOf('firefox') > -1) { // Firefox
            browser = 'firefox';
        }

        return browser;
    },

    /**
     * 페이지 이동시 GET방식으로 전달 받은 URL paramter 조회
     */
    getUrlParams: function () {
        var params = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            params[key] = decodeURI(value);
        });
        return params;
    },

    /**
     * PDF 다운로드
     */
    exportPDF: function (selector, fileName) {
        $(selector).css('font-family', 'Nanum Gothic');

        kendo.drawing.drawDOM($(selector))
            .then(function (group) {
                return kendo.drawing.exportPDF(group, {
                    paperSize: 'auto',
                    margin: { left: '1cm', top: '1cm', right: '1cm', bottom: '1cm' }
                })
            })
            .done(function (data) {
                // Save the PDF file
                kendo.saveAs({
                    dataURI: data,
                    fileName: fileName + ".pdf",
                    // proxyURL: "@Url.Action("Pdf_Export_Save")"
                });
            });
    },

    rgb2hex: function (rgb) {
        if (rgb.search("rgb") == -1) {
            return rgb;
        } else {
            rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);

            function hex(x) {
                return ("0" + parseInt(x).toString(16)).slice(-2);
            }
            return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        }
    },
    unescapeHtml: function (input) {
        // var e = document.createElement('div');
        // e.innerHTML = input;
        // return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
        return input.replaceAll('&amp;', '&')
            .replaceAll('&quot;', '"')
            .replaceAll('&#39;', "'")
            .replaceAll('&lt;', '<')
            .replaceAll('&gt;', '>')
            .replaceAll('&#x2F;', '/')
            .replaceAll('&#x60;', '`')
            .replaceAll('&#x3D;', '=');
    },

    convertChartData: function (rows) {
        rows.map(function (row) {
            for (var key in row) {
                var _val = row[key];
                if (typeof _val === 'object') {
                    for (var vKey in _val) {
                        row[key + '.' + vKey] = _val[vKey];
                    }
                }
            }
        });
        return (rows);
    },

    /**
     * 누적되지 않을 키값을 받아 그 외 항목들은 행별로 누적한다
     */
    stackDataList: function (rows, unStackKeyList) {
        // var rows = [{month:1, b:1},{month:2, b:2},{month:3, b:3}];
        // var unStackKeyList = ['month'];
        // var stackDataList = [{month:1, b:1},{month:2, b:3},{month:3, b:6}];
        var stackDataList = [];
        $(rows).each(function (i, row) {
            var data = {};
            for (var key in row) {
                if (unStackKeyList.indexOf(key) > -1) {
                    data[key] = row[key];
                } else {
                    if (i > 0) {
                        var beforeRow = stackDataList[i - 1];
                        data[key] = row[key] + beforeRow[key];
                        // 값이 NaN이면 공백으로 만들어준다.
                        if (isNaN(data[key])) { data[key] = ""; };
                    } else {
                        data[key] = row[key];
                    }
                }
            }
            stackDataList.push(data);
        });
        return stackDataList;
    },

    /**
     * 누적될 키값을 받아 해당  항목들은 행안에서 누적한다
     */
    stackColumnDataList: function (rows, stackKeyList) {
        var stackDataList = [];
        console.log(rows);
        $(rows).each(function (i, row) {
            var data = {};
            for (var key in row) {
                var keyIndex = stackKeyList.indexOf(key);
                if (keyIndex > -1) {
                    data[key] = 0;
                    for (var j = 0; j <= keyIndex; j++) {
                        data[key] += row[stackKeyList[j]];
                    }
                } else {
                    data[key] = row[key];
                }
            }

            stackDataList.push(data);
        });
        return stackDataList;
    },

    /**
     * upload 위한 javascript-form 객체 생성
     */
    createForms: function (options) {
        /**
         * FormData() 를 통하여 Ajax Upload를 구현함.
         * 1. Form 생성
         * 2. Kendo Upload의 input 객체를 찾아 Form에 추가
         * 3. sMenuId등 기타 parameter 들을 input 객체로 만들어 Form에 추가
         * 4. ajax 전송
         */

        var form = document.createElement("form");
        form.name = "fileupload";
        form.enctype = "multipart/form-data";
        form.method = "post";

        // 파일 체크
        if (options.input) {
            var input = options.input;
            if (input.files.length > 0) {
                input.setAttribute("name", "file");
                form.appendChild(input);
            }
        }

        // 메뉴아이디 체크
        if (AIV_MENU.sMenuId) {
            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("name", "sMenuId");
            input.setAttribute("value", AIV_MENU.sMenuId);
            form.appendChild(input);
        }

        // gId체크 (optional)
        if (options.gId) {
            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("name", "gId");
            input.setAttribute("value", options.gId);
            form.appendChild(input);
            var input2 = document.createElement("input");
            input2.setAttribute("type", "text");
            input2.setAttribute("name", "gid"); // gId 대신 gid로 받는 API도 있음...
            input2.setAttribute("value", options.gId);
            form.appendChild(input2);
        }

        // cmdTag체크 (optional)
        if (options.cmdTag) {
            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("name", "cmdTag");
            input.setAttribute("value", options.cmdTag);
            form.appendChild(input);
        }

        // idFile체크 (optional)
        if (options.idFile) {
            var input = document.createElement("input");
            input.setAttribute("type", "text");
            input.setAttribute("name", "idFile");
            input.setAttribute("value", options.idFile);
            form.appendChild(input);
        }

        return form;
    }

};

function SetPosCenter(obj) {
    var objWidth = obj.css('width');
    objWidth = objWidth.replace("px", "");
    var leftMargin = -1 * objWidth / 2;

    obj.css('position', 'absolute');
    obj.css('left', '50%');
    obj.css('margin-left', leftMargin + "px");
}

function getInternetExplorerVersion() {
    var rv = -1; // Return value assumes failure.    
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}



/*
 * getCookieInfo() : 쿠키 조회	
 */
function getCookieInfo(key) {

    //sys.log("<br><br>========== Cookie Info (JavaScript) ==========");

    var cookies = document.cookie;
    var returnVal = "";

    if (cookies == null || cookies == undefined || cookies == "") {
        //sys.log("<br>cookie is null.");

    } else {
        //sys.log("<br>cookies : " + cookies);

        var splitCookies = cookies.split(";");

        //sys.log("<br>cookies.length : " + splitCookies.length);

        for (var i = 0; i < splitCookies.length; i++) {

            //sys.log("<br>cookies[" + i + "] : " + splitCookies[i]);

            var keyval = splitCookies[i].split("=");
            keyval[0] = keyval[0].replace(/^\s+|\s+$/g, "");

            //sys.log("<br>cookies[" + i + "] : " + keyval[0] + " : " + decodeURIComponent(keyval[1]));

            if (keyval[0] == key) {
                returnVal = decodeURIComponent(keyval[1]);
                break;
            }
        }
    }

    return returnVal;
}


/**
 * 자릿수 앞자리 0 채우기
 * leadingZeros([num], [자릿수]) 
 * > leadingZeros(12, 3) -> '012'
 */
function leadingZeros(n, digits) {

    var zero = '';

    n = n.toString();


    if (n.length < digits) {
        for (i = 0; i < digits - n.length; i++) {

            zero += '0';
        }
    }

    return zero + n;
}


// ------------------------------------------------------------------------------------
// 현재 시간정보 호출
// ------------------------------------------------------------------------------------
function getCurTimeStamp(style) {
    var d = new Date();

    if (typeof (style) == "undefined") {
        // yyyy-mm-dd hh:mm:ss 형식으로 리턴
        var s =
            leadingZeros(d.getFullYear(), 4) + '-' +
            leadingZeros(d.getMonth() + 1, 2) + '-' +
            leadingZeros(d.getDate(), 2) + ' ' +

            leadingZeros(d.getHours(), 2) + ':' +
            leadingZeros(d.getMinutes(), 2) + ':' +
            leadingZeros(d.getSeconds(), 2);

    } else if ((typeof (style) != "undefined") && (style == 1)) {
        // yyyymmddhhmmss 형식으로 리턴
        var s =
            leadingZeros(d.getFullYear(), 4) +
            leadingZeros(d.getMonth() + 1, 2) +
            leadingZeros(d.getDate(), 2) +

            leadingZeros(d.getHours(), 2) +
            leadingZeros(d.getMinutes(), 2) +
            leadingZeros(d.getSeconds(), 2);
    } else if ((typeof (style) != "undefined") && (style == 2)) {
        // yyyymmdd 형식으로 리턴
        var s =
            leadingZeros(d.getFullYear(), 4) +
            leadingZeros(d.getMonth() + 1, 2) +
            leadingZeros(d.getDate(), 2);
    } else if ((typeof (style) != "undefined") && (style == 3)) {
        // hhmmss 형식으로 리턴
        var s =
            leadingZeros(d.getHours(), 2) +
            leadingZeros(d.getMinutes(), 2) +
            leadingZeros(d.getSeconds(), 2);
    }
    return s;
}


// ------------------------------------------------------------------------------------
// 천단위 콤마(,) 찍기
// ------------------------------------------------------------------------------------
function numberWithCommas(x) {
    if ((x != '') && (x > 0) && (x != 'null')) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
        return 0;
    }
}

// ------------------------------------------------------------------------------------
// 객체 효과주기(fadein/fadeout)
// ------------------------------------------------------------------------------------
function divEffectRepeat(id) {
    var $element = $('#' + id);
    var nCnt = 0;

    $element.bind('cusfadeOut', function () {
        $(this).fadeOut(500, function () {
            $(this).trigger('cusfadeIn');
        });
        $(this).css('filter', 'none');
    });
    $element.bind('cusfadeIn', function () {
        $(this).fadeIn(1000, function () {
            nCnt++;
            //					if (nCnt>3) {
            //						return;
            //					}
            $(this).trigger('cusfadeOut');
        });
        $(this).css('filter', 'none');
    });
    $element.trigger('cusfadeOut');


}



/**
 * WebStorage 확인함수
 * @param {WebStorage 객체유형} type 
 */
function storageAvailable(type) {
    try {
        var storage = window[type],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    } catch (e) {
        return e instanceof DOMException && (
            // Firefox를 제외한 모든 브라우저
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // 코드가 존재하지 않을 수도 있기 때문에 테스트 이름 필드도 있습니다.
            // Firefox를 제외한 모든 브라우저
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // 이미 저장된 것이있는 경우에만 QuotaExceededError를 확인하십시오.
            storage.length !== 0;
    }
}


String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    //    if (hours   < 10) {hours   = "0"+hours;}
    //    if (minutes < 10) {minutes = "0"+minutes;}
    //    if (seconds < 10) {seconds = "0"+seconds;}
    var hours_txt, minutes_txt, seconds_txt;

    if (hours == 0) hours_txt = '';
    else hours_txt = hours + '시간';
    if (minutes == 0) minutes_txt = '';
    else minutes_txt = minutes + '분';
    if (seconds == 0) seconds_txt = '';
    else seconds_txt = seconds + '초';

    var time_txt = hours_txt + minutes_txt + seconds_txt;
    return time_txt;
}


String.prototype.toMDHHMM = function () {
    var ret = this.replace(/-/g, '').replace(/ /g, '');
    return ret.substring(6, 4) + "월" + ret.substring(8, 6) + "일 " + ret.substring(8);
}


// 파라미터 값 받아오기
var getParam = function (key) {
    var _parammap = {};
    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }

        _parammap[decode(arguments[1])] = decode(arguments[2]);
    });

    return _parammap[key];
};



// ------------------------------------------------------------------------------------
// 데이터 포맷 변경
// ------------------------------------------------------------------------------------
Date.prototype.format = function (formatter) {
    var d = this;
    var yyyy = leadingZeros(d.getFullYear(), 4);
    var mm = leadingZeros(d.getMonth() + 1, 2);
    var dd = leadingZeros(d.getDate(), 2);
    var hh = leadingZeros(d.getHours(), 2);
    var mi = leadingZeros(d.getMinutes(), 2);
    var ss = leadingZeros(d.getSeconds(), 2);

    formatter = formatter.replace('yyyy', yyyy);
    formatter = formatter.replace('mm', mm);
    formatter = formatter.replace('dd', dd);
    formatter = formatter.replace('hh', hh);
    formatter = formatter.replace('mi', mi);
    formatter = formatter.replace('ss', ss);
    return formatter;
}

// ------------------------------------------------------------------------------------
// 전화번호 포맷 변경
// ------------------------------------------------------------------------------------
String.prototype.phoneFormat = function (masking) {
    var num = this;
    var formatNum = '';

    if (num.length == 11) {
        if (masking) {
            formatNum = num.replace(/(\d{3})(\d{4})(\d{4})/, '$1-****-$3');
        } else {
            formatNum = num.replace(/(\d{3})(\d{4})(\d{4})/, '$1-$2-$3');
        }
    } else if (num.length == 8) {
        formatNum = num.replace(/(\d{4})(\d{4})/, '$1-$2');
    } else {
        if (num.indexOf('02') == 0) {
            if (masking) {
                formatNum = num.replace(/(\d{2})(\d{4})(\d{4})/, '$1-****-$3');
            } else {
                formatNum = num.replace(/(\d{2})(\d{4})(\d{4})/, '$1-$2-$3');
            }
        } else {
            if (masking) {
                formatNum = num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-***-$3');
            } else {
                formatNum = num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
            }
        }
    }
    return formatNum;

}

// ------------------------------------------------------------------------------------
// 문자열 전체 변환
// ------------------------------------------------------------------------------------
String.prototype.replaceAll = function (bStr, aStr) {
    return this.split(bStr).join(aStr);
};


// jQuery prototypes
(function ($) {

    // 스크롤바 존재 체크
    $.fn.hasScrollBar = function () {
        return this.get(0).scrollHeight > this.height();
    }

})(jQuery);



/**
 * TEST DATA
 */
function getTestData(obj, len) {
    var arr = [];
    for (var i = 1; i <= len; i++) {
        var row = $.extend({}, obj);
        for (key in obj) {
            if (key == 'rank') {
                row[key] = i
            } else if (key == 'updtDt' || key == 'dt') {
                row[key] = (new Date()).format('yyyy-mm-dd hh:mi:ss')
            } else if (key == 'de') {
                row[key] = (new Date()).format('yyyy-mm-dd')
            } else if (key == 'time') {
                row[key] = (new Date()).format('hh:mi:ss')
            } else {
                row[key] = (Math.random() * 3).toFixed(0)
            }
        }
        arr.push(row)
    }
    return arr;
}