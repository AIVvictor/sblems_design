/**
 * 통신
 */

(function (w) {
    var AIV_COMM = {};

    // 최종 호출 옵션 저장.
    var ajaxLastOptions = "";

    /**
     * 서버 서비스 호출
     * @param {옵션들..} options 
     */
    var aivAjax = function (options) {

        // 최종 호출 옵션 저장.(토큰 만료시 재요청하기 위함)
        ajaxLastOptions = options;

        options.method = options.method ? options.method : 'GET'; // method 미존재시 GET을 디폴트로 한다.
        options.async = options.async === false ? false : true;  // 명확히 false 로 들어왔을때만 sync이며 그 외 undefined 등 다른 값은 true

        // GET 방식일때와 POST방식일때를 바꾸어 형식지정
        if (options.method.toUpperCase() == 'GET') {
            // options.data = $.param(options.data);
        } else if (options.method.toUpperCase() == 'DELETE') {
            if (Array.isArray(options.data)) {    // 배열일때는 ','를 이용하여 키=values 형식으로 만들어준다. ex: [{id=1},{id=2}] => ?id=1,2
                var delData = {};
                $(options.data).each(function (i, data) {
                    if (i == 0) {
                        delData = data;
                    } else {
                        for (key in delData) {
                            delData[key] += ',' + data[key];
                        }
                    }
                })
                options.url = options.url + '?' + $.param(delData);
                delete options.data;

            } else if (options.data) {
                options.url = options.url + '?' + $.param(options.data);
                delete options.data;
            }
        } else { // POST -> [Content-Type: application/json] 헤더옵션 때문에 JSON.stringify() 사용 필요
            delete options.data.file;   // file 객체의 경우 stringify 할 수 없는 형태의 객체이므로 제외. (선 파일다운로드 로직에서 사용하였으므로 없어도 됨)
            options.data = JSON.stringify(options.data);
        }
        console.log('---------------------------------');
        console.log('api url: ', options.url);
        console.log('params: ', options.data);
        $.ajax({
            type: options.method,
            async: options.async,
            url: AIV_COMM.HOST_URL + options.url,
            // global: false,
            // processData : false,
            // dataType: 'json',
            ntentType: "application/json",
            beforeSend: function (xhr) {
                // xhr.setRequestHeader("Content-Type", "application/json");    // [contentType : "application/json"] 중복옵션
                // xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

                xhr.setRequestHeader('Accept', 'application/json;charset=UTF-8');
                xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

                var token = AIV_USER.getUserInfo().token;
                if (AIV_UTIL.isNotEmpty(token)) {
                    xhr.setRequestHeader('jwt-header', token);
                }
            },
            data: options.data,
            success: function (responseData, textStatus, jqXHR) {
                console.log('success: ', responseData.resultData);
                console.log('---------------------------------');
                // API에서 응답이 왔을 경우 분기처리
                if (responseData.resultCode === "000" || responseData.resultCode === "200") { // 성공시
                    if (options.success) {
                        if (responseData.resultMessage == 'NO_DATA' || typeof responseData.resultData === 'undefined') {
                            responseData.resultData = [];
                        }
                        options.success(responseData, options, textStatus, jqXHR);
                    }
                } else if (responseData.resultCode === "100") {   // 알 수 없는 에러이지만, 개발기간동안 임시로 success를 하게 함
                    if (options.success) {
                        options.success(responseData, options, textStatus, jqXHR);
                    }
                } else {
                    if (options.error) {
                        options.error(responseData, options, textStatus, jqXHR);
                    } else {
                        ajaxErrorThrown(responseData, options);
                    }
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                console.log('error', responseData);
                // 별도의 에러처리 콜백이 없을 경우 공통 처리 함수로 처리한다.
                if (options.error) {
                    options.error(responseData, options);
                } else {
                    ajaxErrorThrown(responseData, options);
                }
            },
            complete: function (jqXHR, textStatus) {
                if (options.complete) {
                    options.complete(jqXHR, options, textStatus);
                }
            }
        });
    };

    /**
     * 파일Ajax upload
     * @param {*} options 
     * @param {options} form // 필수
     */
    var fileUpload = function (options) {
        if (typeof options == 'undefined' || typeof options.form == undefined) {
            alert('파일 업로드에 필요한 데이터가 부족합니다.');
            return;
        }

        var data = new FormData(options.form);
        var url = '/api/v1/common/file';
        $.ajax({
            type: 'POST',
            async: false,   // 동기화
            enctype: 'multipart/form-data',
            url: ENV_PROP.API_SERVER_URL + url,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Accept', 'application/json;charset=UTF-8');
                xhr.setRequestHeader('jwt-header', AIV_USER.getUserInfo().token);
            },
            data: data,
            success: function (jsonData) {
                console.log(jsonData.resultData)
                if (jsonData.resultCode == '000') {
                    if (typeof options.success === 'function') {
                        options.success(jsonData.resultData);
                    }
                } else {
                    alert("업로드가 실패하였습니다.");
                    console.error('upload error', responseData, textStatus, errorThrown);
                }
            },
            error: function (responseData, textStatus, errorThrown) {
                alert("업로드가 실패하였습니다.");
                console.error('upload error', responseData, textStatus, errorThrown);
            }
        });
    }

    /**
     * 파일 다운로드
     * @param {*} responseData 
     * @param {*} options 
     */
    var fileDownload = function (sMenuId, gId, idFile, fileNm) {
        // var sMenuId = options.sMenuId;
        // var gId = options.gId;

        try {
            var url = ENV_PROP.API_SERVER_URL + '/api/v1/common/file/' + gId + '?idFile=' + idFile + '&sMenuId=' + sMenuId;

            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.setRequestHeader('jwt-header', AIV_USER.getUserInfo().token);
            xhr.responseType = 'arraybuffer';
            xhr.addEventListener('load', function () {
                if (xhr.status === 200) {
                    var file = new Blob([xhr.response]);
                    var aTag = document.createElement("a");
                    aTag.href = URL.createObjectURL(file);
                    aTag.download = fileNm;
                    aTag.click();
                }
            });
            xhr.send();

        } catch (error) {
            // alert('다운로드 에러!');
            console.error(error)
        }
    }


    /*
    * @param {수신데이터} responseData 
    */
    // TODO: 재정의 필요
    var ajaxErrorThrown = function (responseData, options) {
        console.error("ajaxErrorThrown", responseData, options);


        var errorCode = responseData.resultCode || responseData.status || responseData.statusText;
        var errorMessage = '';

        if (responseData.resultMessage) { // 서버에서 정의한 익셉션션
            errorMessage = (responseData.resultMessage + '\n[' + responseData.resultData + ']')
        } else if (responseData.responseJSON) {   // 서버에서 미정의한 익셉션 but, java에서 정의된 기본 익셉션
            errorMessage = (responseData.responseJSON.error + '\n[' + responseData.responseJSON.message + ']')
        } else {    // 그 외 환경에서의 통신에러
            errorMessage = responseData.statusText;
        }

        alert(errorMessage);

        if (errorCode == 100) {   // 알 수 없는 에러
        } else if (errorCode == 300) {    // INVALID_INPUT_FIELD
        } else if (errorCode == 500) {    // INVALID_INPUT_FIELD
        } else if (errorCode == 900) {
        }
        console.log(errorMessage);


        /*
        var status = responseData.status;
        var message = responseData.statusText;
        var errorCode = responseData.statusText;
        if (status == 400) {    // BAD Request
            errorLoc = "ERR_STATUS_" + status;
            alert("[" + status + "] " + message);
        } else if (status == 401) {
            // 세션만료..
            if (errorCode == 2) {   // 정의되지 않은 예외
                alert("[" + status + "-" + errorCode + "] " + message);
            } else if (errorCode == 9) {   // 유효하지않은토큰
                alert("[" + status + "-" + errorCode + "] " + message);
            } else if (errorCode == 10) {   // 권한없음
                alert("[" + status + "-" + errorCode + "] " + message);
            } else if (errorCode == 11) {   // 토큰만료
                // 토큰 리프레쉬 서비스 호출 - 이전최종요청도 자동호출됨.
                tokenRefresh();
            } else if (errorCode == 12) {   // 리프레쉬 토큰만료
                // location.href = AIV_COMM.HOST_URL + '/';
                location.href = '/';
            } else {
                alert("[" + status + "] " + message);
            }
        } else if (status == 404) {
            alert("[" + status + "] " + message);
        } else {
            // alert("정의되지 않은 오류.");
            systemShutdown.alert();
        }
         */

        // systemShutdown.alert();
    };

    /* 정의되지 않은 오류 발생시 알림창 생성(5초이내 재발생시 무시) */
    var systemShutdown = {
        lastTime: null, // 시스템 점검 알림 시간 저장
        interval: 5 * 1000, // 5초 이내 연속 발생시 한번만 보여주도록 설정
        alert: function () {
            var alertFlag = false;

            if (this.lastTime == null) {
                alertFlag = true;
            } else {
                var nowTime = (new Date()).getTime();
                if (nowTime - this.lastTime > this.interval) {
                    alertFlag = true;
                }
            }

            if (alertFlag) {
                alert('시스템 점검중입니다. 잠시 후 다시 이용해주세요.');
                var userConfirmTime = (new Date()).getTime();
                this.lastTime = userConfirmTime;
            }
        }
    }


    /**
     * 토큰 리프레쉬 서비스 호출, 호출후 최종요청 다시 호출한다.
     */
    var tokenRefresh = function () {
        console.log("tokenRefresh 진입");
        // 이전요청 저장
        var curOptions = ajaxLastOptions;
        // 서비스
        aivAjax({
            method: "GET",
            callBackID: "TOKEN_REFRESH",
            url: AIV_COMM.HOST_URL + AIV_USER.SVC_REFRESH_TOKEN_URL,
            success: function (data, options) {
                // 데이타 저장
                var returnObj = data;

                // 토큰다시저장
                AIV_USER.setUserInfo("TOKEN", returnObj.token);

                // 이전 요청 재처리
                aivAjax(curOptions);
            }
        });
    };

    // $.aiv = $.aiv || {};
    var AIV_COMM = {
        HOST_URL: ENV_PROP.API_SERVER_URL,
        ajax: aivAjax,
        fileUpload: fileUpload,
        fileDownload: fileDownload
    };
    window.AIV_COMM = AIV_COMM;
    // $.aiv.ajax = AIV_COMM;
}(window));

