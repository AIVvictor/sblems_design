/**
 * 사용자 정보관리 (로그인/아웃 등 사용자 환경정보 관리)
 */

(function (w) {

	// var AIV_USER = AIV_USER || {};

	/**
	 * 스토리지 타입
	 * 0:로컬스토리지, 1:세션스토리지
	 */
	var storageType = 0;
	var storage = storageType == 0 ? w.localStorage : w.sessionStorage;


	/**
	 * 유저 로그인/정보 관련 API 관리
	 */
	var API = {
		// 서비스 URL 관리
		url: {
			login: '/api/v1/login',					// 로그인 요청 API
			allMenu: '/api/v1/common/menu/all',		// 전체메뉴 API
			authMenu: '/api/v1/common/menu/auth'	// 권한별메뉴 API
		},
		// 사용되는 함수는 뒤에서 구현
		fn: {
			login: null,
			getAllMenu: null,
			getAuthMenu: null,
		}
	}

	/**
	 * 페이지 정의
	 */
	var page = {
		login: '/v1/login.html',		// 권한 없을 경우 이동할 페이지 URL
		welcome: '/v1/home.html',	// 로그인 후 진입 첫 페이지 URL
		passAuth: ['/', '/index.html', '/v1/login.html']	// 권한 미체크 페이지들
	}
	
	/**
	 * 유저정보 기본형태
	 */
	var userInfo = {
		'sEmpId': '',
		'sAuthType': '',
		'token': ''
	};


	/**
	 * Token이 있는지 확인하여 없을 경우 페이지 이동
	 */
	var checkNullToken = function(){
		// 1. 세션이 있는지 체크
		var hasSession = AIV_UTIL.isNotEmpty(getUserInfo().token);

		// 2. 세션이 필요한 페이지인지 체크
		var isPassPage = page.passAuth.find(function(p){
 			return (p == location.pathname) || (p == location.pathname + '#');
		});

		// 3. 세션이 없고 세션 패스 가능한 페이지가 아니면 인덱스 페이지로 이동
		if ( !hasSession && !isPassPage ) {
			location.href = page.login;
		}
	}

	/**
	 * 로그인 요청처리(by username, password)
	 */
	API.fn.login = function (data) {
		// 로그인 요청(ajax)
		AIV_COMM.ajax({
			method : 'POST',
			url : API.url.login,
			data : data,
			success: function(jsonData, options, textStatus, jqXHR){
				setUserInfo(jsonData, jqXHR);	// 유저 정보 저장
				// API.fn.getAllMenu();			// 전체 메뉴 정보 조회	:dev mode
				API.fn.getAuthMenu();			// 권한 메뉴 정보 조회
			}
		});
	}

	/**
	 * 로그아웃 요청처리(clear storage)
	 */
	API.fn.logout = function(){
		resetUserInfo();	// 유저정보/스토리지 초기화
		checkNullToken();	// 권한토큰 없을 경우 로그인페이지로 이동
	};

	/**
	 * 전체 메뉴 조회 :dev mode
	 */
	API.fn.getAllMenu = function () {
		AIV_COMM.ajax({
			method : 'GET',
			url : API.url.allMenu,
			success: function(jsonData){
				setMenuInfo(jsonData);			// 메뉴 저장
				location.href = page.welcome;	// 첫 페이지로 이동
			}
		});
	}

	/**
	 * 권한별 메뉴 조회
	 */
	API.fn.getAuthMenu = function () {
		// 로그인 요청(ajax)
		AIV_COMM.ajax({
			method : 'GET',
			url : API.url.authMenu,
			success: function(jsonData){
				setMenuInfo(jsonData);			// 메뉴 저장
				location.href = page.welcome;	// 첫 페이지로 이동
			}
		});
	}

	/**
	 * 스토리지에서 유저 정보 및 메뉴 정보를 가져와 저장한다.
	 */
	var initUserInfo = function(){
		// AIV_USER.userInfo = getUserInfo();
	}

	/**
	 * 유저 정보 및 웹스토리지를 초기화 한다
	 */
	var resetUserInfo = function(){
		// 사용자정보 초기화
		storage.clear();
	}

	/**
	 * 유저 정보를 웹스토리지에 저장한다.
	 */
	var setUserInfo = function(jsonData, jqXHR){
		
		initUserInfo();	// 정보 초기화
		
		if ( jsonData.resultCode == '000' ) {
			// 유저 정보 세팅
			userInfo.sEmpId = 	 jsonData.resultData.sEmpId;
			userInfo.sEmpNm = 	 jsonData.resultData.sEmpNm;
			userInfo.sAuthType = jsonData.resultData.sAuthType;
			// userInfo.token = 	 jqXHR.getResponseHeader('jwt-header');	// CORS규격 변경에 의해 사용중지되었음
			userInfo.token = 	 jsonData.resultData.jwtHeader;

			// 스토리지 저장
			storage.setItem('aiv_userInfo', JSON.stringify(userInfo));
		} else {

		}

		
	}

	/**
	 * 사용자 저장 정보 리턴
	 */
	var getUserInfo = function() {
		return JSON.parse(storage.getItem('aiv_userInfo')) || {};
	};


	/**
	 * 메뉴정보를 스토리지에 저장한다
	 */
	var setMenuInfo = function(jsonData){
		if ( jsonData.resultCode == '000' ) {
			storage.setItem('aiv_menus', JSON.stringify(jsonData.resultData));
		}
	}
	/**
	 * 메뉴정보를 스토리지에서 가져온다
	 */
	var getMenuInfo = function(){
		return JSON.parse(storage.getItem('aiv_menus')) || [];
	}

	/**
	 * Modal layer를 통해 암호를 변경한다.
	 */
	var updateUserInfo = function(){
		alert('구현대기중');
	}





	
	if ( storageType == 0 ) {
		w.localStorage = storage;
	} else {
		w.sessionStorage = storage;
	}

	
	var RETURN_AIV_USER = {
		login: API.fn.login,
		logout: API.fn.logout,
		checkNullToken: checkNullToken,
		// userInfo: userInfo,
		getUserInfo: getUserInfo,
		getMenuInfo: getMenuInfo,
		updateUserInfo: updateUserInfo,
    };
	w.AIV_USER = RETURN_AIV_USER;
	
	checkNullToken();	// 세션이 있는지 확인
	// initUserInfo();		// 유저 정보를 입력
}(window));