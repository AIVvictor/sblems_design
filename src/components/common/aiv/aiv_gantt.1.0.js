/**
 * Gantt 차트
 */
(function ($) {

    // 기본 설정값

    var _color = {
        ldng: '#66d0ff',    // 상차 기본 색
        dlvy: '#00b0ff',    // 배송 기본 색
        ldngC: '#ff4646',   // C등급 기본 색
        dlvyC: '#e04545',   // C등급 기본 색
        ldngFF: '#d6d6d6',   // FF등급 기본 색
        dlvyFF: '#c1c1c1'   // FF등급 기본 색
        // cGrade: '#ff4646'   // C등급 기본 색
    }

    // 태스크 템플릿
    var _taskTemplate = '<div> #= gb #(#= grade #)</div>';

    // 툴팁 템플릿
    var _tooltipTemplate = function (rowOptions) {
        var task = rowOptions.task;
        var _el = '';
        _el += '<div class="k-task-details" style="display:inline-block;">';
        _el += '    <strong class="text-center">' + task.gb + ' 상세 정보</strong>';
        _el += '    <hr class="mt-2 mb-2">';
        _el += '    <div class="k-task-pct mt-1 mb-1">' + task.dlvyDe + '</div>';
        _el += '    <strong class="mb-1" >' + task.vrn + '</strong>';
        _el += '    <strong class="mb-3" >' + task.routeNo + '(' + task.caralcTy + '/' + task.vhcleTy + '톤)</strong>';
        _el += '    <hr class="mt-2 mb-2">';
        _el += '    <ul class="k-reset">';
        _el += '        <li>' + task.gb + ' 등급: ' + task.grade + '</li>';

        _el += '        <hr style="margin: 0;padding: 0;border-style: dashed;"/>';
        _el += '        <li>예정 시작 시간: ' + task.start.format('yyyy-mm-dd hh:mi') + '</li>';
        _el += '        <li>예정 종료 시간: ' + task.end.format('yyyy-mm-dd hh:mi') + '</li>';
        _el += '        <li>예정 소요 시간: ' + (new Date(task.end - task.start - 9 * 60 * 60 * 1000)).format('hh:mi') + '</li>';
        
        _el += '        <hr style="margin: 0;padding: 0;border-style: dashed;"/>';
        _el += '        <li>실제 시작 시간: ' + task.start.format('hh:mi') + '</li>';
        _el += '        <li>실제 종료 시간: ' + task.end.format('hh:mi') + '</li>';
        _el += '        <li>실제 소요 시간: ' + (new Date(task.end - task.start - 9 * 60 * 60 * 1000)).format('hh:mi') + '</li>';

        _el += '        <hr style="margin: 0;padding: 0;border-style: dashed;"/>';
        // if ( task.grade == 'FF' ) {
        //     _el += '        <li>(예정) 시작 시간: ' + task.start.format('yyyy-mm-dd hh:mi') + '</li>';
        //     _el += '        <li>(예정) 종료 시간: ' + task.end.format('yyyy-mm-dd hh:mi') + '</li>';
        //     _el += '        <li>(예정) 소요 시간: ' + (new Date(task.end - task.start - 9 * 60 * 60 * 1000)).format('dd일 hh:mi') + '</li>';
        // } else {
        //     _el += '        <li>시작 시간: ' + task.start.format('hh:mi') + '</li>';
        //     _el += '        <li>종료 시간: ' + task.end.format('hh:mi') + '</li>';
        //     _el += '        <li>소요 시간: ' + (new Date(task.end - task.start - 9 * 60 * 60 * 1000)).format('hh:mi') + '</li>';
        // }
        _el += '        <li>기준 시간: ' + task.stdTime + '</li>';
        _el += '    </ul>';
        _el += '</div>';
        return _el;
    }

    var _pdfText = 'PDF Download';
    var _fileName = AIV_MENU.currentPageTitle + '.pdf' || "2ch_export";

    var _defaultOptions = {
        dataSource: {
            type: 'json',
            schema: {
                model: {
                    id: "id",
                    // id: "rowId",
                    fields: {
                        id: { from: "id", type: "number" },
                        // orderId: { from: "OrderID", type: "number", validation: { required: true } },
                        // parentId: { from: "ParentID", type: "number", defaultValue: null, validation: { required: true } },
                        // vrn: { from: "vrn", type: "string" },
                        // title: { from: "title", defaultValue: "", type: "string" },   // title == vrn
                        start: { from: "start", type: "date" },
                        end: { from: "end", type: "date" },
                        // percentComplete: { from: "percentComplete", type: "number", defaultValue: 1 },
                        summary: { from: "summary", type: "boolean", defaultValue: false },
                        expanded: { from: "expanded", type: "boolean", defaultValue: false }
                    }
                }
            }
        },
        views: [
            // {
            //     type: "day",
            //     slotSize: 50,
            //     selected: true,
            //     date: new Date(options.preDe + " 16:00"),
            //     range: {
            //         start: new Date(options.preDe + " 00:00"),
            //         end: new Date(options.dlvyDe + " 24:00")
            //     },
            //     timeHeaderTemplate: kendo.template("#= start.format('hh:mi')#"),
            //     dayHeaderTemplate: function(t){
            //         if( t.start.getDate() == 1 ) {
            //             return '전일';
            //         } else {
            //             return '당일';
            //         }
            //     },
            // }, {
            //     type: "week",
            //     slotSize: 1671/7,
            //     range: {
            //         start: new Date(kendo.date.previousDay(TAB1.params.dlvyDe.kValue()).format('yyyy-mm-dd') + " 00:00"),
            //         end: new Date(TAB1.params.dlvyDe.kValue().format('yyyy-mm-dd') + " 24:00")
            //     },
            //     dayHeaderTemplate: kendo.template("#= start.format('yyyy-mm-dd')#"),
            //     weekHeaderTemplate: kendo.template("#= start.format('yyyy-mm-dd')# ~ #= end.format('yyyy-mm-dd')#"),
            // },
            // "month"
        ],
        tooltip: {
            visible: true,
            template: _tooltipTemplate
        },
        taskTemplate: _taskTemplate,    // TASK TITLE
        listWidth: 125, //160,
        height: 700,
        editable: false,
        showWorkHours: false,
        showWorkDays: false,
        snap: false,
        toolbar: [
            {
                name: "pdf",
                text: _pdfText,
            },
        ],
        pdf:{
            fileName: _fileName
        },
        dataBound: function () {  // 컬럼 색 변화
            var gantt = this;
            gantt.element.find(".k-task").each(function (e) {
                var bgColor = '',
                    bdColor = '';
                var dataItem = gantt.dataSource.getByUid($(this).attr("data-uid"));

                if (dataItem.gb == '상차') {
                    // C등급 색 지정
                    if (dataItem.grade == 'C') {
                        bgColor = _color.ldngC;
                        bdColor = _color.ldngC;
                    // FF등급 색 지정
                    } else if (dataItem.grade == 'FF') {
                        bgColor = _color.ldngFF;
                        bdColor = _color.ldngFF;
                    // 상차 기본 색 지정
                    } else {
                        bgColor = _color.ldng;
                        bdColor = _color.ldng;
                    }
                } else {
                    // C등급 색 지정
                    if (dataItem.grade == 'C') {
                        bgColor = _color.dlvyC;
                        bdColor = _color.dlvyC;
                    // FF등급 색 지정
                    } else if (dataItem.grade == 'FF') {
                        bgColor = _color.dlvyFF;
                        bdColor = _color.dlvyFF;
                    // 배송 기본 색 지정
                    } else {
                        bgColor = _color.dlvy;
                        bdColor = _color.dlvy;
                    }
                }
                this.style.backgroundColor = bgColor;
                this.style.borderColor = bdColor;
            });
        }
    }

    // 그리드 세팅
    $.fn.AIV_GANTT = function (options) {

        var me = this;
        var selfId = me.attr('id');
        // me.options = options;

        // 기본 및 사용자 정의 옵션
        var defaultOptions = _defaultOptions;
        defaultOptions.columns = options.columns;
        var nextDe = new Date(new Date(options.dlvyDe).getTime() + 1*24*60*60*1000).format('yyyy-mm-dd'); // add 1day

        defaultOptions.views = [{
            type: "day",
            slotSize: 50,
            selected: true,
            date: new Date(options.preDe + "T16:00"),
            range: {
                start: new Date(options.preDe + "T00:00"),
                // end: new Date(options.dlvyDe + "T24:00")
                end: new Date(nextDe + "T24:00")
            },
            timeHeaderTemplate: kendo.template("#= start.format('hh:mi')#"),
            dayHeaderTemplate: kendo.template("#= start.format('yyyy-mm-dd')#"),
        }];

        // Gantt 차트 생성
        me.kendoGantt(defaultOptions);
        // Gantt 차트 data 객체 저장
        me.self = me.data("kendoGantt");

        // 데이터 생성
        me.setData = function (datas, dlvyDe) {

            var ganttDatas = [];
            $(datas).each(function(i, data){
                // start 및 end 시간 있는 데이터만 차트 데이터로 사용
                var nullTime = new Date(null).getTime();
                if ( data.start.getTime() != nullTime && data.end.getTime() != nullTime ) {
                    ganttDatas.push(data);
                }
            })

            // 초기화
            me.self.setDataSource(new kendo.data.GanttDataSource({}));
            
            var ganttDataSource = new kendo.data.GanttDataSource({
                data: ganttDatas
            });
            // 데이터 입력
            me.self.setDataSource(ganttDataSource)

            me.self.resize();

            var cDate = new Date(dlvyDe);
            var cDateFmt = cDate.format('yyyy-mm-dd');
            var pDate = kendo.date.previousDay(cDate);
            var pDateFmt = pDate.format('yyyy-mm-dd');

            var _date = pDate.format('yyyy-mm-ddT16:00');
            var range = {
                start: new Date(pDateFmt + "T00:00"),    // view 시작
                // end: new Date(cDateFmt + "T24:00")       // view 종료
                end: new Date(nextDe + "T24:00")       // view 종료
            };
            me.self.date(new Date(_date));  // view 기본 위치
            me.self.range(range);           // view 기본 범위

            // 중복 행 제거
            removeRow(selfId);
            // 좌측 컬럼에 CSS 설정
            setColumnCss(selfId);
            // 좌측 컬럼에 클릭 이벤트 설정
            setColumnEventGoDtls(selfId);
            
            // 간격 재조정
            me.self.refresh();

        }

        return me;
    }

    // 그리드 세팅
    $.fn.AIV_GANTT_DTLS = function (options) {

        var me = this;
        var selfId = me.attr('id');
        // me.options = options;

        // 기본 및 사용자 정의 옵션
        var defaultOptions = _defaultOptions;
        defaultOptions.columns = options.columns;
        defaultOptions.views = [{
            type: "day",
            slotSize: 50,
            selected: true,
            date: new Date(options.preDe + "T16:00"),
            range: {
                start: new Date(options.preDe + "T00:00"),
                end: new Date(options.dlvyDe + "T24:00")
            },
            timeHeaderTemplate: kendo.template("#= start.format('hh:mi')#"),
            dayHeaderTemplate: function(t){
                if( t.start.getDate() == 1 ) {
                    return '전일';
                } else {
                    return '당일';
                }
            }
        }];

        // Gantt 차트 생성 및 data 객체 저장
        me.self = me.kendoGantt(defaultOptions).data("kendoGantt");

        // 데이터 생성
        me.setData = function (datas, dlvyDe) {

            // 초기화
            me.self.setDataSource(new kendo.data.GanttDataSource({}));

            var ganttDataSource = new kendo.data.GanttDataSource({
                data: datas
            });
            // 데이터 입력
            me.self.setDataSource(ganttDataSource)

            $(me).find('.k-gantt-layout.k-gantt-treelist').css('width', 125);
            me.self.resize();

            var cDate = new Date(dlvyDe);
            var cDateFmt = cDate.format('yyyy-mm-dd');
            var pDate = kendo.date.previousDay(cDate);
            var pDateFmt = pDate.format('yyyy-mm-dd');

            var _date = pDate.format('yyyy-mm-ddT16:00');
            var range = {
                start: new Date(pDateFmt + "T00:00"),    // view 시작
                end: new Date(cDateFmt + "T24:00")       // view 종료
            };
            me.self.date(new Date(_date));  // view 기본 위치
            me.self.range(range);           // view 기본 범위

            // 중복 행 제거
            // for (var i=0 ; i<10; i++) {
                removeRow(selfId);
            // }

            // 좌측 컬럼에 css 정의
            setColumnCss(selfId);
            // 좌측 컬럼에 클릭 이벤트 설정
            setColumnEventPopDriveDiary(selfId);

            // 간격 재조정
            me.self.refresh();
            
        }

        return me;
    }

    // 중복 행 제거
    function removeRow(id) {
        
        // 크롬 환경에서 실행시 버그로 인해 중간 행과 첫행 위치 변환
        if ( AIV_UTIL.checkBroswer() == 'chrome' ) {

            // 차량번호 리스트
            var idSelector = '#' + id + ' .k-gantt-treelist .k-grid-content';
            var $idTlList = $(idSelector).find('tr');

            // task 리스트
            var taskSelector = '#' + id + ' .k-gantt-timeline table.k-gantt-tasks';
            var $taskTrList = $(taskSelector).find('tr');

            // row 리스트
            var rowSelector = '#' + id + ' .k-gantt-timeline table.k-gantt-rows';
            var $rowTrList = $(rowSelector).find('tr');

            var taskLen = $taskTrList.length;
            for ( var i = taskLen-1; i>=0; i-- ) {
                if ( i !=0 && $idTlList.eq(i-1).text() == $idTlList.eq(i).text() ) {
                    $taskTrList.eq( i-1 ).find('td')
                        .append($taskTrList.eq(i).find('div.k-task-wrap')); // task 이동

                    $idTlList.eq(i).remove();   // 중복 차량번호 제거
                    $taskTrList.eq(i).remove(); // task 이동 후 남은 DOM 제거
                    $rowTrList.eq(i).remove();  // task 에 맞게 row DOM 제거
                }
            }
        }
    }

    // 좌측 컬럼에 CSS 정의
    function setColumnCss(id) {
        var $tdList = $('#' + id + ' .k-gantt-treelist .k-grid-content').find('tr').find('td:eq(0)');
        $tdList.each(function (i, td) {
            $(td).css('text-align', 'center')
        })
    }

    // 좌측 컬럼에 클릭 이벤트 정의 -> 상세 탭으로 이동
    function setColumnEventGoDtls(id) {
        var $tdList = $('#' + id + ' .k-gantt-treelist .k-grid-content').find('tr').find('td:eq(0)');
        $tdList.each(function (i, td) {
            var uid = $(td).closest('tr').attr('data-uid');
            $(td).css('cursor', 'pointer')
                .css('cursor', 'pointer')
                .attr('onclick', 'TAB1.fn.goDtls(event,\'' + uid + '\')')
        });
    }

    // 좌측 컬럼에 클릭 이벤트 정의 -> 운행일지 팝업 호출
    function setColumnEventPopDriveDiary(id){
        var $tdList = $('#' + id + ' .k-gantt-treelist .k-grid-content').find('tr').find('td:eq(0)');
        $tdList.each(function (i, td) {
            var uid = $(td).closest('tr').attr('data-uid');
            var dlvyDe = $(td).text();
            var vrn = TAB2.params.vrn.kValue();
            $(td).css('cursor', 'pointer')
                .css('cursor', 'pointer')
                .attr('onclick', 'AIV_WINDOW.driveDtls(\'' + dlvyDe + '\',\'' + vrn + '\')');
        })
    }

}(jQuery));