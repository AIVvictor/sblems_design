// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
// Templates Meta Parts
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
document.write('<meta http-equiv="content-type" content="text/html;charset=UTF-8" />');
document.write('<meta charset="utf-8"></meta>');
document.write('<meta http-equiv="X-UA-Compatible" content="IE=edge"></meta>');
document.write('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />');
document.write('<link rel="apple-touch-icon" href="/pages/ico/60.png">');
document.write('<link rel="apple-touch-icon" sizes="76x76" href="/pages/ico/76.png">');
document.write('<link rel="apple-touch-icon" sizes="120x120" href="/pages/ico/120.png">');
document.write('<link rel="apple-touch-icon" sizes="152x152" href="/pages/ico/152.png">');
document.write('<meta name="apple-mobile-web-app-capable" content="yes">');
document.write('<meta name="apple-touch-fullscreen" content="yes">');
document.write('<meta name="apple-mobile-web-app-status-bar-style" content="default">');
document.write('<meta content="" name="description" />');
document.write('<meta content="" name="author" />');


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
// Kendo UI
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
// Plugin JS
document.write('<script src="/src/assets/kendo/plugins/pace/pace.min.js" type="text/javascript"></script>');
// Plugin CSS
document.write('<link href="/src/assets/kendo/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />');
document.write('<link href="/src/assets/kendo/plugins/bootstrap/css/bootstrap.custom.css" rel="stylesheet" type="text/css" />');    // bootstrap.min.css
document.write('<link href="/src/assets/kendo/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />');
document.write('<link href="/src/assets/kendo/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />');
document.write('<link href="/src/assets/kendo/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />');
document.write('<link href="/src/assets/kendo/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />');
// Kendo JS
document.write('<script src="/src/assets/kendo/js/kendo/jquery.min.js"></script>');
document.write('<script src="/src/assets/kendo/js/sockjs.min.js"></script>');
document.write('<script src="/src/assets/kendo/js/stomp.min.js"></script>');
document.write('<script src="/src/assets/kendo/js/jquery.ajax-cross-origin.min.js"></script>');
document.write('<script src="/src/assets/kendo/js/kendo/jszip.min.js"></script>');
document.write('<script src="/src/assets/kendo/js/kendo/kendo.all.min.js"></script>');
document.write('<script src="/src/assets/kendo/js/kendo/cultures/kendo.culture.ko-KR.min.js"></script>');
document.write('<script src="/src/assets/kendo/plugins/modernizr.custom.js" type="text/javascript"></script>');
document.write('<script src="/src/assets/kendo/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>');
document.write('<script src="/src/assets/kendo/plugins/tether/js/tether.min.js" type="text/javascript"></script>');
document.write('<script src="/src/assets/kendo/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>');
document.write('<script src="/src/assets/kendo/plugins/jquery/jquery-easy.js" type="text/javascript"></script>');
document.write('<script src="/src/assets/kendo/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>');
document.write('<script src="/src/assets/kendo/plugins/jquery-bez/jquery.bez.min.js"></script>');
document.write('<script src="/src/assets/kendo/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>');
document.write('<script src="/src/assets/kendo/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>');
document.write('<script src="/src/assets/kendo/plugins/jquery-actual/jquery.actual.min.js"></script>');
document.write('<script src="/src/assets/kendo/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>');
document.write('<script src="/src/assets/kendo/plugins/classie/classie.js" type="text/javascript"></script>');
// Kendo CSS
document.write('<link href="/src/assets/kendo/css/nanumgothic.css" rel="stylesheet" type="text/css">');
document.write('<link href="/src/assets/kendo/css/nanumgothiccoding.css" rel="stylesheet" type="text/css">');
document.write('<link href="/src/assets/kendo/css/nanummyeongjo.css" rel="stylesheet" type="text/css">');
document.write('<link href="/src/assets/kendo/css/nanumbrushscript.css" rel="stylesheet" type="text/css">');
document.write('<link href="/src/assets/kendo/css/nanumpenscript.css" rel="stylesheet" type="text/css">');
document.write('<link href="/src/assets/kendo/css/nanumbarungothic.css" rel="stylesheet" type="text/css">');
document.write('<link href="/src/assets/kendo/css/kendo/kendo.common.min.css" rel="stylesheet">');
document.write('<link href="/src/assets/kendo/css/kendo/kendo.rtl.min.css" rel="stylesheet">');
document.write('<link href="/src/assets/kendo/css/kendo/kendo.material.custom.css" rel="stylesheet">');
document.write('<link href="/src/assets/kendo/css/kendo/kendo.default.mobile.min.css" rel="stylesheet">');

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
// Pages JS
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
document.write('<link href="/src/assets/pages/css/pages-icons.css" rel="stylesheet" type="text/css">');
document.write('<link href="/src/assets/pages/css/pages.custom.css" rel="stylesheet" type="text/css" class="main-stylesheet" />');  // pages.css
// document.write('<script src="/src/assets/pages/js/pages.js" type="text/javascript"></script>');
// document.write('<script src="/src/assets/kendo/js/scripts.js" type="text/javascript"></script>');

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
// amCharts 4
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
document.write('<script src="/src/assets/amcharts_4.5.14/amcharts4/core.js"></script>');
document.write('<script src="/src/assets/amcharts_4.5.14/amcharts4/charts.js"></script>');
document.write('<script src="/src/assets/amcharts_4.5.14/amcharts4/themes/material.js"></script>');
document.write('<script src="/src/assets/amcharts_4.5.14/amcharts4/lang/ko_KR.js"></script>');

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
// Custom CSS
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
document.write('<link href="/src/css/kendo.custom.css" rel="stylesheet" type="text/css">');
document.write('<link href="/src/css/aiv.style.css" rel="stylesheet" type="text/css">');

// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
// AIV Common JS
// ----------------------------------------------------------------------------------------------------------------------------------------------------------------
document.write('<script src="/src/components/common/aiv/aiv_include.1.0.js" type="text/javascript"></script>');





// 최상단 HEADER 세팅.
// $("#body_header").load("/v1/menu/body_header.html");

// 페이지상단 HEADER 세팅.
//$("#page_header").load("/ui/menu/page_header.html");

// 페이지하단 FOOTER 세팅.
//$("#page_footer").load("/ui/menu/page_footer.html");

// 우측 슬라이드 QUICKVIEW 로드..
// $("#page_slider").load("/v1/menu/page_slider.html");