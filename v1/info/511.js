﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    // FORM 의 mode와 같이 버튼도 show/hide 함수
    PAGE.fn.setModeBtn = function (mode) {
        if (mode == 'edit') {
            PAGE.btn.edit.hide();
            PAGE.btn.save.show();
            PAGE.btn.cancel.show();
        } else if (mode == 'read') {
            PAGE.btn.edit.show();
            PAGE.btn.save.hide();
            PAGE.btn.cancel.hide();
        }
    }

    // FORM 데이터 로드 함수
    PAGE.fn.formDataLoad = function () {
        //1. 서버로부터 기존의 저장된 데이터를 불러와 텍스트 박스를 채움
        AIV_COMM.ajax({
            method: "GET",
            url: "/api/v1/info/511/dtl",
            success: function (responsData) {
                PAGE.form.main.setAllValue(responsData.resultData);
            }
        });
    }

    // 편집버튼 클릭시 함수
    PAGE.fn.onBtnEdit = function () {
        PAGE.form.main.setMode('edit');
        PAGE.fn.setModeBtn('edit');
    }

    // 저장버튼 클릭시 함수
    PAGE.fn.onBtnSave = function () {
        var formsValue = PAGE.form.main.getAllValue();
        formsValue.sCorpId = '7000000';

        if ( PAGE.form.main.isValid() ) {   // 유효성 체크
            // TODO: save api
            AIV_COMM.ajax({
                method: "POST",
                url: "/api/v1/info/511/dtl",
                data: formsValue,
                success: function (responseData) {
                    PAGE.fn.formDataLoad();
                }
            });
            PAGE.form.main.setMode('read');
            PAGE.fn.setModeBtn('read');
        }
    }

    // 취소버튼 클릭시 함수
    PAGE.fn.onBtnCancel = function () {
        PAGE.fn.formDataLoad();
        PAGE.form.main.setMode('read');
        PAGE.fn.setModeBtn('read');
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT();

    // PAGE 내 폼 객체 세팅
    PAGE.form.main = $('#form').AIV_FORM_INIT();

    // 버튼 세팅
    PAGE.btn.edit = $("#search_area #btnEdit").AIV_BUTTON({
        title: "편집",
        class: 'k-primary',
        icon: 'edit',
        onClick: PAGE.fn.onBtnEdit,
    });

    PAGE.btn.save = $("#search_area #btnSave").AIV_BUTTON({
        title: "저장",
        class: 'k-primary',
        icon: 'save',
        onClick: PAGE.fn.onBtnSave,
    });

    PAGE.btn.cancel = $("#search_area #btnCancel").AIV_BUTTON({
        title: "취소",
        // class: 'k-primary',
        icon: 'cancel',
        onClick: PAGE.fn.onBtnCancel,
    });

    // form data load
    PAGE.fn.formDataLoad();
}