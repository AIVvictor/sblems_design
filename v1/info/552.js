﻿var TAB1 = AIV_UTIL.initPageObj();
var TAB2 = AIV_UTIL.initPageObj();
var TAB3 = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    // 탭 클릭시 이벤트 설정
    $('.nav-tabs a').click(function () {
        $(this).tab('show');
    });

    // 1번 탭 초기설정
    fn_initTab1();
    // 2번 탭 초기설정
    fn_initTab2();
    // 3번 탭 초기설정
    fn_initTab3();
}

// TAB1
function fn_initTab1() {
    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. TAB1.fn.*     사용 함수 설정
     * 2. TAB1.params.* 설정 기능 객체 설정 (input)
     * 3. TAB1.btn.*    이벤트 발생 객체 설정 (button)
     * 4. TAB1.grid.*   그리드 객체 설정
     * 5. TAB1.chart.*  함수 객체 설정
     */

    // TAB1 Main Grid 조회
    TAB1.fn.search = function () {
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/5521/ie/ls?sCefCd=2&sEmtCd=3',
            success: function (responseData) {
                TAB1.grid.main.setData(responseData.resultData);
                TAB1.grid.sub.setData([]);
                TAB1.grid.sub.idEngCef = undefined;
            }
        })
    };

    // TAB1 Sub Grid 조회
    TAB1.fn.searchSubGrid = function (idEngCef) {
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/5511/def/ls',
            data: {
                'idEngCef': idEngCef
            },
            success: function (responseData) {
                TAB1.grid.sub.idEngCef = idEngCef;
                TAB1.grid.sub.setData(responseData.resultData);
            }
        })
    }

    // TAB1 내 컨트롤객체 세팅
    TAB1.params = $("#search_area1").AIV_SEARCH_INIT({
        onKeyup: TAB1.fn.search
    });

    // 버튼 세팅
    TAB1.btn.search = $("#search_area1 #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: TAB1.fn.search,
    });

    // 그리드 세팅
    TAB1.grid.main = $("#grid1").AIV_GRID({
        height: 300,
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'add', 'destroy', 'save', 'cancel'
        ],
        events: {
            editToolbarOnClick: function () {
                var isEditMode = TAB1.grid.sub.isEditMode();
                // main grid를 편집모드로 변경시 sub grid를 읽기모드로 바꿔줘야 함
                if (isEditMode) {
                    $(TAB1.grid.sub).find('.k-grid-cancel-changes').click();    // sub grid의 cancel toolbar 클릭
                    TAB1.grid.sub.setData([]);
                    TAB1.grid.sub.idEngCef = undefined;
                }
                return false;   // true로 반환할 경우 편집모드로 변환이 안됨
            }
        },
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/info/5521/ie/ls',
                beforeSend: function (hasCheckRow) {
                    $(hasCheckRow).each(function (i, row) {
                        row.sCefCd = 2;
                        row.sEmtCd = 3;
                    })
                    return hasCheckRow;
                },
                success: function () {
                    TAB1.fn.search();
                }
            },
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/5521/ie/ls',
                beforeSend: function (hasCheckRow) {
                    var deleteData = {};
                    var idLists = [];
                    $(hasCheckRow).each(function (i, row) {
                        idLists.push(row.idCefMod);
                    })
                    deleteData.idList = idLists.join(',');
                    return deleteData;
                },
                success: function () {
                    TAB1.fn.search();
                }
            }
        },
        columns: [
            { kField: "selectable" },
            {
                kField: "sEngCdNm",
                template: function (dataItem) {
                    var isEditMode = TAB1.grid.main.isEditMode();
                    if (isEditMode) {
                        return dataItem.sEngCdNm;
                    } else {
                        return '<a href="#" onClick="javascript:TAB1.fn.searchSubGrid(' + dataItem.idEngCef + ');">' + dataItem.sEngCdNm + '</a>';
                    }
                }
            },
            { kField: "sMrvengCdNm" },
            { kField: "sInUntCdNm" },
            { kField: "sCalUntCdNm" },
            { kField: "nFactor" },
            { kField: "nOxyVal" },
            {
                kField: "sEmiCd"
                , template: function(dataItem){
                    return dataItem.sEmiNm;
                }
            },
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    TAB1.grid.sub = $("#grid2").AIV_GRID({
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'add', 'destroy', 'save', 'cancel'
        ],
        events: {
            editToolbarOnClick: function () {
                if (AIV_UTIL.isEmpty(TAB1.grid.sub.idEngCef)) {
                    alert('편집할 행을 선택해 주십시오.');
                    return true;
                }
                var isEditMode = TAB1.grid.main.isEditMode();
                if (isEditMode) {
                    alert('편집중인 그리드를 저장 또는 취소하신 뒤 편집할 수 있습니다.');
                    return true;    // 이후 이벤트 진행을 멈출지 여부를 return 해 주어야 함
                } else {
                    return false;   // true로 반환할 경우 편집모드로 변환이 안됨
                }
            }
        },
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/info/5521/ief/ls',
                beforeSend: function (hasCheckRow) {
                    $(hasCheckRow).each(function (i, row) {
                        row.idEngCef = TAB1.grid.sub.idEngCef;
                    })
                    return hasCheckRow;
                },
                success: function () {
                    TAB1.fn.searchSubGrid(TAB1.grid.sub.idEngCef);
                }
            },
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/5521/ief/ls',
                beforeSend: function (hasCheckRow) {
                    var deleteData = {};
                    var idLists = [];
                    $(hasCheckRow).each(function (i, row) {
                        idLists.push(row.idCefMod);
                    })
                    deleteData.idList = idLists.join(',');
                    return deleteData;
                },
                success: function () {
                    TAB1.fn.searchSubGrid(TAB1.grid.sub.idEngCef);
                }
            }
        },
        columns: [
            { kField: 'selectable' },
            { kField: "cYear" },
            { kField: "nHeat" },
            { kField: "nTotHeat" },
            {
                title: '배출계수',
                columns: [
                    { kField: "nCo2Cef" },
                    { kField: "nCh4Cef" },
                    { kField: "nN2oCef" },
                ]
            },
            { kField: 'nModCal' }
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    TAB1.fn.search(); // 최초 실행
}

// TAB2
function fn_initTab2() {
    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. TAB2.fn.*     사용 함수 설정
     * 2. TAB2.params.* 설정 기능 객체 설정 (input)
     * 3. TAB2.btn.*    이벤트 발생 객체 설정 (button)
     * 4. TAB2.grid.*   그리드 객체 설정
     * 5. TAB2.chart.*  함수 객체 설정
     */

    // TAB2 Main Grid 조회
    TAB2.fn.search = function () {
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/5521/ie/ls?sCefCd=2&sEmtCd=4',
            success: function (responseData) {
                TAB2.grid.main.setData(responseData.resultData);
                TAB2.grid.sub.setData([]);
                TAB2.grid.sub.idEngCef = undefined;
            }
        })
    };

    // TAB2 Sub Grid 조회
    TAB2.fn.searchSubGrid = function (idEngCef) {
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/5521/ief/ls',
            data: {
                'idEngCef': idEngCef
            },
            success: function (responseData) {
                TAB2.grid.sub.idEngCef = idEngCef;
                TAB2.grid.sub.setData(responseData.resultData);
            }
        })
    }

    // TAB2 내 컨트롤객체 세팅
    TAB2.params = $("#search_area2").AIV_SEARCH_INIT({
        onKeyup: TAB2.fn.search
    });

    // 버튼 세팅
    TAB2.btn.search = $("#search_area2 #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: TAB2.fn.search,
    });

    // 그리드 세팅
    TAB2.grid.main = $("#grid3").AIV_GRID({
        height: 300,
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'add', 'destroy', 'save', 'cancel'
        ],
        events: {
            editToolbarOnClick: function () {
                var isEditMode = TAB2.grid.sub.isEditMode();
                // main grid를 편집모드로 변경시 sub grid를 읽기모드로 바꿔줘야 함
                if (isEditMode) {
                    $(TAB2.grid.sub).find('.k-grid-cancel-changes').click();    // sub grid의 cancel toolbar 클릭
                    TAB2.grid.sub.setData([]);
                    TAB2.grid.sub.idEngCef = undefined;
                }
                return false;   // true로 반환할 경우 편집모드로 변환이 안됨
            }
        },
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/info/5521/ie/ls',
                beforeSend: function (hasCheckRow) {
                    $(hasCheckRow).each(function (i, row) {
                        row.sCefCd = 2;
                        row.sEmtCd = 4;
                    })
                    return hasCheckRow;
                },
                success: function () {
                    TAB2.fn.search();
                }
            },
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/5521/ie/ls',
                beforeSend: function (hasCheckRow) {
                    var deleteData = {};
                    var idLists = [];
                    $(hasCheckRow).each(function (i, row) {
                        idLists.push(row.idCefMod);
                    })
                    deleteData.idList = idLists.join(',');
                    return deleteData;
                },
                success: function () {
                    TAB2.fn.search();
                }
            }
        },
        columns: [
            { kField: "selectable" },
            {
                kField: "sEngCdNm",
                template: function (dataItem) {
                    var isEditMode = TAB2.grid.main.isEditMode();
                    if (isEditMode) {
                        return dataItem.sEngCdNm;
                    } else {
                        return '<a href="#" onClick="javascript:TAB2.fn.searchSubGrid(' + dataItem.idEngCef + ');">' + dataItem.sEngCdNm + '</a>';
                    }
                }
            },
            { kField: "sMrvengCdNm" },
            { kField: "sInUntCdNm" },
            { kField: "sCalUntCdNm" },
            { kField: "nFactor" },
            { kField: "nOxyVal" },
            {
                kField: "sEmiCd"
                , template: function(dataItem){
                    return dataItem.sEmiNm;
                }
            },
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    TAB2.grid.sub = $("#grid4").AIV_GRID({
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'add', 'destroy', 'save', 'cancel'
        ],
        events: {
            editToolbarOnClick: function () {
                if (AIV_UTIL.isEmpty(TAB2.grid.sub.idEngCef)) {
                    alert('편집할 행을 선택해 주십시오.');
                    return true;
                }
                var isEditMode = TAB2.grid.main.isEditMode();
                if (isEditMode) {
                    alert('편집중인 그리드를 저장 또는 취소하신 뒤 편집할 수 있습니다.');
                    return true;    // 이후 이벤트 진행을 멈출지 여부를 return 해 주어야 함
                } else {
                    return false;   // true로 반환할 경우 편집모드로 변환이 안됨
                }
            }
        },
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/info/5521/ief/ls',
                beforeSend: function (hasCheckRow) {
                    $(hasCheckRow).each(function (i, row) {
                        row.idEngCef = TAB2.grid.sub.idEngCef;
                    })
                    return hasCheckRow;
                },
                success: function () {
                    TAB2.fn.search(TAB2.grid.sub.idEngCef);
                }
            },
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/5521/ief/ls',
                beforeSend: function (hasCheckRow) {
                    var deleteData = {};
                    var idLists = [];
                    $(hasCheckRow).each(function (i, row) {
                        idLists.push(row.idCefMod);
                    })
                    deleteData.idList = idLists.join(',');
                    return deleteData;
                },
                success: function () {
                    TAB2.fn.search(TAB2.grid.sub.idEngCef);
                }
            }
        },
        columns: [
            { kField: 'selectable' },
            { kField: "cYear" },
            { kField: "nHeat" },
            { kField: "nTotHeat" },
            {
                title: '배출계수',
                columns: [
                    { kField: "nCo2Cef" },
                    { kField: "nCh4Cef" },
                    { kField: "nN2oCef" },
                ]
            },
            { kField: 'nModCal' }
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    TAB2.fn.search(); // 최초 실행
}

// TAB3
function fn_initTab3() {
    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. TAB3.fn.*     사용 함수 설정
     * 2. TAB3.params.* 설정 기능 객체 설정 (input)
     * 3. TAB3.btn.*    이벤트 발생 객체 설정 (button)
     * 4. TAB3.grid.*   그리드 객체 설정
     * 5. TAB3.chart.*  함수 객체 설정
     */

    // TAB3 Main Grid 조회
    TAB3.fn.search = function () {
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/5521/ie/ls?sCefCd=2&sEmtCd=5',
            success: function (responseData) {
                TAB3.grid.main.setData(responseData.resultData);
                TAB3.grid.sub.setData([]);
                TAB3.grid.sub.idEngCef = undefined;
            }
        })
    };

    // TAB3 Sub Grid 조회
    TAB3.fn.searchSubGrid = function (idEngCef) {
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/5521/ief/ls',
            data: {
                'idEngCef': idEngCef
            },
            success: function (responseData) {
                TAB3.grid.sub.idEngCef = idEngCef;
                TAB3.grid.sub.setData(responseData.resultData);
            }
        })
    }

    // TAB3 내 컨트롤객체 세팅
    TAB3.params = $("#search_area3").AIV_SEARCH_INIT({
        onKeyup: TAB3.fn.search
    });

    // 버튼 세팅
    TAB3.btn.search = $("#search_area3 #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: TAB3.fn.search,
    });

    // 그리드 세팅
    TAB3.grid.main = $("#grid5").AIV_GRID({
        height: 300,
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'add', 'destroy', 'save', 'cancel'
        ],
        events: {
            editToolbarOnClick: function () {
                var isEditMode = TAB3.grid.sub.isEditMode();
                // main grid를 편집모드로 변경시 sub grid를 읽기모드로 바꿔줘야 함
                if (isEditMode) {
                    $(TAB3.grid.sub).find('.k-grid-cancel-changes').click();    // sub grid의 cancel toolbar 클릭
                    TAB3.grid.sub.setData([]);
                    TAB3.grid.sub.idEngCef = undefined;
                }
                return false;   // true로 반환할 경우 편집모드로 변환이 안됨
            }
        },
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/info/5521/ie/ls',
                beforeSend: function (hasCheckRow) {
                    $(hasCheckRow).each(function (i, row) {
                        row.sCefCd = 2;
                        row.sEmtCd = 5;
                    })
                    return hasCheckRow;
                },
                success: function () {
                    TAB3.fn.search();
                }
            },
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/5521/ie/ls',
                beforeSend: function (hasCheckRow) {
                    var deleteData = {};
                    var idLists = [];
                    $(hasCheckRow).each(function (i, row) {
                        idLists.push(row.idCefMod);
                    })
                    deleteData.idList = idLists.join(',');
                    return deleteData;
                },
                success: function () {
                    TAB3.fn.search();
                }
            }
        },
        columns: [
            { kField: "selectable" },
            {
                kField: "sEngCdNm",
                template: function (dataItem) {
                    var isEditMode = TAB3.grid.main.isEditMode();
                    if (isEditMode) {
                        return dataItem.sEngCdNm;
                    } else {
                        return '<a href="#" onClick="javascript:TAB3.fn.searchSubGrid(' + dataItem.idEngCef + ');">' + dataItem.sEngCdNm + '</a>';
                    }
                }
            },
            { kField: "sMrvengCdNm" },
            { kField: "sInUntCdNm" },
            { kField: "sCalUntCdNm" },
            { kField: "nFactor" },
            { kField: "nOxyVal" },
            {
                kField: "sEmiCd"
                , template: function(dataItem){
                    return dataItem.sEmiNm;
                }
            },
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    TAB3.grid.sub = $("#grid6").AIV_GRID({
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'add', 'destroy', 'save', 'cancel'
        ],
        events: {
            editToolbarOnClick: function () {
                if (AIV_UTIL.isEmpty(TAB3.grid.sub.idEngCef)) {
                    alert('편집할 행을 선택해 주십시오.');
                    return true;
                }
                var isEditMode = TAB3.grid.main.isEditMode();
                if (isEditMode) {
                    alert('편집중인 그리드를 저장 또는 취소하신 뒤 편집할 수 있습니다.');
                    return true;    // 이후 이벤트 진행을 멈출지 여부를 return 해 주어야 함
                } else {
                    return false;   // true로 반환할 경우 편집모드로 변환이 안됨
                }
            }
        },
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/info/5521/ief/ls',
                beforeSend: function (hasCheckRow) {
                    $(hasCheckRow).each(function (i, row) {
                        row.idEngCef = TAB3.grid.sub.idEngCef;
                    })
                    return hasCheckRow;
                },
                success: function () {
                    TAB3.fn.searchSubGrid(TAB3.grid.sub.idEngCef);
                }
            },
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/5521/ief/ls',
                beforeSend: function (hasCheckRow) {
                    var deleteData = {};
                    var idLists = [];
                    $(hasCheckRow).each(function (i, row) {
                        idLists.push(row.idCefMod);
                    })
                    deleteData.idList = idLists.join(',');
                    return deleteData;
                },
                success: function () {
                    TAB3.fn.searchSubGrid(TAB3.grid.sub.idEngCef);
                }
            }
        },
        columns: [
            { kField: 'selectable' },
            { kField: "cYear" },
            { kField: "nHeat" },
            { kField: "nTotHeat" },
            {
                title: '배출계수',
                columns: [
                    { kField: "nCo2Cef" },
                    { kField: "nCh4Cef" },
                    { kField: "nN2oCef" },
                ]
            },
            { kField: 'nModCal' }
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    TAB3.fn.search(); // 최초 실행
}