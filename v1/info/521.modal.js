﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */


    // 조회 함수
    PAGE.fn.search = function () {
        //1. 서버로부터 기존의 저장된 데이터를 불러와 텍스트 박스를 채움
        AIV_COMM.ajax({
            method: 'GET',
            dataType: 'json',
            url: '/api/v1/info/521/ls',
            success: function (data, options) {
                PAGE.grid.main.setData(data.resultData);
                var dummy = {
                    'resultData': [{
                        'idSite': 001,
                        'idEngPoint': null,
                        'sSiteId': '00000',
                        'nOrder': null,
                        'sSiteNm': 'A사업장',
                        'sOwner': null,
                        'sSiteRegNum': '124-5-36-3-1',
                        'sSiteAddr': null,
                        'sSiteTel': null,
                        'sSiteCat': '물류',
                        'sParty': null,
                        'cCloseYear': null,
                        'sPicNm': '김세방',
                        'sPicDept': null,
                        'sPicPos': null,
                        'sPicTel': '02-2039-2999',
                        'sPicHp': null,
                        'sPicMail': null,
                        'sPicFax': null,
                        'sMainPrd': null,
                        'sSmlEmiYn': null,
                        'sCertPro': null,
                        'sCertEnv': null,
                        'sRegId': null,
                        'dRegDate': null,
                        'sModId': null,
                        'dModDate': null,
                        'cDelYn': 'N',
                        'gMap': null,
                        'gPhoto': null,
                        'gArr': null,
                        'gPro': null,
                        'idDept': null,
                        'sDeptId': null,
                        'sUDeptId': null,
                        'sDeptNm': null,
                        'sFullDeptId': null,
                        'sFullDeptNm': null,
                        'sMapFileNm': null,
                        'sPhotoFileNm': null,
                        'sArrFileNm': null,
                        'sProFileNm': null
                    }, {
                        'idSite': 002,
                        'idEngPoint': null,
                        'sSiteId': '00000',
                        'nOrder': null,
                        'sSiteNm': 'B사업장',
                        'sOwner': null,
                        'sSiteRegNum': '124-5-36-3-1',
                        'sSiteAddr': null,
                        'sSiteTel': null,
                        'sSiteCat': '물류',
                        'sParty': null,
                        'cCloseYear': null,
                        'sPicNm': '김세방',
                        'sPicDept': null,
                        'sPicPos': null,
                        'sPicTel': '02-2039-2999',
                        'sPicHp': null,
                        'sPicMail': null,
                        'sPicFax': null,
                        'sMainPrd': null,
                        'sSmlEmiYn': null,
                        'sCertPro': null,
                        'sCertEnv': null,
                        'sRegId': null,
                        'dRegDate': null,
                        'sModId': null,
                        'dModDate': null,
                        'cDelYn': 'N',
                        'gMap': null,
                        'gPhoto': null,
                        'gArr': null,
                        'gPro': null,
                        'idDept': null,
                        'sDeptId': null,
                        'sUDeptId': null,
                        'sDeptNm': null,
                        'sFullDeptId': null,
                        'sFullDeptNm': null,
                        'sMapFileNm': null,
                        'sPhotoFileNm': null,
                        'sArrFileNm': null,
                        'sProFileNm': null
                    }]
                }
                PAGE.grid.main.setData(dummy.resultData);
            }
        })
    }

    // 등록 버튼 클릭 시 함수
    PAGE.fn.add = function () {
        var _content = "";
        _content += "<div id='form' class='aiv_form_area'>"
        _content += "<h4>법인기본정보</h4>"
        _content += "  <div class='row mt-3'>"
        _content += "    <div class='col-lg-12'>"
        _content += "      <table class='table table-bordered'>"
        _content += "        <colgroup>"
        _content += "          <col width='20%'>"
        _content += "          <col width='30%'>"
        _content += "          <col width='20%'>"
        _content += "          <col width='30%'>"
        _content += "        </colgroup>"
        _content += "        <tbody>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>사업장일련번호</td>"
        _content += "            <td>"
        _content += "              <input id='idSite'>"
        _content += "            </td>"
        _content += "            <td class='aiv_form_label'>정렬순서</td>"
        _content += "            <td><input id='nOrder'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>사업장명</td>"
        _content += "            <td><input id='sSiteNm'></td>"
        _content += "            <td class='aiv_form_label'>사업장구분</td>"
        _content += "            <td>"
        _content += "              <input id='sSiteId'>"
        _content += "            </td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>대표자명</td>"
        _content += "            <td><input id='sOwner'></td>"
        _content += "            <td class='aiv_form_label'>사업자등록번호</td>"
        _content += "            <td><input id='sSiteRegNum'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>사업장소재지</td>"
        _content += "            <td><input id='sSiteAddr'></td>"
        _content += "            <td class='aiv_form_label'>사업장대표전화</td>"
        _content += "            <td><input id='sSiteTel'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>업종</td>"
        _content += "            <td><input id='sSiteCat'></td>"
        _content += "            <td class='aiv_form_label'>폐쇄연도</td>"
        _content += "            <td><input id='cCloseYear'></td>"
        _content += "          </tr>"
        _content += "        </tbody>"
        _content += "      </table>"
        _content += "    </div>"
        _content += "  </div>"
        _content += "  <h4>담당자정보</h4>"
        _content += "  <div class='row mt-3'>"
        _content += "    <div class='col-lg-12'>"
        _content += "      <table class='table table-bordered'>"
        _content += "        <colgroup>"
        _content += "          <col width='20%'>"
        _content += "          <col width='30%'>"
        _content += "          <col width='20%'>"
        _content += "          <col width='30%'>"
        _content += "        </colgroup>"
        _content += "        <tbody>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>이름</td>"
        _content += "            <td>"
        _content += "              <input id='sPicNm'>"
        _content += "            </td>"
        _content += "            <td class='aiv_form_label'>부서</td>"
        _content += "            <td><input id='sPicDept'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>직급</td>"
        _content += "            <td><input id='sPicPos'></td>"
        _content += "            <td class='aiv_form_label'>전화번호</td>"
        _content += "            <td>"
        _content += "              <input id='sPicTel'>"
        _content += "              <span data-for='PhoneNumber' class='k-invalid-msg'></span>"
        _content += "            </td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>휴대전화</td>"
        _content += "            <td>"
        _content += "              <input id='sPicHp'>"
        _content += "              <span data-for='PhoneNumber' class='k-invalid-msg'></span>"
        _content += "            </td>"
        _content += "            <td class='aiv_form_label'>이메일</td>"
        _content += "            <td>"
        _content += "              <input id='sPicMail'>"
        _content += "            </td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>팩스</td>"
        _content += "            <td>"
        _content += "              <input id='sPicFax'>"
        _content += "            </td>"
        _content += "          </tr>"
        _content += "        </tbody>"
        _content += "      </table>"
        _content += "    </div>"
        _content += "  </div>"
        _content += "  <h4>현황</h4>"
        _content += "  <div class='row mt-3'>"
        _content += "    <div class='col-lg-12'>"
        _content += "      <table class='table table-bordered'>"
        _content += "        <colgroup>"
        _content += "          <col width='20%'>"
        _content += "          <col width='80%'>"
        _content += "        </colgroup>"
        _content += "        <tbody>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>주요생산제품</td>"
        _content += "            <td><input id='sMainPrd'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>소량배출사업장</td>"
        _content += "            <td><input id='sSmlEmiYn'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>품질경영시스템인증</td>"
        _content += "            <td><input id='sCertPro'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>환경경영시스템인증</td>"
        _content += "            <td><input id='sCertEnv'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>약도</td>"
        _content += "            <td><input id='gMap'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>사진</td>"
        _content += "            <td><input id='gPhoto'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>시설배치도</td>"
        _content += "            <td><input id='gArr'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>공정도</td>"
        _content += "            <td><input id='gPro'></td>"
        _content += "          </tr>"
        _content += "        </tbody>"
        _content += "      </table>"
        _content += "    </div>"
        _content += "  </div>"
        _content += "</div>"
        _content += "<button id='btnSave'>저장</button>"
        _content += "<button id='btnCancel'>취소</button>"

        var pageWindow = AIV_WINDOW.init({
            title: '등록',
            width: 1100,
            height: 900,
            content: _content,
            resizable: true,
        }).open();

        PAGE.form.main = $('#kwindow-details #form').AIV_FORM_INIT();
        PAGE.form.main.setMode('edit');

        $('#kwindow-details #btnSave').on('click', function () {
            var formsValue = PAGE.form.main.getAllValue();
            AIV_COMM.ajax({
                method: 'POST',
                dataType: 'json',
                url: '/api/v1/info/521/ls',
                data: formsValue,
                success: function () { }
            })
            pageWindow.self.close();
        });

        // 취소버튼 클릭
        $('#kwindow-details #btnCancel').on('click', function () {
            pageWindow.self.close();
        });
    }

    // 검색영역 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT();

    // 조회 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });
    // 등록 버튼 세팅
    PAGE.btn.save = $("#search_area #btnAdd").AIV_BUTTON({
        title: "등록",
        class: 'k-primary',
        icon: 'save',
        onClick: PAGE.fn.add,
    });

    // 그리드 세팅
    PAGE.grid.main = $('#grid').AIV_GRID({
        height: 500,
        toolbar: [
            // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'destroy', 'excel' // 추가, 추소, 엑셀저장
        ],
        columns: [
            { kField: 'selectable' },
            { kField: 'idSite', editable: false },
            {
                kField: 'sSiteNm',
                template: function (dataItem) {
                    return "<a href='#' onclick='PAGE.fn.onBtnEdit(" + dataItem.idSite + ")'>" + dataItem.sSiteNm + "</a>";
                },
                editable: false
            },
            { kField: 'sSiteCat', editable: false },
            { kField: 'sSiteRegNum', editable: false },
            { kField: 'sPicNm', title: '담당자', editable: false },
            { kField: 'sPicTel', title: '담당자연락처', editable: false },
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });


    PAGE.fn.onBtnEdit = function (idSiteValue) {
        //1. 서버로부터 기존의 저장된 데이터를 불러와 텍스트 박스를 채움
        AIV_COMM.ajax({
            method: 'GET',
            dataType: 'json',
            url: '/api/v1/info/521/ls',
            success: function (data) {
                data.resultData.map(function (rows) {
                    for (key in rows) {
                        _val = rows[key];
                        if (_val == idSiteValue) {
                            PAGE.form.main.setAllValue(rows);
                        }
                    }
                })
                var dummy = {
                    'resultData': [{
                        'idSite': 001,
                        'idEngPoint': null,
                        'sSiteId': '00000',
                        'nOrder': null,
                        'sSiteNm': 'A사업장',
                        'sOwner': null,
                        'sSiteRegNum': '124-5-36-3-1',
                        'sSiteAddr': null,
                        'sSiteTel': null,
                        'sSiteCat': '물류',
                        'sParty': null,
                        'cCloseYear': null,
                        'sPicNm': '김세방',
                        'sPicDept': null,
                        'sPicPos': null,
                        'sPicTel': '02-2039-2999',
                        'sPicHp': null,
                        'sPicMail': null,
                        'sPicFax': null,
                        'sMainPrd': null,
                        'sSmlEmiYn': null,
                        'sCertPro': null,
                        'sCertEnv': null,
                        'sRegId': null,
                        'dRegDate': null,
                        'sModId': null,
                        'dModDate': null,
                        'cDelYn': 'N',
                        'gMap': null,
                        'gPhoto': null,
                        'gArr': null,
                        'gPro': null,
                        'idDept': null,
                        'sDeptId': null,
                        'sUDeptId': null,
                        'sDeptNm': null,
                        'sFullDeptId': null,
                        'sFullDeptNm': null,
                        'sMapFileNm': null,
                        'sPhotoFileNm': null,
                        'sArrFileNm': null,
                        'sProFileNm': null
                    }, {
                        'idSite': 002,
                        'idEngPoint': null,
                        'sSiteId': '00000',
                        'nOrder': null,
                        'sSiteNm': 'A사업장',
                        'sOwner': null,
                        'sSiteRegNum': '124-5-36-3-1',
                        'sSiteAddr': null,
                        'sSiteTel': null,
                        'sSiteCat': '물류',
                        'sParty': null,
                        'cCloseYear': null,
                        'sPicNm': '김세방',
                        'sPicDept': null,
                        'sPicPos': null,
                        'sPicTel': '02-2039-2999',
                        'sPicHp': null,
                        'sPicMail': null,
                        'sPicFax': null,
                        'sMainPrd': null,
                        'sSmlEmiYn': null,
                        'sCertPro': null,
                        'sCertEnv': null,
                        'sRegId': null,
                        'dRegDate': null,
                        'sModId': null,
                        'dModDate': null,
                        'cDelYn': 'N',
                        'gMap': null,
                        'gPhoto': null,
                        'gArr': null,
                        'gPro': null,
                        'idDept': null,
                        'sDeptId': null,
                        'sUDeptId': null,
                        'sDeptNm': null,
                        'sFullDeptId': null,
                        'sFullDeptNm': null,
                        'sMapFileNm': null,
                        'sPhotoFileNm': null,
                        'sArrFileNm': null,
                        'sProFileNm': null
                    }]
                }
                dummy.resultData.map(function (rows) {
                    for (key in rows) {
                        _val = rows[key];
                        if (_val == idSiteValue) {
                            PAGE.form.main.setAllValue(rows);
                        }
                    }
                })
            }
        })

        var _content = "";
        _content += "<div id='form' class='aiv_form_area'>"
        _content += "  <h4>법인기본정보</h4>"
        _content += "  <div class='row mt-3'>"
        _content += "    <div class='col-lg-12'>"
        _content += "      <table class='table table-bordered'>"
        _content += "        <colgroup>"
        _content += "          <col width='20%'>"
        _content += "          <col width='30%'>"
        _content += "          <col width='20%'>"
        _content += "          <col width='30%'>"
        _content += "        </colgroup>"
        _content += "        <tbody>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>사업장일련번호</td>"
        _content += "            <td>"
        _content += "              <input id='idSite'>"
        _content += "            </td>"
        _content += "            <td class='aiv_form_label'>정렬순서</td>"
        _content += "            <td><input id='nOrder'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>사업장명</td>"
        _content += "            <td><input id='sSiteNm'></td>"
        _content += "            <td class='aiv_form_label'>사업장구분</td>"
        _content += "            <td>"
        _content += "              <input id='sSiteId'>"
        _content += "            </td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>대표자명</td>"
        _content += "            <td><input id='sOwner'></td>"
        _content += "            <td class='aiv_form_label'>사업자등록번호</td>"
        _content += "            <td><input id='sSiteRegNum'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>사업장소재지</td>"
        _content += "            <td><input id='sSiteAddr'></td>"
        _content += "            <td class='aiv_form_label'>사업장대표전화</td>"
        _content += "            <td><input id='sSiteTel'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>업종</td>"
        _content += "            <td><input id='sSiteCat'></td>"
        _content += "            <td class='aiv_form_label'>폐쇄연도</td>"
        _content += "            <td><input id='cCloseYear'></td>"
        _content += "          </tr>"
        _content += "        </tbody>"
        _content += "      </table>"
        _content += "    </div>"
        _content += "  </div>"
        _content += "  <h4>담당자정보</h4>"
        _content += "  <div class='row mt-3'>"
        _content += "    <div class='col-lg-12'>"
        _content += "      <table class='table table-bordered'>"
        _content += "        <colgroup>"
        _content += "          <col width='20%'>"
        _content += "          <col width='30%'>"
        _content += "          <col width='20%'>"
        _content += "          <col width='30%'>"
        _content += "        </colgroup>"
        _content += "        <tbody>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>이름</td>"
        _content += "            <td>"
        _content += "              <input id='sPicNm'>"
        _content += "            </td>"
        _content += "            <td class='aiv_form_label'>부서</td>"
        _content += "            <td><input id='sPicDept'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>직급</td>"
        _content += "            <td><input id='sPicPos'></td>"
        _content += "            <td class='aiv_form_label'>전화번호</td>"
        _content += "            <td>"
        _content += "              <input id='sPicTel'>"
        _content += "              <span data-for='PhoneNumber' class='k-invalid-msg'></span>"
        _content += "            </td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>휴대전화</td>"
        _content += "            <td>"
        _content += "              <input id='sPicHp'>"
        _content += "              <span data-for='PhoneNumber' class='k-invalid-msg'></span>"
        _content += "            </td>"
        _content += "            <td class='aiv_form_label'>이메일</td>"
        _content += "            <td>"
        _content += "              <input id='sPicMail'>"
        _content += "            </td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>팩스</td>"
        _content += "            <td>"
        _content += "              <input id='sPicFax'>"
        _content += "            </td>"
        _content += "          </tr>"
        _content += "        </tbody>"
        _content += "      </table>"
        _content += "    </div>"
        _content += "  </div>"
        _content += "  <h4>현황</h4>"
        _content += "  <div class='row mt-3'>"
        _content += "    <div class='col-lg-12'>"
        _content += "      <table class='table table-bordered'>"
        _content += "        <colgroup>"
        _content += "          <col width='20%'>"
        _content += "          <col width='80%'>"
        _content += "        </colgroup>"
        _content += "        <tbody>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>주요생산제품</td>"
        _content += "            <td><input id='sMainPrd'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>소량배출사업장</td>"
        _content += "            <td><input id='sSmlEmiYn'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>품질경영시스템인증</td>"
        _content += "            <td><input id='sCertPro'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>환경경영시스템인증</td>"
        _content += "            <td><input id='sCertEnv'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>약도</td>"
        _content += "            <td><input id='gMap'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>사진</td>"
        _content += "            <td><input id='gPhoto'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>시설배치도</td>"
        _content += "            <td><input id='gArr'></td>"
        _content += "          </tr>"
        _content += "          <tr>"
        _content += "            <td class='aiv_form_label'>공정도</td>"
        _content += "            <td><input id='gPro'></td>"
        _content += "          </tr>"
        _content += "        </tbody>"
        _content += "      </table>"
        _content += "    </div>"
        _content += "  </div>"
        _content += "</div>"
        _content += "<button id='btnSave'>저장</button>"
        _content += "<button id='btnCancel'>취소</button>"

        var pageWindow = AIV_WINDOW.init({
            title: '등록',
            width: 1100,
            height: 900,
            content: _content,
            resizable: true,
        }).open();

        PAGE.form.main = $('#form').AIV_FORM_INIT();
        PAGE.form.main.setMode('edit');

        $('#btnSave').on('click', function () {
            var formsValue = PAGE.form.main.getAllValue();
            AIV_COMM.ajax({
                method: 'POST',
                dataType: 'json',
                url: '/api/v1/info/521/ls',
                data: formsValue,
                success: function () {

                },
            })
            pageWindow.self.close();
        });

        // 취소버튼 클릭
        $('#btnCancel').on('click', function () {
            pageWindow.self.close();
        });
    }

    PAGE.fn.search();
}