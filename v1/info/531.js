﻿PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/531/ls',
            data: params,
            success: function (responseData, options) {
                PAGE.grid.main.setData(responseData.resultData);
            }
        })
    }

    // if (PAGE.form.main.isValid()) {
    //     var params = PAGE.form.main.getAllValue();
    //     params.cDelYn = 'N';
    //     AIV_COMM.ajax({
    //         method: 'POST',
    //         url: '/api/v1/info/531/ls',
    //         data: params,
    //         success: function (responseData) {
    //             alert('저장되었습니다.');
    //             pageWindow.self.close();
    //             PAGE.fn.search();
    //         }
    //     })
    // } else {
    //     alert('값을 입력해 주세요.');
    // }


    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: "k-primary",
        icon: "search",
        onClick: PAGE.fn.search,
    });

    PAGE.grid.main = $('#mainGrid').AIV_GRID({
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'add', 'save', 'destroy', 'cancel'
        ],
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/info/531/ls',
                beforeSend: function (hasCheckRow) {
                    return hasCheckRow;
                },
                success: function () {
                    PAGE.fn.search();
                }
            },
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/531/ls/sub',
                beforeSend: function (hasCheckRow) {
                    var deleteData = {};
                    $(hasCheckRow).each(function (i, row) {
                        if ( deleteData[row.idSite] == undefined ) { // 사업장id에 조직을 넣는 경우가 최초일 경우
                            deleteData[row.idSite] = row.idErpDept;
                        } else {    // 이미 등록된 사업장 id-조직이 있을 경우 기존조직id + ',' + 추가조직id를 해준다
                            deleteData[row.idSite] += ',' + row.idErpDept;
                        }
                    })
                    return deleteData;
                },
                success: function () {
                    PAGE.fn.search();
                }
            }
        },
        columns: [
            { kField: 'selectable' },
            {
                kField: 'sSiteNm',
                // template: function (dataItem) {
                //     var isEditMode = PAGE.grid.main.isEditMode();
                //     return '<a href="#" onclick="PAGE.fn.onBtnEdit(\'' + dataItem.sDeptId + '\'' + ',' + isEditMode + ')">' + dataItem.sSiteNm + '</a>';
                // },
                // rowspan: true
            }, {
                kField: 'sDeptNm', title: '조직명',
                template: function (dataItem) {
                    return dataItem.sDeptNm;
                }
            }
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    PAGE.fn.search();
}