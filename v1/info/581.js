﻿PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
    * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
    * 1. PAGE.fn.*     사용 함수 설정
    * 2. PAGE.params.* 설정 기능 객체 설정 (input)
    * 2. PAGE.form.*   설정 기능 객체 설정 (form)
    * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
    * 4. PAGE.grid.*   그리드 객체 설정
    * 5. PAGE.chart.*  함수 객체 설정
    */

    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/581/ls',
            data: params,
            success: function (responseData, options) {
                $(responseData.resultData).each(function (i, rows) {
                    rows.cYm = rows.cYear + '-' + leadingZeros(rows.cMon, 2);
                })
                PAGE.grid.main.setData(responseData.resultData);
            }
        })
    }

    // 그리드 조회시 실행될 파라미터 조회 함수 작성
    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 그리드 세팅
    PAGE.grid.main = $("#grid").AIV_GRID({
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'add', 'destroy', 'save', 'cancel'
        ],
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/info/581/ls',
                beforeSend: function (hasCheckRow) {
                    $(hasCheckRow).each(function (i, row) {
                        row.cYear = row.cYm.split('-')[0];
                        row.cMon = row.cYm.split('-')[1];
                    })
                    return hasCheckRow;
                },
                success: function () {
                    PAGE.fn.search();
                }
            },
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/581/ls',
                beforeSend: function (hasCheckRow) {
                    var deleteData = {};
                    var idLists = [];
                    $(hasCheckRow).each(function (i, row) {
                        idLists.push(row.idSunPower);
                    })
                    deleteData.idList = idLists.join(',');
                    return deleteData;
                },
                success: function () {
                    PAGE.fn.search();
                }
            }
        },
        columns: [
            { kField: 'selectable' },
            { kField: "nPower" },
            { kField: 'cYm' },
            { kField: "nPoint" },
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    // 버튼 세팅
    PAGE.btn.edit = $("#search_area #btnEdit").AIV_BUTTON({
        title: "편집",
        class: 'k-primary',
        icon: 'edit',
        onClick: PAGE.fn.onBtnEdit,
    });
    PAGE.btn.cancel = $("#search_area #btnCancel").AIV_BUTTON({
        title: "취소",
        // class: 'k-primary',
        icon: 'cancel',
        onClick: PAGE.fn.onBtnCancel,
    });

    PAGE.fn.search();
}