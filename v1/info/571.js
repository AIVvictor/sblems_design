﻿PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    // treeview 선택 시 버튼 show/hidden
    PAGE.fn.showBtn = function(type){
        if ( type == 'add' ) {
            PAGE.btn.btnAdd.show();
            PAGE.btn.btnEdit.hide();
            PAGE.btn.btnDel.hide();
        } else if ( type == 'edit' ) {
            PAGE.btn.btnAdd.hide();
            PAGE.btn.btnEdit.show();
            PAGE.btn.btnDel.show();
        } else if ( type == 'hide' ) {
            PAGE.btn.btnAdd.hide();
            PAGE.btn.btnEdit.hide();
            PAGE.btn.btnDel.hide();
        }
    }

    // treeview data set
    PAGE.fn.setTreeView = function(id) {
        if (AIV_UTIL.isEmpty(id) ) {
            id = '';
        }
        AIV_COMM.ajax({
            url: '/api/v1/info/571/ls', // 배출시설 목록 조회
            data: {
                idSite: id
            },
            success: function(responseData){
                var pointDatas = responseData.resultData;
                var treeDatas = [];

                // ajaxData를 treeviewData형식으로 변경
                $(pointDatas).each(function(i, point){
                    var hasPointSite = treeDatas.find(function(d){return d.idSite == point.idSite;})
                    point.text = point.sPointNm;
                    point.sprite = 'html';
                    point.items = [];
                    if ( typeof hasPointSite !== 'undefined' ) {
                        hasPointSite.items.push(point)
                    } else {
                        var folder = {
                            text: point.sSiteNm,
                            sprite: 'folder',   // rootfolder, folder, pdf, html, image
                            idSite: point.idSite,
                            sSiteNm: point.sSiteNm,
                        };
                        if ( point.idEngPoint ) {
                            folder.items = [point]; // 배출시설이 있으면 바로 items에 넣는다
                        }
                        treeDatas.push(folder);
                    }
                });

                console.log('treeDatas', treeDatas)

                PAGE.treeview.main.setData([{
                    text:'세방', sprite: 'rootfolder', items: treeDatas
                }]);
                PAGE.treeview.main.self.expand(".k-item");
                PAGE.treeview.main.self.expand(".k-item");

                PAGE.fn.showBtn('hide');
            }
        })
    }

    // treeview 항목 선택 시 함수
    PAGE.fn.onSelectTreeview = function(e) {
        var dataItem = PAGE.treeview.main.self.dataItem(e.node);
        if ( dataItem.sprite == 'folder' ) {    // 사업장 선택 시
            PAGE.fn.showBtn('add');
            PAGE.form.main.setEmpty();

        } else if ( dataItem.sprite == 'html' ) {    // 배출시설 선택 시
            AIV_COMM.ajax({
                url: '/api/v1/info/571/dtl', // 배출시설 상세 조회
                data: {
                    idEngPoint: dataItem.idEngPoint,
                    idSite: dataItem.idSite,
                },
                success: function(responseData){
                    var formsValue = responseData.resultData[0];
                    formsValue.sInvSeq = formsValue.sPointId;   // sPointId = 배출시설일련번호
                    formsValue.sEngCd = formsValue.sEngCdNm + '[' + formsValue.sEmtCdNm + ']';   // 배출원 = sEngCdNm[sEmtCdNm]
                    formsValue.nCapa = formsValue.nCapa + ' ' + formsValue.sUntCapaCdNm;   // sPointId = 배출시설일련번호

                    PAGE.form.main.setAllValue(formsValue); // form 에 값을 넣는다
                    PAGE.fn.showBtn('edit');
                }
            });
        } else {
            PAGE.fn.showBtn('hide');
        }
    }

    
    // 등록, 수정, 삭제 버튼 눌렀을때 동작 함수
    PAGE.fn.add = function(e){
        PAGE.fn.onModal(true);
    }
    PAGE.fn.edit = function(e){
        PAGE.fn.onModal(false);
    }
    PAGE.fn.delete = function(e){
        var selectedTreeData = PAGE.treeview.main.self.dataItem(PAGE.treeview.main.self.select());
        var sPointNm = selectedTreeData.sPointNm;
        var idEngPoint = selectedTreeData.idEngPoint;
        if ( confirm('배출시설(' + sPointNm + ')을 삭제하시겠습니까?') ) {
            AIV_COMM.ajax({
                method: 'DELETE',
                dataType: 'json',
                url: '/api/v1/info/571/dtl',
                data: {
                    idEngPoint: idEngPoint
                },
                success: function (responseData) {
                    alert('삭제 되었습니다.');
                    PAGE.params.idSite.change();   // 사이트dropdown fireevent(change) -> treeview reset
                }
            })

        }
    }

    // modal을 띄워 등록/수정을 실행
    PAGE.fn.onModal = function (isAddMode) {
        var selectedTreeData = PAGE.treeview.main.self.dataItem(PAGE.treeview.main.self.select());

        var _content = $('#modal1').html();
        var pageWindow = AIV_WINDOW.init({
            title: isAddMode ? '등록' : '수정',
            width: 900,
            height: 600,
            content: _content,
            resizable: true,
        }).open();

        if ( isAddMode ) {
            // kendo dropdown 생성 전에 disabled 속성 삽입
            $('#kwindow-details #form_detail #cEYear').attr('disabled', 'disabled');
        }
        PAGE.form.detail = $('#kwindow-details #form_detail').AIV_FORM_INIT();
        PAGE.form.detail.setMode('edit');
        PAGE.form.detail.sPointNm.parent().width('38%');
        PAGE.form.detail.sFuelEff.parent().width('38%');

        if ( isAddMode ) {
            // 등록시에는 종료년도 입력이 불가능하므로 값을 미선택으로 강제 변경
            PAGE.form.detail.cEYear.kFormValue('');
        } else {
            // 수정시에는 ajax통신을 통해 가져온 값으로 데이터 입력

            AIV_COMM.ajax({
                url: '/api/v1/info/571/dtl', // 배출시설 상세 조회
                data: {
                    idEngPoint: selectedTreeData.idEngPoint,
                    idSite: selectedTreeData.idSite,
                },
                success: function(responseData){
                    var formsValue = responseData.resultData[0];
                    // formsValue.sInvSeq = formsValue.sPointId;   // sPointId = 배출시설일련번호
                    // formsValue.sEngCd = formsValue.sEngCdNm + '[' + formsValue.sEmtCdNm + ']';   // 배출원 = sEngCdNm[sEmtCdNm]
                    // formsValue.nCapa = formsValue.nCapa + ' ' + formsValue.sUntCapaCdNm;   // sPointId = 배출시설일련번호

                    PAGE.form.detail.setAllValue(formsValue); // form 에 값을 넣는다
                }
            });

            // PAGE.form.detail.setAllValue(selectedTreeData);
        }

        // form 저장 버튼 설정
        $('#kwindow-details #btnSave').show().AIV_BUTTON({
            title: "저장",
            class: 'k-primary',
            icon: 'save',
            onClick: function () {
                var formsValue = PAGE.form.detail.getAllValue();

                var selectedTreeData = PAGE.treeview.main.self.dataItem(PAGE.treeview.main.self.select())

                if ( !isAddMode ) {  // 수정일 경우 기등록된 id값을 넣어줌
                    formsValue.idEngPoint = selectedTreeData.idEngPoint;
                }

                formsValue.idEngCef = formsValue.sEngCd;  // 에너지원의 값 == T_ENG_CEF의 id값
                formsValue.cDelYn = 'N';
                formsValue.cPointYn = 'Y';
                formsValue.idSite = selectedTreeData.idSite;

                AIV_COMM.ajax({
                    method: 'POST',
                    dataType: 'json',
                    url: '/api/v1/info/571/dtl',
                    data: formsValue,
                    success: function (responseData) {
                        alert('정상적으로 입력되었습니다.');
                        pageWindow.self.close();
                        PAGE.params.idSite.change();   // 사이트dropdown fireevent(change) -> treeview reset
                    }
                })
            }
        });

        // 취소버튼 클릭
        $('#kwindow-details #btnCancel').show().AIV_BUTTON({
            title: "취소",
            icon: 'cancel',
            onClick: function () {
                pageWindow.self.close();
            }
        });
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });
    PAGE.params.idSite.change(function(e){
        PAGE.fn.setTreeView(this.value);
        PAGE.form.main.setEmpty();
    })
    
    // treeview 생성
    PAGE.treeview.main = $("#treeview").AIV_TREEVIEW({
        select: PAGE.fn.onSelectTreeview
    });

    // 버튼 세팅
    PAGE.btn.btnAdd = $("#btnAdd").AIV_BUTTON({
        title: "배출시설등록",
        class: 'k-primary',
        icon: 'add',
        onClick: PAGE.fn.add
    });
    PAGE.btn.btnEdit = $("#btnEdit").AIV_BUTTON({
        title: "배출시설수정",
        class: 'k-primary',
        icon: 'edit',
        onClick: PAGE.fn.edit
    });
    PAGE.btn.btnDel = $("#btnDel").AIV_BUTTON({
        title: "배출시설삭제",
        class: 'k-primary',
        icon: 'delete',
        onClick: PAGE.fn.delete
    });


    // 그리드 세팅
    PAGE.form.main = $('#form').AIV_FORM_INIT();

    // TreeView set datas
    PAGE.fn.setTreeView('');
}