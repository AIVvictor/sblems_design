﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
 * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
 * 1. PAGE.fn.*     사용 함수 설정
 * 2. PAGE.params.* 설정 기능 객체 설정 (input)
 * 2. PAGE.form.*   설정 기능 객체 설정 (form)
 * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
 * 4. PAGE.grid.*   그리드 객체 설정
 * 5. PAGE.chart.*  함수 객체 설정
 */

    PAGE.fn.search = function () {
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/553/etc/emi/ls?sCefId=3&?sEmtCd=9',
            success: function (responseData) {
                PAGE.grid.main.setData(responseData.resultData);
                PAGE.grid.sub.setData([]);
                PAGE.grid.sub.idEngCef = undefined;
            }
        })
    }

    PAGE.fn.searchSubGrid = function (idEngCef) {
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/553/etc/gr/ls',
            data: {
                'idEngCef': idEngCef
            },
            success: function (responseData) {
                PAGE.grid.sub.idEngCef = idEngCef;
                PAGE.grid.sub.setData(responseData.resultData);
            }
        })
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 그리드 세팅
    PAGE.grid.main = $("#grid1").AIV_GRID({
        height: 300,
        toolbar: [
            // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel' // 추가, 추소, 엑셀저장
        ],
        editToolbar: [
            'add', 'destroy', 'save', 'cancel'
        ],
        events: {
            editToolbarOnClick: function () {
                var isEditMode = PAGE.grid.sub.isEditMode();
                // main grid를 편집모드로 변경시 sub grid를 읽기모드로 바꿔줘야 함
                if (isEditMode) {
                    $(PAGE.grid.sub).find('.k-grid-cancel-changes').click();    // sub grid의 cancel toolbar 클릭
                    PAGE.grid.sub.setData([]);
                    PAGE.grid.sub.idEngCef = undefined;
                }
                return false;   // true로 반환할 경우 편집모드로 변환이 안됨
            }
        },
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/info/553/etc/emi/ls',
                beforeSend: function (hasCheckRow) {
                    $(hasCheckRow).each(function (i, row) {
                        row.sCefCd = 3;
                        row.sEmtCd = 9;
                    })
                    return hasCheckRow;
                },
                success: function () {
                    PAGE.fn.search();
                }
            },
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/553/etc/emi/ls',
                beforeSend: function (hasCheckRow) {
                    var deleteData = {};
                    var idLists = [];
                    $(hasCheckRow).each(function (i, row) {
                        idLists.push(row.idCefMod);
                    })
                    deleteData.idList = idLists.join(',');
                    return deleteData;
                },
                success: function () {
                    PAGE.fn.search();
                }
            }
        },
        columns: [
            { kField: "selectable" },
            {
                kField: "sOthCefNm",
                template: function (dataItem) {
                    var isEditMode = PAGE.grid.main.isEditMode();
                    if (isEditMode) {
                        return dataItem.sOthCefNm;
                    }
                    else {
                        return '<a href="#" onClick="javascript:PAGE.fn.searchSubGrid(' + dataItem.idEngCef + ');">' + dataItem.sOthCefNm + '</a>';
                    }
                }
            },
            {
                kField: "sInUntCdNm",
                editor: function(container){
                    return AIV_MODEL.editor.sInUntCdNm(container, {emptyValue:true});
                }
            },
            { kField: "sOthPurpose" },
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });
    
    // 그리드 세팅
    PAGE.grid.sub = $("#grid2").AIV_GRID({
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'add', 'destroy', 'save', 'cancel'
        ],
        events: {
            editToolbarOnClick: function () {
                if (AIV_UTIL.isEmpty(PAGE.grid.sub.idEngCef)) {
                    alert('편집할 행을 선택해 주십시오.');
                    return true;
                }
                var isEditMode = PAGE.grid.main.isEditMode();
                if (isEditMode) {
                    alert('편집중인 그리드를 저장 또는 취소하신 뒤 편집할 수 있습니다.');
                    return true;    // 이후 이벤트 진행을 멈출지 여부를 return 해 주어야 함
                } else {
                    return false;   // true로 반환할 경우 편집모드로 변환이 안됨
                }
            }
        },
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/info/553/etc/gr/ls',
                beforeSend: function (hasCheckRow) {
                    $(hasCheckRow).each(function (i, row) {
                        row.idEngCef = PAGE.grid.sub.idEngCef;
                    })
                    return hasCheckRow;
                },
                success: function () {
                    PAGE.fn.searchSubGrid(PAGE.grid.sub.idEngCef);
                }
            },
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/553/etc/gr/ls',
                beforeSend: function (hasCheckRow) {
                    var deleteData = {};
                    var idLists = [];
                    $(hasCheckRow).each(function (i, row) {
                        idLists.push(row.idCefMod);
                    })
                    deleteData.idList = idLists.join(',');
                    return deleteData;
                },
                success: function () {
                    PAGE.fn.searchSubGrid(PAGE.grid.sub.idEngCef);
                }
            }
        },
        columns: [
            { kField: 'selectable' },
            { kField: "sGhgCd" },
            { kField: "cPer" },
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    PAGE.fn.search(); // 최초 실행
}