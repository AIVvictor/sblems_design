﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    // 조회 함수
    PAGE.fn.search = function () {
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/info/521/ls',
            success: function (responseData, options) {
                var gridData = responseData.resultData.filter(function (d) { return d.cDelYn != 'Y' })
                PAGE.grid.main.setData(gridData);
            }
        })
    }

    // 등록 버튼 클릭 시 함수
    PAGE.fn.onBtnAdd = function () {
        PAGE.fn.onModal('add');
    }

    // 편집 or 읽기화면
    PAGE.fn.onBtnEdit = function (idSite, isEditMode) {
        if ( isEditMode ) {
            PAGE.fn.onModal('edit', idSite);
        } else {
            PAGE.fn.onModal('read', idSite);
        }
    }

    PAGE.fn.onModal = function(mode, idSite){
        // mode: add, edit, read
        var _content = $('#modal1').html();
        var _title = '';

        switch (mode) {
            case 'add' : _title = '등록'; break;
            case 'edit': _title = '수정'; break;
            case 'read': _title = '상세조회'; break;
        }

        var pageWindow = AIV_WINDOW.init({
            title: _title,
            width: 1100,
            height: 700,
            content: _content,
            resizable: true,
        }).open();

        PAGE.form.main = $('#kwindow-details #form').AIV_FORM_INIT();

        // 조회 혹은 편집이면 기존 데이터 불러와 입력
        if ( mode == 'read' || mode == 'edit' ){
            var isReadMode = (mode == 'read');
            AIV_COMM.ajax({
                method: 'GET',
                url: '/api/v1/info/521/dtl',
                data:{
                    idSite: idSite
                },
                success: function (responseData) {
                    var datas = responseData.resultData;

                    // 첨부파일의 gId, idFile, fileNm은 별도세팅해주어야 함
                    if ( datas.gMap ) {
                        PAGE.form.main.gMap.kFormValue({
                            idFile: datas.sMapIdFile,
                            sFileNm: datas.sMapFileNm,
                            gFileId: datas.gMap,
                            readMode: isReadMode      // file객체에 기존 file정보를 입력할때 읽기모드가 필요시 설정
                        });
                    }
                    if ( datas.gPhoto ) {
                        PAGE.form.main.gPhoto.kFormValue({
                            idFile: datas.sPhotoIdFile,
                            sFileNm: datas.sPhotoFileNm,
                            gFileId: datas.gPhoto,
                            readMode: isReadMode
                        });
                    }
                    if ( datas.gArr ) {
                        PAGE.form.main.gArr.kFormValue({
                            idFile: datas.sArrIdFile,
                            sFileNm: datas.sArrFileNm,
                            gFileId: datas.gArr,
                            readMode: isReadMode
                        });
                    }
                    if ( datas.gPro ) {
                        PAGE.form.main.gPro.kFormValue({
                            idFile: datas.sArrIdFile,
                            sFileNm: datas.sProFileNm,
                            gFileId: datas.gPro,
                            readMode: isReadMode
                        });
                    }

                    delete datas.gMap;  // 두번 세팅하지 않도록 설정
                    delete datas.gPhoto;
                    delete datas.gArr;
                    delete datas.gPro;
                    PAGE.form.main.setAllValue(datas);
                }
            });
        }

        // mode에 따라 form-mode 변경 및 버튼 설정
        if ( mode == 'read' ){
            PAGE.form.main.setMode('read');
        } else if ( mode == 'add' || mode == 'edit' ) {
            PAGE.form.main.setMode('edit');

            // 저장버튼 / API 설정
            $('#kwindow-details #btnSave').show().AIV_BUTTON({
                title: '저장',
                class: 'k-primary',
                icon: 'save',
                onClick: function () {
                    // valid 체크
                    if ( !PAGE.form.main.isValid() ) {
                        alert('값을 입력해 주세요.');
                        return;
                    }
    
                    // 파일 순차적 업로드 (약도, 사진, 시설배치도, 공정도 순서)
                    var uploadFiles = [PAGE.form.main.gMap, PAGE.form.main.gPhoto, PAGE.form.main.gArr, PAGE.form.main.gPro];
                    var gFileIdArr = [null, null, null, null];
                    $(uploadFiles).each(function(i, uploadFile){
                        var sFileNmVal = uploadFile.kFormValue();
        
                        if ( sFileNmVal.cmdTag == 'R' ) {   // R: 파일정보를 읽은 후 아무 작동 없을 경우
                            gFileIdArr[i] = sFileNmVal.gFileId;   // group file id 만 기억하고 pass 함.
    
                        } else if ( sFileNmVal.cmdTag == 'D' ) {    // D: 기존 업로드 되어 있는 파일을 삭제버튼 누른 경우
                            gFileIdArr[i] = sFileNmVal.gFileId;           // group file id를 기억 후 파일을 삭제함
                            uploadFile.upload({
                                gId: gFileIdArr[i],
                                cmdTag: sFileNmVal.cmdTag,
                                idFile: sFileNmVal.idFile,
                                success: function(responseData){
                                    gFileIdArr[i] = '';
                                }
                            })
        
                        } else if ( sFileNmVal.cmdTag == 'U' ) {
                            gFileIdArr[i] = sFileNmVal.gFileId;           // group file id를 기억 후 파일을 삭제함
                            uploadFile.upload({
                                gId: gFileIdArr[i],
                                cmdTag: sFileNmVal.cmdTag,
                                idFile: sFileNmVal.idFile,
                                success: function(responseData){
                                    gFileIdArr[i] = gFileIdArr[i] || responseData.gid;  // group file id가 비어있을 경우(최초업로드일 경우) 반환된 gid를 기억함
                                }
                            });
    
                        } else {    // 파일 미선택시 아무 동작하지 않음.
                        }
                    });
    
    
                    var formsValue = PAGE.form.main.getAllValue();

                    // 각각의 gFileId를 넣어줌
                    formsValue.gMap   = gFileIdArr[0];
                    formsValue.gPhoto = gFileIdArr[1];
                    formsValue.gArr   = gFileIdArr[2];
                    formsValue.gPro   = gFileIdArr[3];

                    formsValue.cDelYn = 'N';
    
                    AIV_COMM.ajax({
                        method: 'POST',
                        url: '/api/v1/info/521/dtl',
                        data: formsValue,
                        success: function (responseData) {
                            PAGE.fn.search();
                            alert('저장되었습니다');
                            pageWindow.self.close();
                        }
                    });

                }
            })
        }

        // 취소버튼 클릭
        $('#kwindow-details #btnCancel').show().AIV_BUTTON({
            title: '취소',
            icon: 'cancel',
            onClick: function () {
                pageWindow.self.close();
            }
        });

    }

    // 검색영역 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT();

    // 조회 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 등록 버튼 세팅
    PAGE.btn.save = $("#search_area #btnAdd").AIV_BUTTON({
        title: "등록",
        class: 'k-primary',
        icon: 'add',
        onClick: PAGE.fn.onBtnAdd,
    });

    // 그리드 세팅
    PAGE.grid.main = $('#grid').AIV_GRID({
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'destroy', 'cancel'
        ],
        api: {
            delete: {
                method: 'DELETE',
                url: '/api/v1/info/521/ls',
                beforeSend: function (hasCheckRow) {
                    var saveRow = [];
                    $(hasCheckRow).each(function (i, cRow) {
                        var data = {};
                        data.idSites = cRow.idSite;
                        saveRow.push(data);
                    });
                    return saveRow;
                },
                success: function (responseData) {
                    PAGE.fn.search();
                }
            }
        },
        columns: [
            { kField: 'selectable' },
            { kField: 'idSite', editable: false },
            {
                kField: 'sSiteNm',
                template: function (dataItem) {
                    var isEditMode = PAGE.grid.main.isEditMode();
                    return "<a href='#' onclick='PAGE.fn.onBtnEdit(" + dataItem.idSite + ',' + isEditMode + ")'>" + dataItem.sSiteNm + "</a>";
                },
                editable: false
            },
            { kField: 'sSiteCat', editable: false },
            { kField: 'sSiteRegNum', editable: false },
            { kField: 'sPicNm', title: '담당자', editable: false },
            { kField: 'sSiteTel', title: '담당자연락처', editable: false },
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    PAGE.fn.search();
}