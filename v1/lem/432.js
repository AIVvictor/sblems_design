﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    PAGE.fn.setChart = function (dummy) {
        PAGE.chart.column.setData(dummy);
    };
    PAGE.fn.setGrid = function (dummy) {
        PAGE.grid.main.setData(dummy);
    };
    PAGE.fn.search = function () {

        //TODO: ajax
        var dummy = (function () {
            return [{
                "cYear": "2019",
                "nGoal": 1720,
                "nAmt": 1697,
            }, {
                "cYear": "2018",
                "nGoal": 1525,
                "nAmt": 1479,
            }, {
                "cYear": "2017",
                "nGoal": 1588,
                "nAmt": 1573,
            }, {
                "cYear": "2016",
                "nGoal": 1427,
                "nAmt": 1193,
            }, {
                "cYear": "2015",
                "nGoal": 1441,
                "nAmt": 1434,
            }, {
                "cYear": "2019",
                "nGoal": 1441,
                "nAmt": 1434,
            }];
        })();

        var chartData = AIV_UTIL.convertChartData(dummy);

        //PAGE.grid.main.reload();
        PAGE.fn.setChart(chartData);
        PAGE.fn.setGrid(dummy);
    };

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 그리드 세팅
    PAGE.grid.main = $("#grid").AIV_GRID({
        toolbar: [
            // 'report', 'excel', 'save'    // 추가, 추소, 엑셀저장
            'excel' // 추가, 추소, 엑셀저장
        ],
        //apiUrl: "/api/v1/sum/211/ls",           // CRUD의 기본 URL 작성
        //getParams: PAGE.fn.getParams,     // 그리드 조회 시 실행될 파라미터 조회 함수 적용
        columns: [
            { kField: "cYear", editable: false }, // aiv_model.js 내 kField 입력시 기타 속성에 대해 자동 입력 가능
            { kField: "nGoal", editable: false },
            { kField: "nAmt", editable: false },
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        height: 300
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    // 차트 세팅
    PAGE.chart.column = $("#chart").AIV_CHART({
        kType: 'XYChart'
    });

    // Create axes
    var categoryAxis = PAGE.chart.column.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "cYear";
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.cellStartLocation = 0.2
    categoryAxis.renderer.cellEndLocation = 0.8
    //categoryAxis.renderer.minGridDistance = 0;
    //categoryAxis.renderer.labels.template.rotation = 45;
    //categoryAxis.renderer.labels.template.contentWidth = 10;

    var valueAxis1 = PAGE.chart.column.yAxes.push(new am4charts.ValueAxis());

    var valueAxis2 = PAGE.chart.column.yAxes.push(new am4charts.ValueAxis());

    // Create series
    var series1 = PAGE.chart.column.series.push(new am4charts.LineSeries());
    series1.name = "목표";
    series1.dataFields.categoryX = "cYear";
    series1.dataFields.valueY = "nGoal";

    var series2 = PAGE.chart.column.series.push(new am4charts.LineSeries());
    series2.name = "실적";
    series2.dataFields.categoryX = "cYear";
    series2.dataFields.valueY = "nAmt";

    PAGE.chart.column.legend = new am4charts.Legend();

    PAGE.fn.search(); // 최초 실행
}