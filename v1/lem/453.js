﻿var TAB1 = AIV_UTIL.initPageObj();
var TAB2 = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    // 탭 클릭시 이벤트 설정
    $('.nav-tabs a').click(function () {
        $(this).tab('show');
        var tabIndex = $(this).parent().index();
        if (tabIndex == 1 && firstFlag) {
            TAB2.fn.search();
            firstFlag = false;
        } else if (tabIndex == 2) {
            // TAB3.grid.main.gridResize();
        }
    });

    // 1번 탭 초기설정
    fn_initTab1();
    // 2번 탭 초기설정
    fn_initTab2();
}

function fn_initTab1() {

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. TAB1.fn.*     사용 함수 설정
     * 2. TAB1.params.* 설정 기능 객체 설정 (input)
     * 3. TAB1.btn.*    이벤트 발생 객체 설정 (button)
     * 4. TAB1.grid.*   그리드 객체 설정
     * 5. TAB1.chart.*  함수 객체 설정
     */

    TAB1.fn.setChart = function (dummy) {
        TAB1.chart.column.setData(dummy);
    };

    TAB1.fn.setGrid = function (dummy) {
        TAB1.grid.main.setData(dummy);
    };

    TAB1.fn.search = function () {

        //TODO: ajax
        var dummy = (function () {
            function rd() { return Math.random() * 100; }
            return [{
                "cMon": "12",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "11",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "09",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "08",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "07",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "06",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "05",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "04",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "03",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "02",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "01",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }];
        })();

        var chartData = AIV_UTIL.convertChartData(dummy);

        //PAGE.grid.main.reload();
        TAB1.fn.setChart(chartData);
        TAB1.fn.setGrid(dummy);
    };

    // TAB1 내 컨트롤객체 세팅
    TAB1.params = $("#search_area1").AIV_SEARCH_INIT({
        onKeyup: TAB1.fn.search
    });

    // 버튼 세팅
    TAB1.btn.search = $("#search_area1 #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: TAB1.fn.search,
    });

    // 그리드 세팅
    TAB1.grid.main = $("#grid1").AIV_GRID({
        height: 250,
        toolbar: [
            // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'excel' // 추가, 추소, 엑셀저장
        ],
        columns: [
            { kField: "cMon", editable: false }, // aiv_model.js 내 kField 입력시 기타 속성에 대해 자동 입력 가능
            { kField: "arrMon.nGoal", editable: false },
            { kField: "arrMon.nAmt", editable: false }
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    // 차트 세팅
    TAB1.chart.column = $("#chart1").AIV_CHART({
        kType: 'XYChart'
    });

    // Create axes
    var categoryAxis = TAB1.chart.column.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "cMon";
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.inversed = true;
    //categoryAxis.renderer.minGridDistance = 0;
    //categoryAxis.renderer.labels.template.rotation = 45;
    //categoryAxis.renderer.labels.template.contentWidth = 10;

    var valueAxis = TAB1.chart.column.yAxes.push(new am4charts.ValueAxis());
    //valueAxis.min = 2000;
    //valueAxis.max = 20000;
    //valueAxis.renderer.minGridDistance = 15;

    // Create series
    var series1 = TAB1.chart.column.series.push(new am4charts.LineSeries());
    series1.name = "목표";
    series1.dataFields.valueY = "arrMon.nGoal";
    series1.dataFields.categoryX = "cMon";

    var series2 = TAB1.chart.column.series.push(new am4charts.LineSeries());
    series2.name = "실적";
    series2.dataFields.valueY = "arrMon.nAmt";
    series2.dataFields.categoryX = "cMon";

    TAB1.chart.column.legend = new am4charts.Legend();

    TAB1.fn.search(); // 최초 실행
}

function fn_initTab2() {

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. TAB2.fn.*     사용 함수 설정
     * 2. TAB2.params.* 설정 기능 객체 설정 (input)
     * 3. TAB2.btn.*    이벤트 발생 객체 설정 (button)
     * 4. TAB2.grid.*   그리드 객체 설정
     * 5. TAB2.chart.*  함수 객체 설정
     */

    TAB2.fn.setChart = function (dummy) {
        TAB2.chart.column.setData(dummy);
    };

    TAB2.fn.setGrid = function (dummy) {
        TAB2.grid.main.setData(dummy);
    };

    TAB2.fn.search = function () {

        //TODO: ajax
        var dummy = (function () {
            function rd() { return Math.random() * 100; }
            return [{
                "cMon": "12",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "11",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "09",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "08",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "07",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "06",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "05",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "04",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "03",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "02",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }, {
                "cMon": "01",
                "arrMon": {
                    "nGoal": rd(),
                    "nAmt": rd()
                }
            }];
        })();

        var chartData = AIV_UTIL.convertChartData(dummy);

        //PAGE.grid.main.reload();
        TAB2.fn.setChart(chartData);
        TAB2.fn.setGrid(dummy);
    };

    // TAB2 내 컨트롤객체 세팅
    TAB2.params = $("#search_area2").AIV_SEARCH_INIT({
        onKeyup: TAB2.fn.search
    });

    // 버튼 세팅
    TAB2.btn.search = $("#search_area2 #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: TAB2.fn.search,
    });

    // 그리드 세팅
    TAB2.grid.main = $("#grid2").AIV_GRID({
        height: 250,
        toolbar: [
            // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'excel' // 추가, 추소, 엑셀저장
        ],
        columns: [
            { kField: "cMon", editable: false }, // aiv_model.js 내 kField 입력시 기타 속성에 대해 자동 입력 가능
            { kField: "arrMon.nGoal", editable: false },
            { kField: "arrMon.nAmt", editable: false }
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    // 차트 세팅
    TAB2.chart.column = $("#chart2").AIV_CHART({
        kType: 'XYChart'
    });

    // Create axes
    var categoryAxis = TAB2.chart.column.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "cMon";
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.inversed = true;
    //categoryAxis.renderer.minGridDistance = 0;
    //categoryAxis.renderer.labels.template.rotation = 45;
    //categoryAxis.renderer.labels.template.contentWidth = 10;

    var valueAxis = TAB2.chart.column.yAxes.push(new am4charts.ValueAxis());
    //valueAxis.min = 2000;
    //valueAxis.max = 20000;
    //valueAxis.renderer.minGridDistance = 15;

    // Create series
    var series1 = TAB2.chart.column.series.push(new am4charts.LineSeries());
    series1.name = "목표";
    series1.dataFields.valueY = "arrMon.nGoal";
    series1.dataFields.categoryX = "cMon";

    var series2 = TAB2.chart.column.series.push(new am4charts.LineSeries());
    series2.name = "실적";
    series2.dataFields.valueY = "arrMon.nAmt";
    series2.dataFields.categoryX = "cMon";

    TAB2.chart.column.legend = new am4charts.Legend();

    TAB2.fn.search(); // 최초 실행
}