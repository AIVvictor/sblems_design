﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/lem/461/ls',
            data: params,
            success: function (responseData, options) {
                PAGE.grid.main.setData(responseData.resultData);
            }
        });
    };

    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: "k-primary",
        icon: "search",
        onClick: PAGE.fn.search
    });

    // 그리드 세팅
    PAGE.grid.main = $("#grid").AIV_GRID({
        toolbar: [
            // 'report', 'excel', 'save'    // 추가, 추소, 엑셀저장
            "edit",
            "excel" // 추가, 추소, 엑셀저장
        ],
        editToolbar: [
            // 'report', 'excel', 'save'    // 추가, 추소, 엑셀저장
            "add",
            "destroy",
            "save",
            "cancel",
        ],
        columns: [
            { kField: "selectable" },
            { kField: "sVrn", editable: false },
            { kField: "sTransComNm", editable: false },
            { kField: "sLineCd" },
            { kField: "logiWeiNm", editable: false },
            { kField: "sCarType", editable: false },
            { kField: "sVin", editable: false },
            { kField: "fuelTypeNm", editable: false },
            { kField: "sCarMade", editable: false },
            { kField: "useTypeNm", editable: false },
            { kField: "carStatusNm", editable: false },
            { kField: "sCarCompany", editable: false },
            { kField: "sCarModel", editable: false },
            { kField: "dRegDate", title: "등록일시", editable: false },
            { kField: "dModDate", editable: false },
            { kField: "sCarLoanDate", editable: false },
            { kField: "sFuelEff" }
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    PAGE.fn.search(); // 최초 실행
};
