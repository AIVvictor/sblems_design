﻿var PAGE = AIV_UTIL.initPageObj();

function fn_initPage() {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/lem/452/ls',
            data: params,
            success: function (responseData, textStatus, jqXHR) {
                if (AIV_UTIL.isEmpty(responseData.resultData)) {
                    responseData = {
                        "resultCode": "000",
                        "resultMessage": "SUCCESS",
                        "resultData": [{
                            "year": "2019",
                            "m1TonKm": 3.0,
                            "m1Gco2": 2.0,
                            "m1Mj": 1.0,
                            "m1EngVol": 0.0,
                            "m2TonKm": 5.0,
                            "m2Gco2": 4.0,
                            "m2Mj": 1.0,
                            "m2EngVol": 11.0,
                            "m3TonKm": null,
                            "m3Gco2": null,
                            "m3Mj": null,
                            "m3EngVol": null,
                            "m4TonKm": null,
                            "m4Gco2": null,
                            "m4Mj": null,
                            "m4EngVol": null,
                            "m5TonKm": null,
                            "m5Gco2": null,
                            "m5Mj": null,
                            "m5EngVol": null,
                            "m6TonKm": null,
                            "m6Gco2": null,
                            "m6Mj": null,
                            "m6EngVol": null,
                            "m7TonKm": null,
                            "m7Gco2": null,
                            "m7Mj": null,
                            "m7EngVol": null,
                            "m8TonKm": null,
                            "m8Gco2": null,
                            "m8Mj": null,
                            "m8EngVol": null,
                            "m9TonKm": null,
                            "m9Gco2": null,
                            "m9Mj": null,
                            "m9EngVol": null,
                            "m10TonKm": null,
                            "m10Gco2": null,
                            "m10Mj": null,
                            "m10EngVol": null,
                            "m11TonKm": null,
                            "m11Gco2": null,
                            "m11Mj": null,
                            "m11EngVol": null,
                            "m12TonKm": null,
                            "m12Gco2": null,
                            "m12Mj": null,
                            "m12EngVol": null,
                            "q1TonKm": 8.0,
                            "q2TonKm": 0.0,
                            "q3TonKm": 0.0,
                            "q4TonKm": 0.0,
                            "q1Gco2": 6.0,
                            "q2Gco2": 0.0,
                            "q3Gco2": 0.0,
                            "q4Gco2": 0.0,
                            "q1Mj": 2.0,
                            "q2Mj": 0.0,
                            "q3Mj": 0.0,
                            "q4Mj": 0.0,
                            "q1EngVol": 11.0,
                            "q2EngVol": 0.0,
                            "q3EngVol": 0.0,
                            "q4EngVol": 0.0
                        }]
                    };
                }
                if (AIV_UTIL.isEmpty(responseData.resultData)) {
                    PAGE.grid.main.setColumns([])
                    PAGE.grid.main.setData([]);
                    PAGE.chart.main.setData([]);
                } else {
                    var chartData = PAGE.fn.convertChartData(responseData.resultData);
                    var gridData = PAGE.fn.convertGridData(responseData.resultData);

                    var isStack = $(PAGE.params.monthStack).is(':checked');
                    if (isStack) {
                        chartData = AIV_UTIL.vStackDataList(chartData, ['cMon']);
                        gridData = AIV_UTIL.vStackDataList(gridData, ['cMon']);
                    }

                    PAGE.fn.createSeries(responseData.resultData);
                    PAGE.chart.main.setData(chartData);
                    PAGE.grid.main.setData(gridData);
                }
            }
        })
    };

    // API로 받아온 데이터를 차트 형식으로 변경
    PAGE.fn.convertChartData = function (rows) {
        var chartData = [];
        for (var i = 1; i < 13; i++) {
            var m = { 'cMon': leadingZeros(i, 2) + '월' };
            $(rows).each(function (k, row) {
                // 차트 데이터를 생성한다.
                for (var key in row) {
                    _val = row[key];
                    key = key.toLowerCase();
                    if (key.indexOf(i.toString()) != -1 && key.indexOf('engvol') != -1) {
                        m['_' + row.year + '_EngVol'] = _val;
                    }
                }
            })
            chartData.push(m)
        }
        return chartData;
    }

    // API로 받아온 데이터를 그리드 형식으로 변경
    PAGE.fn.convertGridData = function (rows) {
        var gridData = [];
        var columnsData = [{ kField: 'cMon' }];
        for (var i = 1; i < 13; i++) {
            var m = { 'cMon': leadingZeros(i, 2) };
            $(rows).each(function (k, row) {
                // 동일한 연도의 컬럼이 없다면 추가해준다.
                if (columnsData.findIndex(e => e.title === row.year + '년') < 0) {
                    columnsData.push(
                        { field: '_' + row.year + '_EngVol', title: row.year + '년', format: "{0:n3}" }
                    )
                }
                // 그리드 데이터를 생성한다.
                for (var key in row) {
                    _val = row[key];
                    key = key.toLowerCase();
                    if (key.indexOf(i.toString()) != -1 && key.indexOf('engvol') != -1) {
                        m['_' + row.year + '_EngVol'] = _val;
                    }
                }
            })
            gridData.push(m)
        }
        PAGE.grid.main.setColumns(columnsData);
        return gridData;
    }

    PAGE.fn.createSeries = function (rows) {
        // push 하며 series를 넣는 방식이기 때문에 처음에 clear해줌
        PAGE.chart.main.series.clear();

        // 서버로 부터 받은 데이터에서 연도 데이터 추출
        var years = [];
        $(rows).each(function (i, row) {
            years[i] = row.year;
        })

        // 선택한 연도의 수만큼 for문을 돌며 시리즈 생성
        for (var i = 0; i < years.length; i++) {
            PAGE.chart.main.pushSeries({
                type: 'LineSeries',
                name: years[i] + "년",
                dataFields: {
                    categoryX: 'cMon',
                    valueY: '_' + years[i] + '_EngVol'
                }
            });
        }
    }

    // 그리드 조회시 실행될 파라미터 조회 함수 작성
    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        params.cYear = params.years.join(',');
        delete params.years;
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });

    PAGE.params.years.selectAll();

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: "k-primary",
        icon: "search",
        onClick: PAGE.fn.search,
    });

    // 그리드 세팅
    PAGE.grid.main = $("#grid").AIV_GRID({
        toolbar: [
            // "cancel" ,"add" ,"edit" ,"save" ,"excel" ,"pdf" ,"destroy" // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            "excel" // 추가, 추소, 엑셀저장
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: "비콘 관리"     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    // 차트 세팅
    PAGE.chart.main = $("#chart").AIV_CHART({
        kType: "XYChart",
        xAxes: [{
            type: 'CategoryAxis',
            dataFields: {
                category: 'cMon'
            },
            renderer: {
                grid: {
                    template: {
                        disabled: true
                    }
                },
                cellStartLocation: 0.2,
                cellEndLocation: 0.8
            }
        }],
        yAxes: [{
            type: 'ValueAxis',
            min: 0
        }]
    });

    PAGE.fn.search(); // 최초 실행
}