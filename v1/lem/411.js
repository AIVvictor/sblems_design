﻿var TAB1 = AIV_UTIL.initPageObj();
var TAB2 = AIV_UTIL.initPageObj();
var TAB3 = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();
    fn_initTAB1();
    fn_initTAB2();
    fn_initTAB3();
}

var fn_initTAB1 = function () {
    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    TAB1.fn.search = function () {
        var params = TAB1.fn.getParams();
        params.transTypeCd = '1';   // 화물차

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/lem/411/ls',
            data: params,
            success: function (responseData, options) {
                TAB1.grid.main.setData(responseData.resultData);
            }
        })

    };

    TAB1.fn.getParams = function () {
        var params = TAB1.params.getAllValue();
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    TAB1.params = $("#search_area1").AIV_SEARCH_INIT({
        onKeyup: TAB1.fn.search
    });

    // 버튼 세팅
    TAB1.btn.search = $("#search_area1 #btnSearch").AIV_BUTTON({
        title: "조회",
        class: "k-primary",
        icon: "search",
        onClick: TAB1.fn.search
    });

    // 그리드 세팅
    TAB1.grid.main = $("#tab1_grid").AIV_GRID({
        toolbar: [
            // 'report', 'excel', 'save'    // 추가, 추소, 엑셀저장
            "edit",
            "excel" // 추가, 추소, 엑셀저장
        ],
        editToolbar: [
            'save', 'cancel'
        ],
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/lem/411/ls',
                beforeSend: function (hasCheckRow) {
                    return hasCheckRow;
                },
                success: function (responseData) {
                    TAB1.fn.search();
                }
            }
        },
        columns: [
            { kField: "selectable" },
            { kField: "dateMon", editable: false }, // 년월
            { kField: "transTypeNm", editable: false }, // 운송수단 구분
            { kField: "sVrn", editable: false }, // 차량번호
            { kField: "sLineCd", editable: false }, // 노선구분
            { kField: "sTransComNm", editable: false }, // 운수회사
            { kField: "tonNm", editable: false }, // 톤수
            { kField: "sSelfVrnNm", editable: false }, // 자차여부
            { kField: "loadageSum", editable: false }, // 적재량
            { kField: "distanceSum" }, // 운송거리
            { kField: "toeSum", editable: false }, // 환산톤(TOE)
            { kField: "sFuelEff", editable: false }, // 공인연비
            { kField: "engVolSum", editable: false }, // 연료사용량
            { kField: "mjSum", editable: false }, // 에너지사용량
            { kField: "gco2Sum", editable: false }, // 온실가스배출량
            { kField: "basUnt", editable: false }, // 온실가스원단위
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    TAB1.fn.search(); // 최초 실행
};


var fn_initTAB2 = function () {
    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    TAB2.fn.search = function () {
        var params = TAB2.fn.getParams();
        params.transTypeCd = '3';   // 철도

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/lem/411/ls',
            data: params,
            success: function (responseData, options) {
                TAB2.grid.main.setData(responseData.resultData);
            }
        })
    };

    TAB2.fn.getParams = function () {
        var params = TAB2.params.getAllValue();
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    TAB2.params = $("#search_area2").AIV_SEARCH_INIT({
        onKeyup: TAB2.fn.search
    });

    // 버튼 세팅
    TAB2.btn.search = $("#search_area2 #btnSearch").AIV_BUTTON({
        title: "조회",
        class: "k-primary",
        icon: "search",
        onClick: TAB2.fn.search
    });

    // 그리드 세팅
    TAB2.grid.main = $("#tab2_grid").AIV_GRID({
        toolbar: [
            // 'report', 'excel', 'save'    // 추가, 추소, 엑셀저장
            "edit",
            "excel" // 추가, 추소, 엑셀저장
        ],
        editToolbar: [
            'save', 'cancel'
        ],
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/lem/411/ls',
                beforeSend: function (hasCheckRow) {
                    return hasCheckRow;
                    /*
                    var saveRows = [];
                    $(hasCheckRow).each(function(i, row){
                        var saveRow = {
                            idLogisTransMonInfo: row.idLogisTransMonInfo,
                            fareSum: row.fareSum,
                            loadageSum: row.loadageSum,
                            distanceSum: row.distanceSum,
                        }
                        saveRows.push(saveRow);
                    })
                    return saveRows;
                    */
                },
                success: function (responseData) {
                    TAB2.fn.search();
                }
            }
        },
        columns: [
            { kField: "selectable" },
            { kField: "dateMon", editable: false }, // 년월
            { kField: "transTypeNm", editable: false }, // 운송수단 구분
            { kField: "fareSum" }, // 운임료
            { kField: "loadageSum" }, // 적재량
            { kField: "distanceSum" }, // 운송거리
            { kField: "sFuelEff", editable: false }, // 공인연비
            { kField: "toeSum", editable: false }, // 환산톤(TOE)
            { kField: "engVolSum", editable: false }, // 연료사용량
            { kField: "mjSum", editable: false }, // 에너지사용량
            { kField: "gco2Sum", editable: false }, // 온실가스배출량
            { kField: "basUnt", editable: false }, // 온실가스원단위
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    TAB2.fn.search(); // 최초 실행
};



var fn_initTAB3 = function () {
    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    TAB3.fn.search = function () {
        var params = TAB3.fn.getParams();
        params.transTypeCd = '3';   // 선박

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/lem/411/ls',
            data: params,
            success: function (responseData, options) {
                TAB3.grid.main.setData(responseData.resultData);
            }
        })

    };

    TAB3.fn.getParams = function () {
        var params = TAB3.params.getAllValue();
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    TAB3.params = $("#search_area3").AIV_SEARCH_INIT({
        onKeyup: TAB3.fn.search
    });

    // 버튼 세팅
    TAB3.btn.search = $("#search_area3 #btnSearch").AIV_BUTTON({
        title: "조회",
        class: "k-primary",
        icon: "search",
        onClick: TAB3.fn.search
    });

    // 그리드 세팅
    TAB3.grid.main = $("#tab3_grid").AIV_GRID({
        toolbar: [
            // 'report', 'excel', 'save'    // 추가, 추소, 엑셀저장
            "edit",
            "excel" // 추가, 추소, 엑셀저장
        ],
        editToolbar: [
            'save', 'cancel'
        ],
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/lem/411/ls',
                beforeSend: function (hasCheckRow) {
                    return hasCheckRow;
                },
                success: function (responseData) {
                    TAB3.fn.search();
                }
            }
        },
        columns: [
            { kField: "selectable" },
            { kField: "dateMon", editable: false }, // 년월
            { kField: "transTypeNm", editable: false }, // 운송수단 구분
            { kField: "sVrn", title: '선박이름', editable: false },
            { kField: "fareSum" }, // 운임료
            { kField: "loadageSum" }, // 적재량
            { kField: "distanceSum" }, // 운송거리
            { kField: "sFuelEff", editable: false }, // 공인연비
            { kField: "toeSum", editable: false }, // 환산톤(TOE)
            { kField: "engVolSum", editable: false }, // 연료사용량
            { kField: "mjSum", editable: false }, // 에너지사용량
            { kField: "gco2Sum", editable: false }, // 온실가스배출량
            { kField: "basUnt", editable: false }, // 온실가스원단위
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    TAB3.fn.search(); // 최초 실행
};
