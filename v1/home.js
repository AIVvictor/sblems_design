var PAGE = AIV_UTIL.initPageObj();

function chartA(){
  // Create chart instance
  var chart = am4core.create("chartA", am4charts.XYChart);

  // Add data
  chart.data = [ {
    "year": "2003",
    "prop1": 10000,
    "prop2": 10000
  }, {
    "year": "2004",
    "prop1": 12000,
    "prop2": 11000
  }, {
    "year": "2005",
    "prop1": 10000,
    "prop2": 10000
  } ];

  // Create axes
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "year";
  categoryAxis.title.text = "x";
  categoryAxis.renderer.grid.template.location = 4;
  categoryAxis.renderer.minGridDistance = 20;
  categoryAxis.renderer.cellStartLocation = 0.3;
  categoryAxis.renderer.cellEndLocation = 0.7;

  var  valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.min = 0;
  valueAxis.title.text = "y";

  // Create series
  function createSeries(field, name, stacked, fill) {
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.name = name;
    series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
    series.columns.template.fill = am4core.color(fill);
    series.dataFields.valueY = field;
    series.dataFields.categoryX = "year";
    series.stroke = false;
    series.stacked = stacked;
    series.columns.template.width = am4core.percent(95);
  }

  createSeries("prop1", "전사", false, "#8998db");
  createSeries("prop2", "물류", false, "#89c5db");

  // Add legend
  chart.legend = new am4charts.Legend();
} // chartA

function chartB(){
  // Create chart instance
  var chart = am4core.create("chartB", am4charts.XYChart);

  // Add data
  chart.data = [ {
    "year": "2003",
    "prop1": 12000,
    "prop2": 13000,
    "prop3": 10000
  }, {
    "year": "2004",
    "prop1": 12000,
    "prop2": 11000,
    "prop3": 7000
  }, {
    "year": "2005",
    "prop1": 10000,
    "prop2": 15000,
    "prop3": 17000
  } ];

  // Create axes
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "year";
  categoryAxis.title.text = "x";
  categoryAxis.renderer.grid.template.location = 4;
  categoryAxis.renderer.minGridDistance = 20;
  categoryAxis.renderer.cellStartLocation = 0.3;
  categoryAxis.renderer.cellEndLocation = 0.7;

  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.min = 0;
  valueAxis.title.text = "y";

  // Create series
  function createSeries(field, name, stacked, fill) {
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.name = name;
    series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
    series.columns.template.fill = am4core.color(fill);
    series.dataFields.valueY = field;
    series.dataFields.categoryX = "year";
    series.stroke = false;
    series.stacked = stacked;
    series.columns.template.width = am4core.percent(95);
  }

  function createSeries2(field, name, fill) {
    var series2 = chart.series.push(new am4charts.LineSeries());
    series2.name = name;
    series2.stroke = am4core.color(fill);
    series2.strokeWidth = 3;
    series2.dataFields.valueY = field;
    series2.dataFields.categoryX = "year";
  }

  createSeries("prop1", "물류", false, "#ffce66");
  createSeries("prop2", "물류 외", false, "#ffa969");
  createSeries2("prop3", "전사", "#ffa619");

  // Add legend
  chart.legend = new am4charts.Legend();
} // chartB

function chartC(){
  // Create chart instance
  var chart = am4core.create("chartC", am4charts.XYChart);

  // Add data
  chart.data = [ {
    "year": "2003",
    "prop1": 10000,
    "prop2": 10000
  }];

  // Create axes
  var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "year";
  categoryAxis.title.text = "x";
  categoryAxis.renderer.grid.template.location = 2;
  categoryAxis.renderer.minGridDistance = 20;
  categoryAxis.renderer.cellStartLocation = 0.4;
  categoryAxis.renderer.cellEndLocation = 0.6;

  var  valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.min = 0;
  valueAxis.title.text = "y";

  // Create series
  function createSeries(field, name, stacked, fill) {
    var series = chart.series.push(new am4charts.ColumnSeries());
    series.name = name;
    series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
    series.columns.template.fill = am4core.color(fill);
    series.columns.template.width = am4core.percent(90);
    series.dataFields.valueY = field;
    series.dataFields.categoryX = "year";
    series.stroke = false;
    series.stacked = stacked;
  }

  createSeries("prop1", "2019", false, "#f77a7a");
  createSeries("prop2", "2018", false, "#ff9676");

  // Add legend
  chart.legend = new am4charts.Legend();
} // chartC

function fn_initPage() {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    chartA();
    chartB();
    chartC();
}
