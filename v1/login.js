var fn_initPage = function() {
    // web storage 가 지원되는 브라우저인지 체크
    if (!AIV_UTIL.checkWebStorage()) {
        alert("죄송합니다. 지원되지않는 브라우져입니다.");
    }
    $('#form-login').validate();
    $("#username").focus();
};
    
/**
 * 로그인 요청처리
 */
function fn_login() {
    var datas = {
        'sEmpId' : $('#username').val(),
        'sEmpPwd' : $('#password').val()
    };
    AIV_USER.login(datas);
}

// catch - enter key event
function captureReturnKey(e) { 
    if(e.keyCode==13) { // enter key
        fn_login();
    }
} 