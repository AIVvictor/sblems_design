document.write('<nav class="page-sidebar" data-pages="sidebar" style="display:none;">');
document.write('    <div class="sidebar-overlay-slide from-top" id="appMenu">');
document.write('    </div>');
document.write('    <div class="sidebar-header">');
document.write('        <img src="/src/images/sm_ci/ci_logo_darkgray.png" alt="logo" class="ci_logo" data-src="/src/images/sm_ci/ci_logo_darkgray.png" data-src-retina="/src/images/sm_ci/ci_logo_darkgray.png">');
document.write('        <div class="sidebar-header-controls">');
document.write('            <button type="button" class="btn btn-link hidden-sm-down" data-toggle-pin="sidebar"><i class="fa fs-12"></i>');
document.write('            </button>');
document.write('        </div>');
document.write('    </div>');
document.write('    <div class="sidebar-menu">');
document.write('        <ul class="menu-items">');
document.write('        </ul>');
document.write('        <div class="clearfix"></div>');
document.write('    </div>');
document.write('</nav>');

// document.write('<link href="/src/assets/pages/css/pages-icons.css" rel="stylesheet" type="text/css">');
// document.write('<link href="/src/assets/pages/css/pages.custom.css" rel="stylesheet" type="text/css" class="main-stylesheet" />');  // pages.css
document.write('<script src="/src/assets/pages/js/pages.js" type="text/javascript"></script>');
document.write('<script src="/src/assets/kendo/js/scripts.js" type="text/javascript"></script>');

// 현재 페이지까지 메뉴 open
$(document).ready(function(){
    $('.page-sidebar').show();
    $('ul.menu-items').html(AIV_MENU.menuHTML);

    $('.sidebar-menu')
        .find('li[mid=' + AIV_MENU.sMenuId + ']').addClass('open')
        .parents('li').find('> a').click();

    $("#body_header").load("/v1/menu/body_header.html", function() {
        $("#loginUserInfo").html(AIV_USER.getUserInfo().sEmpNm + ' 님');    // HEADER 내 유저정보 입력
    }); // 최상단 HEADER 세팅.

    // $("#page_header").load("/ui/menu/page_header.html"); // 페이지상단 HEADER 세팅.
    // $("#page_footer").load("/ui/menu/page_footer.html"); // 페이지하단 FOOTER 세팅.
    // $("#page_slider").load("/v1/menu/page_slider.html"); // 우측 슬라이드 QUICKVIEW 로드..
    $('#page_footer').remove(); // 페이지 하단 FOOTER 제거 (미사용)
});