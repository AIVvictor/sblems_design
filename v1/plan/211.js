﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/plan/211/ls',
            data: params,
            success: function (responseData, textStatus, jqXHR) {
                if (AIV_UTIL.isEmpty(responseData.resultData)) {
                    responseData = {
                        "resultCode": "000",
                        "resultMessage": "SUCCESS",
                        "resultData": [{
                            "cYear": "2019-12",
                            "n1YearGj": 2,
                            "n1YearToe": 2,
                            "n1YearCo2": 2,
                            "n2YearGj": 2,
                            "n2YearToe": 2,
                            "n2YearCo2": 2,
                            "n3YearGj": 2,
                            "n3YearToe": 2,
                            "n3YearCo2": 2,
                            "n4YearGj": 2,
                            "n4YearToe": 2,
                            "n4YearCo2": 2,
                            "n5YearGj": 2,
                            "n5YearToe": 2,
                            "n5YearCo2": 2,
                            "nGj": 3,
                            "nToe": 3,
                            "nCo2": 3
                        }, {
                            "cYear": "2019-11",
                            "n1YearGj": 2,
                            "n1YearToe": 2,
                            "n1YearCo2": 2,
                            "n2YearGj": 2,
                            "n2YearToe": 2,
                            "n2YearCo2": 2,
                            "n3YearGj": 2,
                            "n3YearToe": 2,
                            "n3YearCo2": 2,
                            "n4YearGj": 2,
                            "n4YearToe": 2,
                            "n4YearCo2": 2,
                            "n5YearGj": 2,
                            "n5YearToe": 2,
                            "n5YearCo2": 2,
                            "nGj": 3,
                            "nToe": 3,
                            "nCo2": 3
                        }]
                    }
                }
                if (AIV_UTIL.isEmpty(responseData.resultData)) {
                    PAGE.grid.main.setColumns([])
                    PAGE.grid.main.setData([]);
                    PAGE.chart.main.setData([]);
                } else {
                    var chartData = [];
                    var gridData = [];

                    var gridData = PAGE.fn.convertGridData(responseData.resultData, params);
                    chartData = gridData;


                    PAGE.fn.createSeries(params);
                    PAGE.chart.main.setData(chartData);
                    PAGE.grid.main.setData(gridData);
                }
            }
        })
    };

    // API로 받아온 데이터를 그리드 형식으로 변경
    PAGE.fn.convertGridData = function (rows, params) {
        var gridData = [];
        $(rows).each(function (k, row) {
            data = {};
            data.cYear = row.cYear;
            for (var key in row) {
                _val = row[key];
                key = key.toLowerCase();
                console.log(key)
                if (key.indexOf('n1') != -1 && key.indexOf(params.sScuCd) != -1) {
                    data.prYearAvg = _val
                } else if (key.indexOf('n2') != -1 && key.indexOf(params.sScuCd) != -1) {
                    data.twYearAvg = _val
                } else if (key.indexOf('n3') != -1 && key.indexOf(params.sScuCd) != -1) {
                    data.trYearAvg = _val
                } else if (key.indexOf('n4') != -1 && key.indexOf(params.sScuCd) != -1) {
                    data.foYearAvg = _val
                } else if (key.indexOf('n5') != -1 && key.indexOf(params.sScuCd) != -1) {
                    data.fvYearAvg = _val
                } else if (key.indexOf('n') != -1 && key.indexOf(params.sScuCd) != -1 && key.indexOf('year') == -1) {
                    data.nAmt = _val
                }
            }
            gridData.push(data);
        })
        return gridData;
    }

    // 차트 series 생성함수
    PAGE.fn.createSeries = function () {
        // push 하며 series를 넣는 방식이기 때문에 처음에 clear해줌
        PAGE.chart.main.series.clear();

        // Create series
        PAGE.chart.main.pushSeries({
            type: 'LineSeries',
            name: "전년도원단위 활용",
            dataFields: {
                categoryX: 'cYear',
                valueY: 'prYearAvg'
            },
            tooltipHTML: '<table style="color:white;"><tbody>'
                + '<tr><td colspan="2"><strong>{categoryX}</strong></td></tr>'
                + '<tr><td>전년도원단위 활용: </td>    <td class="text-right pl-2">{prYearAvg}</td></tr>'
                + '<tr><td>2년평균원단위 활용: </td> <td class="text-right pl-2">{twYearAvg}</td></tr>'
                + '<tr><td>3년평균원단위 활용: </td>    <td class="text-right pl-2">{trYearAvg}</td></tr>'
                + '<tr><td>4년평균원단위 활용: </td>    <td class="text-right pl-2">{foYearAvg}</td></tr>'
                + '<tr><td>5년평균원단위 활용: </td>    <td class="text-right pl-2">{fvYearAvg}</td></tr>'
                + '<tr><td>실적: </td>    <td class="text-right pl-2">{nAmt}</td></tr>'
                + '</tbody></table>'
        });

        PAGE.chart.main.pushSeries({
            type: 'LineSeries',
            name: "2년평균원단위 활용",
            dataFields: {
                categoryX: 'cYear',
                valueY: 'prYearAvg'
            },
            tooltipHTML: ''
        });

        PAGE.chart.main.pushSeries({
            type: 'LineSeries',
            name: "3년평균원단위 활용",
            dataFields: {
                categoryX: 'cYear',
                valueY: 'trYearAvg'
            },
            tooltipHTML: ''
        });

        PAGE.chart.main.pushSeries({
            type: 'LineSeries',
            name: "4년평균원단위 활용",
            dataFields: {
                categoryX: 'cYear',
                valueY: 'foYearAvg'
            },
            tooltipHTML: ''
        });

        PAGE.chart.main.pushSeries({
            type: 'LineSeries',
            name: "5년평균원단위 활용",
            dataFields: {
                categoryX: 'cYear',
                valueY: 'fvYearAvg'
            },
            tooltipHTML: ''
        });

        PAGE.chart.main.pushSeries({
            type: 'LineSeries',
            name: " 실적",
            dataFields: {
                categoryX: 'cYear',
                valueY: 'nAmt'
            },
            tooltipHTML: ''
        });
    }

    // 그리드 조회시 실행될 파라미터 조회 함수 작성
    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        params.sScuCd = PAGE.params.unit.kText().toLowerCase();
        delete params.unit;
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });
    PAGE.params.cYearEnd.kValue((new Date()).format('yyyy')); // 현재 년도로 설정

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 그리드 세팅
    PAGE.grid.main = $("#grid").AIV_GRID({
        toolbar: [
            // 'report', 'excel', 'save'    // 추가, 추소, 엑셀저장
            'excel' // 추가, 추소, 엑셀저장
        ],
        columns: [
            { kField: "cYear", editable: false }, // aiv_model.js 내 kField 입력시 기타 속성에 대해 자동 입력 가능
            { kField: "prYearAvg", editable: false },
            { kField: "twYearAvg", editable: false },
            { kField: "trYearAvg", editable: false },
            { kField: "foYearAvg", editable: false },
            { kField: "fvYearAvg", editable: false },
            { kField: "nAmt", editable: false },
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    // 차트 세팅
    PAGE.chart.main = $("#chart").AIV_CHART({
        kType: "XYChart",
        xAxes: [{
            type: 'CategoryAxis',
            dataFields: {
                category: 'cYear'
            },
            renderer: {
                grid: {
                    template: {
                        disabled: true
                    }
                },
                labels: {
                    template: {
                        rotation: 315
                    }
                },
                minGridDistance: 1,
                cellStartLocation: 0.2,
                cellEndLocation: 0.8
            }
        }],
        yAxes: [{
            type: 'ValueAxis',
            min: 0
        }]
    });

    PAGE.fn.search(); // 최초 실행
}