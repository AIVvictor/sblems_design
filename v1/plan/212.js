﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/plan/212/gov/ls',
            data: params,
            success: function (responseData) {
                PAGE.grid.main.setData(responseData.resultData);
                PAGE.grid.sub.setData([]);
            }
        })
    };

    // 에너지 고지서 등록/갱신 팝업
    PAGE.fn.onBtnFileAdd = function (e) {
        var $tr = $(e.target).closest('tr');
        var dataItem = PAGE.grid.main.self.dataItem($tr);

        var _content = $('#modal1').html();

        var pageWindow = AIV_WINDOW.init({
            title: '감축목표 통보자료 등록/갱신',
            width: 1100,
            height: 330,
            content: _content,
            resizable: true,
        }).open();

        PAGE.form.main = $('#kwindow-details #form').AIV_FORM_INIT();
        PAGE.form.main.setMode('edit');
        // PAGE.form.main.cYear.kFormValue(dataItem.cYear);

        // selected dataItem setValue
        PAGE.form.main.setAllValue({
            cYear: dataItem.cYear,
            // sFileNm: dataItem.sFileNm,
            // sFileNmOld: {   // 파일다운로드 모듈에는 해당 객체안에 필요값들을 정리하여 넣는다.
            //     sScreenId: AIV_MENU.sMenuId,
            //     gId: dataItem.gId,
            //     idFile: dataItem.idFile,
            //     sFileNm: dataItem.sFileNm || '',
            // },
            sFileNm: {
                cmdTag: 'U',
                idFile: dataItem.idFile,
                sFileNm: dataItem.sFileNm,
                gFileId: dataItem.gId,
                gId: dataItem.gId,
            }
        });
        // PAGE.form.main.sFileNm.kFormValue({
        //     idFile: dataItem.idFile,
        //     // idFile: dataItem.sFileiId,
        //     sFileNm: dataItem.sFileNm,
        //     gFileId: dataItem.gId,
        //     // readMode: true      // file객체에 기존 file정보를 입력할때 읽기모드가 필요시 설정
        // });



        PAGE.btn.formSave = $("#kwindow-details #btnSave").AIV_BUTTON({
            title: "저장",
            class: 'k-primary',
            icon: 'save',
            onClick: function (e) {

                // dataItem.idErpDept = PAGE.form.main.idErpDept.kValue();     // 그리드 데이터에 부서번호 저장
                // alert(dataItem.idErpDept)
                /**
                 * 그리드 에서 다중저장을 하여야 함.
                 * 그리드 row(dataItem)안에 파일데이터를 옮겨놓아야 하는데,
                 * input file의 정책상 set()이 안되며 레이어가 닫힐때 input도 같이 제거해버리므로 사전에 미리 이동시켜놓음
                 * 그리드에서 저장시 uid를 이용하여 파일 정보를 가져오도록 작성하였음
                 */
                $('input[type=file][uid=' + dataItem.uid + ']').remove();   // 이미 input file을 이동해놓았었다면 삭제
                $(PAGE.form.main.sFileNm)           // FORM 내 파일 DOM을 body로 옮겨놓아 창이 닫히더라도 사라지지 않게 함
                    .appendTo('body')
                    .attr('uid', dataItem.uid);     // 구별할 수 있도록 uid 부여

                console.log(PAGE.form.main.sFileNm)
                console.log(dataItem.uid)

                dataItem.file = PAGE.form.main.sFileNm;

                pageWindow.self.close();            // modal layer close
                PAGE.grid.main.self.select($tr);    // select grid

            },
        });

        PAGE.btn.formCancel = $("#kwindow-details #btnCancel").AIV_BUTTON({
            title: "취소",
            // class: 'k-primary',
            class: 'k-default',
            icon: 'cancel',
            onClick: function (e) {
                pageWindow.self.close();
            },
        });

    }

    // 그리드 조회시 실행될 파라미터 조회 함수 작성
    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search,
        cYear: { type: 'cYear', title: "조회년도", allYn: true }
    });

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 서브 그리드 데이터 조회
    PAGE.fn.searchSubGrid = function (cYear) {
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/plan/212/fac/ls',
            data: {
                cYear: cYear
            },
            success: function (responseData) {
                var subData = responseData.resultData || [];
                PAGE.grid.sub.setData(subData);
            }
        });
    }

    // 그리드 세팅
    PAGE.grid.main = $("#mainGrid").AIV_GRID({
        height: 230,
        mode: 'read',   // read or edit.  default:read
        toolbar: [  // ['cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy'] == [취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제]
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'add', 'save', 'destroy', 'cancel'
        ],
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/plan/212/gov/dtl',
                beforeSend: function (hasCheckRow) {
                    return hasCheckRow;
                },
                success: function () {
                    PAGE.fn.search();
                }
            },
            delete: {
                method: 'POST',
                url: '/api/v1/plan/212/gov/dtl',
                beforeSend: function (hasCheckRow) {
                    // var saveRow = [];
                    // var saveRow = hasCheckRow[0];
                    var saveRow = {
                        cYear: hasCheckRow[0].cYear,
                        nEngGoal: 0,
                        nGasGoal: 0
                    }
                    return saveRow;
                },
                success: function () {
                    PAGE.fn.search();
                }
            }
        },


        columns: [
            { kField: 'selectable' },
            {
                kField: "cYear",
                template: function (dataItem) {
                    return '<a href="#" onclick="javascript:PAGE.fn.searchSubGrid(\'' + dataItem.cYear + '\')">' + dataItem.cYear + '</a>';
                }
            }, // aiv_model.js 내 kField 입력시 기타 속성에 대해 자동 입력 가능
            {
                title: '정부목표',
                columns: [
                    { kField: "nGasGoal" },
                    { kField: "nEngGoal" },
                ]
            },
            {
                title: '사내목표 합계',
                columns: [
                    { kField: "nYearGasGoalSum", editable: false },
                    { kField: "nYearEngGoalSum", title: '에너지(GJ)', editable: false },
                ]
            },
            {
                kField: 'sFileNm',
                title: '감축목표 통보자료',
                /*
                template: function (dataItem) {
                    var isEditMode = PAGE.grid.main.isEditMode();

                    if (isEditMode) {
                        if (AIV_UTIL.isNotEmpty(dataItem.sFileNm)) {
                            var input = '<input type="file" class="fileInputTemplate" id="' + dataItem.uid + '" >기존파일: ' + dataItem.sFileNm + '</input>';
                            return input;   // 편집모드 && 파일이 있을때
                        } else {
                            var input = '<input type="file" class="fileInputTemplate" id="' + dataItem.uid + '" />';
                            return input;   // 편집모드 && 파일이 없을때
                        }
                    } else {
                        if (AIV_UTIL.isNotEmpty(dataItem.sFileNm)) {
                            return "<a href='#' onclick='AIV_COMM.fileDownload(\"" + dataItem.sScreenId + "\",\"" + dataItem.gId + "\",\"" + dataItem.idFile + "\",\"" + dataItem.sFileNm + "\")'>" + dataItem.sFileNm + "</a>";    // 읽기모드 && 파일이 있을때
                        } else {
                            return '';    // 읽기모드 && 파일이 없을때
                        }
                    }
                },
                */

                template: function (dataItem) {
                    var isEditMode = PAGE.grid.main.isEditMode();

                    if (isEditMode) {
                        if (AIV_UTIL.isNotEmpty(dataItem.sFileNm)) {
                            return "<a href='#' onclick='PAGE.fn.onBtnFileAdd(event)'>등록</a>";   // 편집모드 && 파일이 있을때
                        } else {
                            return "<a href='#' onclick='PAGE.fn.onBtnFileAdd(event)'>미등록</a>";   // 편집모드 && 파일이 없을때
                        }
                    } else {
                        if (AIV_UTIL.isNotEmpty(dataItem.sFileNm)) {
                            return "<a href='#' onclick='AIV_COMM.fileDownload(\"" + AIV_MENU.sMenuId + "\",\"" + dataItem.gBillId + "\",\"" + dataItem.idFile + "\",\"" + dataItem.sFileNm + "\")'>" + dataItem.sFileNm + "</a>";    // 읽기모드 && 파일이 있을때
                        } else {
                            return '';    // 읽기모드 && 파일이 없을때
                        }
                    }
                },
                editable: false
            },
        ],
        /*
        dataBound: function (e) {
            var views = e.sender.dataSource.view();
            $(views).each(function (i, dataItem) {
                var $tr = $('tr[data-uid=' + dataItem.uid + ']');
                var ddt = $tr.find('.fileInputTemplate');
                dataItem.file = $(ddt).AIV_UPLOAD({ // grid row를 저장할때 해당 file 객체를 이용하여 저장함
                    select: function (e) {
                        $($tr).find('.k-checkbox').click();
                        // dataItem.sFileNm = e.files[0].name;
                        alert(e.files[0].name);
                    }
                });
            });
        },
        */
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false,
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    PAGE.grid.sub = $("#subGrid").AIV_GRID({
        height: 230,
        toolbar: [
            // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'excel' // 추가, 추소, 엑셀저장
        ],
        columns: [
            { kField: "cYear" }, // aiv_model.js 내 kField 입력시 기타 속성에 대해 자동 입력 가능
            { kField: "sSiteNm" },
            { kField: "nYearGasGoal" },
            { kField: "nYearEngGoal" },
        ],
        editable: false, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    PAGE.fn.search(); // 최초 실행
}