﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
    * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
    * 1. PAGE.fn.*     사용 함수 설정
    * 2. PAGE.params.* 설정 기능 객체 설정 (input)
    * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
    * 4. PAGE.grid.*   그리드 객체 설정
    * 5. PAGE.chart.*  함수 객체 설정
    */

    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            url: '/api/v1/plan/213/ghg/ls',
            method: 'GET',
            data: params,
            success: function (responseData, textStatus, jqXHR) {
                var gridData = PAGE.fn.convertGridData(responseData.resultData);
                PAGE.grid.main.setData(gridData);
            }

        });
    };

    // API로 받아온 데이터를 그리드 형식으로 변경
    PAGE.fn.convertGridData = function (rows) {
        var gridData = [];
        gridData = rows;
        $(rows).each(function (i, row) {
            var sSiteLen = rows.filter(function (r) { return row.sSiteId == r.sSiteId }).length;
            rows[i].nYearEngGoalAvg = rows[i].nYearEngGoal / sSiteLen;
            rows[i].nYearGasGoalAvg = rows[i].nYearGasGoal / sSiteLen;
            rows[i].nEngGoalSumAvg = rows[i].nEngGoalSum / sSiteLen;
            rows[i].nGasGoalSumAvg = rows[i].nGasGoalSum / sSiteLen;
        });

        return gridData;
    }

    // 그리드 조회시 실행될 파라미터 조회 함수 작성
    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area1").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });

    // 버튼 세팅
    PAGE.btn.search = $("#search_area1 #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 그리드 세팅
    PAGE.grid.main = $("#grid1").AIV_GRID({
        mode: 'read',   // read or edit.  default:read
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'save', 'destroy', 'cancel'
        ],

        api: {
            save: {
                method: 'POST',
                url: '/api/v1/plan/213/ghg/ls',
                beforeSend: function (hasCheckRow) {
                    var saveRow = [];
                    $(hasCheckRow).each(function (i, cRow) {
                        var index = -1;
                        $(saveRow).each(function (j, sRow) {
                            var isDupl = (sRow.idSite == cRow.idSite) && (sRow.cYear == cRow.cYear) && (sRow.nYearGasGoal == cRow.nYearGasGoal) && (sRow.nYearEngGoal == cRow.nYearEngGoal);
                            if (isDupl) { index = j; }
                        });
                        if (index > -1) {
                            // 중복데이터
                            saveRow[index].goals.push({
                                idEngPoint: cRow.idEngPoint,
                                m1: cRow.m1, m2: cRow.m2, m3: cRow.m3, m4: cRow.m4, m5: cRow.m5, m6: cRow.m6,
                                m7: cRow.m7, m8: cRow.m8, m9: cRow.m9, m10: cRow.m10, m11: cRow.m11, m12: cRow.m12
                            });
                        } else {
                            // 신규데이터
                            saveRow.push({
                                idSite: cRow.idSite,
                                cYear: cRow.cYear,
                                nYearGasGoal: cRow.nYearGasGoal,
                                nYearEngGoal: cRow.nYearEngGoal,
                                goals: [{
                                    idEngPoint: cRow.idEngPoint,
                                    m1: cRow.m1, m2: cRow.m2, m3: cRow.m3, m4: cRow.m4, m5: cRow.m5, m6: cRow.m6,
                                    m7: cRow.m7, m8: cRow.m8, m9: cRow.m9, m10: cRow.m10, m11: cRow.m11, m12: cRow.m12
                                }]
                            });
                        }
                    });

                    return saveRow;
                },
                success: function (responseData) {
                    PAGE.fn.search();
                }
            },
            delete: {
                method: 'POST',
                url: '/api/v1/plan/213/ghg/ls',
                beforeSend: function (hasCheckRow) {
                    var saveRow = [];
                    $(hasCheckRow).each(function (i, cRow) {
                        var index = -1;
                        $(saveRow).each(function (j, sRow) {
                            var isDupl = (sRow.idSite == cRow.idSite) && (sRow.cYear == cRow.cYear) && (sRow.nYearGasGoal == cRow.nYearGasGoal) && (sRow.nYearEngGoal == cRow.nYearEngGoal);
                            if (isDupl) { index = j; }
                        });
                        if (index > -1) {
                            // 중복데이터
                            saveRow[index].goals.push({
                                idEngPoint: cRow.idEngPoint,
                                m1: 0, m2: 0, m3: 0, m4: 0, m5: 0, m6: 0,
                                m7: 0, m8: 0, m9: 0, m10: 0, m11: 0, m12: 0
                            });
                        } else {
                            // 신규데이터
                            saveRow.push({
                                idSite: cRow.idSite,
                                cYear: cRow.cYear,
                                nYearGasGoal: cRow.nYearGasGoal,
                                nYearEngGoal: cRow.nYearEngGoal,
                                goals: [{
                                    idEngPoint: cRow.idEngPoint,
                                    m1: 0, m2: 0, m3: 0, m4: 0, m5: 0, m6: 0,
                                    m7: 0, m8: 0, m9: 0, m10: 0, m11: 0, m12: 0
                                }]
                            });
                        }
                    });
                    return saveRow;
                },
                success: function (responseData) {
                    PAGE.fn.search();
                }
            }
        },

        dataSource: {
            aggregate: [
                { field: "sSiteNm", aggregate: "count" },    // count, average, min, max, sum 등 집계 가능
                { field: "nYearEngGoalAvg", aggregate: "sum" },    // count, average, min, max, sum 등 집계 가능
                { field: 'nYearGasGoalAvg', aggregate: "sum" },
                { field: 'nEngGoalSumAvg', aggregate: "sum" },
                { field: 'nGasGoalSumAvg', aggregate: "sum" },
            ],
        },
        change: function (e) {    // 컬럼 값 변화시 해당 로직 실행
            var self = e.sender;
            var selectedRows = this.select(); // 선택된 항목 배열
            var selectedDataItems = [];
            for (var i = 0; i < selectedRows.length; i++) {
                var dataItem = this.dataItem(selectedRows[i]);
                var monthSum = Number(dataItem.m1) + Number(dataItem.m2) + Number(dataItem.m3) + Number(dataItem.m4) + Number(dataItem.m5) + Number(dataItem.m6)
                    + Number(dataItem.m7) + Number(dataItem.m8) + Number(dataItem.m9) + Number(dataItem.m10) + Number(dataItem.m11) + Number(dataItem.m12);
                monthSum = isNaN(monthSum) ? '-' : kendo.toString(monthSum, "#,###.000");   // 합계행의 NaN 방지

                $(selectedRows[i]).find('td:eq(10)').text(monthSum);    // nYearGasGoalSum
                selectedDataItems.push(dataItem);
            }

            self.selectedRows = selectedRows; // 선택된 항목을 저장
            self.selectedDataItems = selectedDataItems; // 선택된 항목을 dataItems 형식으로 변환한것을 저장
        },

        columns: [
            { kField: 'selectable' },
            { kField: "cYear", editable: false, rowspan: true },    // rowspan:true 설정시 같은 열의 셀중 같은 값이면 rowspan 실행함
            { kField: "sSiteNm", editable: false, rowspan: true, footerTemplate: "합계", footerAttributes: { "class": "text-center" } },
            {
                title: "목표",
                columns: [{
                    kField: 'nYearEngGoal',
                    rowspan: true,
                    footerTemplate: function (aggregate, b, c) {
                        return kendo.toString(aggregate.nYearEngGoalAvg.sum, "#,###.000") || 0;
                    },
                    footerAttributes: {
                        "class": "text-right"
                    }
                }, {
                    kField: 'nYearGasGoal',
                    rowspan: true,
                    footerTemplate: function (aggregate) {
                        return kendo.toString(aggregate.nYearGasGoalAvg.sum, "#,###.000") || 0;
                    },
                    footerAttributes: {
                        "class": "text-right"
                    }
                }]
            },
            {
                title: "월별목표합계",
                columns: [{
                    kField: 'nEngGoalSum',
                    rowspan: true,
                    editable: false,
                    footerTemplate: function (aggregate) {
                        return kendo.toString(aggregate.nEngGoalSumAvg.sum, "#,###.000") || 0;
                    },
                    footerAttributes: {
                        "class": "text-right"
                    }
                }, {
                    kField: 'nGasGoalSum',
                    rowspan: true,
                    editable: false,
                    footerTemplate: function (aggregate) {
                        return kendo.toString(aggregate.nGasGoalSumAvg.sum, "#,###.000") || 0;
                    },
                    footerAttributes: {
                        "class": "text-right"
                    }
                }]
            },
            { kField: "sPointNm", editable: false },
            { kField: "engNm", editable: false },
            { kField: "calcUntNm", editable: false },
            {
                // kField: "arrGhgPlan",
                title: '월별목표(tCO2)',
                columns: [
                    {
                        kField: "nYearGasGoalSum",
                        editable: false,
                        template: function (dataItem) {
                            var monthSum = Number(dataItem.m1) + Number(dataItem.m2) + Number(dataItem.m3) + Number(dataItem.m4) + Number(dataItem.m5) + Number(dataItem.m6)
                                + Number(dataItem.m7) + Number(dataItem.m8) + Number(dataItem.m9) + Number(dataItem.m10) + Number(dataItem.m11) + Number(dataItem.m12);
                            monthSum = isNaN(monthSum) ? '-' : monthSum;   // 합계행의 NaN 방지

                            return kendo.toString(monthSum, "#,###.000");
                        }
                    },
                    { kField: "m1" },
                    { kField: "m2" },
                    { kField: "m3" },
                    { kField: "m4" },
                    { kField: "m5" },
                    { kField: "m6" },
                    { kField: "m7" },
                    { kField: "m8" },
                    { kField: "m9" },
                    { kField: "m10" },
                    { kField: "m11" },
                    { kField: "m12" },
                ]
            },
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false,
        // autoBind: false
        // dataBound: function (e) {
        // },
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    PAGE.fn.search(); // 최초 실행
}