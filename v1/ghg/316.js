﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    // 조회 함수
    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();
        params.isSite = params.sSiteId;

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/ghg/316/ls',
            data: params,
            success: function (responseData, options) {
                $(responseData.resultData).each(function (i, rows) {
                    if (AIV_UTIL.isNotEmpty(rows.cMon)) {
                        rows.cMon = leadingZeros(rows.cMon, 2);
                    }
                    rows.cYm = rows.cYear + '-' + rows.cMon;
                })
                PAGE.grid.main.setData(responseData.resultData);
            }
        })
    }

    // 에너지 고지서 등록/갱신 팝업
    PAGE.fn.onBtnBillAdd = function (e) {
        var $tr = $(e.target).closest('tr');
        var dataItem = PAGE.grid.main.self.dataItem($tr);

        var _content = $('#modal1').html();

        var pageWindow = AIV_WINDOW.init({
            title: '에너지 고지서 등록/갱신',
            width: 1100,
            height: 500,
            content: _content,
            resizable: true,
        }).open();

        PAGE.form.main = $('#kwindow-details #form').AIV_FORM_INIT();
        PAGE.form.main.setMode('edit');

        // selected dataItem setValue
        PAGE.form.main.setAllValue({
            sSiteNmList: dataItem.idSite,
            cYm: dataItem.cYm,
            sEngCd: dataItem.sEngCd,
            sEngCdNm: dataItem.sEngCdNm,
            idErpDept: dataItem.idErpDept,
            sFileNm: '',
            sFileNmOld: {   // 파일다운로드 모듈에는 해당 객체안에 필요값들을 정리하여 넣는다.
                sScreenId: AIV_MENU.sMenuId,
                gId: dataItem.gBillId,
                sFileNm: dataItem.sFileNm || '',
            },
        });

        PAGE.btn.formSave = $("#kwindow-details #btnSave").AIV_BUTTON({
            title: "저장",
            class: 'k-primary',
            icon: 'save',
            onClick: function (e) {

                dataItem.idErpDept = PAGE.form.main.idErpDept.kValue();     // 그리드 데이터에 부서번호 저장
                alert(dataItem.idErpDept)
                /**
                 * 그리드 에서 다중저장을 하여야 함.
                 * 그리드 row(dataItem)안에 파일데이터를 옮겨놓아야 하는데,
                 * input file의 정책상 set()이 안되며 레이어가 닫힐때 input도 같이 제거해버리므로 사전에 미리 이동시켜놓음
                 * 그리드에서 저장시 uid를 이용하여 파일 정보를 가져오도록 작성하였음
                 */
                $('input[type=file][uid=' + dataItem.uid + ']').remove();   // 이미 input file을 이동해놓았었다면 삭제
                $(PAGE.form.main.sFileNm)           // FORM 내 파일 DOM을 body로 옮겨놓아 창이 닫히더라도 사라지지 않게 함
                    .appendTo('body')
                    .attr('uid', dataItem.uid);     // 구별할 수 있도록 uid 부여

                pageWindow.self.close();            // modal layer close
                PAGE.grid.main.self.select($tr);    // select grid

            },
        });

        PAGE.btn.formCancel = $("#kwindow-details #btnCancel").AIV_BUTTON({
            title: "취소",
            // class: 'k-primary',
            class: 'k-default',
            icon: 'cancel',
            onClick: function (e) {
                pageWindow.self.close();
            },
        });

    }

    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        return params;
    }

    // 검색영역 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        cYear: {
            title: '조회년월'
        }
    });

    // 조회 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 그리드 세팅
    PAGE.grid.main = $('#grid').AIV_GRID({
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'destroy', 'save', 'cancel'
        ],
        api: {
            save: {
                method: 'POST',
                url: '/api/v1/ghg/316/ls',
                beforeSend: function (hasCheckRow) {
                    var saveRow = [];
                    $(hasCheckRow).each(function (i, cRow) {
                        cRow.idErpDept = 1;
                        cRow.cYear = cRow.cYm.split('-')[0];
                        cRow.cMon = cRow.cYm.split('-')[1];
                        var file = $('input[type=file][uid=' + cRow.uid + ']')[0];
                        if (file && file.files.length > 0) {   // 파일이 있으면 업로드 후 기존데이터에 gid 추가하여 반영
                            var form = AIV_UTIL.createForms({
                                input: file,
                                gId: cRow.gBillId
                            });
                            AIV_COMM.fileUpload({
                                form: form,
                                success: function (uploadResponseData) {
                                    cRow.gBillId = uploadResponseData.gid;
                                    cRow.idFile = uploadResponseData.idFile;
                                    cRow.sFileId = uploadResponseData.sFileId;
                                    saveRow.push(cRow);
                                }
                            });
                        } else {
                            saveRow.push(cRow);
                        }
                    });
                    return saveRow;
                },
                success: function () {
                    PAGE.fn.search();
                }
            }
        },
        columns: [
            { kField: 'selectable' },
            { kField: 'sSiteNm', rowspan: true, editable: false },
            { kField: 'cYm', rowspan: true, editable: false },
            { kField: 'sEmtCdNm', rowspan: true, editable: false },
            { kField: 'sPointNm', editable: false },
            { kField: 'sEngCdNm', editable: false },
            { kField: 'sInUntCdNm', editable: false },
            { kField: 'nUseAmt' },
            { kField: 'nCost' },
            { kField: 'nEngEtc', title: '가동시간 외' },
            {
                kField: 'nEngEtcUntCd',
                title: '가동시간<br>외 단위'
                , template: function (dataItem) {
                    return dataItem.nEngEtcUntCdNm;
                }
            },
            {
                title: '고지서',
                template: function (dataItem) {
                    var isEditMode = PAGE.grid.main.isEditMode();

                    if (isEditMode) {
                        if (AIV_UTIL.isNotEmpty(dataItem.sFileNm)) {
                            return "<a href='#' onclick='PAGE.fn.onBtnBillAdd(event)'>등록</a>";   // 편집모드 && 파일이 있을때
                        } else {
                            return "<a href='#' onclick='PAGE.fn.onBtnBillAdd(event)'>미등록</a>";   // 편집모드 && 파일이 없을때
                        }
                    } else {
                        if (AIV_UTIL.isNotEmpty(dataItem.sFileNm)) {
                            return "<a href='#' onclick='AIV_COMM.fileDownload(\"" + AIV_MENU.sMenuId + "\",\"" + dataItem.gBillId + "\",\"" + dataItem.idFile + "\",\"" + dataItem.sFileNm + "\")'>" + dataItem.sFileNm + "</a>";    // 읽기모드 && 파일이 있을때
                        } else {
                            return '';    // 읽기모드 && 파일이 없을때
                        }
                    }



                },
                editable: false
            },
        ],
        editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    PAGE.fn.search();
}