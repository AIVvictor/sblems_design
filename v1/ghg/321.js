﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    var defaultValues = {}; // 초기화 버튼 클릭 시 다시 set 해줄 기본 데이터값 저장객체

    // 검색조건 초기화
    PAGE.fn.reset = function(){
        PAGE.params.setAllValue(defaultValues);
    }

    // 파라미터 조회 함수 작성
    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        params.unit = PAGE.params.unit.kText().toLowerCase();
        params.idSites = params.idSites.join(',');      // 사업장 리스트는 []에서 ,로 변경한다
        params.idEngPoints = params.idPoints.join(','); // 배출시설 리스트는 []에서 ,로 변경한다
        delete params.idPoints; // idEngPoints로 대체 되었으므로 삭제한다.

        if ( params.idSites == '' ) {
            delete params.idSites;
        }
        if ( params.idEngPoints == '' ) {
            delete params.idEngPoints;
        }
        return params;
    }

    // 검색조건에 따른 조회 -> 그리드, 차트 생성
    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/ghg/321/ls',
            data: params,
            success: function (responseData) {
                var rows = responseData.resultData;
                if (AIV_UTIL.isEmpty(rows)) {
                    PAGE.grid.main.setColumns([])
                    PAGE.grid.main.setData([]);
                    PAGE.chart.main.series.clear();
                    PAGE.chart.main.setData([]);
                    return;
                }
                /**
                 * 검색조건에 따라 응답데이터 및 보여주는 형태가 다름
                 * 1. 사업장 전체선택 시
                 * 2. 사업장 n선택 시
                 * 3. 사업장 1선택, 배출시설 n선택 시
                 */
                var viewType = 0;
                if ( typeof params.idSites === 'undefined' ) {  // case 1
                    viewType = 1;
                } else if ( params.idSites.split(',').length > 1 ) {  // case 2
                    viewType = 2;
                } else if ( params.idSites.split(',').length == 1 ) {
                    if ( params.idEngPoints && params.idEngPoints.split(',').length > 1  ) {  // case 3
                        viewType = 3;
                    } else {  // case 2
                        viewType = 2;
                    }
                } else {
                    alert('error\nno case');
                    return;
                }

                // 차트 & 그리드 설정+세팅
                PAGE.fn.createSeries(viewType, rows, params);
                PAGE.fn.createGrid(viewType, rows, params);

                // 데이터 변환
                var chartData = PAGE.fn.convertChartData(viewType, rows, params);
                var gridData = PAGE.fn.convertGridData(viewType, rows, params);

                console.log('chartData', chartData);
                console.log('gridData', gridData);
                
                // 데이터 적용
                PAGE.chart.main.setData(chartData);
                PAGE.grid.main.setData(gridData);
            }
        })
    };

    // 차트 시리즈 생성
    PAGE.fn.createSeries = function(viewType, rows, params){
        if ( viewType == 1 ) {
            PAGE.fn.createSeries1(rows, params);
        } else if ( viewType == 2 ) {
            PAGE.fn.createSeries2(rows, params);
        } else if ( viewType == 3 ) {
            PAGE.fn.createSeries3(rows, params);
        }
    }
    // 차트 시리즈 생성1
    PAGE.fn.createSeries1 = function (rows, params) {
        // push 하며 series를 넣는 방식이기 때문에 처음에 clear해줌
        PAGE.chart.main.series.clear();

        // Create series
        PAGE.chart.main.pushSeries({
            type: 'ColumnSeries',
            name: '고정연소',
            dataFields: {
                categoryX: 'cYear',
                valueY: 'fixCmbst_value'
            },
            tooltipHTML: '<table style="color:white;"><tbody>'
                + '<tr><td colspan="2"><strong>{categoryX}</strong></td></tr>'
                + '<tr><td>고정연소: </td>    <td class="text-right pl-2">{fixCmbst_value}</td></tr>'
                + '<tr><td>이동연소: </td> <td class="text-right pl-2">{mvngCmbst_value}</td></tr>'
                + '<tr><td>전기: </td>    <td class="text-right pl-2">{indirEmission_electricPower}</td></tr>'
                + '<tr><td>스팀: </td>    <td class="text-right pl-2">{indirEmission_steam}</td></tr>'
                + '<tr><td>온수: </td> <td class="text-right pl-2">{indirEmission_hotWater}</td></tr>'
                + '<tr><td>기타배출: </td>    <td class="text-right pl-2">{etcEmission_value}</td></tr>'
                + '<tr><td>합계: </td>    <td class="text-right pl-2">{sum}</td></tr>'
                + '</tbody></table>'
        });

        PAGE.chart.main.pushSeries({
            type: 'ColumnSeries',
            name: '이동연소',
            dataFields: {
                categoryX: 'cYear',
                valueY: 'mvngCmbst_value'
            },
            tooltipHTML: ''
        });

        PAGE.chart.main.pushSeries({
            type: 'ColumnSeries',
            name: '전기',
            dataFields: {
                categoryX: 'cYear',
                valueY: 'indirEmission_electricPower'
            },
            tooltipHTML: ''
        });

        PAGE.chart.main.pushSeries({
            type: 'ColumnSeries',
            name: '스팀',
            dataFields: {
                categoryX: 'cYear',
                valueY: 'indirEmission_steam'
            },
            tooltipHTML: ''
        });

        PAGE.chart.main.pushSeries({
            type: 'ColumnSeries',
            name: '온수',
            dataFields: {
                categoryX: 'cYear',
                valueY: 'indirEmission_hotWater'
            },
            tooltipHTML: ''
        });

        PAGE.chart.main.pushSeries({
            type: 'ColumnSeries',
            name: '기타배출',
            dataFields: {
                categoryX: 'cYear',
                valueY: 'etcEmission_value'
            },
            tooltipHTML: ''
        });

        PAGE.chart.main.pushSeries({
            type: 'LineSeries',
            name: '합계',
            dataFields: {
                categoryX: 'cYear',
                valueY: 'sum'
            },
            bullet: {
                strokeWidth: 5
            },
            tooltipHTML: ''
        });
    }
    // 차트 시리즈 생성2
    PAGE.fn.createSeries2 = function (rows, params) {
        // push 하며 series를 넣는 방식이기 때문에 처음에 clear해줌
        PAGE.chart.main.series.clear();


        var tooltipList = [];
        var tooltipHTML = '<table style="color:white;"><tbody>'
            + '<tr><td colspan="2"><strong>{categoryX}</strong></td></tr>';

        for( var key in rows ) {
            $(rows[key]).each(function(i, yearData){
                var siteNm = yearData.sDesc;
                if ( tooltipList.find(function(c){ return c == siteNm}) == undefined ) {
                    tooltipHTML += '<tr><td>'+siteNm+': </td>    <td class="text-right pl-2">{'+siteNm+'}</td></tr>';
                    tooltipList.push(siteNm);
                }
            });
        }
        tooltipHTML +=  '<tr><td>합계: </td>    <td class="text-right pl-2">{sum}</td></tr>';
        tooltipHTML += '</tbody></table>';

        var columnsList = [];
        for( var key in rows ) {
            $(rows[key]).each(function(i, yearData){
                var siteNm = yearData.sDesc;
                if ( columnsList.find(function(c){ return c == siteNm}) == undefined ) {
                    PAGE.chart.main.pushSeries({
                        type: 'ColumnSeries',
                        name: siteNm,
                        dataFields: {
                            categoryX: 'cYear',
                            valueY: siteNm
                        },
                        tooltipHTML: (columnsList.length==0 ? tooltipHTML : '') // 첫 시리즈에만 적용
                    });
                    columnsList.push(siteNm);
                }
            });
        }

        PAGE.chart.main.pushSeries({
            type: 'LineSeries',
            name: '합계',
            dataFields: {
                categoryX: 'cYear',
                valueY: 'sum'
            },
            bullet: {
                strokeWidth: 5
            },
            tooltipHTML: ''
        });

        return;
        
        // Create series
        tooltipHTML: '<table style="color:white;"><tbody>'
            + '<tr><td colspan="2"><strong>{categoryX}</strong></td></tr>'
            + '<tr><td>고정연소: </td>    <td class="text-right pl-2">{fixCmbst_value}</td></tr>'
            + '<tr><td>이동연소: </td> <td class="text-right pl-2">{mvngCmbst_value}</td></tr>'
            + '<tr><td>전기: </td>    <td class="text-right pl-2">{indirEmission_electricPower}</td></tr>'
            + '<tr><td>스팀: </td>    <td class="text-right pl-2">{indirEmission_steam}</td></tr>'
            + '<tr><td>온수: </td> <td class="text-right pl-2">{indirEmission_hotWater}</td></tr>'
            + '<tr><td>기타배출: </td>    <td class="text-right pl-2">{etcEmission_value}</td></tr>'
            + '<tr><td>합계: </td>    <td class="text-right pl-2">{sum}</td></tr>'
            + '</tbody></table>'
    }
    // 차트 시리즈 생성3
    PAGE.fn.createSeries3 = function (rows, params) {
        // push 하며 series를 넣는 방식이기 때문에 처음에 clear해줌
        PAGE.chart.main.series.clear();


        var tooltipList = [];
        var tooltipHTML = '<table style="color:white;"><tbody>'
            + '<tr><td colspan="2"><strong>{categoryX}</strong></td></tr>';

        for( var key in rows ) {
            $(rows[key]).each(function(i, yearData){
                var siteNm = yearData.sDesc;
                if ( tooltipList.find(function(c){ return c == siteNm}) == undefined ) {
                    tooltipHTML += '<tr><td>'+siteNm+': </td>    <td class="text-right pl-2">{'+siteNm+'}</td></tr>';
                    tooltipList.push(siteNm);
                }
            });
        }
        tooltipHTML +=  '<tr><td>합계: </td>    <td class="text-right pl-2">{sum}</td></tr>';
        tooltipHTML += '</tbody></table>';

        var columnsList = [];
        for( var key in rows ) {
            $(rows[key]).each(function(i, yearData){
                var siteNm = yearData.sDesc;
                if ( columnsList.find(function(c){ return c == siteNm}) == undefined ) {
                    PAGE.chart.main.pushSeries({
                        type: 'ColumnSeries',
                        name: siteNm,
                        dataFields: {
                            categoryX: 'cYear',
                            valueY: siteNm
                        },
                        tooltipHTML: (columnsList.length==0 ? tooltipHTML : '') // 첫 시리즈에만 적용
                    });
                    columnsList.push(siteNm);
                }
            });
        }

        PAGE.chart.main.pushSeries({
            type: 'LineSeries',
            name: '합계',
            dataFields: {
                categoryX: 'cYear',
                valueY: 'sum'
            },
            bullet: {
                strokeWidth: 5
            },
            tooltipHTML: ''
        });

        return;
        
        // Create series
        tooltipHTML: '<table style="color:white;"><tbody>'
            + '<tr><td colspan="2"><strong>{categoryX}</strong></td></tr>'
            + '<tr><td>고정연소: </td>    <td class="text-right pl-2">{fixCmbst_value}</td></tr>'
            + '<tr><td>이동연소: </td> <td class="text-right pl-2">{mvngCmbst_value}</td></tr>'
            + '<tr><td>전기: </td>    <td class="text-right pl-2">{indirEmission_electricPower}</td></tr>'
            + '<tr><td>스팀: </td>    <td class="text-right pl-2">{indirEmission_steam}</td></tr>'
            + '<tr><td>온수: </td> <td class="text-right pl-2">{indirEmission_hotWater}</td></tr>'
            + '<tr><td>기타배출: </td>    <td class="text-right pl-2">{etcEmission_value}</td></tr>'
            + '<tr><td>합계: </td>    <td class="text-right pl-2">{sum}</td></tr>'
            + '</tbody></table>'
    }

    // API로 받아온 데이터를 차트 형식으로 변경
    PAGE.fn.convertChartData = function (viewType, rows, params) {
        var chartDatas = [];
        if ( viewType == 1 ) {
            chartDatas = PAGE.fn.convertChartData1(rows, params);
        } else if ( viewType == 2 ) {
            chartDatas = PAGE.fn.convertChartData2(rows, params);
        } else if ( viewType == 3 ) {
            chartDatas = PAGE.fn.convertChartData3(rows, params);
        }
        return chartDatas;
    }
    // API로 받아온 데이터를 차트 형식으로 변경
    PAGE.fn.convertChartData1 = function (rows, params) {
        var dataType = params.unit;
        dataType = dataType == 'tj' ? 'gj' : dataType;
        var chartDatas = [];

        for (var key in rows) {
            var chartData = {
                fixCmbst_value: undefined,  // 초기값 필요시 변경가능 ex: 0
                mvngCmbst_value: undefined,
                indirEmission_electricPower: undefined,
                indirEmission_steam: undefined,
                indirEmission_hotWater: undefined,
                etcEmission_value: undefined,
                sum: 0
            };
            $(rows[key]).each(function(j, yearData){
                // chartData.
                chartData.cYear = yearData.cYear + '년';
                chartData.co2 = yearData.co2;
                chartData.gj = yearData.gj;
                chartData.sDesc = yearData.sDesc;
                chartData.toe = yearData.toe;

                switch (yearData.sDesc) {
                    case '고정':
                        chartData.fixCmbst_value = yearData[dataType];
                        break;
                    case '이동':
                        chartData.mvngCmbst_value = yearData[dataType];
                        break;
                    case '간접-전기':
                        chartData.indirEmission_electricPower = yearData[dataType];
                        break;
                    case '간접-스팀':
                        chartData.indirEmission_steam = yearData[dataType];
                        break;
                    case '간접-온수':
                        chartData.indirEmission_hotWater = yearData[dataType];
                        break;
                    case '기타':
                        chartData.etcEmission_value = yearData[dataType];
                        break;
                }
                chartData.sum += isNaN(yearData[dataType]) ? 0 : yearData[dataType];
            });

            chartDatas.push(chartData);
        }
        return chartDatas;
    }
    PAGE.fn.convertChartData2 = function (rows, params) {
        var dataType = params.unit;
        dataType = dataType == 'tj' ? 'gj' : dataType;
        var chartDatas = [];

        for (var key in rows) {
            var chartData = { sum: 0 };
            $(rows[key]).each(function(j, yearData){
                chartData.cYear = yearData.cYear;

                var siteNm = yearData.sDesc;
                chartData[siteNm] = yearData[dataType];
                chartData.sum += isNaN(yearData[dataType]) ? 0 : yearData[dataType];
            });
            chartDatas.push(chartData);
        }
        return chartDatas;
    }
    PAGE.fn.convertChartData3 = function (rows, params) {
        var dataType = params.unit;
        dataType = dataType == 'tj' ? 'gj' : dataType;
        var chartDatas = [];

        for (var key in rows) {
            var chartData = { sum: 0 };
            $(rows[key]).each(function(j, yearData){
                chartData.cYear = yearData.cYear;

                var siteNm = yearData.sDesc;
                chartData[siteNm] = yearData[dataType];
                chartData.sum += isNaN(yearData[dataType]) ? 0 : yearData[dataType];
            });
            chartDatas.push(chartData);
        }
        return chartDatas;
    }
    

    // 그리드 생성
    PAGE.fn.createGrid = function (viewType, rows, params) {
        if ( viewType == 1 ) {
            PAGE.fn.createGrid1(rows, params);
        } else if ( viewType == 2 ) {
            PAGE.fn.createGrid2(rows, params);
        } else if ( viewType == 3 ) {
            PAGE.fn.createGrid3(rows, params);
        }
    }
    // 그리드 생성
    PAGE.fn.createGrid1 = function (rows, params) {
        // 그리드 세팅
        PAGE.grid.main = $("#grid").empty().AIV_GRID({
            toolbar: [
                'excel' // 추가, 추소, 엑셀저장
            ],
            columns: [
                { kField: "cYear" }, // aiv_model.js 내 kField 입력시 기타 속성에 대해 자동 입력 가능
                {
                    title: "직접배출",
                    columns: [
                        { kField: "fixCmbst_value" },
                        { kField: "mvngCmbst_value" },
                    ]
                },
                {
                    title: "간접배출",
                    columns: [
                        { kField: "indirEmission_electricPower" },
                        { kField: "indirEmission_steam" },
                        { kField: "indirEmission_hotWater" },
                    ]
                },
                { kField: "etcEmission_value" },
                { kField: 'sum', title: '합계' }
            ],
            editable: false,
            pageable: false,
            autoBind: false
        });
    }
    PAGE.fn.createGrid2 = function (rows, params) {
        var columnsList = [{ kField: 'cYear' }];
        for( var key in rows ) {
            $(rows[key]).each(function(i, yearData){
                var siteNm = yearData.sDesc;
                if ( columnsList.find(function(c){ return c.title == siteNm}) == undefined ) {
                    columnsList.push({
                        title: siteNm,
                        field: siteNm
                    })
                }
            });
        }
        columnsList.push({ kField: 'sum', title: '합계' });


        // 그리드 세팅
        PAGE.grid.main = $("#grid").empty().AIV_GRID({
            height: 300,
            toolbar: [
                'excel' // 추가, 추소, 엑셀저장
            ],
            columns: columnsList,
            editable: false,
            pageable: false,
            autoBind: false
        });
    }
    PAGE.fn.createGrid3 = function (rows, params) {
        var columnsList = [{ kField: 'cYear' }];
        for( var key in rows ) {
            $(rows[key]).each(function(i, yearData){
                var siteNm = yearData.sDesc;
                if ( columnsList.find(function(c){ return c.title == siteNm}) == undefined ) {
                    columnsList.push({
                        title: siteNm,
                        field: siteNm
                    })
                }
            });
        }
        columnsList.push({ kField: 'sum', title: '합계' });


        // 그리드 세팅
        PAGE.grid.main = $("#grid").empty().AIV_GRID({
            height: 300,
            toolbar: [
                'excel' // 추가, 추소, 엑셀저장
            ],
            columns: columnsList,
            editable: false,
            pageable: false,
            autoBind: false
        });
    }

    // 그리드 데이터로 변환
    PAGE.fn.convertGridData = function (viewType, rows, params) {
        var gridDatas = [];
        if ( viewType == 1 ) {
            gridDatas = PAGE.fn.convertGridData1(rows, params);
        } else if ( viewType == 2 ) {
            gridDatas = PAGE.fn.convertGridData2(rows, params);
        } else if ( viewType == 3 ) {
            gridDatas = PAGE.fn.convertGridData3(rows, params);
        }
        return gridDatas;
    }
    // API로 받아온 데이터를 그리드 형식으로 변경
    PAGE.fn.convertGridData1 = function (rows, params) {
        var dataType = params.unit;
        dataType = dataType == 'tj' ? 'gj' : dataType;
        var gridDatas = [];

        for (var key in rows) {
            var gridData = {
                fixCmbst_value: undefined,  // 초기값 필요시 변경가능 ex: 0
                mvngCmbst_value: undefined,
                indirEmission_electricPower: undefined,
                indirEmission_steam: undefined,
                indirEmission_hotWater: undefined,
                etcEmission_value: undefined,
                sum: 0
            };
            $(rows[key]).each(function(j, yearData){
                // gridData.
                gridData.cYear = yearData.cYear;
                gridData.co2 = yearData.co2;
                gridData.gj = yearData.gj;
                gridData.sDesc = yearData.sDesc;
                gridData.toe = yearData.toe;

                switch (yearData.sDesc) {
                    case '고정':
                        gridData.fixCmbst_value = yearData[dataType];
                        break;
                    case '이동':
                        gridData.mvngCmbst_value = yearData[dataType];
                        break;
                    case '간접-전기':
                        gridData.indirEmission_electricPower = yearData[dataType];
                        break;
                    case '간접-스팀':
                        gridData.indirEmission_steam = yearData[dataType];
                        break;
                    case '간접-온수':
                        gridData.indirEmission_hotWater = yearData[dataType];
                        break;
                    case '기타':
                        gridData.etcEmission_value = yearData[dataType];
                        break;
                }
                gridData.sum += isNaN(yearData[dataType]) ? 0 : yearData[dataType];
            });

            gridDatas.push(gridData);
        }
        return gridDatas;
    }
    PAGE.fn.convertGridData2 = function (rows, params) {
        var dataType = params.unit;
        dataType = dataType == 'tj' ? 'gj' : dataType;
        var gridDatas = [];

        for (var key in rows) {
            var gridData = {
                sum: 0
            };
            $(rows[key]).each(function(j, yearData){
                // gridData.
                gridData.cYear = yearData.cYear;

                var siteNm = yearData.sDesc;
                gridData[siteNm] = yearData[dataType];
                gridData.sum += isNaN(yearData[dataType]) ? 0 : yearData[dataType];
            });

            gridDatas.push(gridData);
        }
        return gridDatas;
    }
    PAGE.fn.convertGridData3 = function (rows, params) {
        var dataType = params.unit;
        dataType = dataType == 'tj' ? 'gj' : dataType;
        var gridDatas = [];

        for (var key in rows) {
            var gridData = {
                sum: 0
            };
            $(rows[key]).each(function(j, yearData){
                // gridData.
                gridData.cYear = yearData.cYear;

                var siteNm = yearData.sDesc;
                gridData[siteNm] = yearData[dataType];
                gridData.sum += isNaN(yearData[dataType]) ? 0 : yearData[dataType];
            });

            gridDatas.push(gridData);
        }
        return gridDatas;
    }




    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search,
        cYear: { title: '분석기간' },
        idSites: {
            onChange: function (e) {
                var idSites = e.sender.value();
                var $idPointsWrapper = PAGE.params.idPoints.closest('.aiv_search_input_wrap');
                console.log(idSites);

                if ( idSites.length == 1 ) {
                    $idPointsWrapper.show();

                    var data = idSites[0]; // TODO: change
                    AIV_COMM.ajax({
                        method: 'GET',
                        url: '/api/v1/info/571/ls',
                        data: {
                            idSite: data
                        },
                        success: function (res) {
                            var datas = [];
                            $(res.resultData).each(function(i, data){
                                datas.push({
                                    value: data.idEngPoint, // sPointId?
                                    text: data.sPointNm
                                })
                            })
                            PAGE.params.idPoints.self.setDataSource(datas);
                            // PAGE.params.idPoints.kValue(datas);
                        }
                    })
                } else {
                    PAGE.params.idPoints.kValue([]);
                    $idPointsWrapper.hide();
                }
            }
        }
    });

    // PAGE.params.cYear.kValue('2015');   // TODO: remove
    // PAGE.params.idSites.kValue([3, 4, 5, 11, 13]);   // TODO: remove
    // PAGE.params.idSites.kValue([3]);   // TODO: remove
    // PAGE.params.idPoints.kValue([1,2,3]);   // TODO: remove
    
    defaultValues = PAGE.params.getAllValue();
    PAGE.params.idPoints.closest('.aiv_search_input_wrap').hide();  // 최초, 사업장선택이 "전체"이므로 배출시설을 "숨김"처리 한다.

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });
    PAGE.btn.reset = $("#search_area #btnReset").AIV_BUTTON({
        title: "초기화",
        class: 'k-default',
        icon: 'reset',
        onClick: PAGE.fn.reset,
    });

    // 차트 세팅
    PAGE.chart.main = $("#chart").AIV_CHART({
        kType: "XYChart",
        xAxes: [{
            type: 'CategoryAxis',
            dataFields: {
                category: 'cYear'
            }
        }],
        yAxes: [{
            type: 'ValueAxis',
            min: 0
        }]
    });

    PAGE.fn.search(); // 최초 실행
}