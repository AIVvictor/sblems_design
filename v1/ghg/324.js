﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/ghg/324/ls',
            data: params,
            success: function (responseData) {
                if (AIV_UTIL.isEmpty(responseData.resultData)) {
                    PAGE.grid.main.setColumns([])
                    PAGE.grid.main.setData([]);
                    PAGE.chart.main.setData([]);
                } else {
                    var chartData = PAGE.fn.convertChartData(responseData.resultData, params);
                    var gridData = PAGE.fn.convertGridData(responseData.resultData, params);

                    var isStack = $(PAGE.params.monthStack).is(':checked');
                    if (isStack) {
                        chartData = AIV_UTIL.stackDataList(chartData, ['cMon', 'nRate']);
                        gridData = AIV_UTIL.stackDataList(gridData, ['cMon', 'nRate']);
                    }

                    PAGE.fn.createSeries(params);
                    PAGE.chart.main.setData(chartData);
                    PAGE.grid.main.setData(gridData);
                }
            }
        })
    };

    // API로 받아온 데이터를 차트 형식으로 변경
    PAGE.fn.convertChartData = function (rows, params) {
        var chartData = [];
        $(rows).each(function (i, row) {
            var data = {};
            data.cMon = row.cMon + '월';
            data.nGoal = row.nMonEngGoal;
            switch (params.unit) {
                case 'gj':
                    data.nAmt = row.nGj;
                    break;
                case 'toe':
                    data.nAmt = row.nToe;
                    break;
                case 'tco2':
                    data.nAmt = row.nCo2;
                    break;
                case 'ch4':
                    data.nAmt = row.nCh4;
                    break;
                case 'n2o':
                    data.nAmt = row.nN2o;
                    break;
                case 'tco2eq':
                    data.nAmt = row.nTco2eq;
                    break;
            }
            data.nRate = (data.nAmt / data.nGoal) * 100;
            if (isNaN(data.nRate)) {
                data.nRate = "";
            }
            chartData.push(data);
        })
        return chartData;
    }

    // API로 받아온 데이터를 그리드 형식으로 변경
    PAGE.fn.convertGridData = function (rows, params) {
        var gridData = [];
        var columnsData = [
            { kField: 'cMon' }, // aiv_model.js 내 kField 입력시 기타 속성에 대해 자동 입력 가능
            { kField: 'nGoal', title: '목표(' + params.unit + ')' },
            { kField: 'nAmt', title: '실적(' + params.unit + ')' },
            { kField: 'nRate' }, // aiv_model.js 내 kField 입력시 기타 속성에 대해 자동 입력 가능
        ]
        $(rows).each(function (i, row) {
            var data = {};
            data.cMon = row.cMon;
            data.nGoal = row.nMonEngGoal;
            switch (params.unit) {
                case 'gj':
                    data.nAmt = row.nGj;
                    break;
                case 'toe':
                    data.nAmt = row.nToe;
                    break;
                case 'tco2':
                    data.nAmt = row.nCo2;
                    break;
                case 'ch4':
                    data.nAmt = row.nCh4;
                    break;
                case 'n2o':
                    data.nAmt = row.nN2o;
                    break;
                case 'tco2eq':
                    data.nAmt = row.nTco2eq;
                    break;
            }
            data.nRate = (data.nAmt / data.nGoal) * 100;
            if (isNaN(data.nRate)) {
                data.nRate = "";
            }
            gridData.push(data);
        })
        PAGE.grid.main.setColumns(columnsData);
        return gridData;
    }

    PAGE.fn.createSeries = function (params) {
        // push 하며 series를 넣는 방식이기 때문에 처음에 clear해줌
        PAGE.chart.main.series.clear();

        PAGE.chart.main.pushSeries({
            type: 'ColumnSeries',
            name: '목표(' + params.unit + ')',
            dataFields: {
                categoryX: 'cMon',
                valueY: 'nGoal'
            },
            tooltipHTML: '<table style="color:white;"><tbody>'
                + '<tr><td colspan="2"><strong>{categoryX}</strong></td></tr>'
                + '<tr><td>목표: </td>    <td class="text-right pl-2">{nGoal}</td></tr>'
                + '<tr><td>실적: </td> <td class="text-right pl-2">{nAmt}</td></tr>'
                + '<tr><td>달성률: </td>    <td class="text-right pl-2">{nRate}</td></tr>'
                + '</tbody></table>'
        });

        PAGE.chart.main.pushSeries({
            type: 'ColumnSeries',
            name: '실적(' + params.unit + ')',
            dataFields: {
                categoryX: 'cMon',
                valueY: 'nAmt'
            },
            tooltipHTML: ''
        });

        PAGE.chart.main.pushSeries({
            type: 'LineSeries',
            name: '달성률(%)',
            dataFields: {
                categoryX: 'cMon',
                valueY: 'nRate'
            },
            tooltipHTML: '',
            yAxis: {
                index: 1
            }
        });
    }

    // 그리드 조회시 실행될 파라미터 조회 함수 작성
    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        params.unit = PAGE.params.unit.kText().toLowerCase();
        delete params.site;
        delete params.monthStack;
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });

    // 체크박스 클릭 시 누적된 값으로 조회
    PAGE.params.monthStack.bind("click", function () {
        PAGE.fn.search();
    });

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 그리드 세팅
    PAGE.grid.main = $("#grid").AIV_GRID({
        height: 300,
        toolbar: [
            // 'report', 'excel', 'save'    // 추가, 추소, 엑셀저장
            'excel' // 추가, 추소, 엑셀저장
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    // 차트 세팅
    PAGE.chart.main = $("#chart").AIV_CHART({
        kType: "XYChart",
        xAxes: [{
            type: 'CategoryAxis',
            dataFields: {
                category: 'cMon'
            },
            renderer: {
                grid: {
                    template: {
                        disabled: true
                    }
                },
                cellStartLocation: 0.2,
                cellEndLocation: 0.8
            }
        }],
        yAxes: [{
            type: 'ValueAxis',
            min: 0
        }, {
            type: 'ValueAxis',
            min: 0,
            max: 100,
            renderer: {
                opposite: true
            }
        }],
    });

    PAGE.fn.search(); // 최초 실행
}