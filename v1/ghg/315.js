﻿var PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    // 메인 그리드 조회 함수
    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();
        PAGE.grid.sub.setData([]);
        params.idsite = params.idSite;
        params.cMon = Number(params.cMon);  // '01'로 보낼 경우 응답안함 -> 1로 바꾸어줌
        
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/ghg/315/ls',
            data: params,
            success: function (responseData, options) {
                var gridData = [];
                $(responseData.resultData).each(function (i, rows) {
                    if (rows.siDelYn != 'Y') {
                        if (rows.cMon) {
                            rows.cMon = leadingZeros(rows.cMon);
                        }
                        rows.cYm = rows.cYear + '-' + rows.cMon;

                        gridData.push(rows);
                    }
                })
                PAGE.grid.main.setData(gridData);
            }
        })
    };

    // 서브 그리드 조회 함수
    PAGE.fn.searchSub = function (e) {
        var dataItem = PAGE.grid.main.self.dataItem($(e.currentTarget).closest("tr"));
        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/ghg/315/ls?idSite=' + dataItem.idSite,
            success: function (data) {
                PAGE.grid.sub.setData(data.resultData);
            }
        })
    };

    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        return params;
    }

    // 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        cYear: {
            title: '조회년월'
        },
        onKeyup: PAGE.fn.search
    });

    // 조회 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 마감 버튼 세팅
    PAGE.btn.close = $("#search_area #btnClose").AIV_BUTTON({
        title: "마감",
        class: 'k-default',
        icon: 'save',
        onClick: function () {
            var closeParams = PAGE.params.getAllValue();
            closeParams.cCloseYn = 'Y'; // 마감:Y
            closeParams.cMon = Number(closeParams.cMon);    // '01' -> 1

            // if (AIV_UTIL.isEmpty(closeParams.idSite)) {
                // alert('사업장을 선택해주세요.');
            // } else {
                AIV_COMM.ajax({
                    method: 'POST',
                    url: '/api/v1/ghg/315/close',
                    data: closeParams,
                    success: function (data) {
                        if (data.resultCode == "000") {
                            // resultMessage: "SUCCESS"
                            alert('마감 되었습니다');
                        }
                    }
                });
            // }

        },
    });

    // 마감취소 버튼 세팅
    PAGE.btn.cancel = $("#search_area #btnCancel").AIV_BUTTON({
        title: "마감취소",
        class: 'k-default',
        icon: 'reload',
        onClick: function () {
            var closeParams = PAGE.params.getAllValue();
            closeParams.cMon = Number(closeParams.cMon);

            // if (AIV_UTIL.isEmpty(closeParams.idSite)) {
                // alert('사업장을 선택해주세요.');
            // } else {
                AIV_COMM.ajax({
                    method: 'POST',
                    url: '/api/v1/ghg/315/close/cancel',
                    data: closeParams,
                    success: function (data) {
                        if (data.resultCode == "000") {
                            alert('마감취소 되었습니다');
                        }
                    }
                });
            // }
        },
    });

    // 메인 그리드 세팅
    PAGE.grid.main = $("#mainGrid").AIV_GRID({
        height: 200,
        toolbar: [
            // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'excel' // 추가, 추소, 엑셀저장
        ],
        columns: [
            {
                kField: "sSiteNm",
                template: function (dataItem) {
                    return '<a href="#" onClick="javascript:PAGE.fn.searchSub(event);">' + dataItem.sSiteNm + '</a>'
                }
            },
            { kField: "cYm" },
            { kField: "cCloseYn" },
            { kField: "sModDate" },
            { kField: "t1GjSum" },
            { kField: "t1ToeSum" },
            { kField: "t2Tco2eqSum" },
            { kField: "t1Tco2eqSum" },
            { kField: "tco2eqSumDeltaRate" }
        ],
        // selectable: true,
        // editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    // 서브 그리드 세팅
    PAGE.grid.sub = $("#subGrid").AIV_GRID({
        height: 300,
        toolbar: [
            // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'excel' // 추가, 추소, 엑셀저장
        ],
        columns: [
            { kField: 'sSiteNm' },
            { kField: 'cefNm' },
            { kField: 'entNm' },
            { kField: 'sInUntNm' },
            { kField: 'nUseAmt' },
            { kField: 'nCost' },
        ],
        // editable: true, // 수정 기능 추가
        pageable: false
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    PAGE.fn.search(); // 최초 실행
}