﻿var PAGE = AIV_UTIL.initPageObj();

function fn_initPage() {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/sum/111/ls',
            data: params,
            success: function (responseData) {
                if (AIV_UTIL.isEmpty(responseData.resultData)) {
                    PAGE.grid.main.setColumns([])
                    PAGE.grid.main.setData([]);
                    PAGE.chart.main.setData([]);
                } else {
                    params.unit = PAGE.params.unit.kText().toLowerCase();

                    var chartData = PAGE.fn.converChartData(responseData.resultData, params);
                    var gridData = PAGE.fn.converGridData(responseData.resultData, params);

                    var isStack = $(PAGE.params.monthStack).is(':checked');
                    if (isStack) {
                        chartData = AIV_UTIL.stackDataList(chartData, ['cMon']);
                        gridData = AIV_UTIL.stackColumnDataList(gridData, ['_1' + params.unit, '_2' + params.unit, '_3' + params.unit, '_4' + params.unit, '_5' + params.unit, '_6' + params.unit, '_7' + params.unit, '_8' + params.unit, '_9' + params.unit, '_10' + params.unit, '_11' + params.unit, '_12' + params.unit]);
                    }

                    PAGE.fn.createSeries(responseData.resultData, params);
                    PAGE.chart.main.setData(chartData);
                    PAGE.grid.main.setData(gridData);
                }
            }
        })
    };

    // API로 받아온 데이터를 차트 형식으로 변경
    PAGE.fn.converChartData = function (rows, params) {
        var chartData = [];
        var cYear = params.cYear;
        var bYear = (params.cYear) - 1;
        for (var i = 1; i < 13; i++) {
            var data = {};
            data.cMon = leadingZeros(i, 2) + '월';
            for (var key in rows) {
                _val = rows[key];
                for (var _key in _val) {
                    __val = _val[_key];
                    $(__val).each(function (k, row) {
                        if (row.cMon == i) {
                            switch (row.cYear) {
                                case cYear:
                                    data[cYear + _key + params.unit] = row[params.unit];
                                    break;
                                case bYear:
                                    data[bYear + _key + params.unit] = row[params.unit];
                                    break;
                            }
                        }
                    })
                }
            }
            chartData.push(data);
        }
        return chartData;
    }

    // API로 받아온 데이터를 그리드 형식으로 변경
    PAGE.fn.converGridData = function (rows, params) {
        var gridData = [];
        var columnsData = [
            { kField: "cYear", editable: false },
            { kField: "sLogiTypeCd", editable: false },
            { kField: "sUntCd", editable: false }
        ];
        for (var key in rows) {
            _val = rows[key];
            for (var _key in _val) {
                __val = _val[_key];
                data = {};
                $(__val).each(function (i, row) {
                    if (columnsData.findIndex(e => e.field === '_' + row.cMon + params.unit) < 0) {
                        columnsData.push({ field: '_' + row.cMon + params.unit, title: row.cMon + '월', format: "{0:n0}" });
                    }
                    data['_' + row.cMon + params.unit] = row[params.unit];
                })
                data.cYear = key;
                switch (_key) {
                    case '1':
                        data.sLogiTypeCd = '물류';
                        break;
                    case '2':
                        data.sLogiTypeCd = '물류 외';
                        break;
                }
                data.sUntCd = params.unit;
                gridData.push(data);
            }
        }
        PAGE.grid.main.setColumns(columnsData);
        return gridData;
    }

    PAGE.fn.createSeries = function (rows, params) {
        // push 하며 series를 넣는 방식이기 때문에 처음에 clear해줌
        PAGE.chart.main.series.clear();

        for (key in rows) {
            // Create series
            PAGE.chart.main.pushSeries({
                type: 'LineSeries',
                name: key + " 합계",
                dataFields: {
                    categoryX: 'cMon',
                    valueY: key + 'sum'
                },
            });

            PAGE.chart.main.pushSeries({
                type: 'ColumnSeries',
                name: key + " 물류",
                dataFields: {
                    categoryX: 'cMon',
                    valueY: key + '1' + params.unit
                }
            });

            PAGE.chart.main.pushSeries({
                type: 'ColumnSeries',
                name: key + " 물류 외",
                dataFields: {
                    categoryX: 'cMon',
                    valueY: key + '2' + params.unit
                }
            });

        }
    }

    // 그리드 조회시 실행될 파라미터 조회 함수 작성
    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        delete params.unit
        delete params.monthStack;
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });

    // 체크박스 클릭 시 누적된 값으로 조회
    PAGE.params.monthStack.bind("click", function (e) {
        PAGE.fn.search();
    });

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: "k-primary",
        icon: "search",
        onClick: PAGE.fn.search,
    });

    // 그리드 세팅
    PAGE.grid.main = $("#grid").AIV_GRID({
        toolbar: [
            // "cancel" ,"add" ,"edit" ,"save" ,"excel" ,"pdf" ,"destroy" // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            "excel" // 추가, 추소, 엑셀저장
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: "비콘 관리"     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    // 차트 세팅
    PAGE.chart.main = $("#chart").AIV_CHART({
        kType: "XYChart",
        xAxes: [{
            type: 'CategoryAxis',
            dataFields: {
                category: 'cMon'
            },
            renderer: {
                minGridDistance: 1,
            }
        }],
        yAxes: [{
            type: 'ValueAxis',
            min: 0
        }]
    });

    PAGE.fn.search(); // 최초 실행
}