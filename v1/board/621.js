﻿PAGE = AIV_UTIL.initPageObj();

var fn_initPage = function () {
    // 페이지 타이틀 및 경로 입력
    AIV_UTIL.initPage();

    /*
     * ## 다음과 같은 순서로 페이지 내 사용 스크립트 작성 ##
     * 1. PAGE.fn.*     사용 함수 설정
     * 2. PAGE.params.* 설정 기능 객체 설정 (input)
     * 2. PAGE.form.*   설정 기능 객체 설정 (form)
     * 3. PAGE.btn.*    이벤트 발생 객체 설정 (button)
     * 4. PAGE.grid.*   그리드 객체 설정
     * 5. PAGE.chart.*  함수 객체 설정
     */

    // 조회 버튼 설정
    PAGE.fn.search = function () {
        var params = PAGE.fn.getParams();

        AIV_COMM.ajax({
            method: 'GET',
            url: '/api/v1/board/621/ls',
            data: params,
            success: function (responseData, textStatus, jqXHR) {
                PAGE.grid.main.setData(responseData.resultData);
            }
        })
    }

    // 등록버튼 클릭
    PAGE.fn.onBtnAdd = function () {
        PAGE.fn.onBtnEdit();
    }

    // 편집버튼 클릭
    PAGE.fn.onBtnEdit = function (idBoard) {
        // modal 생성
        var _content = $('#modal1').html();
        var pageWindow = AIV_WINDOW.init({
            title: '공지사항',
            width: 1100,
            height: 700,
            content: _content,
            resizable: true,
        }).open();

        // form 생성
        PAGE.form.main = $('#form').AIV_FORM_INIT();
        PAGE.form.main.setMode('edit');

        if ( AIV_UTIL.isNotEmpty(idBoard) ) {
            // form 데이터 입력
            AIV_COMM.ajax({
                method: 'GET',
                async: false,
                url: '/api/v1/board/621/dtl?nBoardId=' + idBoard,
                success: function (responseData, textStatus, jqXHR) {
                    var data = responseData.resultData;
                    PAGE.form.main.setAllValue(data);
                    $(data.files).each(function(i, file){
                        PAGE.form.main['sFileNm'+(i+1)].kFormValue({
                            idFile: file.idFile,
                            sFileNm: file.sFileNm,
                            gFileId: data.gFileId,
                        });
                    });
                }
            })
        }

        // form 저장 버튼 설정
        $('#btnSave').show().AIV_BUTTON({
            title: "저장",
            class: "k-primary",
            icon: "save",
            onClick: function(){
                var uploadFiles = [PAGE.form.main.sFileNm1, PAGE.form.main.sFileNm2, PAGE.form.main.sFileNm3, PAGE.form.main.sFileNm4, PAGE.form.main.sFileNm5];
                var gFileId = '';
                $(uploadFiles).each(function(i, uploadFile){
                    var sFileNmVal = uploadFile.kFormValue();
    
                    if ( sFileNmVal.cmdTag == 'R' ) {   // R: 파일정보를 읽은 후 아무 작동 없을 경우
                        gFileId = gFileId || sFileNmVal.gFileId;   // group file id 만 기억하고 pass 함.

                    } else if ( sFileNmVal.cmdTag == 'D' ) {    // D: 기존 업로드 되어 있는 파일을 삭제버튼 누른 경우
                        gFileId = gFileId || sFileNmVal.gFileId;           // group file id를 기억 후 파일을 삭제함
                        uploadFile.upload({
                            gId: gFileId,
                            cmdTag: sFileNmVal.cmdTag,
                            idFile: sFileNmVal.idFile,
                            success: function(responseData){
                            }
                        })
    
                    } else if ( sFileNmVal.cmdTag == 'U' ) {
                        gFileId = gFileId || sFileNmVal.gFileId;           // group file id를 기억 후 파일을 삭제함
                        uploadFile.upload({
                            gId: gFileId,
                            cmdTag: sFileNmVal.cmdTag,
                            idFile: sFileNmVal.idFile,
                            success: function(responseData){
                                gFileId = gFileId || responseData.gid;  // group file id가 비어있을 경우(최초업로드일 경우) 반환된 gid를 기억함
                            }
                        });

                    } else {    // 파일 미선택시 아무 동작하지 않음.
                    }
                });


                var formsValue = PAGE.form.main.getAllValue();
                var userInfo = AIV_USER.getUserInfo();
                formsValue.sRegEmpId = userInfo.sEmpId;
                formsValue.idBoard = idBoard;
                formsValue.gFileId = gFileId;

                // form 형식으로 되어 있는 파라미터들은 삭제 (gId만 필요)
                delete formsValue.sFileNm1;
                delete formsValue.sFileNm2;
                delete formsValue.sFileNm3;
                delete formsValue.sFileNm4;
                delete formsValue.sFileNm5;

                AIV_COMM.ajax({
                    method: 'POST',
                    url: '/api/v1/board/621/dtl',
                    data: formsValue,
                    success: function (responseData, textStatus, jqXHR) {
                        PAGE.fn.search();
                    }
                })

                pageWindow.self.close();
            }
        });

        // form 취소 버튼 설정
        $('#btnCancel').show().AIV_BUTTON({
            title: "취소",
            onClick: function () {
                pageWindow.self.close();
            }
        });
    }

    // 상세버튼 클릭
    PAGE.fn.onBtnRead = function (idBoard) {
        // modal 생성
        var _content = $('#modal1').html();
        var pageWindow = AIV_WINDOW.init({
            title: '공지사항',
            width: 1100,
            height: 700,
            content: _content,
            resizable: true,
        }).open();

        // form 생성
        PAGE.form.main = $('#form').AIV_FORM_INIT();
        PAGE.form.main.setMode('read');

        // form 데이터 입력
        AIV_COMM.ajax({
            method: 'GET',
            async: false,
            data: {
                nBoardId: idBoard
            },
            url: '/api/v1/board/621/dtl',
            success: function (responseData, textStatus, jqXHR) {
                var data = responseData.resultData;
                PAGE.form.main.setAllValue(data);
                $(data.files).each(function(i, file){
                    PAGE.form.main['sFileNm'+(i+1)].kFormValue({
                        idFile: file.idFile,
                        sFileNm: file.sFileNm,
                        gFileId: data.gFileId,
                        readMode: true      // file객체에 기존 file정보를 입력할때 읽기모드가 필요시 설정
                    });
                });
            }
        });


        // form 취소 버튼 설정
        $('#btnClose').show().AIV_BUTTON({
            title: "닫기",
            onClick: function () {
                pageWindow.self.close();
            }
        });
    }

    // 그리드 조회시 실행될 파라미터 조회 함수 작성
    PAGE.fn.getParams = function () {
        var params = PAGE.params.getAllValue();
        params.sTitle = params.title;
        delete params.title;
        return params;
    }

    // PAGE 내 컨트롤객체 세팅
    PAGE.params = $("#search_area").AIV_SEARCH_INIT({
        onKeyup: PAGE.fn.search
    });

    // 버튼 세팅
    PAGE.btn.search = $("#search_area #btnSearch").AIV_BUTTON({
        title: "조회",
        class: 'k-primary',
        icon: 'search',
        onClick: PAGE.fn.search,
    });

    // 버튼 세팅
    PAGE.btn.add = $("#search_area #btnAdd").AIV_BUTTON({
        title: "등록",
        class: 'k-primary',
        icon: 'add',
        onClick: PAGE.fn.onBtnAdd,
    });

    // 그리드 세팅
    PAGE.grid.main = $("#grid").AIV_GRID({
        toolbar: [  // 'cancel' ,'add' ,'edit' ,'save' ,'excel' ,'pdf' ,'destroy' // 취소, 신규, 변경, 저장, 엑셀다운로드, PDF다운로드, 삭제
            'edit', 'excel'  // 편집, 엑셀저장
        ],
        editToolbar: [
            'destroy', 'cancel'
        ],
        api: {
            delete: {
                method: 'DELETE',
                url: '/api/v1/board/621/dtl',
                beforeSend: function (hasCheckRow) {
                    var saveRow = [];
                    $(hasCheckRow).each(function (i, cRow) {
                        var data = {};
                        data.nBoardId = cRow.idBoard;
                        saveRow.push(data);
                    });
                    return saveRow;
                },
                success: function (responseData) {
                    PAGE.fn.search();
                }
            }
        },
        columns: [
            { kField: 'selectable' },
            {
                kField: 'sTitle',
                template: function (dataItem) {
                    var isEditMode = PAGE.grid.main.isEditMode();
                    if ( isEditMode ) {
                        return "<a href='#' onclick='PAGE.fn.onBtnEdit(" + dataItem.idBoard + ")'>" + dataItem.sTitle + "</a>";
                    } else {
                        return "<a href='#' onclick='PAGE.fn.onBtnRead(" + dataItem.idBoard + ")'>" + dataItem.sTitle + "</a>";
                    }
                },
                editable: false
            },
            { kField: "sRegEmpId", editable: false },
            { kField: "dRegDate", editable: false },
        ],
        editable: true, // 수정 기능 추가
        pageable: false,
        autoBind: false             // false설정시 최초 load 하지 않음 (default:true)
        // , popdelete : true      // 컬럼마지막에 삭제버튼 추가
        // , popedit : true        // 컬럼마지막에 수정버튼 추가(팝업윈도우에서 수정 가능)
        // , groupable: true       // 컬럼헤더를 드래그하여 그룹시켜주는 기능
        // , sortable: true        // 컬럼헤더를 클릭하여 정렬하는 기능
        // , autoFitColumn: true   // 전체 컬럼 대상 width 자동 적용
        // , fileName: '비콘 관리'     // 엑셀 및 pdf 다운로드시 파일명 저장
    });

    PAGE.fn.search();
}