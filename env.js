var ENV_SERVER_TYPE = 'local';
// var ENV_SERVER_TYPE = 'test';
// var ENV_SERVER_TYPE = 'prod';

if ( ENV_SERVER_TYPE == 'prod' ) {
   window.ENV_PROP = {
      API_SERVER_URL: '',
      WEB_SERVER_URL: '',
   }
   // console.log = function(){};   // console.log 제거
} else if ( ENV_SERVER_TYPE == 'test' ) {
   window.ENV_PROP = {
      API_SERVER_URL: '',
      WEB_SERVER_URL: '',
   }
} else if ( ENV_SERVER_TYPE == 'local' ) {
   window.ENV_PROP = {
      API_SERVER_URL: 'http://13.124.185.51:8080', // AWS API SERVER
      // API_SERVER_URL: 'http://192.168.0.107:8080', // kdh
      // API_SERVER_URL: 'http://127.0.0.1:8080', // jhb
      // WEB_SERVER_URL: 'http://192.168.0.107:5500',
      WEB_SERVER_URL: 'http://localhost:5500',  // common
   }
}
window.ENV_PROP.sDeptId = '70000000';  // 부서 최상위 ID